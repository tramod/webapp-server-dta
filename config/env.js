import dotenv from 'dotenv';
import path from 'path';
import { configEvents } from '../app/services/eventBus.js';

// Config entries with default value
const defaults = {
  TM_PORT: 8088,
  TM_DB_DIALECT: 'postgres',
  TM_MODEL_CACHE_PREFIX: 'DEV',
  TM_MODEL_DTA_MAX_THREADS: 1,
};

// Config entries which are mandatory for app startup
const mandatory = [];

/*
  Loads dotenv file.
  All TM_* env values are automatically merged.
  Auto fills default value if omitted.
  Checks if mandatory values are filled.
*/
dotenv.config({
  ...(process.env.NODE_ENV === 'test' && {
    path: path.resolve(process.cwd(), '.env.test'),
  }),
});

const config = {};

const envKeys = Object.keys(process.env).filter((val) => val.startsWith('TM_'));
const defaultsKeys = Object.keys(defaults);
const configKeysWithDefaults = [...new Set([...envKeys, ...defaultsKeys])];

for (const key of configKeysWithDefaults) {
  config[key] = process.env[key] ?? defaults[key];
}

for (const key of mandatory) {
  if (!config[key]) throw new Error(`Mandatory env key ${key} missing!`);
}

config.TM_DB = parseDBConfig(config);

// Helper method, should be used only in test overrides where dynamic change is supported
function setConfig(key, value) {
  config[key] = value;
  configEvents.emit(`CONFIG:CHANGED:${key}`, value);
}

export default config;
export { setConfig };

function parseDBConfig(parsedConfig) {
  const dbKeys = Object.keys(parsedConfig).filter((val) =>
    val.startsWith('TM_DB_'),
  );
  return dbKeys.reduce((db, key) => {
    const parsedKey = key.split('_')[2].toLowerCase();
    // eslint-disable-next-line no-param-reassign
    db[parsedKey] = parsedConfig[key];
    return db;
  }, {});
}
