const path = require('path');
require('dotenv').config({
  ...(process.env.NODE_ENV === 'test' && {
    path: path.resolve(process.cwd(), '.env.test'),
  }),
});

module.exports = {
  development: {
    dialect: process.env.TM_DB_DIALECT ?? 'postgres',
    username: process.env.TM_DB_USERNAME,
    password: process.env.TM_DB_PASSWORD,
    database: process.env.TM_DB_DATABASE,
    host: process.env.TM_DB_HOST,
    schema: process.env.TM_DB_SCHEMA ?? 'public',
    searchPath: process.env.TM_DB_SCHEMA ?? 'public',
    dialectOptions: {
      prependSearchPath: true,
    },
  },
  production: {
    dialect: process.env.TM_DB_DIALECT ?? 'postgres',
    username: process.env.TM_DB_USERNAME,
    password: process.env.TM_DB_PASSWORD,
    database: process.env.TM_DB_DATABASE,
    host: process.env.TM_DB_HOST,
    schema: process.env.TM_DB_SCHEMA ?? 'public',
    searchPath: process.env.TM_DB_SCHEMA ?? 'public',
    dialectOptions: {
      prependSearchPath: true,
    },
  },
  test: {
    dialect: process.env.TM_DB_DIALECT ?? 'postgres',
    username: process.env.TM_DB_USERNAME,
    password: process.env.TM_DB_PASSWORD,
    database: process.env.TM_DB_DATABASE,
    host: process.env.TM_DB_HOST,
    schema: process.env.TM_DB_SCHEMA ?? 'public',
    searchPath: process.env.TM_DB_SCHEMA ?? 'public',
    dialectOptions: {
      prependSearchPath: true,
    },
  },
};
