module.exports = { getTrafficModelsFromEnvs };

function getTrafficModelsFromEnvs() {
  const DTAEndpoint = process.env.TM_MODEL_DTA_ENDPOINT ?? null;
  const DTAName = process.env.TM_MODEL_DTA_NAME ?? null;
  const DTAApiKey = process.env.TM_MODEL_DTA_API_KEY ?? null;

  const models = [];
  if (DTAEndpoint && DTAName && DTAApiKey)
    models.push({
      name: DTAName,
      endPoint: DTAEndpoint,
      endPointApiKey: DTAApiKey,
      type: 'DTA',
      isDefault: true,
      isValid: false,
    });
  return models;
}
