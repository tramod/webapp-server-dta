import { faker, getRandomValue, getRandomExistingUserId } from './utils.js';

const getEventData = async (overrides = {}) => ({
  itemId: overrides.itemId || null,
  scenarioRowId: overrides.scenarioRowId || null,
  name: overrides.name || `random event - ${Date.now()}`,
  included: overrides.included ?? faker.datatype.boolean(),
  description:
    overrides.description !== undefined
      ? overrides.description
      : getRandomValue(faker.lorem.sentence(10), ''),
  note:
    overrides.note !== undefined
      ? overrides.note
      : getRandomValue(faker.lorem.sentence(10), ''),
  authorUserId: overrides.authorUserId || (await getRandomExistingUserId()),
  dateFrom: overrides.dateFrom !== undefined ? overrides.dateFrom : null,
  dateTo: overrides.dateTo !== undefined ? overrides.dateTo : null,
});

export { getEventData };
export default getEventData;
