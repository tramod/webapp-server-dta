import { faker, getRandomValue, getRandomExistingUserId } from './utils.js';

const getScenarioData = async (overrides = {}) => ({
  itemId: overrides.itemId || null,
  name: overrides.name || `random scenario - ${Date.now()}`,
  description:
    overrides.description !== undefined
      ? overrides.description
      : getRandomValue(faker.lorem.sentence(10), ''),
  note:
    overrides.note !== undefined
      ? overrides.note
      : getRandomValue(faker.lorem.sentence(10), ''),
  authorUserId: overrides.authorUserId || (await getRandomExistingUserId()),
  dateFrom: overrides.dateFrom !== undefined ? overrides.dateFrom : null,
  dateTo: overrides.dateTo !== undefined ? overrides.dateTo : null,
  isPublic:
    overrides.isInPublicList === true
      ? true
      : overrides.isPublic ?? faker.datatype.boolean(),
  isInPublicList:
    overrides.isPublic === false
      ? false
      : overrides.isInPublicList ?? faker.datatype.boolean(),
  modelType: overrides.modelType || undefined,
});

export { getScenarioData };
export default getScenarioData;
