import { User, Credential, Organization } from '../index.js';
import { getUserData } from './user.data.js';

export default async (
  overrides,
  { withCredentials = true, withOrganization = true, hashPassword = true } = {},
) => {
  const { user, credential, organization } = getUserData(overrides);

  const userModel = await User.create(
    { ...user, ...(withCredentials && { credential }) },
    { include: [Credential] },
  );

  if (withCredentials && !hashPassword) {
    // update password directly & ignore (both individual and bulk) hooks
    await Credential.update(
      { password: overrides.password },
      {
        where: { user_id: userModel.id },
        hooks: false,
        individualHooks: false,
      },
    );
  }

  let orgModel = null;
  if (withOrganization && organization)
    [orgModel] = await Organization.findOrCreate({
      where: organization,
      defaults: organization,
    });

  await userModel.setOrganization(orgModel?.id || null);

  // non encrypted credentials to be added to the resulted factory object
  // to simplify logging-in inside tests
  const rawCredentials = {
    username: userModel.username || userModel.email,
    password: credential.password,
  };

  return { user: userModel, rawCredentials, organization: orgModel };
};
