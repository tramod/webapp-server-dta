import { TrafficModel } from '../index.js';

export default async (overrides) => {
  const trafficModelData = getTrafficModelData(overrides);
  const trafficModel = await TrafficModel.create(trafficModelData, {});

  return { trafficModel };
};

export function getTrafficModelData(overrides = {}) {
  const trafficModelData = {
    name: 'test-model',
    endPoint: 'https://testUrl',
    endPointApiKey: 'test-key',
    type: 'DTA',
    isDefault: true,
    isValid: true,
    isBroken: false,
    timestamp: new Date(),
    ...overrides,
  };

  return trafficModelData;
}
