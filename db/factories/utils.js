import { faker } from '@faker-js/faker/locale/cz';
import { User, db, Op } from '../index.js';

const ADMIN_USER_ROLE = '100';

export const getExistingUserId = async (user) => {
  const existingUser = await User.findOne({
    where: user,
  });

  return existingUser ? existingUser.id : null;
};

export const getRandomExistingUserId = async () => {
  const randomUser = await User.findOne({
    order: db.random(),
  });

  return randomUser ? randomUser.id : null;
};

export const getAdminUsers = async () => {
  // TODO: improve admin recognitioning
  const adminUsers = await User.findAll({
    where: { roles: { [Op.like]: `%${ADMIN_USER_ROLE}%` } },
  });

  return adminUsers || [];
};

export const percentChance = (percent) => Math.random() < percent / 100;

export const getRandomValue = (val1, val2, { percent = 50 } = {}) =>
  percentChance(percent) ? val1 : val2;

export { faker };

export default {
  percentChance,
  getExistingUserId,
  getRandomExistingUserId,
  getAdminUsers,
  getRandomValue,
};
