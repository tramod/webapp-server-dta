import { Modification } from '../index.js';
import { getModificationData } from './modification.data.js';
import scenarioService from '../../app/services/scenarios.js';

export default async (
  overrides = {},
  {
    generateItemId = true,
    attachedEventModel = null,
    updateScenarioDates = true,
  } = {},
) => {
  const modificationData = await getModificationData(overrides);
  const modificationModel = await Modification.create(
    parseModificationData(modificationData),
  );

  if (generateItemId)
    await modificationModel.update({
      itemId: modificationModel.rowId,
      ...(!modificationData.name && {
        name: `${modificationData.type}-${modificationModel.rowId}`,
      }),
    });

  if (attachedEventModel) {
    await attachedEventModel.addModification(modificationModel);
    if (updateScenarioDates) {
      const scenarioModel = await attachedEventModel.getScenario();
      if (scenarioModel) {
        await scenarioService.deepUpdateScenarioDates(scenarioModel);
        await modificationModel.reload();
      }
    }
  }

  return modificationModel;
};

function parseModificationData(modificationData) {
  const baseData = { ...modificationData };
  const jsonData = {};
  const baseModificationProps = [
    'itemId',
    'eventRowId',
    'type',
    'mode',
    'name',
    'description',
    'note',
    'dateFrom',
    'dateTo',
  ];

  for (const prop in baseData) {
    if (!baseModificationProps.includes(prop)) {
      jsonData[prop] = baseData[prop];
      delete baseData[prop];
    }
  }

  return { ...baseData, data: jsonData };
}
