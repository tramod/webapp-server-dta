import { Event } from '../index.js';
import { getEventData } from './event.data.js';
import modificationFactory from './modification.js';
import scenarioService from '../../app/services/scenarios.js';

export default async (
  overrides = {},
  {
    withRandomModifications = false,
    randomModificationsCount = 3,
    generateItemId = true,
    attachedScenarioModel = null,
    updateScenarioDates = true,
  } = {},
) => {
  const eventData = await getEventData(overrides);
  const eventModel = await Event.create(eventData);
  const eventModifications = [];

  if (generateItemId) await eventModel.update({ itemId: eventModel.rowId });

  if (overrides.modifications)
    /* eslint-disable no-await-in-loop */
    for (const modOverrides of overrides.modifications) {
      eventModifications.push(
        await addModificationToEvent(modOverrides, eventModel, {
          generateItemId,
        }),
      );
    }

  if (withRandomModifications)
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < randomModificationsCount; i++) {
      eventModifications.push(
        await addModificationToEvent({}, eventModel, {
          generateItemId,
        }),
      );
    }

  if (attachedScenarioModel) {
    await attachedScenarioModel.addEvent(eventModel);
    if (updateScenarioDates) {
      await scenarioService.deepUpdateScenarioDates(attachedScenarioModel);
      await eventModel.reload();
    }
  }

  return { event: eventModel, modifications: eventModifications };
};

async function addModificationToEvent(
  modOverrides,
  eventModel,
  { generateItemId = true } = {},
) {
  const modificationModel = await modificationFactory(modOverrides, {
    generateItemId,
    attachedEventModel: eventModel,
    updateScenarioDates: false,
  });

  return modificationModel;
}
