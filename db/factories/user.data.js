import { faker, percentChance } from './utils.js';

const getCredentialData = (overrides = {}) => ({
  password: overrides.password || Math.random().toString(36).substr(2),
});

const getOrganizationData = (overrides = {}) => ({
  name: faker.company.name(),
  ...overrides,
});

const getUserData = (overrides = {}) => {
  const user = {
    email: faker.internet.email(), // E-mail validation not available
    username: faker.internet.userName(),
    roles: null,
    ...overrides,
  };
  delete user.password;
  delete user.organization;

  const credential = getCredentialData({ password: overrides.password });

  const gerRandomOrganizationOrNull = () =>
    percentChance(50) ? getOrganizationData() : null;

  const organization = overrides.organization
    ? getOrganizationData({ name: overrides.organization })
    : gerRandomOrganizationOrNull();

  return { user, credential, organization };
};

export { getUserData, getCredentialData, getOrganizationData };
export default { getUserData, getCredentialData, getOrganizationData };
