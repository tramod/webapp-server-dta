import { faker, percentChance, getRandomValue } from './utils.js';

const allowedTypeModes = [
  { type: 'link', mode: 'new' },
  { type: 'link', mode: 'existing' },
  { type: 'node', mode: 'existing' },
  // { type: 'generator', mode: 'new' },
  // { type: 'generator', mode: 'existing' },
];

const getModificationData = async (overrides = {}) => {
  const { type: randType, mode: randMode } =
    faker.helpers.arrayElement(allowedTypeModes);
  const modType = parseModType(overrides, randType);
  const modMode = parseModMode(overrides, randMode);
  const modData = overrides.data || getDataByTypeMode(modType, modMode);

  return {
    itemId: overrides.itemId || null,
    eventRowId: overrides.eventRowId || null,
    type: modType,
    mode: modMode,
    name:
      overrides.name || (percentChance(10) ? `random mod - ${Date.now()}` : ''),
    description:
      overrides.description !== undefined
        ? overrides.description
        : getRandomValue(faker.lorem.sentence(10), ''),
    note:
      overrides.note !== undefined
        ? overrides.note
        : getRandomValue(faker.lorem.sentence(10), ''),
    dateFrom:
      overrides.dateFrom !== undefined
        ? overrides.dateFrom
        : faker.date.recent(7),
    dateTo:
      overrides.dateTo !== undefined ? overrides.dateTo : faker.date.soon(7),
    ...modData,
  };
};

function parseModType(overrides, random) {
  if (overrides.type) return overrides.type;
  return random;
}

function parseModMode(overrides, random) {
  if (overrides.mode) return overrides.mode;
  if (overrides.type === 'node') return 'existing';
  return random;
}

function getDataByTypeMode(type, mode) {
  switch (`${type}-${mode}`) {
    case 'link-existing':
      return getDataLinkExisting();
    case 'link-new':
      return getDataLinkNew();
    case 'node-existing':
      return getDataNodeExisting();
    case 'node-new':
      return {
        node: [
          faker.datatype.number({ min: 0, max: 100, precision: 5 }),
          faker.datatype.number({ min: 0, max: 100, precision: 5 }),
        ],
      };
    case 'generator-new':
      return getDataGeneratorNew();
    case 'generator-existing':
      return getDataGeneratorExisting();
    default:
      return {};
  }
}

// TODO maybe add multiple variants sets
const linkSpeedCapacityVariants = [
  { speed: 0, capacity: 0 },
  { speed: 25, capacity: 250 },
  { speed: 50, capacity: 1500 },
  { speed: 90, capacity: 2000 },
  {
    speed: () => faker.datatype.number({ min: 0, max: 130 }),
    capacity: () => faker.datatype.number({ min: 0, max: 3600 }),
  },
];
const getLinkSpeedCapacity = () => {
  const variant = faker.helpers.arrayElement(linkSpeedCapacityVariants);
  if (typeof variant.speed === 'number') return variant;
  return { speed: variant.speed(), capacity: variant.capacity() };
};
const linkExistingIdList = [
  3500, 3656, 3662, 4106, 4108, 4111, 6557, 8638, 8794, 8800, 9244, 9246, 9249,
  11695,
];

function getDataLinkExisting() {
  return {
    linkId: faker.helpers.arrayElements(linkExistingIdList),
    ...getLinkSpeedCapacity(),
    lanes: 1,
  };
}

const linkNewGeometries = [
  {
    coordinates: [
      [1484115.45246557, 6404945.79533315],
      [1483777.654977668, 6404548.301198245],
      [1482867.1388429995, 6403915.431488169],
      [1482678.83899611, 6403590.15793787],
    ],
    source: 438,
    target: 1556,
  },
  {
    coordinates: [
      [1484985.00959254, 6398424.20257765],
      [1485377.14581396, 6397894.70200018],
    ],
    source: 1920,
    target: 1928,
  },
  {
    coordinates: [
      [1495232.0044749, 6400977.3573495],
      [1494883.2794704, 6402192.67951242],
    ],
    source: 295,
    target: 370,
  },
  {
    coordinates: [
      [1498276.21088926, 6404786.16033297],
      [1497108.52039009, 6406066.67928394],
    ],
    source: 3081,
    target: 509,
  },
];

function getDataLinkNew() {
  const isTwoWay = faker.datatype.boolean();
  const { speed, capacity } = isTwoWay
    ? getLinkSpeedCapacity()
    : { speed: null, capacity: null };
  const isTwoWayProps = {
    twoWayCapacity: capacity,
    twoWaySpeed: speed,
    twoWayLanes: 1,
  };
  return {
    ...faker.helpers.arrayElement(linkNewGeometries),
    ...getLinkSpeedCapacity(),
    twoWay: isTwoWay,
    ...isTwoWayProps,
    lanes: 1,
  };
}

const nodeTransitVariants = [
  {
    nodeId: 2664,
    linkFromList: [6800, 4510, 4517, 9659],
    linkToList: [11938, 9648, 9655, 4521],
  },
  {
    nodeId: 2241,
    linkFromList: [9252, 9253, 11768],
    linkToList: [4114, 4115, 6630],
  },
  {
    nodeId: 2274,
    linkFromList: [4143, 4140, 1570, 11766],
    linkToList: [9281, 9278, 6628],
  },
];
// const nodeTransitCostVariants = [
//   -1,
//   0,
//   30,
//   60,
//   () => faker.datatype.number({ min: -1, max: 180 }),
// ];
// const getNodeTransitCost = () => {
//   const variant = faker.helpers.arrayElement(nodeTransitCostVariants);
//   return typeof variant === 'number' ? variant : variant();
// };

function getDataNodeExisting() {
  const nodeTransit = faker.helpers.arrayElement(nodeTransitVariants);
  return {
    node: nodeTransit.nodeId,
    linkFrom: faker.helpers.arrayElement(nodeTransit.linkFromList),
    linkTo: faker.helpers.arrayElement(nodeTransit.linkToList),
    capacity: 100,
  };
}

const generatorNewVariants = [
  {
    generator: [1490033.1879219804, 6403088.874815011],
    nodesList: [1788, 2727, 2667],
  },
  {
    generator: [1486551.3898966308, 6401324.173400444],
    nodesList: [2444, 4463, 336, 2271, 567, 1912],
  },
  {
    generator: [1491401.225966172, 6399773.651123657],
    nodesList: [
      3882, 3902, 3903, 2821, 3883, 3905, 3901, 3904, 3874, 3873, 3877, 3880,
    ],
  },
  {
    generator: [1488262.3929299386, 6406524.3717162805],
    nodesList: [1321, 1322, 1328, 1331, 952, 1210, 1211, 1311],
  },
];
const getGeneratorTraffic = () => {
  const isLowTraffic = percentChance(70);
  return {
    inTraffic: faker.datatype.number({
      min: 0,
      max: isLowTraffic ? 50 : 1000,
    }),
    outTraffic: faker.datatype.number({
      min: 0,
      max: isLowTraffic ? 50 : 1000,
    }),
  };
};

function getDataGeneratorNew() {
  const { generator, nodesList } =
    faker.helpers.arrayElement(generatorNewVariants);
  return {
    generator,
    nodes: faker.helpers.arrayElements(nodesList),
    ...getGeneratorTraffic(),
  };
}

const generatorExistingVariants = [
  {
    generator: [97],
    originalNode: [1330],
    nodesList: [1330, 1325, 1329, 1331, 1328, 1180, 1182],
  },
  {
    generator: [5],
    originalNode: [2613],
    nodesList: [2613, 2612, 2215, 2212, 2602],
  },
  {
    generator: [252],
    originalNode: [2296],
    nodesList: [2296, 2256, 2254, 2255, 2288, 2290],
  },
  {
    generator: [150],
    originalNode: [3789],
    nodesList: [
      3789, 3787, 3780, 3801, 3790, 2440, 2437, 2464, 2467, 3794, 3788,
    ],
  },
];

function getDataGeneratorExisting() {
  const { generator, originalNode, nodesList } = faker.helpers.arrayElement(
    generatorExistingVariants,
  );
  const keepOriginalNode = percentChance(75);
  return {
    generator,
    nodes: keepOriginalNode
      ? originalNode
      : faker.helpers.arrayElements(nodesList),
    ...getGeneratorTraffic(),
  };
}

export { getModificationData };
export default getModificationData;
