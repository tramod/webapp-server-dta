import {
  Scenario,
  ScenarioAccess,
  ModelSection,
  TrafficModel,
} from '../index.js';
import { getScenarioData } from './scenario.data.js';
import eventFactory from './event.js';
import { getTrafficModelData } from './trafficModel.js';
import { faker, getAdminUsers, getRandomExistingUserId } from './utils.js';
import scenarioService from '../../app/services/scenarios.js';

const DEFAULT_AUTHOR_ACCESS = 'editor';
const ACCESS_LEVEL_OPTIONS = ['inserter', 'viewer', 'editor'];

export default async (
  overrides = {},
  {
    withRandomEvents = false,
    randomEventsCount = 1,
    withAuthorAccess = true,
    withAdminAccess = false,
    withRandomAccess = false,
    withTrafficModel = false,
    generateItemId = true,
  } = {},
) => {
  const scenarioData = await getScenarioData(overrides);
  const scenarioAccesses = overrides.scenarioAccesses || [];
  const modelSections = [];
  const scenarioEvents = [];

  if (withAuthorAccess)
    scenarioAccesses.push({
      level: DEFAULT_AUTHOR_ACCESS,
      userId: scenarioData.authorUserId,
    });

  if (withAdminAccess) {
    const adminUsers = await getAdminUsers();
    adminUsers.forEach((adminUser) => {
      scenarioAccesses.push({
        level: DEFAULT_AUTHOR_ACCESS,
        userId: adminUser.id,
      });
    });
  }

  if (withRandomAccess) {
    const randomUserId = await getRandomExistingUserId();
    scenarioAccesses.push({
      level: faker.helpers.arrayElement(ACCESS_LEVEL_OPTIONS),
      userId: randomUserId,
    });
  }

  const uniqueAccesses = [
    ...new Map(scenarioAccesses.map((acc) => [acc.userId, acc])).values(),
  ];
  const scenarioModel = await Scenario.create(
    {
      ...scenarioData,
      scenarioAccesses: uniqueAccesses,
      createdAt: new Date(),
    },
    { include: [ScenarioAccess] },
  );

  if (generateItemId)
    await scenarioModel.update({ itemId: scenarioModel.rowId });

  if (overrides.modelSections)
    /* eslint-disable no-await-in-loop */
    for (const modelSectionsData of overrides.modelSections) {
      modelSections.push(
        await addModelSectionToScenario(modelSectionsData, scenarioModel),
      );
    }

  if (overrides.events)
    /* eslint-disable no-await-in-loop */
    for (const evOverrides of overrides.events) {
      scenarioEvents.push(
        await addEventToScenario(evOverrides, scenarioModel, {
          generateItemId,
        }),
      );
    }
  else if (withRandomEvents)
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < randomEventsCount; i++) {
      scenarioEvents.push(
        await addEventToScenario({}, scenarioModel, {
          withRandomModifications: true,
          generateItemId,
        }),
      );
    }

  let trafficModelModel = null;
  const { modelType } = scenarioData;
  if (withTrafficModel && modelType) {
    [trafficModelModel] = await TrafficModel.findOrCreate({
      where: { type: modelType, isDefault: true },
      defaults: getTrafficModelData({ type: modelType }),
    });

    await scenarioModel.setTrafficModel(trafficModelModel?.id || null);
  }

  // Recalculate dates if events present
  if (overrides.events || withRandomEvents)
    await scenarioService.deepUpdateScenarioDates(scenarioModel);

  return {
    scenario: scenarioModel,
    events: scenarioEvents,
    accesses: uniqueAccesses,
    trafficModel: trafficModelModel,
  };
};

async function addEventToScenario(
  evOverrides,
  scenarioModel,
  { withRandomModifications = false, generateItemId = true } = {},
) {
  const { event: eventModel } = await eventFactory(evOverrides, {
    withRandomModifications,
    generateItemId,
    attachedScenarioModel: scenarioModel,
    updateScenarioDates: false,
  });

  return eventModel;
}

async function addModelSectionToScenario(
  { key, dateFrom, dateTo, codeList, hash, isValid = true },
  scenarioModel,
) {
  const modelSection = await ModelSection.create({ key, hash, isValid });

  await scenarioModel.addModelSection(modelSection, {
    through: { dateFrom, dateTo, codeList },
  });

  return modelSection;
}
