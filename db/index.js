import Sequelize from 'sequelize';
import cls from 'cls-hooked';
import envConfig from '../config/env.js';
import * as modelClasses from '../app/models/index.js';
import logger from '../app/utils/logger.js';

const { TM_DB: config } = envConfig;
const models = {};

const clsDB = cls.createNamespace('db-transactions');
Sequelize.useCLS(clsDB);

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  {
    ...config,
    benchmark: true,
    logging: (msg, time) => {
      // rTracer id not picked up in sequelize ops
      logger.debug(msg, { time });
    },
    // Some possible DB infrastructure errors retry
    retry: {
      match: [
        Sequelize.ConnectionError,
        Sequelize.ConnectionTimedOutError,
        Sequelize.TimeoutError,
        /Deadlock/i,
      ],
      max: 3,
    },
  },
);

for (const modelFn of Object.values(modelClasses)) {
  const model = modelFn(sequelize, Sequelize);
  const { name } = model;
  const upperCaseName = name.charAt(0).toUpperCase() + name.slice(1);
  models[upperCaseName] = model;
}

for (const modelName of Object.keys(models)) {
  if (models[modelName].associate)
    models[modelName].associate(models, Sequelize);
  if (models[modelName].registerScopes)
    models[modelName].registerScopes(models, Sequelize);
}

models.sequelize = sequelize;
models.Sequelize = Sequelize;

function closeDbConnection() {
  sequelize.close();
}

// Created Models need to be manually added to export list
export const {
  Scenario,
  Event,
  ScenarioEvent,
  ScenarioAccess,
  User,
  UserToken,
  Credential,
  Organization,
  EditSession,
  Modification,
  ScenarioModelSection,
  ModelSection,
  TrafficModel,
} = models;
export { Sequelize, closeDbConnection, clsDB };
export const { Op } = Sequelize;
export const db = sequelize;
export default models;
