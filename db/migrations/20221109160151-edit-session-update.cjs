const schema = process.env.TM_DB_SCHEMA ?? 'public';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      `DELETE FROM "edit_sessions" WHERE "state" = 'closed'`,
    );

    await queryInterface.removeColumn(
      { tableName: 'edit_sessions', schema },
      'state',
    );

    await queryInterface.removeColumn(
      { tableName: 'edit_sessions', schema },
      'close_on_saved',
    );

    await queryInterface.addColumn(
      { tableName: 'edit_sessions', schema },
      'computation_timestamp',
      {
        allowNull: true,
        type: Sequelize.DATE,
      },
    );

    await queryInterface.changeColumn(
      { tableName: 'edit_sessions', schema },
      'computation_state',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'notStarted',
      },
    );

    await queryInterface.addColumn(
      { tableName: 'edit_sessions', schema },
      'computation_warning',
      {
        type: Sequelize.BOOLEAN,
      },
    );
    await queryInterface.sequelize.query(
      `UPDATE "edit_sessions" SET "computation_warning" = false`,
    );
    await queryInterface.changeColumn(
      { tableName: 'edit_sessions', schema },
      'computation_warning',
      {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    );

    await queryInterface.sequelize.query(
      `UPDATE "edit_sessions" SET "computation_state" = 'notStarted' WHERE "computation_state" = 'computed'`,
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      { tableName: 'edit_sessions', schema },
      'computation_timestamp',
    );

    await queryInterface.removeColumn(
      { tableName: 'edit_sessions', schema },
      'computation_warning',
    );

    await queryInterface.addColumn(
      { tableName: 'edit_sessions', schema },
      'state',
      {
        type: Sequelize.STRING,
      },
    );
    await queryInterface.sequelize.query(
      `UPDATE "edit_sessions" SET "state" = 'running'`,
    );
    await queryInterface.changeColumn(
      { tableName: 'edit_sessions', schema },
      'state',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'running',
      },
    );

    await queryInterface.addColumn(
      { tableName: 'edit_sessions', schema },
      'close_on_saved',
      {
        type: Sequelize.BOOLEAN,
      },
    );
    await queryInterface.sequelize.query(
      `UPDATE "edit_sessions" SET "close_on_saved" = false`,
    );
    await queryInterface.changeColumn(
      { tableName: 'edit_sessions', schema },
      'close_on_saved',
      {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    );

    await queryInterface.changeColumn(
      { tableName: 'edit_sessions', schema },
      'computation_state',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'computed',
      },
    );
  },
};
