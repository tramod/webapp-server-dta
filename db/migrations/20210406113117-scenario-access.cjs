module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Create scenario-access table
    await queryInterface.createTable('scenario_accesses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      scenario_row_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'scenarios',
          key: 'row_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      level: {
        type: Sequelize.DataTypes.ENUM('editor', 'inserter', 'viewer'),
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('scenario_accesses');
  },
};
