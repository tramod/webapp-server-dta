module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('edit_sessions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      source_item_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'scenarios',
          key: 'item_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updated_row_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'scenarios',
          key: 'row_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'running',
      },
      computation_state: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'computed',
      },
      has_change: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      has_computable_change: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      save_on_computed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      close_on_saved: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('edit_sessions');
  },
};
