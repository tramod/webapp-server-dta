const schema = process.env.TM_DB_SCHEMA ?? 'public';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      { tableName: 'model_sections', schema },
      'is_valid',
      {
        type: Sequelize.BOOLEAN,
      },
    );

    await queryInterface.sequelize.query(
      `update "model_sections" set "is_valid" = true`,
    );

    await queryInterface.changeColumn('model_sections', 'is_valid', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      default: true,
    });

    await queryInterface.addColumn(
      { tableName: 'traffic_models', schema },
      'is_valid',
      {
        type: Sequelize.BOOLEAN,
      },
    );
    await queryInterface.sequelize.query(
      `update "traffic_models" set "is_valid" = true`,
    );
    await queryInterface.changeColumn('traffic_models', 'is_valid', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      default: false,
    });

    await queryInterface.addColumn(
      { tableName: 'traffic_models', schema },
      'is_broken',
      {
        type: Sequelize.BOOLEAN,
      },
    );
    await queryInterface.sequelize.query(
      `update "traffic_models" set "is_broken" = false`,
    );
    await queryInterface.changeColumn('traffic_models', 'is_broken', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      default: false,
    });

    await queryInterface.addColumn(
      { tableName: 'traffic_models', schema },
      'timestamp',
      {
        type: Sequelize.DATE,
        allowNull: true,
      },
    );
  },

  down: async (queryInterface) => {
    await queryInterface.removeColumn(
      { tableName: 'model_sections', schema },
      'is_valid',
    );

    await queryInterface.removeColumn(
      { tableName: 'traffic_models', schema },
      'is_valid',
    );

    await queryInterface.removeColumn(
      { tableName: 'traffic_models', schema },
      'is_broken',
    );

    await queryInterface.removeColumn(
      { tableName: 'traffic_models', schema },
      'timestamp',
    );
  },
};
