const schema = process.env.TM_DB_SCHEMA ?? 'public';
const { getTrafficModelsFromEnvs } = require('../utils/trafficModels.cjs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('traffic_models', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      end_point: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      end_point_api_key: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      type: {
        type: Sequelize.DataTypes.ENUM('DTA'),
        allowNull: false,
      },
      is_default: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addColumn(
      { tableName: 'scenarios', schema },
      'traffic_model_id',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'traffic_models',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const [scenarioResults] = await queryInterface.sequelize.query(
      `SELECT * FROM "scenarios"`,
    );

    if (scenarioResults.length === 0) return;

    const defaultModelsData = getTrafficModelsFromEnvs();
    if (defaultModelsData.length === 0) return;

    for (let i = 0; i < defaultModelsData.length; i++) {
      const modelData = defaultModelsData[i];
      // eslint-disable-next-line no-await-in-loop
      await queryInterface.sequelize.query(`INSERT INTO "traffic_models" VALUES
    (${i + 1}, '${modelData.name}', '${modelData.endPoint}', '${
        modelData.endPointApiKey
      }', '${modelData.type}', ${
        modelData.isDefault
      }, '${new Date().toISOString()}','${new Date().toISOString()}')`);
    }

    // every existing Scenario with unspecified traffic model is considered as first found
    await queryInterface.sequelize.query(
      `update "scenarios" set "traffic_model_id" = 1 where "traffic_model_id" IS NULL`,
    );
  },

  down: async (queryInterface) => {
    await queryInterface.removeColumn(
      { tableName: 'scenarios', schema },
      'traffic_model_id',
    );

    await queryInterface.dropTable('traffic_models');
  },
};
