const schema = process.env.TM_DB_SCHEMA ?? 'public';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('model_sections', null, {
      truncate: true,
      cascade: true,
    });

    await queryInterface.removeColumn(
      { tableName: 'model_sections', schema },
      'code_list',
    );

    await queryInterface.addColumn(
      { tableName: 'scenario_model_sections', schema },
      'code_list',
      {
        type: Sequelize.JSON,
        allowNull: true,
      },
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('model_sections', null, {
      truncate: true,
      cascade: true,
    });

    await queryInterface.removeColumn(
      { tableName: 'scenario_model_sections', schema },
      'code_list',
    );

    await queryInterface.addColumn(
      { tableName: 'model_sections', schema },
      'code_list',
      {
        type: Sequelize.JSON,
        allowNull: true,
      },
    );
  },
};
