module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add modelSection table
    await queryInterface.createTable('model_sections', {
      key: {
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true,
      },
      code_list: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      hash: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    // Create scenarioModelSection join table
    await queryInterface.createTable('scenario_model_sections', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      scenario_row_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'scenarios',
          key: 'row_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      model_section_key: {
        type: Sequelize.STRING,
        references: {
          model: 'model_sections',
          key: 'key',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      date_from: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      date_to: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('scenario_model_sections');

    await queryInterface.dropTable('model_sections');
  },
};
