module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('modifications', {
      row_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      item_id: {
        type: Sequelize.INTEGER,
      },
      mode: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '',
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      note: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      date_from: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
      },
      date_to: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
      },
      data: {
        type: Sequelize.JSONB,
      },
      event_row_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'events',
          key: 'row_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('modifications');
  },
};
