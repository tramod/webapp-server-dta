module.exports = {
  async up(queryInterface, Sequelize) {
    const { User, Op } = await import('../index.js');

    // TODO Rewrite without model usage, with raw SQL
    await User.update(
      {
        username: Sequelize.literal(`split_part(email, '@', 1)`),
      },
      {
        where: { username: { [Op.eq]: null } },
      },
    );

    await queryInterface.changeColumn('users', 'username', {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
      unique: true,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.changeColumn('users', 'username', {
      type: Sequelize.DataTypes.STRING,
      allowNull: true,
      unique: true,
    });
  },
};
