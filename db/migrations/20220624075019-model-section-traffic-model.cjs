const schema = process.env.TM_DB_SCHEMA ?? 'public';

module.exports = {
  async up(queryInterface, Sequelize) {
    // const { TrafficModel, ModelSection, Op } = await import('../index.js');

    await queryInterface.addColumn(
      { tableName: 'model_sections', schema },
      'traffic_model_id',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'traffic_models',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const [results] = await queryInterface.sequelize.query(
      `SELECT * FROM "traffic_models"`,
    );

    if (results.length) {
      await queryInterface.sequelize.query(
        `update "model_sections" set "traffic_model_id" = ${results[0].id} where "traffic_model_id" IS NULL`,
      );
    }
  },

  async down(queryInterface) {
    await queryInterface.removeColumn(
      { tableName: 'model_sections', schema },
      'traffic_model_id',
    );
  },
};
