module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('scenarios', {
      row_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      note: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      item_id: {
        type: Sequelize.INTEGER,
        unique: true,
      },
      date_from: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
      },
      date_to: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
      },
      is_public: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      is_in_public_list: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addIndex('scenarios', {
      fields: ['item_id'],
      unique: true,
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('scenarios');
  },
};
