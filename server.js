import { appWithStartUp as app } from './app.js';
import ErrorHandler from './app/utils/errorHandler.js';

// eslint-disable-next-line no-unused-vars
process.on('unhandledRejection', (reason, promise) => {
  throw new Error(`UnhandledRejection: ${reason}`); // Pass to uncaughtException
});

process.on('uncaughtException', (error) => {
  ErrorHandler.handleError(error);
});

app.init(({ config, logger }) => {
  app.listen(config.TM_PORT, () => {
    logger.info(`==> Listening on port ${config.TM_PORT}.`);
  });
});
