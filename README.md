# (TraMod) WebApp Server

Server side component of TraMod WebApp. Tightly coupled to WebApp client component and build on top of traffic-modeller modelling server application (modelAPI). Thus main feature is management of traffic models (STA/DTA) and traffic scenarios based on these models data. Server app also manages calculations of scenarios and their distribution to modelAPI.

Build on Express framework and works with PostgreSQL.

## APP usage

1. Make sure you have recommended/tested NODE version installed (v16.17.0+)
2. Run `$ npm install` or `$ npm install --production` in production context.
3. Check/create .env file (options available are stored in .env.example)
4. Prepare database (see steps below)
5. Initialize users, traffic models and their data (see steps below)
6. Run `$ npm run serve` which starts up Nodemon in watch mode
   In production we normally manage app process with [PM2 process manager](https://pm2.keymetrics.io).
   In production we use NGINX proxy, for which some assumptions are made in code (mainly for security headers and other settings)

## DB Init

- App is developed and tested with PostgreSQL version 13.5

- For development you can use prepared docker DB container, `$ docker-compose up`. (This is not configured and recommended for production usage. Add postgres.env for this option to work)

- Check DB .env config and manually create required DB schema (this is not provided by migrations)

- Run `$ npm run db:migrate` to initialize DB models

- Rerun if DB models changed with new version

## User and traffic model data init

At least one traffic model and user DB item entry is needed for TraMod WebApp to work correctly.

1. User

- You can use provided user seed `$ npm run db:seed` or,
- You can use node helper script [see createUser.js](./scripts/readme.md)

2. Traffic Model

- If .env is filled with TrafficModelAPI options, these are loaded during migration or
- You can use node helper script [see createTrafficModel.js](./scripts/readme.md)

3. Traffic Model data

- default (baseModel) cache is required to be calculated for client to work correctly
- Its checked (and calculated) on server app startup or
- You can calculate it with node script `$ node scripts/calcDefault.js` or `$ node scripts/calcDefault.js --modelType DTA` for DTA, to check config and setup without starting app

## TESTING

### UNIT

Run `$ npm run test:unit` to process unit test (custom middleware and service definition coverage)

### INTEGRATION

Run `$ npm run test:integration` to process integration tests.
In preparation phase testing DB is set up with mock seed data.
**Usage of .env.test recommended.**
