import { ModelSection, closeDbConnection } from '../db/index.js';
import {
  deleteTrafficCache,
  DEFAULT_TRAFFIC_CACHE,
} from '../app/services/modelAPI.js';
import {
  shouldClearDefaultCache,
  clearDefaultCache,
  getTrafficModel,
} from './utils.js';

(async () => {
  const modelSections = await ModelSection.findAll();
  const trafficModel = await getTrafficModel();

  await Promise.allSettled(
    modelSections.map(async (modelSection) => {
      if (modelSection.key === DEFAULT_TRAFFIC_CACHE) return;
      try {
        await deleteTrafficCache({ cacheName: modelSection.key, trafficModel });
        await modelSection.destroy();
      } catch (error) {
        console.log(`Could not remove model section ${modelSection.key}`);
      }
    }),
  );

  if (shouldClearDefaultCache()) await clearDefaultCache(trafficModel);

  closeDbConnection();
})();
