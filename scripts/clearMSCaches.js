import { ModelSection, closeDbConnection } from '../db/index.js';
import {
  deleteTrafficCache,
  getTrafficCacheList,
  DEFAULT_TRAFFIC_CACHE,
} from '../app/services/modelAPI.js';
import {
  shouldClearDefaultCache,
  clearDefaultCache,
  getTrafficModel,
} from './utils.js';
import config from '../config/env.js';

(async () => {
  const { TM_MODEL_CACHE_PREFIX } = config;
  const trafficModel = await getTrafficModel();
  const localModelSections = await ModelSection.findAll();
  const localKeys = localModelSections.map((item) => item.key);

  const serverModelSections = await getTrafficCacheList({ trafficModel });
  const serverMSItems = serverModelSections.map((item) => {
    const [prefix] = item.cacheName.split('-');
    return { prefix, key: item.cacheName };
  });
  const namespacedServerItems = serverMSItems.filter(
    ({ prefix }) => prefix === TM_MODEL_CACHE_PREFIX,
  );

  const extraServerItems = namespacedServerItems.filter(
    ({ key }) => !localKeys.includes(key),
  );

  await Promise.allSettled(
    extraServerItems.map(async (modelSection) => {
      if (modelSection.key === DEFAULT_TRAFFIC_CACHE) return;
      try {
        await deleteTrafficCache({ cacheName: modelSection.key, trafficModel });
      } catch (error) {
        console.log(`Could not remove model section ${modelSection.key}`);
      }
    }),
  );

  if (shouldClearDefaultCache()) await clearDefaultCache(trafficModel);

  closeDbConnection();
})();
