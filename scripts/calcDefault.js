import { calcDefaultCache, getTrafficModel, logger } from './utils.js';

(async () => {
  const trafficModel = await getTrafficModel();
  await calcDefaultCache(trafficModel);
  if (!trafficModel.isValid) logger.error('DEFAULT CACHE NOT CALCULATED!');
})();
