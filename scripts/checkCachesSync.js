import { getTrafficModel } from './utils.js';
import { ModelSection } from '../db/index.js';
import { getTrafficCacheList } from '../app/services/modelAPI.js';
import logger from '../app/utils/logger.js';
import config from '../config/env.js';

(async () => {
  const { TM_MODEL_CACHE_PREFIX } = config;
  const trafficModel = await getTrafficModel();
  const localModelSections = await ModelSection.findAll();
  const localKeys = localModelSections.map((item) => item.key);

  const serverModelSections = await getTrafficCacheList({ trafficModel });
  const serverKeys = serverModelSections.reduce((list, item) => {
    const [prefix] = item.cacheName.split('-');
    if (prefix === TM_MODEL_CACHE_PREFIX) list.push(item.cacheName);
    return list;
  }, []);

  const missingOnServer = localKeys.filter((key) => !serverKeys.includes(key));
  if (missingOnServer.length > 0)
    logger.error(
      `Model server caches[${missingOnServer.length}] not found on server`,
    );
  else logger.info('Model caches sync OK');
})();
