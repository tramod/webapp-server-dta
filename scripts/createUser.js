import userFactory from '../db/factories/user.js';
import { getScriptArg } from './utils.js';

(async () => {
  const email = getScriptArg('--email');
  const username = getScriptArg('--username');
  const password = getScriptArg('--password');
  const ADMIN_USER_ROLE = 100;

  if (!email || !username || !password)
    throw new Error('Required userdata not provided');

  await userFactory(
    { email, username, password, roles: [ADMIN_USER_ROLE] },
    { withOrganization: false },
  );
})();
