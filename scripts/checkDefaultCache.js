import {
  testDefaultCache,
  calcDefaultCache,
  getTrafficModel,
  logger,
} from './utils.js';

(async () => {
  const trafficModel = await getTrafficModel();
  if (!(await testDefaultCache(trafficModel))) {
    await calcDefaultCache(trafficModel);
    if (!trafficModel.isValid) logger.error('DEFAULT CACHE NOT CALCULATED!');
  }
})();
