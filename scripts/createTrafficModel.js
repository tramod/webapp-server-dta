import trafficModelFactory from '../db/factories/trafficModel.js';
import { getScriptArg } from './utils.js';

(async () => {
  const name = getScriptArg('--modelName');
  const type = getScriptArg('--modelType');
  const endPoint = getScriptArg('--endPoint');
  const endPointApiKey = getScriptArg('--apiKey');

  if (!name || !type || !endPoint || !endPointApiKey)
    throw new Error('Required userdata not provided');

  await trafficModelFactory({
    name,
    type,
    endPoint,
    endPointApiKey,
    isValid: false,
    timestamp: null,
  });
})();
