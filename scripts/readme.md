# TraMod server helper scripts

Each script can be run from repo basePath (recommended) or from this folder (requires .env in this folder).
Some scripts have optional/required script params, but beware that no sophisticated validation is present so check your input to prevent unexpected results.
From basePath `$ node scripts/script.js --scriptParam true`

**calcDefault.js** - calculates default (baseModel) cache of trafficModel (overwrites previous result), traffic model is set to valid if calculation is finished

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default

**calculate.js** - helper to calculate all scenarios of selected traffic model, recalculates all scenarios but if some of registered model section (caches) matches, its reused

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default

**clearLocalCaches.js** - helper to remove all modelSections of trafficModel currently stored in server DB. They are removed both from modelAPI and DB

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default
- --clearDefault - [true] arg to remove base model cache of cache prefix space

**clearMSCaches.js** - helper to remove all modelAPI caches of local prefix (TM_MODEL_CACHE_PREFIX .env key) which are not present in server DB (to clear old model caches)

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default
- --clearDefault - [true] arg to remove base model cache of cache prefix space

**createTrafficModel.js** - helper to insert trafficModel to server DB (there is currently no option to do this directly in app). No proper validations are done during insertion, so calcDefault script run is recommended to check inserted model is valid before running app.

- --modelName - required, name of model to insert, (modelName to identify it in calls to modelAPI).
- --modelType - [STA, DTA] required, type of model to insert
- --endPoint - required, url to modelAPI endpoint
- --apiKey - Api key for protected modelAPI routes

**createUser.js** - helper to insert user to serverDB (at least one user is required to register other users). No proper validations so check correctness asap.

- --email - required
- --username - required
- --password - required

**checkDefaultCache.js** - checks existence of default cache of selected trafficModel on server and modelAPI, and tries to calculate if not found (intended to use pre-start)

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default

**checkMSHealth.js** - check if selected traffic model is loaded (and tries to load), checks default cache (same as above). Logs result. Intended to use as running cron job

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default

**checkCachesSync.js** - check if local caches are present on model server, logs results. Intended to use as running cron job for monitoring

- --modelType - [STA,DTA] specify modelType for traffic model selection, STA is default
