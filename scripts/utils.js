import trafficModelService from '../app/services/trafficModels.js';

const testDefaultCache = async (...args) =>
  trafficModelService.checkBaseModel(...args);
const calcDefaultCache = async (...args) =>
  trafficModelService.calculateBaseModel(...args);
const clearDefaultCache = async (...args) =>
  trafficModelService.removeBaseModel(...args);

export { default as logger } from '../app/utils/logger.js';

export {
  getScriptArg,
  shouldClearDefaultCache,
  isDTAModel,
  clearDefaultCache,
  testDefaultCache,
  calcDefaultCache,
  getTrafficModel,
};

function getScriptArg(arg) {
  const params = process.argv.slice(2);
  const argIndex = params.findIndex((param) => param === arg);
  return argIndex > -1 ? params[argIndex + 1] : null;
}

const isArgTrue = (arg) => getScriptArg(arg) === 'true';

function shouldClearDefaultCache() {
  return isArgTrue('--clearDefault');
}

function isDTAModel() {
  return isArgTrue('--dtaModel');
}

async function getTrafficModel() {
  const modelType = getScriptArg('--modelType') ?? 'DTA';
  return trafficModelService.getTrafficModel({ type: modelType });
}
