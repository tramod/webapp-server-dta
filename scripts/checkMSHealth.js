import { getServerHealthy, loadModel } from '../app/services/modelAPI.js';
import {
  testDefaultCache,
  calcDefaultCache,
  getTrafficModel,
  logger,
} from './utils.js';

(async () => {
  const trafficModel = await getTrafficModel();
  if (!(await isHealthy({ trafficModel }))) {
    try {
      await loadModel({ trafficModel });
    } catch (error) {
      logger.error('Could not reload model');
      return;
    }
  }

  if (!(await testDefaultCache(trafficModel))) {
    await calcDefaultCache(trafficModel);
    if (!trafficModel.isValid) logger.error('Could not refresh default cache');
    return;
  }

  logger.info('Model server check OK');
})();

async function isHealthy({ trafficModel }) {
  try {
    await getServerHealthy({ trafficModel });
    return true;
  } catch (error) {
    return false;
  }
}
