import scenarioCalculationService from '../app/services/calculations/index.js';
import { Scenario } from '../db/index.js';
import { getTrafficModel } from './utils.js';

(async () => {
  const trafficModel = await getTrafficModel();
  const scenarios = await Scenario.findAll({
    where: {
      trafficModelId: trafficModel.id,
    },
  });

  await Promise.allSettled(
    scenarios.map(async (scenario) => {
      await scenarioCalculationService.calculateScenario(scenario, () => {});
    }),
  );

  // Do not close db connection eagerly as async non-awaited processes may be still running
})();
