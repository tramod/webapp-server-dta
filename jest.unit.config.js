import config from './jest.config.js';

export default {
  ...config,
  testMatch: ['**/unit/**/?(*.)+(spec|test).[jt]s'],
  setupFilesAfterEnv: ['./tests/jest.setup.unit.js'],
};
