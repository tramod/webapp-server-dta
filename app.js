import express from 'express';
// Auto install itself on express, has to be imported before any route is created
// Async errors in route controllers automatically send to error route
import 'express-async-errors';
import {
  useTrustProxy,
  useCors,
  useBodyParser,
  useSession,
  usePassport,
  useHttpLogger,
  useHelmet,
  useDecoratedRequest,
  useReqId,
} from './app/middleware/index.js';
import { db } from './db/index.js';
import createRoutes from './app/routes/index.js';
import { AppStartUp } from './app/services/core.js';

const app = express();
useTrustProxy(app);
useCors(app);
useBodyParser(app);
useHelmet(app);
useDecoratedRequest(app);
const sessionStore = useSession(app, db);
usePassport(app);
useReqId(app);
useHttpLogger(app);
createRoutes(app);

const appWithStartUp = new AppStartUp({ app, db, sessionStore });

export default app;
export { appWithStartUp, sessionStore };
