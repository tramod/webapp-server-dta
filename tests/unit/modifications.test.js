import {
  getChildrenDepModIds,
  getParentDepModIds,
  getParentAndChildDepModIdsInRange,
  parseModificationExtrasDependencies,
} from '../../app/services/modification.js';

describe('Modification service', () => {
  describe('parseModificationExtrasDependencies function', () => {
    it('returns empty dependency list if modification has no extra dependencies', () => {
      const testModification = {
        id: 1,
        type: 'link',
        mode: 'new',
        data: { source: 2, target: 3 },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual([]);
    });

    it('returns dependency list for existing link', () => {
      const testModification = {
        id: 1,
        type: 'link',
        mode: 'existing',
        data: { linkId: ['extra-2'] },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual(['2']);
    });

    it('returns dependency list for new link', () => {
      const testModification = {
        id: 1,
        type: 'link',
        mode: 'new',
        data: { source: 'extra-2', target: 'extra-3' },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual(['2', '3']);
    });

    it('returns dependency list for existing node', () => {
      const testModification = {
        id: 1,
        type: 'node',
        mode: 'existing',
        data: { node: 'extra-2', linkFrom: 'extra-3', linkTo: 'extra-4' },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual(['2', '3', '4']);
    });

    it('returns empty dependency list for new node', () => {
      const testModification = {
        id: 1,
        type: 'node',
        mode: 'new',
        data: { node: [1478120.8206991316, 6414656.663293345] },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual([]);
    });

    it('returns dependency list for existing generator', () => {
      const testModification = {
        id: 1,
        type: 'generator',
        mode: 'existing',
        data: { generator: ['extra-2'], nodes: ['extra-3', 'extra-4', 5] },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual(['2', '3', '4']);
    });

    it('returns empty dependency list for new generator', () => {
      const testModification = {
        id: 1,
        type: 'generator',
        mode: 'new',
        data: { nodes: ['extra-3', 'extra-4', 5] },
      };
      const dependencies =
        parseModificationExtrasDependencies(testModification);

      expect(dependencies).toEqual(['3', '4']);
    });
  });

  describe('getChildrenDepModIds function', () => {
    const dependencyList = [
      { id: 1, dependencies: [] },
      { id: 2, dependencies: [] },
      { id: 3, dependencies: ['1', '2'] },
      { id: 4, dependencies: ['1'] },
      { id: 5, dependencies: ['3'] },
      { id: 6, dependencies: ['4'] },
    ];

    it('returns empty dependencies', () => {
      const dependencies = getChildrenDepModIds({
        dependencyList,
        modificationId: 6,
      });

      expect(dependencies).toEqual([]);
    });

    it('returns one level dependencies', () => {
      const dependencies = getChildrenDepModIds({
        dependencyList,
        modificationId: 4,
      });

      expect(dependencies).toEqual([6]);
    });

    it('returns multi level dependencies', () => {
      const dependencies = getChildrenDepModIds({
        dependencyList,
        modificationId: 1,
      });

      expect(dependencies).toEqual([3, 5, 4, 6]);
    });
  });

  describe('getParentDepModIds function', () => {
    const dependencyList = [
      { id: 1, dependencies: [] },
      { id: 2, dependencies: [] },
      { id: 3, dependencies: ['1', '2'] },
      { id: 4, dependencies: ['1'] },
      { id: 5, dependencies: ['3'] },
      { id: 6, dependencies: ['4'] },
    ];

    it('returns empty dependencies', () => {
      const dependencies = getParentDepModIds({
        dependencyList,
        modificationId: 1,
      });

      expect(dependencies).toEqual([]);
    });

    it('returns one level dependencies', () => {
      const dependencies = getParentDepModIds({
        dependencyList,
        modificationId: 3,
      });

      expect(dependencies).toEqual(['1', '2']);
    });

    it('returns multi level dependencies', () => {
      const dependencies = getParentDepModIds({
        dependencyList,
        modificationId: 5,
      });

      expect(dependencies).toEqual(['3', '1', '2']);
    });

    it('returns dependencies of modification that is not on the dep list', () => {
      const dependencies = getParentDepModIds({
        dependencyList,
        modificationId: 7,
        defaultDepMods: ['3', '6'],
      });

      expect(dependencies).toEqual(['3', '1', '2', '6', '4']);
    });
  });

  describe('getParentAndChildDepModIdsInRange function', () => {
    const dependencyList = [
      {
        id: 1,
        dependencies: [],
        dateFrom: '2021-01-11T00:00:00.731Z',
        dateTo: '2021-05-11T00:00:00.731Z',
      },
      {
        id: 2,
        dependencies: [],
        dateFrom: '2021-01-11T00:00:00.731Z',
        dateTo: '2021-05-11T00:00:00.731Z',
      },
      {
        id: 3,
        dependencies: ['1', '2'],
        dateFrom: '2021-02-11T00:00:00.731Z',
        dateTo: '2021-04-11T00:00:00.731Z',
      },
      {
        id: 4,
        dependencies: ['1'],
        dateFrom: '2021-02-11T00:00:00.731Z',
        dateTo: '2021-04-11T00:00:00.731Z',
      },
      {
        id: 5,
        dependencies: ['3'],
        dateFrom: '2021-02-11T00:00:00.731Z',
        dateTo: '2021-03-11T00:00:00.731Z',
      },
      {
        id: 6,
        dependencies: ['4'],
        dateFrom: '2021-02-11T00:00:00.731Z',
        dateTo: '2021-03-11T00:00:00.731Z',
      },
    ];

    it('returns empty dependencies for new modification without extras', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: null,
        defaultDepMods: [],
        dateRange: [null, null],
      });

      expect(dependencies).toEqual([]);
    });

    it('returns empty dependencies for new modification within date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: null,
        defaultDepMods: ['1'],
        dateRange: ['2021-02-11T00:00:00.731Z', '2021-04-11T00:00:00.731Z'],
      });

      expect(dependencies).toEqual([]);
    });

    it('returns parent dependency for new modification outside date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: null,
        defaultDepMods: ['1'],
        dateRange: [null, null],
      });

      expect(dependencies).toEqual([
        {
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
          dependencies: [],
          dependency: 'parent',
          id: 1,
        },
      ]);
    });

    it('returns empty dependencies for updated modification without extras', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: 1,
        defaultDepMods: [],
        dateRange: [null, null],
      });

      expect(dependencies).toEqual([]);
    });

    it('returns empty dependencies for updated modification within date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: 3,
        defaultDepMods: ['1', '2'],
        dateRange: ['2021-02-11T00:00:00.731Z', '2021-04-11T00:00:00.731Z'],
      });

      expect(dependencies).toEqual([]);
    });

    it('returns parent dependencies for updated modification outside date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: 3,
        defaultDepMods: ['1', '2'],
        dateRange: [null, null],
      });

      expect(dependencies).toEqual([
        {
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
          dependencies: [],
          dependency: 'parent',
          id: 1,
        },
        {
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
          dependencies: [],
          dependency: 'parent',
          id: 2,
        },
      ]);
    });

    it('returns child dependencies for updated modification outside date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: 1,
        defaultDepMods: [],
        dateRange: ['2021-03-11T00:00:00.731Z', '2021-04-11T00:00:00.731Z'],
      });

      expect(dependencies).toEqual([
        {
          dateFrom: '2021-02-11T00:00:00.731Z',
          dateTo: '2021-04-11T00:00:00.731Z',
          dependencies: ['1', '2'],
          dependency: 'child',
          id: 3,
        },
        {
          dateFrom: '2021-02-11T00:00:00.731Z',
          dateTo: '2021-04-11T00:00:00.731Z',
          dependencies: ['1'],
          dependency: 'child',
          id: 4,
        },
      ]);
    });

    it('returns both child and parent dependencies for updated modification not fitting date range', () => {
      const dependencies = getParentAndChildDepModIdsInRange({
        dependencyList,
        modificationId: 3,
        defaultDepMods: ['1', '2'],
        dateRange: ['2021-03-11T00:00:00.731Z', null],
      });

      expect(dependencies).toEqual([
        {
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
          dependencies: [],
          dependency: 'parent',
          id: 1,
        },
        {
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
          dependencies: [],
          dependency: 'parent',
          id: 2,
        },
        {
          dateFrom: '2021-02-11T00:00:00.731Z',
          dateTo: '2021-03-11T00:00:00.731Z',
          dependencies: ['3'],
          dependency: 'child',
          id: 5,
        },
      ]);
    });
  });
});
