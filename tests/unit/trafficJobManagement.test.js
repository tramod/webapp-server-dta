import { afterEach, beforeAll, expect, jest } from '@jest/globals';

import TrafficJobHandler from '../../app/services/calculations/trafficJobHandler.js';
import TrafficJobManager from '../../app/services/calculations/trafficJobManager.js';

import {
  axiosMock,
  mockPostTrafficJob,
  mockGetTrafficJobStatus,
  mockKillTrafficJob,
  resetAxiosRequestHistory,
} from '../integration/helpers/mockAxios.js';
import { waitForServer } from '../utils.js';

const mockModel = {
  name: 'test-model',
  type: 'DTA',
  id: 123,
  endPoint: 'test-url',
};
const mockedModelSection = { id: 'ModelSection' };
const mockedModelSectionArray = [mockedModelSection];

const loggerMock = { warn: jest.fn(), verbose: jest.fn() };
const msKeyPrefix = 'TEST';
const modelSectionServiceMock = {
  createModelSection: jest.fn(() => mockedModelSection),
  getLoneModelSections: jest.fn(() => mockedModelSectionArray),
  clearModelSections: jest.fn(() => null),
};

const testJob = {
  params: ['a', 'b', 'c'],
  hash: 'TEST-HASH',
  trafficModel: mockModel,
};

const anotherJob = {
  params: ['a'],
  hash: 'SECOND-HASH',
  trafficModel: mockModel,
};

beforeAll(() => {
  axiosMock.registerInstance(mockModel, { delay: 10 });
  axiosMock.setInstance(mockModel);
});

afterEach(() => {
  resetAxiosRequestHistory();
  jest.clearAllMocks();
});

describe('TrafficJobHandler', () => {
  it('Can be created and accept one job', () => {
    const handler = new TrafficJobHandler({
      logger: loggerMock,
      modelSectionService: modelSectionServiceMock,
      modelSectionKeyPrefix: msKeyPrefix,
    });
    expect(handler.isWaiting).toBe(true);

    handler.registerJob(testJob);
    expect(handler.currentJob).toEqual(testJob);
    expect(handler.isWaiting).toBe(false);
    expect(() => handler.registerJob({})).toThrow();
  });

  it('Job is handled with happy ending modelSection created by service and returned', async () => {
    mockPostTrafficJob();
    mockGetTrafficJobStatus();

    const handler = new TrafficJobHandler({
      logger: loggerMock,
      modelSectionService: modelSectionServiceMock,
      modelSectionKeyPrefix: msKeyPrefix,
    });
    handler.registerJob(testJob);

    const modelSection = await handler.handleJob();
    expect(modelSection).toEqual(mockedModelSection);
    expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(1);
    expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledWith({
      key: expect.anything(),
      hash: testJob.hash,
      trafficModelId: mockModel.id,
    });

    expect(axiosMock.mock.history.get.length).toBe(1);
    expect(axiosMock.mock.history.post.length).toBe(1);
    expect(handler.isWaiting).toBe(true);
  });

  it('Returns thread limit when post success without id', async () => {
    mockPostTrafficJob({ response: { jobId: null } });
    mockGetTrafficJobStatus();

    const handler = new TrafficJobHandler({
      logger: loggerMock,
      modelSectionService: modelSectionServiceMock,
      modelSectionKeyPrefix: msKeyPrefix,
    });
    handler.registerJob(testJob);

    const notModelSection = await handler.handleJob();
    expect(notModelSection).toBe('thread-limit');
    expect(handler.isWaiting).toBe(true);
  });

  it('Process multiple jobs in series', async () => {
    mockPostTrafficJob();
    mockGetTrafficJobStatus();

    const handler = new TrafficJobHandler({
      logger: loggerMock,
      modelSectionService: modelSectionServiceMock,
      modelSectionKeyPrefix: msKeyPrefix,
    });
    handler.registerJob(testJob);
    await handler.handleJob();

    handler.registerJob(anotherJob);
    await handler.handleJob();
    expect(axiosMock.mock.history.get.length).toBe(2);
    expect(axiosMock.mock.history.post.length).toBe(2);
    expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(2);
  });

  it('Can cancel job in multiple phases', async () => {
    mockPostTrafficJob();
    mockGetTrafficJobStatus();

    const handler = new TrafficJobHandler({
      logger: loggerMock,
      modelSectionService: modelSectionServiceMock,
      modelSectionKeyPrefix: msKeyPrefix,
    });
    handler.registerJob(testJob);

    // Cancel in posting phase
    const shouldCancel = handler.handleJob();
    handler.cancelJob();
    await shouldCancel;

    expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(0);
    expect(axiosMock.mock.history.get.length).toBe(0);
    expect(axiosMock.mock.history.post.length).toBe(1);
    expect(await shouldCancel).toBe('cancelled');

    // Cancel while getting status
    mockGetTrafficJobStatus({ response: { status: 'RUNNING' } });
    mockKillTrafficJob();

    handler.registerJob(testJob);
    const shouldAlsoCancel = handler.handleJob();
    await waitForServer(20);
    handler.cancelJob();
    await shouldAlsoCancel;

    expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(0);
    expect(axiosMock.mock.history.get.length).not.toBe(0);
    expect(axiosMock.mock.history.delete.length).toBe(1);
    expect(await shouldAlsoCancel).toBe('cancelled');
  });

  // TODO test retries?
});

describe('TrafficJobManager', () => {
  describe('Calculate', () => {
    it('Manages job to resolve parses', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus();

      const calculation = handler.calculate({ ...testJob, scenarioId: 1 });
      expect(handler.queue.length).toBe(0); // Job already taken
      await calculation;
      expect(await calculation).toBe(mockedModelSection);
      expect(modelSectionServiceMock.createModelSection).toHaveBeenCalled();
    });

    it('Manages multiple jobs with queue list', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus();

      const calculation = handler.calculate({ ...testJob, scenarioId: 1 });
      const calculation2 = handler.calculate({
        ...anotherJob,
        scenarioId: 1,
      });

      expect(handler.queue.length).toBe(1); // First one running, second one in queue
      await calculation;
      expect(handler.queue.length).toBe(0); // Second running
      await calculation2;
      expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(
        2,
      );
    });

    it('Handles error and throws it', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST', maxRetries: 1 },
      );
      mockPostTrafficJob({ code: 400 });

      const calculation = handler.calculate({ ...testJob, scenarioId: 1 });
      try {
        await calculation;
      } catch (error) {
        expect(error).toBe('Traffic job error');
      }
    });

    it('Handles same hash from multiple scenarios', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus();

      const scenario1 = handler.calculate({ ...testJob, scenarioId: 1 });
      handler.calculate({ ...testJob, scenarioId: 2 });
      handler.calculate({ ...testJob, scenarioId: 3 });
      expect(handler.queue.length).toBe(0); // Only one job
      await scenario1;
      expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(
        1,
      );
    });

    it('Manages multiple jobs in parallel if multiple threads set', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 2, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus();

      const calculation = handler.calculate({ ...testJob, scenarioId: 1 });
      const calculation2 = handler.calculate({
        ...anotherJob,
        scenarioId: 2,
      });

      expect(handler.queue.length).toBe(0); // First one running, second one in queue
      await calculation;
      await calculation2;
      expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(
        2,
      );
    });
  });

  describe('Cancel', () => {
    it('Manages to cancel jobs by scenarioId both from handler and queue', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus({ response: { status: 'RUNNING' } });
      mockKillTrafficJob();

      handler.calculate({ ...testJob, scenarioId: 1 }).catch((e) => {
        expect(e).toBe('Traffic job cancelled');
      });
      handler
        .calculate({
          ...anotherJob,
          scenarioId: 1,
        })
        .catch((e) => {
          expect(e).toBe('Traffic job cancelled');
        });
      expect(handler.queue.length).toBe(1); // First one running, second one in queue

      await waitForServer(20); // Wait for post job
      handler.cancel({ scenarioId: 1 });
      await waitForServer(20);

      expect(modelSectionServiceMock.createModelSection).not.toHaveBeenCalled();
      expect(axiosMock.mock.history.delete.length).toBe(1);
    });

    it('Do not cancel job when only one scenario cancels', async () => {
      const handler = new TrafficJobManager(
        {
          logger: loggerMock,
          modelSectionService: modelSectionServiceMock,
          trafficModel: mockModel,
        },
        { maxThreads: 1, msKeyPrefix: 'TEST' },
      );
      mockPostTrafficJob();
      mockGetTrafficJobStatus();

      handler.calculate({ ...testJob, scenarioId: 1 }).catch((e) => {
        expect(e).toBe('Traffic job cancelled');
      });
      const awaitJob = handler.calculate({ ...testJob, scenarioId: 2 });

      expect(handler.queue.length).toBe(0); // Only one job
      handler.cancel({ scenarioId: 1 });
      await awaitJob;
      expect(modelSectionServiceMock.createModelSection).toHaveBeenCalledTimes(
        1,
      );
    });
  });
});
