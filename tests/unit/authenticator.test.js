import { jest } from '@jest/globals';
import isAuthenticated from '../../app/middleware/authenticator.js';

describe('Authenticator middleware', () => {
  it('Should pass request with authenticated user', () => {
    const mockNext = jest.fn();
    const mockReq = {
      user: true,
    };

    isAuthenticated(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
  });

  it('Should block request without user, send UnAuthenticated response', () => {
    const mockNext = jest.fn();
    const mockRes = {
      status: jest.fn(),
      json: jest.fn(),
    };
    const mockReq = {};

    mockRes.status.mockImplementation(() => mockRes);
    isAuthenticated(mockReq, mockRes, mockNext);

    expect(mockNext).not.toHaveBeenCalled();
    expect(mockRes.status).toHaveBeenCalledWith(401);
    expect(mockRes.json).toHaveBeenCalled();
  });
});
