import { expect, it, jest } from '@jest/globals';
import { editModeParserWrapper } from '../../app/middleware/editModeParser.js';
import { TmErrorGeneral } from '../../app/utils/error.js';

describe('Resource fetch middleware', () => {
  const ITEM_ID = 2;
  const USR_ID = 3;
  const COPY_ID = 4;

  const sessionMock = {
    updatedRowId: COPY_ID,
    updateTimestamp: jest.fn(() => {}),
    isUndecided: jest.fn(() => {}),
  };

  const serviceInstance = {
    getEditSession: jest.fn(() => sessionMock),
  };

  const mockNext = jest.fn();

  afterEach(() => {
    sessionMock.updateTimestamp.mockClear();
    serviceInstance.getEditSession.mockClear();
    mockNext.mockClear();
  });

  const getBaseReq = (override) => ({
    params: {
      scenarioId: ITEM_ID,
    },
    query: {
      useChanged: true,
    },
    method: 'GET',
    route: {
      path: '/:scenarioId',
    },
    $tm: {},
    user: {
      id: USR_ID,
    },
    ...override,
  });

  const standardConfig = {};

  it('Should skip when edit mode is not active on app level', async () => {
    const editModeDisabledConfig = {
      TM_EDIT_MODE_DISABLE: true,
    };
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      editModeDisabledConfig,
    );
    const req = getBaseReq();
    await editModeParser(req, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(req.$tm.isEditSession).toBe(false);
    expect(serviceInstance.getEditSession).not.toHaveBeenCalled();
  });

  it('Should skip when route is GET without editMode query', async () => {
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq({
      query: {
        useChanged: false,
      },
    });
    await editModeParser(req, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(req.$tm.isEditSession).toBe(false);
    expect(serviceInstance.getEditSession).not.toHaveBeenCalled();
  });

  it('Should not skip when route is GET without editMode query but on ignore list', async () => {
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq({
      query: {
        useChanged: false,
      },
      route: {
        path: '/:scenarioId/edit',
      },
    });
    await editModeParser(req, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(req.$tm.isEditSession).toBe(true);
    expect(serviceInstance.getEditSession).toHaveBeenCalled();
  });

  it('Should provide edit session on GET request with editMode query', async () => {
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq();
    await editModeParser(req, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(req.$tm.isEditSession).toBe(true);
    expect(req.$tm.editSession).toBe(sessionMock);
    expect(serviceInstance.getEditSession).toHaveBeenCalled();
    expect(serviceInstance.getEditSession).toHaveBeenCalledWith(
      ITEM_ID,
      expect.anything(),
    );
  });

  it('Should provide edit session on NON-GET request', async () => {
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq({ method: 'POST' });
    await editModeParser(req, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(req.$tm.isEditSession).toBe(true);
    expect(req.$tm.editSession).toBe(sessionMock);
    expect(serviceInstance.getEditSession).toHaveBeenCalled();
    expect(serviceInstance.getEditSession).toHaveBeenCalledWith(
      ITEM_ID,
      expect.anything(),
    );
  });

  it('Should update session time on interaction', async () => {
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq({ method: 'POST' });
    await editModeParser(req, null, mockNext);

    expect(sessionMock.updateTimestamp).toHaveBeenCalled();
  });

  it('Should throw when no active session is found for scenario and user', async () => {
    serviceInstance.getEditSession.mockImplementationOnce(() => null);
    const editModeParser = editModeParserWrapper(
      serviceInstance,
      standardConfig,
    );
    const req = getBaseReq();

    try {
      await editModeParser(req, null, mockNext);
    } catch (error) {
      expect(error).toBeInstanceOf(TmErrorGeneral);
    }
  });
});
