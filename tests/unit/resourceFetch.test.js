import { beforeEach, expect, jest } from '@jest/globals';
import { resourceFetchWrapper } from '../../app/middleware/resourceFetch.js';
import { TmNotFoundError } from '../../app/utils/error.js';

describe('Resource fetch middleware', () => {
  const scenarioServiceMock = {
    getScenario: jest.fn(() => 'scenario'),
  };
  let resourceFetch;

  const SCE_ID = 1;
  const EVT_ID = 2;
  const USR_ID = 3;
  const COPY_ID = 4;

  const BASE_PARAMS = {
    allEvents: false,
    allModifications: false,
    allAccesses: false,
    allCalculations: false,
    trafficModel: false,
    eventId: undefined,
    isItemId: true,
    modificationId: undefined,
  };

  beforeEach(() => {
    resourceFetch = resourceFetchWrapper(scenarioServiceMock);
  });

  afterEach(() => {
    Object.values(scenarioServiceMock).forEach((mock) => mock.mockClear);
  });

  it('Should skip when no ids in param provided', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {},
      query: {},
    };

    await resourceFetch()(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.scenario).toBeUndefined();
    expect(scenarioServiceMock.getScenario).not.toHaveBeenCalled();
  });

  it('Should expose plain scenario when only base param called', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {
        scenarioId: SCE_ID,
      },
      path: `/${SCE_ID}`,
      user: {
        id: USR_ID,
      },
      $tm: {
        isEditSession: false,
      },
      query: {},
    };

    await resourceFetch()(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.scenario).toBe('scenario');
    expect(scenarioServiceMock.getScenario).toHaveBeenCalledWith(
      SCE_ID,
      USR_ID,
      {
        ...BASE_PARAMS,
        isItemId: true,
      },
    );
  });

  it('Should expose scenario with all events with deep path without event id', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {
        scenarioId: SCE_ID,
      },
      path: `/${SCE_ID}/something/nested`,
      user: {
        id: USR_ID,
      },
      $tm: {
        isEditSession: false,
      },
      query: {},
    };

    await resourceFetch()(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.scenario).toBe('scenario');
    expect(scenarioServiceMock.getScenario).toHaveBeenCalledWith(
      SCE_ID,
      USR_ID,
      {
        ...BASE_PARAMS,
        isItemId: true,
      },
    );
  });

  it('Should expose scenario with one events with both ids in path', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {
        scenarioId: SCE_ID,
        evtId: EVT_ID,
      },
      path: `/${SCE_ID}/something/nested/${EVT_ID}`,
      user: {
        id: USR_ID,
      },
      $tm: {
        isEditSession: false,
      },
      query: {},
    };

    await resourceFetch()(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.scenario).toBe('scenario');
    expect(scenarioServiceMock.getScenario).toHaveBeenCalledWith(
      SCE_ID,
      USR_ID,
      { ...BASE_PARAMS, isItemId: true, eventId: EVT_ID },
    );
  });

  it('Should fetch copied scenario in editSession, with sourceId filled', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {
        scenarioId: SCE_ID,
        evtId: EVT_ID,
      },
      path: `/${SCE_ID}/something/nested/${EVT_ID}`,
      user: {
        id: USR_ID,
      },
      $tm: {
        isEditSession: true,
        fetchScenarioId: COPY_ID,
        fetchScenarioIsItemId: false,
      },
      query: {},
    };

    scenarioServiceMock.getScenario.mockImplementationOnce(() => ({
      type: 'withEvent',
    }));

    await resourceFetch()(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.scenario.type).toBe('withEvent');
    expect(mockReq.$tm.scenario.sourceId).toBe(SCE_ID);
    expect(scenarioServiceMock.getScenario).toHaveBeenCalledWith(
      COPY_ID,
      USR_ID,
      { ...BASE_PARAMS, isItemId: false, eventId: EVT_ID },
    );
  });

  it('Should throw when resource requested but not found', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      params: {
        scenarioId: SCE_ID,
      },
      path: `/${SCE_ID}`,
      user: {
        id: USR_ID,
      },
      $tm: {
        isEditSession: false,
      },
      query: {},
    };

    try {
      await resourceFetch()(mockReq, null, mockNext);
    } catch (error) {
      expect(error).toBeInstanceOf(TmNotFoundError);
    }
  });
});
