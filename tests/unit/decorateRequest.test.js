import { jest } from '@jest/globals';
import decorateRequest from '../../app/middleware/decorateRequest.js';

describe('Authenticator middleware', () => {
  it('Should add $tm object property to req object', () => {
    const mockNext = jest.fn();
    const mockReq = {};

    decorateRequest(mockReq, null, mockNext);

    expect(mockReq.$tm).toEqual({});
    expect(mockNext).toHaveBeenCalled();
  });
});
