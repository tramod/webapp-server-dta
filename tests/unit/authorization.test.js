import { jest, describe, expect } from '@jest/globals';

import { defineAbilityFor } from '../../app/services/authorization.js';
import isAuthorized from '../../app/middleware/authorizator.js';
import { TmNotAuthorized } from '../../app/utils/error.js';

describe('Authenticator middleware', () => {
  function getAbilityMock(checkVal = true) {
    return {
      cannot: jest.fn(() => checkVal),
    };
  }

  const mockScenario = {
    formatForAbility: jest.fn(),
  };
  const testKey = 'able';

  it('Should pass request with authorized user', () => {
    const mockAbility = getAbilityMock(false);
    const mockReq = {
      $tm: {
        scenario: mockScenario,
        ability: mockAbility,
      },
    };
    const mockNext = jest.fn();

    isAuthorized({ abilityKey: testKey })(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
  });

  it('Should block request without authorized user, with thrown error', () => {
    const mockAbility = getAbilityMock(true);
    const mockReq = {
      $tm: {
        scenario: mockScenario,
        ability: mockAbility,
      },
    };
    const mockNext = jest.fn();
    const ERROR_MSG = 'error test message';

    const test = () =>
      isAuthorized({ abilityKey: testKey, errorMsg: ERROR_MSG })(
        mockReq,
        null,
        mockNext,
      );

    expect(test).toThrow();
    expect(test).toThrow(TmNotAuthorized);
    expect(test).toThrow(`UnAuthorized to ${ERROR_MSG}`);
    expect(mockNext).not.toHaveBeenCalled();
  });
});

const TEST_USER = { id: 1 };
class Scenario {
  constructor({
    level = 'editor',
    eventAuthorId = TEST_USER.id,
    eventIncluded = true,
  } = {}) {
    this.level = level;
    this.events = [{ authorUserId: eventAuthorId, included: eventIncluded }];
  }

  static get modelName() {
    return 'scenario';
  }
}

describe('Ability rules', () => {
  it('Returns defined ability', async () => {
    const ability = await defineAbilityFor(TEST_USER);
    expect(ability).toHaveProperty('can');
    expect(ability).toHaveProperty('cannot');

    const scenario = new Scenario();
    const authCheck = ability.can('get', scenario);
    expect(typeof authCheck).toBe('boolean');
  });

  describe('Rule access combinations', () => {
    // access key - scenario object
    const scenarios = [
      ['editor', new Scenario()],
      [
        'inserter(own:not_included)',
        new Scenario({ level: 'inserter', eventIncluded: false }),
      ],
      [
        'inserter(foreign:not_included)',
        new Scenario({
          level: 'inserter',
          eventAuthorId: 2,
          eventIncluded: false,
        }),
      ],
      ['inserter(own:included)', new Scenario({ level: 'inserter' })],
      [
        'inserter(foreign:included)',
        new Scenario({ level: 'inserter', eventAuthorId: 2 }),
      ],
      ['viewer', new Scenario({ level: 'viewer' })],
      ['unknown', new Scenario({ level: null })],
    ];

    // rule - name; expected - keys to be true
    const rules = [
      {
        rule: 'get',
        expected: [
          'editor',
          'inserter(own:not_included)',
          'inserter(foreign:not_included)',
          'inserter(own:included)',
          'inserter(foreign:included)',
          'viewer',
        ],
      },
      { rule: 'update', expected: ['editor'] },
      { rule: 'delete', expected: ['editor'] },
      {
        rule: 'create:event',
        expected: [
          'editor',
          'inserter(own:not_included)',
          'inserter(foreign:not_included)',
          'inserter(own:included)',
          'inserter(foreign:included)',
        ],
      },
      {
        rule: 'create:event:included',
        expected: ['editor'],
      },
      {
        rule: 'update:event',
        expected: ['editor', 'inserter(own:not_included)'],
      },
      {
        rule: 'update:event:included',
        expected: ['editor'],
      },
      {
        rule: 'delete:event',
        expected: ['editor', 'inserter(own:not_included)'],
      },
    ];

    const cases = rules.reduce((acc, entry) => {
      const scenarioCases = scenarios.map((scenario) => [
        entry.rule,
        ...scenario,
        entry.expected.includes(scenario[0]),
      ]);
      return [...acc, ...scenarioCases];
    }, []);

    test.each(cases)(
      'Rule %s, access type %s',
      async (ruleName, _accessType, scenario, expected) => {
        const ability = await defineAbilityFor(TEST_USER);
        expect(ability.can(ruleName, scenario)).toBe(expected);
      },
    );
  });
});
