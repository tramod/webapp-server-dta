import { beforeEach, expect, jest } from '@jest/globals';
import { abilityProviderWrapper } from '../../app/middleware/abilityProvider.js';

describe('Ability provider middleware', () => {
  let mockCache;
  let abilityProvider;
  const ability = { ability: 'object' };
  const createAbility = jest.fn(async () => ability);

  beforeEach(() => {
    mockCache = new Map();
    abilityProvider = abilityProviderWrapper(mockCache, createAbility);
  });

  afterEach(() => {
    createAbility.mockClear();
  });

  it('Should return no user simple abilities without user', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      user: null,
      $tm: {},
    };

    await abilityProvider(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.ability).toBe(ability);
    expect(createAbility).toHaveBeenCalled();
    expect(createAbility).toHaveBeenCalledWith(null);
  });

  it('Should expose ability from cache if available', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      user: { id: 1 },
      $tm: {},
    };

    // Add to cache
    mockCache.set(mockReq.user.id, await createAbility());
    createAbility.mockClear(); // Reset mock

    await abilityProvider(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.ability).toBe(ability);
    expect(createAbility).not.toHaveBeenCalled();
  });

  it('Should expose ability from createFn if not available in cache, and save to cache', async () => {
    const mockNext = jest.fn();
    const mockReq = {
      user: { id: 1 },
      $tm: {},
    };

    await abilityProvider(mockReq, null, mockNext);

    expect(mockNext).toHaveBeenCalled();
    expect(mockReq.$tm.ability).toBe(ability);
    expect(createAbility).toHaveBeenCalled();
    expect(createAbility).toHaveBeenCalledWith(mockReq.user);
    expect(mockCache.get(mockReq.user.id)).toBe(ability);
  });
});
