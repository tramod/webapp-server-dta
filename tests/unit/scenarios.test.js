import { jest } from '@jest/globals';
import Sequelize from 'sequelize';
import { ScenarioService } from '../../app/services/scenarios.js';

const mockData = [{ name: 'scenario1', type: 'private', authorId: 1 }];
const scenarioModel = {
  findAll: jest.fn(),
  scope: jest.fn(),
};
const models = { Scenario: scenarioModel };
scenarioModel.scope.mockImplementation(() => scenarioModel);

const scenarioService = new ScenarioService({
  models,
  Op: Sequelize.Op,
});

// proof of concept test, current function is glue between controller and orm, should be tested in integration
describe('Scenario service', () => {
  describe('getAll - Return valid scenario list', () => {
    it('Should return list', async () => {
      scenarioModel.findAll.mockImplementationOnce(() => mockData);
      const scenarios = await scenarioService.getScenarios(1);

      expect(scenarios).toEqual(mockData);
    });
  });
});
