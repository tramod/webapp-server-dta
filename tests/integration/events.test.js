import { beforeAll, it } from '@jest/globals';

import { getAppInstance, truncateDB } from './helpers/general.js';
import { expectInvalidInputError } from './helpers/errorResponses.js';
import {
  userFactory,
  scenarioFactory,
  eventFactory,
  getEventData,
} from '../../db/factories/index.js';
import { Event, Modification } from '../../db/index.js';
import Formatter from './formatters.js';
import config, { setConfig } from '../../config/env.js';
import eventService from '../../app/services/events.js';

let appInstance;
let baseUser;
let baseUserCredentials;

const editModeDisableSetting = config.TM_EDIT_MODE_DISABLE;
beforeAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', true);
});
afterAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', editModeDisableSetting);
});

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: baseUser, rawCredentials: baseUserCredentials } =
    await userFactory());
});

// TODO Update after access system implemented
// TODO Improve test cases build with factories
describe('Events', () => {
  describe('GetEventsByScenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns events from scenario', async () => {
      const EVENT_COUNT = 5;
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}, {}, {}],
      });
      const SCENARIO_ID = scenario.itemId;
      const response = await appInstance
        .get(`/scenarios/${SCENARIO_ID}/events`)
        .expect(200);
      expect(response.body.events.length).toBe(EVENT_COUNT);
    });

    it('Fails when scenario does not exist or has no events', async () => {
      const SCENARIO_NOT_EXISTS_ID = 2;
      const { scenario: SCENARIO_WITHOUT_EVENTS } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      await appInstance
        .get(`/scenarios/${SCENARIO_NOT_EXISTS_ID}/events`)
        .expect(404);
      await appInstance
        .get(`/scenarios/${SCENARIO_WITHOUT_EVENTS.itemId}/events`)
        .expect(404);
    });

    it('Return events in expected format', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ included: true }],
      });
      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/events`)
        .expect(200);

      const [event] = events;
      const expectedEvent = Formatter.eventModelToResponse({
        included: true,
        ...event.get(),
      });
      expect(response.body.events[0]).toMatchObject(expectedEvent);
      expect(response.body.events[0]).not.toHaveProperty('rowId');
      expect(response.body.events[0]).not.toHaveProperty('itemId');
      expect(response.body.events[0]).toHaveProperty('createdAt');
      expect(response.body.events[0]).toHaveProperty('updatedAt');
    });

    it('Returns event with author username as owner field', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id }],
      });
      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/events`)
        .expect(200);

      const responseResource = response.body.events[0];
      expect(responseResource).toHaveProperty('owner');
      expect(responseResource.owner).toEqual(baseUser.username);
    });

    it('Fails when scenario id is not valid', async () => {
      await appInstance.get(`/scenarios/abc/events`).expect(400);
      await appInstance.get(`/scenarios/@1/events`).expect(400);
    });
  });

  describe('GetEventById', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));
    it('Return event from scenario', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ included: true }],
      });
      const [event] = events;
      const SCENARIO_ID = scenario.itemId;
      const EVENT_ID = event.itemId;
      const response = await appInstance
        .get(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .expect(200);

      const expectedEvent = Formatter.eventModelToResponse(event);
      expect(response.body.event).toMatchObject(expectedEvent);
    });

    it('Fails with NotFound Error: scenario', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id }],
      });
      const [event] = events;
      const EVENT_ID = event.itemId;
      const NOT_SCENARIO_ID = scenario.itemId + 100;
      await appInstance
        .get(`/scenarios/${NOT_SCENARIO_ID}/events/${EVENT_ID}`)
        .expect(404);
    });

    it('Fails with NotFound error: event', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const NOT_EVENT_ID = 1;
      await appInstance
        .get(`/scenarios/${scenario.itemId}/events/${NOT_EVENT_ID}`)
        .expect(404);
    });

    it('Fails with NotFound Error: event is not included in scenario', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      const { events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id }],
      });
      const [event] = events;
      const SCENARIO_ID = scenario.itemId;
      const OTHERS_EVENT_ID = event.itemId;
      await appInstance
        .get(`/scenarios/${SCENARIO_ID}/events/${OTHERS_EVENT_ID}`)
        .expect(404);
    });

    const validationCases = [
      ['ScenarioId', 'abc', 1, ['scenarioId']],
      ['ScenarioId', '@1', 1, ['scenarioId']],
      ['ScenarioId', 1.1, 1, ['scenarioId']],
      ['EventId', 1, 'abc', ['evtId']],
      ['EventId', 1, '@1', ['evtId']],
      ['EventId', 1, 1.1, ['evtId']],
    ];
    test.each(validationCases)(
      'Fails with invalid input: %s',
      async (_testName, scenarioId, eventId, invalidFields) => {
        await expectInvalidInputError(appInstance, {
          url: `/scenarios/${scenarioId}/events/${eventId}`,
          methodName: 'get',
          invalidFields,
        });
      },
    );
  });

  describe('CreateEvent', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));
    it('Creates event instance and assign id', async () => {
      const EVENTS_IN_DB = 3;
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}],
      });
      const SCENARIO_TARGET_ID = scenario.itemId;
      const EXPECTED_ID = EVENTS_IN_DB + 1;
      const event = await getEventData({ authorUserId: baseUser.id });

      const response = await appInstance
        .post(`/scenarios/${SCENARIO_TARGET_ID}/events`)
        .send(event)
        .expect(201);
      expect(response.body.event.id).toEqual(EXPECTED_ID);
      const expectedEvent = Formatter.eventModelToResponse(event);
      expect(response.body.event).toMatchObject(expectedEvent);
      // TODO GET event by id to check it
    });

    it('Fails with invalid body content', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const SCENARIO_TARGET_ID = scenario.itemId;

      // Only name is mandatory
      const incompleteEvent = {
        description: 'someDescription',
      };
      const incompleteResponse = await appInstance
        .post(`/scenarios/${SCENARIO_TARGET_ID}/events`)
        .send(incompleteEvent)
        .expect(400);
      const incompleteFields = incompleteResponse.body.error.map(
        (err) => err.param,
      );
      expect(incompleteFields).toEqual(['name']);

      const invalidParams = {
        name: '',
        description: 123,
        included: 'truthy',
      };
      const invalidResponse = await appInstance
        .post(`/scenarios/${SCENARIO_TARGET_ID}/events`)
        .send(invalidParams)
        .expect(400);
      const invalidFields = invalidResponse.body.error.map((err) => err.param);
      expect(invalidFields).toEqual(Object.keys(invalidParams));
    });

    it('Fails when parent scenario does not exist', async () => {
      const SCENARIO_NOT_EXISTS_ID = 1;
      const event = await getEventData({ authorUserId: baseUser.id });
      await appInstance
        .post(`/scenarios/${SCENARIO_NOT_EXISTS_ID}/events`)
        .send(event)
        .expect(404);
    });

    it('Fails when scenario id is not valid', async () => {
      const event = await getEventData({ authorUserId: baseUser.id });
      await appInstance.post(`/scenarios/abc/events`).send(event).expect(400);
      await appInstance.post(`/scenarios/@1/events`).send(event).expect(400);
    });
  });

  describe('Delete single event', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Can be deleted by authorized user', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id }],
      });
      const [event] = events;

      await appInstance
        .delete(`/scenarios/${scenario.itemId}/events/${event.itemId}`)
        .expect(200);

      const deletedEvent = await eventService.models.Event.findByPk(
        event.rowId,
      );
      expect(deletedEvent).toEqual(null);
    });

    it('Is deleted with connected modifications', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id, modifications: [{}] }],
      });
      const [event] = events;
      const mods = await event.getModifications();
      const [mod] = mods;

      await appInstance
        .delete(`/scenarios/${scenario.itemId}/events/${event.itemId}`)
        .expect(200);

      expect(await Event.findByPk(event.rowId)).toEqual(null);
      expect(await Modification.findByPk(mod.rowId)).toEqual(null);
    });

    it('Can not be deleted by unauthorized user', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id }],
      });
      const [event] = events;
      const { rawCredentials: noAccessUserCredentials } = await userFactory();

      await appInstance.sessionLogin(noAccessUserCredentials);
      await appInstance
        .delete(`/scenarios/${scenario.itemId}/events/${event.itemId}`)
        .expect(403);
    });

    it('Fails due to dependency error', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });

      const { event: dependingEvent, modifications: mods1 } =
        await eventFactory(
          { modifications: [{ type: 'link', mode: 'new' }] },
          { attachedScenarioModel: scenario },
        );
      const [dependingMod] = mods1;

      const { modifications: mods2 } = await eventFactory(
        { modifications: [{ type: 'link', mode: 'existing' }] },
        { attachedScenarioModel: scenario },
      );
      const [dependentMod] = mods2;

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      await appInstance
        .delete(`/scenarios/${scenario.itemId}/events/${dependingEvent.itemId}`)
        .expect(400);
    });

    it('Deletes event with connected modifications in other events', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });

      const { event: dependingEvent, modifications: mods1 } =
        await eventFactory(
          { modifications: [{ type: 'link', mode: 'new' }] },
          { attachedScenarioModel: scenario },
        );
      const [dependingMod] = mods1;

      const { event: dependentEvent, modifications: mods2 } =
        await eventFactory(
          { modifications: [{ type: 'link', mode: 'existing' }] },
          { attachedScenarioModel: scenario },
        );
      const [dependentMod] = mods2;

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      await appInstance
        .delete(
          `/scenarios/${scenario.itemId}/events/${dependingEvent.itemId}?deleteDependencies=true`,
        )
        .expect(200);

      expect(await Event.findByPk(dependentEvent.rowId)).not.toBe(null);
      expect(await Event.findByPk(dependingEvent.rowId)).toBe(null);
      expect(await Modification.findByPk(dependentMod.rowId)).toBe(null);
      expect(await Modification.findByPk(dependingMod.rowId)).toBe(null);
    });
  });

  describe('Delete events bulk', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Delete provided events and returns success status', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}],
      });
      const eventIds = events.map((event) => event.itemId);

      const deleteResponse = await appInstance
        .delete(
          `/scenarios/${scenario.itemId}/events?ids=${eventIds.join(',')}`,
        )
        .expect(200);
      expect(deleteResponse.body.results.length).toBe(3);
      const errorNumber = deleteResponse.body.results.filter(
        (entry) => !!entry.error,
      ).length;
      expect(errorNumber).toBe(0);

      // Removed from model
      const scenarioEvents = await scenario.getEvents();
      expect(scenarioEvents.length).toBe(0);

      // Events should not be found
      await appInstance.get(`/scenarios/${scenario.itemId}/events`).expect(404);
    });

    it('Delete available events and returns mixed status', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}, {}],
      });
      const eventIds = events.map((event) => event.itemId);
      const idsForTest = [eventIds[0], eventIds[2], 1000, 2000]; // 2 should be present, 2 should be not found

      const deleteResponse = await appInstance
        .delete(
          `/scenarios/${scenario.itemId}/events?ids=${idsForTest.join(',')}`,
        )
        .expect(207);
      const errorNumber = deleteResponse.body.results.filter(
        (entry) => !!entry.error,
      ).length;
      const successNumber = deleteResponse.body.results.filter(
        (entry) => entry.status,
      ).length;
      expect(errorNumber).toBe(2);
      expect(successNumber).toBe(2);

      // Others are not deleted
      const scenarioEvents = await scenario.getEvents();
      expect(scenarioEvents.length).toBe(2);
    });

    it('Responses with code 404 when no event is available', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const idsForTest = [1, 5, 1000, 2000];

      await appInstance
        .delete(
          `/scenarios/${scenario.itemId}/events?ids=${idsForTest.join(',')}`,
        )
        .expect(404);
    });

    it('Fails when source scenario is not found', async () => {
      const SCENARIO_NOT_EXISTS_ID = 1;
      await appInstance
        .delete(`/scenarios/${SCENARIO_NOT_EXISTS_ID}/events?ids=1,2,3`)
        .expect(404);
    });

    it('Validates events ids are integer array(string)', async () => {
      // Invalid scenario id params
      await appInstance.delete(`/scenarios/abc/events?ids=1`).expect(400);
      await appInstance.delete(`/scenarios/@1/events?ids=1`).expect(400);

      // Ids is stringified array -> ids=1,2,3
      await appInstance.delete(`/scenarios/2/events?ids=[1,2,3]`).expect(400);

      // Ids are mandatory
      await appInstance.delete(`/scenarios/3/events?ids=null`).expect(400);
      await appInstance.delete(`/scenarios/4/events`).expect(400);

      // Ids entries are integer number
      await appInstance.delete(`/scenarios/5/events?ids=1,a,3`).expect(400);
      await appInstance.delete(`/scenarios/6/events?ids=1,,3`).expect(400);
    });
  });

  describe('UpdateEvent', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));
    it('Updates existing event instance', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ included: true }],
      });
      const SCENARIO_ID = scenario.itemId;
      const EVENT_ID = events[0].itemId;

      const updateData = await getEventData({
        authorUserId: baseUser.id,
      });
      const updated = await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send(updateData)
        .expect(200);
      expect(updated.body.event.id).toEqual(EVENT_ID);
      expect(updated.body.event).toMatchObject(
        Formatter.eventModelToResponse(updateData),
      );
    });

    it('Updates existing event instance with only one property', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ included: true }],
      });
      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const name = `${event.name}changed`;
      const EVENT_ID = event.itemId;

      const updated = await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ name })
        .expect(200);
      expect(updated.body.event.id).toEqual(EVENT_ID);
      expect(updated.body.event).toMatchObject(
        Formatter.eventModelToResponse({ ...event.get(), id: EVENT_ID, name }),
      );
    });

    it('Can change included property only', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ included: true }],
      });
      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const EVENT_ID = event.itemId;
      expect(event.included).toBe(true);

      const updated = await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ included: false })
        .expect(200);
      expect(updated.body.event.id).toEqual(EVENT_ID);
      expect(updated.body.event.included).toEqual(false);
      await event.reload();
      expect(event.included).toBe(false);
    });

    const validationCases = [
      [
        'Nothing updated',
        {
          body: {},
        },
      ],
      [
        'Invalid Body properties',
        {
          body: {
            name: '',
            description: 123,
          },
          invalidFields: ['name', 'description'],
        },
      ],
      ['Param:ScenarioId', { scenarioId: 1.1 }],
      ['Param:ScenarioId', { scenarioId: 'abc' }],
      ['Param:ScenarioId', { scenarioId: true }],
      ['Param:ScenarioId', { scenarioId: '1b3' }],
      ['Param:EventId', { eventId: 1.1 }],
      ['Param:EventId', { eventId: 'abc' }],
      ['Param:EventId', { eventId: true }],
      ['Param:EventId', { eventId: '1b3' }],
    ];
    test.each(validationCases)(
      'Fails with invalid input: %s',
      async (
        _testName,
        {
          body = { name: 'updated' },
          invalidFields = [],
          scenarioId,
          eventId,
        } = {},
      ) => {
        const { scenario, events } = await scenarioFactory({
          authorUserId: baseUser.id,
          events: [{}],
        });
        const SCENARIO_ID = scenarioId ?? scenario.itemId;
        const EVENT_ID = eventId ?? events[0].itemId;
        if (scenarioId) invalidFields.push('scenarioId');
        if (eventId) invalidFields.push('evtId');
        await expectInvalidInputError(appInstance, {
          url: `/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`,
          data: body,
          methodName: 'patch',
          invalidFields,
        });
      },
    );

    it('Fails with NotFound Error: scenario', async () => {
      const { events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}],
      });
      const SCENARIO_NOT_EXISTS_ID = 100;
      const EVENT_ID = events[0].itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_NOT_EXISTS_ID}/events/${EVENT_ID}`)
        .send({ name: 'updated' })
        .expect(404);
    });

    it('Fails with NotFound Error: event', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const SCENARIO_ID = scenario.itemId;
      const EVENT_NOT_EXISTS_ID = 100;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_NOT_EXISTS_ID}`)
        .send({ name: 'updated' })
        .expect(404);
    });

    it('Fails with NotFound Error: event not included in scenario', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const { events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}],
      });
      const SCENARIO_ID = scenario.itemId;
      const OTHERS_EVENT_ID = events[0].itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${OTHERS_EVENT_ID}`)
        .send({ name: 'updated' })
        .expect(404);
    });

    it('Inserter user cannot update event from other user', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario, events } = await scenarioFactory({
        authorUserId: otherUser.id,
        events: [{ authorUserId: otherUser.id }],
        scenarioAccesses: [{ userId: baseUser.id, level: 'inserter' }],
      });

      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const EVENT_ID = event.itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ name: 'updated' })
        .expect(403);
    });

    it('Inserter user can update own event', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario, events } = await scenarioFactory({
        authorUserId: otherUser.id,
        events: [{ authorUserId: baseUser.id, included: false }],
        scenarioAccesses: [{ userId: baseUser.id, level: 'inserter' }],
      });

      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const EVENT_ID = event.itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ name: 'updated' })
        .expect(200);
    });

    it('Inserter user cannot update own event which is already included', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario, events } = await scenarioFactory({
        authorUserId: otherUser.id,
        events: [{ authorUserId: baseUser.id, included: true }],
        scenarioAccesses: [{ userId: baseUser.id, level: 'inserter' }],
      });

      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const EVENT_ID = event.itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ name: 'updated' })
        .expect(403);
    });

    it('Inserter user cannot update included prop in own event', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario, events } = await scenarioFactory({
        authorUserId: otherUser.id,
        events: [{ authorUserId: baseUser.id, included: false }],
        scenarioAccesses: [{ userId: baseUser.id, level: 'inserter' }],
      });

      const SCENARIO_ID = scenario.itemId;
      const [event] = events;
      const EVENT_ID = event.itemId;

      await appInstance
        .patch(`/scenarios/${SCENARIO_ID}/events/${EVENT_ID}`)
        .send({ included: true })
        .expect(403);
    });

    it('Fails due to dependency error', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });

      const { modifications: mods1 } = await eventFactory(
        { included: false, modifications: [{ type: 'link', mode: 'new' }] },
        { attachedScenarioModel: scenario },
      );
      const [dependingMod] = mods1;

      const { event: dependentEvent, modifications: mods2 } =
        await eventFactory(
          {
            included: false,
            modifications: [{ type: 'link', mode: 'existing' }],
          },
          { attachedScenarioModel: scenario },
        );
      const [dependentMod] = mods2;

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      const updatedData = {
        included: true,
      };

      await appInstance
        .patch(`/scenarios/${scenario.itemId}/events/${dependentEvent.itemId}`)
        .send(updatedData)
        .expect(400);
    });

    it('Updates inclusion of dependant event', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });

      const { event: dependingEvent, modifications: mods1 } =
        await eventFactory(
          { included: false, modifications: [{ type: 'link', mode: 'new' }] },
          { attachedScenarioModel: scenario },
        );
      const [dependingMod] = mods1;

      const { event: dependentEvent, modifications: mods2 } =
        await eventFactory(
          {
            included: false,
            modifications: [{ type: 'link', mode: 'existing' }],
          },
          { attachedScenarioModel: scenario },
        );
      const [dependentMod] = mods2;

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      await appInstance
        .patch(
          `/scenarios/${scenario.itemId}/events/${dependentEvent.itemId}?updateDependencies=true`,
        )
        .send({ included: true })
        .expect(200);

      await dependingEvent.reload();
      await dependentEvent.reload();
      expect(dependingEvent.included).toBe(true);
      expect(dependentEvent.included).toBe(true);
    });
  });

  describe('Copy event', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns copy of the original event', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id, included: false }],
      });
      const [event] = events;
      const eventBody = Formatter.eventModelToResponse(event);
      const expectedBody = {
        ...eventBody,
        name: `(copy) ${eventBody.name}`,
        included: true, // copied event is allways included if it can be included
      };

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/events/${event.itemId}/copy`)
        .expect(201);

      const responseResource = response.body.event;

      expect(responseResource).toMatchObject(expectedBody);
    });

    it('Copies event with all modifications', async () => {
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [
          {
            authorUserId: baseUser.id,
            included: false,
            modifications: [
              { authorUserId: baseUser.id },
              { authorUserId: baseUser.id },
              { authorUserId: baseUser.id },
            ],
          },
        ],
      });
      const [event] = events;
      const mods = await event.getModifications();

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/events/${event.itemId}/copy`)
        .expect(201);

      const responseMods = response.body.event.modifications;

      responseMods.forEach((respMod, i) => {
        const expectedMod = Formatter.modificationModelToResponse(mods[i]);
        expect(respMod).toMatchObject(expectedMod);
      });
    });

    it('Can not by copied by unauthorized user', async () => {
      const { rawCredentials: anotherUserCredentials } = await userFactory();
      const { scenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id, included: false }],
      });
      const [event] = events;

      await appInstance.sessionLogin(anotherUserCredentials);
      await appInstance
        .post(`/scenarios/${scenario.itemId}/events/${event.itemId}/copy`)
        .expect(403);
    });
  });

  describe('Import events', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns importable events for scenario', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}, {}, {}],
      });
      const { scenario: sourceScenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}],
      });
      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/importable-events`)
        .expect(200);
      expect(response.body.events.length).toBe(2);
      expect(response.body.events[0].scenarioId).toBe(sourceScenario.itemId);
      expect(response.body.events[1].scenarioId).toBe(sourceScenario.itemId);
    });

    it('Returns importable events from public scenario', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      await scenarioFactory({
        authorUserId: otherUser.id,
        isInPublicList: true,
        events: [{}, {}],
      });
      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/importable-events`)
        .expect(200);
      expect(response.body.events.length).toBe(2);
    });

    it('Returns no events from another users scenario', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      await scenarioFactory({
        authorUserId: otherUser.id,
        isInPublicList: false,
        events: [{}, {}],
      });
      await appInstance
        .get(`/scenarios/${scenario.itemId}/importable-events`)
        .expect(404);
    });

    it('Fails when scenario does not exist or there are no importable events', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });

      await appInstance.get(`/scenarios/99/importable-events`).expect(404);
      await appInstance
        .get(`/scenarios/${scenario.itemId}/importable-events`)
        .expect(404);
    });

    it('Return events in expected format', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}, {}, {}, {}, {}],
      });
      const { events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}],
      });

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/importable-events`)
        .expect(200);

      const [event] = events;
      const expectedEvent = Formatter.eventModelToResponse({
        ...event.get(),
      });
      expect(response.body.events[0]).toMatchObject(expectedEvent);
      expect(response.body.events[0]).not.toHaveProperty('rowId');
      expect(response.body.events[0]).not.toHaveProperty('itemId');
      expect(response.body.events[0]).toHaveProperty('createdAt');
      expect(response.body.events[0]).toHaveProperty('updatedAt');
      expect(response.body.events[0]).toHaveProperty('owner');
    });

    it('Returns imported copies of events', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      const { scenario: sourceScenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{}],
      });
      const [event] = events;
      const eventBody = Formatter.eventModelToResponse(event);
      const expectedBody = {
        ...eventBody,
        name: `(import) ${eventBody.name}`,
        included: true, // copied event is allways included if it can be included
      };

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/import-events`)
        .send({
          events: [
            { eventId: event.itemId, scenarioId: sourceScenario.itemId },
          ],
        })
        .expect(201);

      const responseResource = response.body.events[0];

      expect(responseResource).toMatchObject(expectedBody);
    });

    it('Imports event with all modifications', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      const { scenario: sourceScenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [
          {
            authorUserId: baseUser.id,
            included: false,
            modifications: [
              { authorUserId: baseUser.id },
              { authorUserId: baseUser.id },
              { authorUserId: baseUser.id },
            ],
          },
        ],
      });
      const [event] = events;
      const mods = await event.getModifications();

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/import-events`)
        .send({
          events: [
            { eventId: event.itemId, scenarioId: sourceScenario.itemId },
          ],
        })
        .expect(201);

      const responseMods = response.body.events[0].modifications;

      responseMods.forEach((respMod, i) => {
        const expectedMod = Formatter.modificationModelToResponse(mods[i]);
        expect(respMod).toMatchObject(expectedMod);
      });
    });

    it('Can not by imported by unauthorized user', async () => {
      const { rawCredentials: anotherUserCredentials } = await userFactory();
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });
      const { scenario: sourceScenario, events } = await scenarioFactory({
        authorUserId: baseUser.id,
        events: [{ authorUserId: baseUser.id, included: false }],
      });
      const [event] = events;

      await appInstance.sessionLogin(anotherUserCredentials);
      await appInstance
        .post(`/scenarios/${scenario.itemId}/import-events`)
        .send({
          events: [
            { eventId: event.itemId, scenarioId: sourceScenario.itemId },
          ],
        })
        .expect(403);
    });

    it('Can not by imported with invalid body content', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
      });

      await appInstance
        .post(`/scenarios/${scenario.itemId}/import-events`)
        .send({
          events: [],
        })
        .expect(400);

      await appInstance
        .post(`/scenarios/${scenario.itemId}/import-events`)
        .send({
          events: [{ eventId: 'asd', scenarioId: 'sdf' }],
        })
        .expect(400);
    });
  });
});
