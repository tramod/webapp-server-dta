import { it } from '@jest/globals';
import { getAppInstance, truncateDB } from './helpers/general.js';
import {
  userFactory,
  getUserData,
  getOrganizationData,
} from '../../db/factories/index.js';
import Formatter from './formatters.js';
import { User, Organization, UserToken } from '../../db/index.js';

let appInstance;
let adminUser;
let adminUserCredentials;
let adminUserToken;

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: adminUser, rawCredentials: adminUserCredentials } =
    await userFactory({ roles: [100] }, { withOrganization: false }));
});

describe('Users', () => {
  describe('Protected routes', () => {
    it('protects routes under authentication', async () => {
      const userId = adminUser.id;
      await userFactory({ organization: 'testorg' });

      const protectedRoutes = [
        { route: '/users', method: 'get' },
        { route: `/users`, method: 'post' },
        { route: `/users/organizations`, method: 'get' },
        { route: `/users/${userId}`, method: 'get' },
        { route: `/users/${userId}`, method: 'patch' },
        { route: `/users/${userId}`, method: 'delete' },
      ];

      protectedRoutes.map(async (entry) => {
        await appInstance[entry.method](entry.route).expect(401);
      });
    });
  });

  describe('Get users', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('returns all users', async () => {
      const { user: user2 } = await userFactory();
      const { user: user3 } = await userFactory();

      const response = await appInstance.get('/users');

      const returnedIds = response.body.users?.map((entry) => entry.id) || [];
      expect(returnedIds.sort()).toEqual([adminUser.id, user2.id, user3.id]);

      const [user] = response.body.users;
      expect(user).toHaveProperty('username');
      expect(user).not.toHaveProperty('email');
      expect(user).not.toHaveProperty('roles');
    });

    it('returns users details with hidden fields when requested as admin', async () => {
      await userFactory();
      await userFactory();

      const response = await appInstance.get('/users?hiddenFields=true');
      const [user] = response.body.users;
      expect(user).toHaveProperty('username');
      expect(user).toHaveProperty('email');
      expect(user).toHaveProperty('roles');
    });

    it('returns 403 for unauthorized user when requesting hidden fields', async () => {
      const { rawCredentials: nonAdminUserCredentials } = await userFactory();
      await appInstance.sessionLogin(nonAdminUserCredentials);
      await appInstance.get('/users?hiddenFields=true').expect(403);
    });

    it('returns 404 with no users', async () => {
      // can not actually happen since the route is under authentication
      // and returns 401 before 404
    });
  });

  describe('Get user organizations', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('returns all organizations', async () => {
      // there is no need to manage organizations by its own in the app right now
      // so there are no separate organization factories yet
      // TODO: update when needed
      const { organization: org1 } = await userFactory({ organization: 'o1' });
      const { organization: org2 } = await userFactory({ organization: 'o2' });
      const { organization: org3 } = await userFactory({ organization: 'o3' });

      const response = await appInstance
        .get(`/users/organizations`)
        .expect(200);

      const returnedIds =
        response.body.organizations?.map((entry) => entry.id) || [];
      expect(returnedIds.sort()).toEqual([org1.id, org2.id, org3.id]);
    });

    it('returns 404 with no organizations', async () => {
      await appInstance.get(`/users/organizations`).expect(404);
    });
  });

  describe('Get user', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('returns user by id', async () => {
      const response = await appInstance
        .get(`/users/${adminUser.id}`)
        .expect(200);

      const expectedUser = {
        ...Formatter.userModelToResponse(adminUser),
      };

      const responseUser = response.body.user;
      expect(responseUser).toMatchObject(expectedUser);
    });

    it('returns user with organization', async () => {
      const { user, organization } = await userFactory({
        organization: 'testorg',
      });

      const response = await appInstance.get(`/users/${user.id}`).expect(200);

      const expectedUser = {
        ...Formatter.userModelToResponse(user, organization),
      };

      const responseUser = response.body.user;
      expect(responseUser).toMatchObject(expectedUser);
    });

    it('returns 403 for unauthorized user', async () => {
      const { rawCredentials: nonAdminUserCredentials } = await userFactory();

      await appInstance.sessionLogin(nonAdminUserCredentials);

      await appInstance.get(`/users/${adminUser.id}`).expect(403);
    });

    it('returns 404 when user is not found', async () => {
      const nonexistingUserId = 101;

      await appInstance.get(`/users/${nonexistingUserId}`).expect(404);
    });

    it('returns 400 when userId is not valid', async () => {
      await appInstance.get(`/users/abc`).expect(400);
      await appInstance.get(`/users/@1`).expect(400);
    });
  });

  describe('Create user', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('creates new user', async () => {
      const { user } = await getUserData();
      const expectedUserId = adminUser.id + 1;

      const response = await appInstance.post(`/users`).send(user).expect(201);

      const responseUser = response.body.user;

      expect(responseUser).toMatchObject(
        Formatter.userModelToResponse({ ...user, id: expectedUserId }),
      );

      const createdUser = await User.findByPk(expectedUserId);
      expect(createdUser.id).toEqual(expectedUserId);
    });

    it('creates new user with new organization', async () => {
      let existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(0);

      const { user } = await getUserData();
      const organization = await getOrganizationData();
      const expectedUserId = adminUser.id + 1;

      const response = await appInstance
        .post(`/users`)
        .send({ ...user, organization: organization.name })
        .expect(201);

      const responseUser = response.body.user;

      expect(responseUser).toMatchObject(
        Formatter.userModelToResponse(
          { ...user, id: expectedUserId },
          organization,
        ),
      );

      existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(organization.name);
    });

    it('creates new user with existing organization', async () => {
      const { organization: existingOrganization } = await userFactory({
        organization: 'testorg',
      });

      let existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(existingOrganization.name);

      const { user } = await getUserData();
      const expectedUserId = adminUser.id + 2;

      const response = await appInstance
        .post(`/users`)
        .send({ ...user, organization: existingOrganization.name })
        .expect(201);

      const responseUser = response.body.user;

      expect(responseUser).toMatchObject(
        Formatter.userModelToResponse(
          { ...user, id: expectedUserId },
          existingOrganization,
        ),
      );

      existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(existingOrganization.name);
    });

    it('returns 403 for unauthorized user', async () => {
      const { rawCredentials: nonAdminUserCredentials } = await userFactory();
      const { user } = await getUserData();

      await appInstance.sessionLogin(nonAdminUserCredentials);
      await appInstance.post(`/users`).send(user).expect(403);
    });

    it('returns 400 when missing e-mail', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, email: undefined })
        .expect(400);
    });

    it('returns 400 when missing username', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, username: undefined })
        .expect(400);
    });

    it('returns 400 when using empty username', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, username: '' })
        .expect(400);
    });

    it('returns 400 when using empty email', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, email: '' })
        .expect(400);
    });

    it('returns 400 when using invalid e-mail', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, email: 'invalid-email' })
        .expect(400);
    });

    it('returns 400 when using existing e-mail', async () => {
      const { user } = await getUserData();
      await appInstance
        .post(`/users`)
        .send({ ...user, email: adminUser.email })
        .expect(400);
    });
  });

  describe('Update user', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('updates user', async () => {
      const { user } = await getUserData();
      delete user.email; // user can not update email without password confirmation

      const response = await appInstance
        .patch(`/users/${adminUser.id}`)
        .send(user)
        .expect(200);

      const updatedUser = response.body.user;

      expect(updatedUser).toMatchObject(
        Formatter.userModelToResponse({
          ...user,
          id: adminUser.id,
          email: adminUser.email,
        }),
      );
    });

    it('updates user with new organization', async () => {
      let existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(0);

      const { user } = await getUserData();
      const organization = await getOrganizationData();
      delete user.email; // user can not update email without password confirmation

      const response = await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ ...user, organization: organization.name })
        .expect(200);

      const updatedUser = response.body.user;

      expect(updatedUser).toMatchObject(
        Formatter.userModelToResponse(
          {
            ...user,
            id: adminUser.id,
            email: adminUser.email,
          },
          organization,
        ),
      );

      existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(organization.name);
    });

    it('updates user with existing organization', async () => {
      const { organization: existingOrganization } = await userFactory({
        organization: 'testorg',
      });

      let existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(existingOrganization.name);

      const { user } = await getUserData();
      delete user.email; // user can not update email without password confirmation

      const response = await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ ...user, organization: existingOrganization.name })
        .expect(200);

      const updatedUser = response.body.user;
      expect(updatedUser).toMatchObject(
        Formatter.userModelToResponse(
          {
            ...user,
            id: adminUser.id,
            email: adminUser.email,
          },
          existingOrganization,
        ),
      );

      existingOrgs = await Organization.findAll();
      expect(existingOrgs.length).toBe(1);
      expect(existingOrgs[0].name).toBe(existingOrganization.name);
    });

    it('updates user password', async () => {
      const { credential } = await getUserData();

      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({
          newPassword: credential.password,
          oldPassword: adminUserCredentials.password,
        })
        .expect(200);
    });

    it('returns 400 on bad new password format', async () => {
      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({
          newPassword: 'asd',
          oldPassword: adminUserCredentials.password,
        })
        .expect(400);
    });

    it('returns 403 on unauthorized password update', async () => {
      const { credential } = await getUserData();

      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({
          newPassword: credential.password,
          oldPassword: 'incorrect',
        })
        .expect(403);

      const { user: nonAdminUser, rawCredentials: nonAdminUserCredentials } =
        await userFactory();

      // event admin can not change password for another user directly
      await appInstance
        .patch(`/users/${nonAdminUser.id}`)
        .send({
          newPassword: credential.password,
          oldPassword: nonAdminUserCredentials.password,
        })
        .expect(403);
    });

    it('returns 404 when user is not found', async () => {
      const { user } = await getUserData();
      const nonexistingUserId = 101;

      await appInstance
        .patch(`/users/${nonexistingUserId}`)
        .send(user)
        .expect(404);
    });

    it('returns 400 when userId is not valid', async () => {
      const { user } = await getUserData();

      await appInstance.patch(`/users/abc`).send(user).expect(400);
      await appInstance.patch(`/users/@1`).send(user).expect(400);
    });

    it('returns 403 for unauthorized user update', async () => {
      const { rawCredentials: nonAdminUserCredentials } = await userFactory();
      const { user } = await getUserData();

      await appInstance.sessionLogin(nonAdminUserCredentials);
      await appInstance.patch(`/users/${adminUser.id}`).send(user).expect(403);
    });

    it('returns 400 when using invalid e-mail', async () => {
      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ email: 'invalid-email' })
        .expect(400);
    });

    it('returns 400 when using existing e-mail', async () => {
      const { user } = await userFactory();

      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ email: user.email })
        .expect(400);
    });

    it('returns 400 when using empty email', async () => {
      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ email: '' })
        .expect(400);
    });

    it('returns 400 when using empty username', async () => {
      await appInstance
        .patch(`/users/${adminUser.id}`)
        .send({ email: '' })
        .expect(400);
    });
  });

  describe('Delete user', () => {
    beforeEach(async () => appInstance.sessionLogin(adminUserCredentials));

    it('deletes user', async () => {
      const { user } = await userFactory();

      await appInstance.delete(`/users/${user.id}`).expect(200);

      const deletedUser = await User.findByPk(user.id);
      expect(deletedUser).toBe(null);
    });

    it('returns 403 for unauthorized user delete', async () => {
      const { rawCredentials: nonAdminUserCredentials } = await userFactory();

      await appInstance.sessionLogin(nonAdminUserCredentials);
      await appInstance.delete(`/users/${adminUser.id}`).expect(403);
    });

    it('returns 404 when user is not found', async () => {
      const nonexistingUserId = 101;

      await appInstance.delete(`/users/${nonexistingUserId}`).expect(404);
    });

    it('returns 400 when userId is not valid', async () => {
      await appInstance.delete(`/users/abc`).expect(400);
      await appInstance.delete(`/users/@1`).expect(400);
    });
  });

  describe('Request password reset', () => {
    it('creates user token on successful request', async () => {
      let existingTokens = await UserToken.findAll();
      expect(existingTokens.length).toBe(0);

      const data = {
        email: adminUser.email,
        subject: 'test',
        body: 'test',
        type: 'create',
      };

      await appInstance.post(`/users/password-reset`).send(data).expect(200);

      existingTokens = await UserToken.findAll();
      expect(existingTokens.length).toBe(1);
      expect(existingTokens[0].userId).toBe(adminUser.id);
    });

    it('returns 200 when user is not found to hide private info', async () => {
      const nonexistingUserEmail = 'franta@pepa.cz';

      await appInstance
        .post(`/users/password-reset`)
        .send({
          email: nonexistingUserEmail,
          subject: 'test',
          body: 'test',
          type: 'create',
        })
        .expect(200);
    });

    it('returns 400 when sending invalid e-mail', async () => {
      const invalidEmail = 'invalidemail';

      await appInstance
        .post(`/users/password-reset`)
        .send({ email: invalidEmail, subject: 'test', body: 'test' })
        .expect(400);

      await appInstance.post(`/users/password-reset`).expect(400);
    });

    it('returns 400 when not sending mandatory params', async () => {
      await appInstance
        .post(`/users/password-reset`)
        .send({ subject: 'test', body: 'test' })
        .expect(400);

      await appInstance.post(`/users/password-reset`).expect(400);

      await appInstance
        .post(`/users/password-reset`)
        .send({ email: adminUser.email, body: 'test' })
        .expect(400);

      await appInstance.post(`/users/password-reset`).expect(400);

      await appInstance
        .post(`/users/password-reset`)
        .send({ email: adminUser.email, subject: 'test' })
        .expect(400);

      await appInstance.post(`/users/password-reset`).expect(400);
    });
  });

  describe('Reset password', () => {
    beforeEach(async () => {
      await appInstance
        .post(`/users/password-reset`)
        .send({
          email: adminUser.email,
          subject: 'test',
          body: 'test',
          type: 'reset',
        })
        .expect(200);

      ({ token: adminUserToken } = await UserToken.findOne({
        where: { userId: adminUser.id },
      }));
    });

    it('resets password on successful request', async () => {
      const { credential } = await getUserData();

      await appInstance
        .post(`/users/${adminUser.id}/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email, password: credential.password })
        .expect(200);
    });

    it('returns 404 when user is not found', async () => {
      const { credential } = await getUserData();
      const nonexistingUserId = 101;
      const nonexistingUserEmail = 'franta@pepa.cz';

      await appInstance
        .post(`/users/${nonexistingUserId}/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email, password: credential.password })
        .expect(404);

      await appInstance
        .post(`/users/${adminUser.id}/password-reset/${adminUserToken}`)
        .send({ email: nonexistingUserEmail, password: credential.password })
        .expect(404);
    });

    it('returns 400 when sending invalid e-mail', async () => {
      const { credential } = await getUserData();
      const invalidEmail = 'invalidemail';

      await appInstance
        .post(`/users/${adminUser.id}/password-reset/${adminUserToken}`)
        .send({ email: invalidEmail, password: credential.password })
        .expect(400);

      await appInstance
        .post(`/users/${adminUser.id}/password-reset/${adminUserToken}`)
        .send({ password: credential.password })
        .expect(400);
    });

    it('returns 400 when userId is not valid', async () => {
      const { credential } = await getUserData();

      await appInstance
        .post(`/users/invalid/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email, password: credential.password })
        .expect(400);

      await appInstance
        .post(`/users/@1/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email, password: credential.password })
        .expect(400);
    });

    it('returns 400 when new password is not valid', async () => {
      const invalidPassword = 'asd';

      await appInstance
        .post(`/users/invalid/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email, password: invalidPassword })
        .expect(400);

      await appInstance
        .post(`/users/@1/password-reset/${adminUserToken}`)
        .send({ email: adminUser.email })
        .expect(400);
    });

    it('returns 403 when using bad token', async () => {
      const { credential } = await getUserData();
      const badToken = 'bigbadtoken';

      await appInstance
        .post(`/users/${adminUser.id}/password-reset/${badToken}`)
        .send({ email: adminUser.email, password: credential.password })
        .expect(403);
    });
  });
});
