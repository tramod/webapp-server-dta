import { beforeEach, expect, it } from '@jest/globals';
import {
  getAppInstance,
  truncateDB,
  waitForServer,
} from './helpers/general.js';
import { userFactory, trafficModelFactory } from '../../db/factories/index.js';
import { resetModelItemsCache } from '../../app/services/modelAPI.js';
import Formatter from './formatters.js';
import {
  resetAxiosRequestHistory,
  mockPostTrafficJob,
  mockGetTrafficJobStatus,
  mockDeleteTrafficCache,
  mockLoadModel,
  // axiosMock,
} from './helpers/mockAxios.js';

let appInstance;

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
});
afterEach(() => {
  resetModelItemsCache();
});

describe('TrafficModels', () => {
  describe('GetAll', () => {
    it('Returns all traffic models', async () => {
      const { trafficModel: modelDTA } = await trafficModelFactory({
        type: 'DTA',
      });

      const response = await appInstance.get('/models');
      expect(response.body.trafficModels).toEqual([
        Formatter.trafficModelModelToResponse(modelDTA),
      ]);
    });

    it('Fails when no traffic models are found', async () => {
      await appInstance.get('/models').expect(404);
    });
  });

  describe('GetModel', () => {
    it('Returns correct traffic model based on param', async () => {
      const { trafficModel: modelDTA } = await trafficModelFactory({
        type: 'DTA',
      });

      const response = await appInstance.get('/models/DTA');
      expect(response.body.trafficModel).toEqual(
        Formatter.trafficModelModelToResponse(modelDTA),
      );
    });

    it('Fails when no traffic models are found', async () => {
      await appInstance.get('/models/DTA').expect(404);
    });

    it('Fails when model type is not valid', async () => {
      await trafficModelFactory({
        type: 'DTA',
      });

      await appInstance.get('/models/1').expect(400);
      await appInstance.get('/models/INVALID').expect(400);
      await appInstance.get('/models/ata').expect(400);
    });
  });

  describe('Refresh model', () => {
    let adminCredentials;

    beforeEach(async () => {
      ({ rawCredentials: adminCredentials } = await userFactory(
        { roles: [100] },
        { withOrganization: false },
      ));
      await appInstance.sessionLogin(adminCredentials);
    });

    afterEach(() => {
      resetAxiosRequestHistory();
    });

    describe('Failures', () => {
      it('Fail when traffic model is not found', async () => {
        await appInstance
          .post('/models/refresh')
          .send({ modelType: 'DTA' })
          .expect(404);
      });

      it('Fails when model type is not valid', async () => {
        await trafficModelFactory({
          type: 'DTA',
        });

        await appInstance
          .post('/models/refresh')
          .send({ modelType: 1 })
          .expect(400);
        await appInstance
          .post('/models/refresh')
          .send({ modelType: 'INVALID' })
          .expect(400);
        await appInstance
          .post('/models/refresh')
          .send({ modelType: 'ata' })
          .expect(400);
        await appInstance.post('/models/refresh').send({}).expect(400);
      });

      it('Fails without login or with non-admin user', async () => {
        const { rawCredentials: nonAdminCredentials } = await userFactory(
          {},
          { withOrganization: false },
        );
        await trafficModelFactory({
          type: 'DTA',
        });

        await appInstance.sessionLogout();
        await appInstance
          .post('/models/refresh')
          .send({ modelType: 'DTA' })
          .expect(401);

        await appInstance.sessionLogin(nonAdminCredentials);
        await appInstance
          .post('/models/refresh')
          .send({ modelType: 'DTA' })
          .expect(403);
      });
    });

    describe('Refresh Flows', () => {
      beforeEach(() => {
        mockGetTrafficJobStatus();
        mockPostTrafficJob();
        mockDeleteTrafficCache();
        mockLoadModel();
      });

      it('Marks traffic model as invalid when successfully called', async () => {
        await trafficModelFactory({
          type: 'DTA',
        });
        const response = await appInstance
          .post('/models/refresh')
          .send({ modelType: 'DTA' });

        expect(response.body.status).toBe(true);
        expect(response.body.trafficModel.isValid).toBe(false);
        expect(response.body.trafficModel.timestamp).toBe(null);
      });

      it('Marks traffic model as valid when base model recalculated (no scenarios)', async () => {
        mockGetTrafficJobStatus({ response: { status: 'RUNNING' } });
        const { trafficModel } = await trafficModelFactory({
          type: 'DTA',
        });
        const oldTimestamp = trafficModel.timestamp;
        await appInstance.post('/models/refresh').send({ modelType: 'DTA' });

        const notReadyResponse = await appInstance.get('/models/DTA');
        expect(notReadyResponse.body.trafficModel.isValid).toBe(false);
        expect(notReadyResponse.body.trafficModel.isBroken).toBe(false);
        expect(notReadyResponse.body.trafficModel.timestamp).toBe(null);

        mockGetTrafficJobStatus();
        await waitForServer(2500);
        const refreshedResponse = await appInstance.get('/models/DTA');
        expect(refreshedResponse.body.trafficModel.isValid).toBe(true);
        expect(refreshedResponse.body.trafficModel.isBroken).toBe(false);
        const newTimestamp = refreshedResponse.body.trafficModel.timestamp;
        expect(newTimestamp).not.toBe(null);
        expect(newTimestamp).not.toBe(oldTimestamp);
        const timestampDiff = new Date(newTimestamp) - new Date(oldTimestamp);
        expect(timestampDiff > 0).toBe(true);
      });

      it('Marks traffic model as invalid if base model calculation fails', async () => {
        mockGetTrafficJobStatus({ response: { status: 'ERROR' } });
        await trafficModelFactory({
          type: 'DTA',
        });

        await appInstance.post('/models/refresh').send({ modelType: 'DTA' });
        await waitForServer(200);
        const brokenResponse = await appInstance.get('/models/DTA');
        expect(brokenResponse.body.trafficModel.isValid).toBe(false);
        expect(brokenResponse.body.trafficModel.isBroken).toBe(true);
      });

      // TODO Add more tests
      // Recalculation of scenarios model sections
      // Caches cleaning
      // TrafficModel refreshes blockers - editSession start/scenario copy
      // TrafficModel appended to some requests
    });
  });
});
