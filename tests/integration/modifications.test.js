import { beforeAll, describe, it } from '@jest/globals';

import { getAppInstance, truncateDB } from './helpers/general.js';
import { invalidParamsTestUtil } from './helpers/utils.js';
import { reuseTypeMode, reuseModData } from './helpers/modifications.js';
import {
  userFactory,
  scenarioFactory,
  eventFactory,
  modificationFactory,
  getModificationData,
} from '../../db/factories/index.js';
import { Modification, Event } from '../../db/index.js';
import Formatter from './formatters.js';
import config, { setConfig } from '../../config/env.js';

let appInstance;
let baseUser;
let baseUserCredentials;
let baseScenario;
let baseEvents;
let baseEvent;

const editModeDisableSetting = config.TM_EDIT_MODE_DISABLE;
beforeAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', true);
});
afterAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', editModeDisableSetting);
});

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: baseUser, rawCredentials: baseUserCredentials } =
    await userFactory());
  ({ scenario: baseScenario, events: baseEvents } = await scenarioFactory(
    {
      authorUserId: baseUser.id,
      events: [{}],
      modelType: 'DTA',
    },
    { withTrafficModel: true },
  ));
  [baseEvent] = baseEvents;
});

const getShortUrl = (scenario, event) =>
  `/scenarios/${scenario?.itemId ?? scenario}/events/${
    event?.itemId ?? event
  }/modifications`;
const getLongUrl = (scenario, event, modification) =>
  `${getShortUrl(scenario, event)}/${modification?.itemId ?? modification}`;

const VALIDATION_CASES = {
  PARAMS: {
    SHORT: [
      ['ScenarioId1', ['abc', 1], ['scenarioId']],
      ['ScenarioId2', ['@1', 1], ['scenarioId']],
      ['ScenarioId3', [1.1, 1], ['scenarioId']],
      ['EventId1', [1, 'abc'], ['evtId']],
      ['EventId2', [1, '@1'], ['evtId']],
      ['EventId3', [1, 1.1], ['evtId']],
    ],
    LONG: [
      ['ScenarioId1', ['abc', 1, 1], ['scenarioId']],
      ['ScenarioId2', ['@1', 1, 1], ['scenarioId']],
      ['ScenarioId3', [1.1, 1, 1], ['scenarioId']],
      ['EventId1', [1, 'abc', 1], ['evtId']],
      ['EventId2', [1, '@1', 1], ['evtId']],
      ['EventId3', [1, 1.1, 1], ['evtId']],
      ['ModificationId1', [1, 1, 'abc'], ['modId']],
      ['ModificationId2', [1, 1, 'link'], ['modId']],
      ['ModificationId3', [1, 1, 1.1], ['modId']],
    ],
  },
  BODY: {
    COMMON: [
      [
        'Invalid body props1',
        {
          name: 123,
          dateFrom: '12-1-1999',
          dateTo: '12/1/1999',
          type: 'link',
          mode: 'new',
        },
        ['name', 'dateFrom', 'dateTo'],
      ],
    ],
    LINK: [
      [
        'Invalid body props1',
        { linkId: 'abc', speed: 'fifty', capacity: 'one' },
        ['linkId', 'speed', 'capacity'],
      ],
      [
        'Invalid body props2',
        { linkId: ['abc'], speed: 'fifty', capacity: 'one' },
        ['linkId', 'speed', 'capacity'],
      ],
      [
        'Invalid body props3',
        { linkId: 1, speed: -5, capacity: -500 },
        ['linkId', 'speed', 'capacity'],
      ],
      [
        'Invalid body props4',
        { linkId: [-1], capacity: 500.5, speed: 50 },
        ['linkId', 'capacity'],
      ],
      [
        'Invalid body props5',
        { linkId: [null], capacity: 500, speed: 50 },
        ['linkId'],
      ],
      [
        'Invalid body props6',
        { linkId: ['extra-123-b'], capacity: 500.5, speed: 50 },
        ['capacity'],
      ],
      [
        'Invalid body props7',
        { linkId: ['extra-a'], capacity: 500, speed: 50 },
        ['linkId'],
      ],
      [
        'Invalid body props8',
        {
          linkId: ['extra-1', 'extra-1-b', 'extra-1-c'],
          capacity: 500,
          speed: 50,
        },
        ['linkId'],
      ],
      [
        'Invalid body props9',
        { linkId: ['extra-1'], capacity: 500.5, speed: 50 },
        ['capacity'],
      ],
    ],
  },
};

describe('Modifications', () => {
  describe('GetModificationsFromEvent', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns modifications from scenario', async () => {
      const MOD_COUNT = 3;
      await modificationFactory({}, { attachedEventModel: baseEvent });
      await modificationFactory({}, { attachedEventModel: baseEvent });
      await modificationFactory({}, { attachedEventModel: baseEvent });
      const response = await appInstance
        .get(getShortUrl(baseScenario, baseEvent))
        .expect(200);
      expect(response.body.modifications.length).toBe(MOD_COUNT);
    });

    it('Fails when scenario/event does not exist or has no modifications', async () => {
      const SCENARIO_NOT_EXISTS = { itemId: 1000 };
      await appInstance
        .get(getShortUrl(SCENARIO_NOT_EXISTS, baseEvent))
        .expect(404);

      const EVENT_NOT_EXISTS = { itemId: 1000 };
      await appInstance
        .get(getShortUrl(baseScenario, EVENT_NOT_EXISTS))
        .expect(404);

      const { scenario: SCENARIO_EVENT_WITHOUT_MODS, events } =
        await scenarioFactory(
          {
            authorUserId: baseUser.id,
            events: [{}],
            modelType: 'DTA',
          },
          { withTrafficModel: true },
        );
      await appInstance
        .get(getShortUrl(SCENARIO_EVENT_WITHOUT_MODS, events[0]))
        .expect(404);
    });

    it('Return modifications in expected format', async () => {
      const linkMod = await modificationFactory(
        { type: 'link' },
        { attachedEventModel: baseEvent },
      );
      const nodeMod = await modificationFactory(
        { type: 'node' },
        { attachedEventModel: baseEvent },
      );
      const response = await appInstance
        .get(getShortUrl(baseScenario, baseEvent))
        .expect(200);

      const respMods = response.body.modifications;
      const expectedLinkMod = Formatter.modificationModelToResponse(linkMod);
      const returnedLinkMode = respMods.find((item) => item.type === 'link');
      expect(returnedLinkMode).toMatchObject(expectedLinkMod);

      const expectedNodeMod = Formatter.modificationModelToResponse(nodeMod);
      const returnedNodeMode = respMods.find((item) => item.type === 'node');
      expect(returnedNodeMode).toMatchObject(expectedNodeMod);
    });

    it('Fails with invalid input', async () => {
      const cases = VALIDATION_CASES.PARAMS.SHORT;
      const getUrlFn = (params) => getShortUrl(...params);
      await invalidParamsTestUtil({ appInstance, cases, getUrlFn });
    });
  });

  describe('GetModificationById', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Return link modification from scenario/event', async () => {
      const mod = await modificationFactory(
        { type: 'link' },
        { attachedEventModel: baseEvent },
      );
      const response = await appInstance
        .get(getLongUrl(baseScenario, baseEvent, mod))
        .expect(200);

      const expectedMod = Formatter.modificationModelToResponse(mod);
      const responseResource = response.body.modification;
      expect(responseResource).toMatchObject(expectedMod);
      expect(responseResource).not.toHaveProperty('rowId');
      expect(responseResource).not.toHaveProperty('itemId');
      expect(responseResource).not.toHaveProperty('eventRowId');
      expect(responseResource).toHaveProperty('createdAt');
      expect(responseResource).toHaveProperty('updatedAt');
    });

    it('Return node modification from scenario/event', async () => {
      const mod = await modificationFactory(
        { type: 'node' },
        { attachedEventModel: baseEvent },
      );
      const response = await appInstance
        .get(getLongUrl(baseScenario, baseEvent, mod))
        .expect(200);

      const expectedMod = Formatter.modificationModelToResponse(mod);
      const responseResource = response.body.modification;
      expect(responseResource).toMatchObject(expectedMod);
      expect(responseResource).not.toHaveProperty('rowId');
      expect(responseResource).not.toHaveProperty('itemId');
      expect(responseResource).not.toHaveProperty('eventRowId');
      expect(responseResource).toHaveProperty('createdAt');
      expect(responseResource).toHaveProperty('updatedAt');
    });

    it('Fails with NotFound Error: scenario/event/modification', async () => {
      const mod = await modificationFactory(
        {},
        {
          attachedEventModel: baseEvent,
        },
      );
      const NOT_ITEM = { itemId: 1000 };
      await appInstance.get(getLongUrl(NOT_ITEM, baseEvent, mod)).expect(404);
      await appInstance
        .get(getLongUrl(baseScenario, NOT_ITEM, mod))
        .expect(404);
      await appInstance
        .get(getLongUrl(baseScenario, baseEvent, NOT_ITEM))
        .expect(404);
    });

    it('Fails with NotFound Error: modification is not included in event', async () => {
      const { modifications } = await eventFactory(
        { authorUserId: baseUser.id, modifications: [{}] },
        { attachedScenarioModel: baseScenario },
      );
      const [mod] = modifications;
      await appInstance
        .get(getLongUrl(baseScenario, baseEvent, mod))
        .expect(404);
    });

    it('Fails with invalid input', async () => {
      const cases = VALIDATION_CASES.PARAMS.LONG;
      const getUrlFn = (params) => getLongUrl(...params);
      await invalidParamsTestUtil({ appInstance, cases, getUrlFn });
    });
  });

  describe('CreateModification - common', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Creates modification instance and assign id', async () => {
      const MODS_IN_DB = 3;
      await modificationFactory({}, { attachedEventModel: baseEvent });
      await modificationFactory({}, { attachedEventModel: baseEvent });
      await modificationFactory({}, { attachedEventModel: baseEvent });
      const EXPECTED_ID = MODS_IN_DB + 1;
      const modification = await getModificationData();

      const response = await appInstance
        .post(getShortUrl(baseScenario, baseEvent))
        .send(modification)
        .expect(201);
      expect(response.body.modification.id).toEqual(EXPECTED_ID);
      const expectedModification = Formatter.modificationModelToResponse({
        ...modification,
        name: modification.name || `${modification.type}-${EXPECTED_ID}`,
      });
      expect(response.body.modification).toMatchObject(expectedModification);
    });

    it('Fails with invalid body content', async () => {
      const url = getShortUrl(baseScenario, baseEvent);
      const cases = [...VALIDATION_CASES.BODY.COMMON];

      const modification = await getModificationData();
      const getBodyFn = (body) => ({
        ...modification,
        ...body,
      });

      await invalidParamsTestUtil({
        appInstance,
        cases,
        url,
        methodName: 'post',
        getBodyFn,
      });
    });

    it('Fails with NotFound Error: scenario/event', async () => {
      const modification = await getModificationData();
      const NOT_ITEM = { itemId: 1000 };

      await appInstance
        .post(getShortUrl(NOT_ITEM, baseEvent))
        .send(modification)
        .expect(404);
      await appInstance
        .post(getShortUrl(baseScenario, NOT_ITEM))
        .send(modification)
        .expect(404);
    });

    it('Fails with invalid params', async () => {
      const cases = VALIDATION_CASES.PARAMS.SHORT;
      const modification = await getModificationData();
      const getUrlFn = (params) => getShortUrl(...params);

      await invalidParamsTestUtil({
        appInstance,
        cases,
        getUrlFn,
        methodName: 'post',
        body: modification,
      });
    });

    it('Fails due to dependency error', async () => {
      const dependingMod = await modificationFactory(
        {
          type: 'link',
          mode: 'new',
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
        },
        { attachedEventModel: baseEvent },
      );

      const newMod = await getModificationData({
        type: 'link',
        mode: 'existing',
        dateFrom: '2021-01-11T00:00:00.731Z',
        dateTo: '2021-06-11T00:00:00.731Z',
      });

      await appInstance
        .post(getShortUrl(baseScenario, baseEvent))
        .send({ ...newMod, linkId: [`extra-${dependingMod.itemId}`] })
        .expect(400);
    });
  });

  describe('CreateModification - link-existing', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Fails with invalid body content', async () => {
      const url = getShortUrl(baseScenario, baseEvent);
      const cases = [
        ['Incomplete body', { speed: null, capacity: null, linkId: null }],
        [
          'All calc values nullish',
          { speed: null, capacity: null, linkId: [1] },
        ],
        ...VALIDATION_CASES.BODY.LINK,
      ];

      const modification = await getModificationData({
        type: 'link',
        mode: 'existing',
      });
      const getBodyFn = (body) => ({ ...modification, ...body });

      await invalidParamsTestUtil({
        appInstance,
        cases,
        url,
        methodName: 'post',
        getBodyFn,
      });
    });
  });

  describe('UpdateModification common', () => {
    let modification;
    beforeEach(async () => {
      await appInstance.sessionLogin(baseUserCredentials);
      modification = await modificationFactory(
        {},
        { attachedEventModel: baseEvent },
      );
    });

    it('Updates existing modification instance', async () => {
      const updateData = await getModificationData({
        ...reuseTypeMode(modification),
        name: 'umpalumpa',
      });
      const MOD_ID = modification.itemId;

      const updated = await appInstance
        .patch(getLongUrl(baseScenario, baseEvent, modification))
        .send(updateData)
        .expect(200);

      expect(updated.body.modification.id).toEqual(MOD_ID);
      expect(updated.body.modification).toMatchObject(
        Formatter.modificationModelToResponse(updateData),
      );
    });

    it('Fails with invalid params', async () => {
      const cases = VALIDATION_CASES.PARAMS.LONG;
      const updateData = await getModificationData(reuseTypeMode(modification));
      const getUrlFn = (params) => getLongUrl(...params);

      await invalidParamsTestUtil({
        appInstance,
        cases,
        getUrlFn,
        methodName: 'patch',
        body: updateData,
      });
    });

    it('Fails with NotFound Error: scenario/event/modification', async () => {
      const updateData = await getModificationData(reuseTypeMode(modification));
      const NOT_ITEM = { itemId: 1000 };
      await appInstance
        .patch(getLongUrl(NOT_ITEM, baseEvent, modification))
        .send(updateData)
        .expect(404);
      await appInstance
        .patch(getLongUrl(baseScenario, NOT_ITEM, modification))
        .send(updateData)
        .expect(404);
      await appInstance
        .patch(getLongUrl(baseScenario, baseEvent, NOT_ITEM))
        .send(updateData)
        .expect(404);
    });

    it('Fails with NotFound Error: modification is not included in event', async () => {
      // ! same modification itemId can be used in multiple events currently
      const { modifications } = await eventFactory(
        {
          authorUserId: baseUser.id,
          modifications: [{}],
        },
        { attachedScenarioModel: baseScenario },
      );
      const [otherModification] = modifications;

      const updateData = await getModificationData(reuseTypeMode(modification));
      await appInstance
        .patch(getLongUrl(baseScenario, baseEvent, otherModification))
        .send(updateData)
        .expect(404);
    });

    it('Fails with invalid body content', async () => {
      const url = getLongUrl(baseScenario, baseEvent, modification);
      const cases = [
        ['Cannot change type - same as nothing', { type: 'node' }],
        ...VALIDATION_CASES.BODY.COMMON,
      ];

      const getBodyFn = (body) => body;

      await invalidParamsTestUtil({
        appInstance,
        cases,
        url,
        methodName: 'patch',
        getBodyFn,
      });
    });

    it('Fails due to dependency error', async () => {
      const dependingMod = await modificationFactory(
        {
          type: 'link',
          mode: 'new',
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
        },
        { attachedEventModel: baseEvent },
      );
      const dependentMod = await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom: '2021-01-11T00:00:00.731Z',
          dateTo: '2021-05-11T00:00:00.731Z',
        },
        { attachedEventModel: baseEvent },
      );

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      const updatedData = {
        ...reuseModData(dependentMod),
        dateFrom: null,
        dateTo: null,
      };

      const response = await appInstance
        .patch(getLongUrl(baseScenario, baseEvent, dependentMod))
        .send(updatedData)
        .expect(400);

      expect(response.body.code).toEqual('DEPENDENCIES_ERROR');
      const formatedMod = Formatter.modificationModelToResponse(dependingMod);
      expect(response.body.data).toMatchObject([
        {
          dateFrom: formatedMod.dateFrom,
          dateTo: formatedMod.dateTo,
          dependencies: [],
          dependency: 'parent',
          id: dependingMod.itemId,
        },
      ]);
    });
  });

  describe('UpdateModification - link-existing', () => {
    let modification;
    beforeEach(async () => {
      await appInstance.sessionLogin(baseUserCredentials);
      modification = await modificationFactory(
        { type: 'link', mode: 'existing' },
        { attachedEventModel: baseEvent },
      );
    });

    it('Fails with invalid body content', async () => {
      const url = getLongUrl(baseScenario, baseEvent, modification);
      const cases = [...VALIDATION_CASES.BODY.LINK];

      const getBodyFn = (body) => ({ ...reuseModData(modification), ...body });

      await invalidParamsTestUtil({
        appInstance,
        cases,
        url,
        methodName: 'patch',
        getBodyFn,
      });
    });
  });

  describe('DeleteModification', () => {
    let modification;
    beforeEach(async () => {
      await appInstance.sessionLogin(baseUserCredentials);
      modification = await modificationFactory(
        {},
        {
          attachedEventModel: baseEvent,
        },
      );
    });

    it('Delete provided modification and returns success status', async () => {
      // Before
      const eventModificationBefore = await baseEvent.getModifications();
      expect(eventModificationBefore.length).not.toBe(0);

      await appInstance
        .delete(getLongUrl(baseScenario, baseEvent, modification))
        .expect(200);

      // Removed from model
      const eventModificationAfter = await baseEvent.getModifications();
      expect(eventModificationAfter.length).toBe(0);
      await appInstance.get(getShortUrl(baseScenario, baseEvent)).expect(404);
    });

    it('Tries to updated parent event updatedAt date after deleting modification', async () => {
      await appInstance
        .delete(getLongUrl(baseScenario, baseEvent, modification))
        .expect(200);

      const updatedEvent = await Event.findByPk(baseEvent.rowId);

      // expect event updatedAt date to be updated
      expect(updatedEvent.updatedAt > baseEvent.updatedAt).toBeTruthy();
    });

    it('Fails with NotFound Error: scenario/event/modification', async () => {
      const NOT_ITEM = { itemId: 1000 };
      await appInstance
        .delete(getLongUrl(NOT_ITEM, baseEvent, modification))
        .expect(404);
      await appInstance
        .delete(getLongUrl(baseScenario, NOT_ITEM, modification))
        .expect(404);
      await appInstance
        .delete(getLongUrl(baseScenario, baseEvent, NOT_ITEM))
        .expect(404);
    });

    it('Fails with invalid params', async () => {
      const cases = VALIDATION_CASES.PARAMS.LONG;
      const getUrlFn = (params) => getLongUrl(...params);

      await invalidParamsTestUtil({
        appInstance,
        cases,
        getUrlFn,
        methodName: 'delete',
      });
    });

    it('Fails due to dependency error', async () => {
      const dependingMod = await modificationFactory(
        { type: 'link', mode: 'new' },
        { attachedEventModel: baseEvent },
      );
      const dependentMod = await modificationFactory(
        { type: 'link', mode: 'existing' },
        { attachedEventModel: baseEvent },
      );

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      await appInstance
        .delete(getLongUrl(baseScenario, baseEvent, dependingMod))
        .expect(400);
    });

    it('Deletes mod with its dependencies', async () => {
      const dependingMod = await modificationFactory(
        { type: 'link', mode: 'new' },
        { attachedEventModel: baseEvent },
      );
      const dependentMod = await modificationFactory(
        { type: 'link', mode: 'existing' },
        { attachedEventModel: baseEvent },
      );

      await dependentMod.update({
        data: {
          ...dependentMod.data,
          linkId: [`extra-${dependingMod.itemId}`],
        },
      });

      await appInstance
        .delete(
          `${getLongUrl(
            baseScenario,
            baseEvent,
            dependingMod,
          )}?deleteDependencies=true`,
        )
        .expect(200);

      expect(await Modification.findByPk(dependingMod.rowId)).toBe(null);
      expect(await Modification.findByPk(dependentMod.rowId)).toBe(null);
    });
  });

  describe('Authorization', () => {
    describe('Foreign scenario', () => {
      let modification;
      let otherUserCredentials;
      const EXPECTED_REJECT_STATUS = 403; // After code change, resource is found but our access in not authorized
      beforeEach(async () => {
        modification = await modificationFactory(
          {},
          { attachedEventModel: baseEvent },
        );

        ({ rawCredentials: otherUserCredentials } = await userFactory());
        await appInstance.sessionLogin(otherUserCredentials);
      });

      it('Cannot create modification without update:event access', async () => {
        const createData = await getModificationData();
        await appInstance
          .post(getShortUrl(baseScenario, baseEvent))
          .send(createData)
          .expect(EXPECTED_REJECT_STATUS);
      });

      it('Cannot update modification without update:event access', async () => {
        const updateData = await getModificationData(
          reuseTypeMode(modification),
        );
        await appInstance
          .patch(getLongUrl(baseScenario, baseEvent, modification))
          .send(updateData)
          .expect(EXPECTED_REJECT_STATUS);
      });

      it('Cannot delete modification without update:event access', async () => {
        await appInstance
          .delete(getLongUrl(baseScenario, baseEvent, modification))
          .expect(EXPECTED_REJECT_STATUS);
      });
    });

    describe('Low access level, eg. inserter with non-nowned event', () => {
      let modification;
      let otherUser;
      let otherUserCredentials;
      let localScenario;
      let localEvents;
      let localEvent;
      const EXPECTED_REJECT_STATUS = 403; // Will found event, but without enough access

      beforeEach(async () => {
        appInstance = getAppInstance();
        ({ user: otherUser, rawCredentials: otherUserCredentials } =
          await userFactory());
        ({ scenario: localScenario, events: localEvents } =
          await scenarioFactory(
            {
              authorUserId: baseUser.id,
              events: [{ authorUserId: baseUser.id }],
              scenarioAccesses: [{ userId: otherUser.id, level: 'inserter' }],
              modelType: 'DTA',
            },
            { withTrafficModel: true },
          ));
        [localEvent] = localEvents;

        modification = await modificationFactory(
          { authorUserId: baseUser.id },
          { attachedEventModel: localEvent },
        );

        await appInstance.sessionLogin(otherUserCredentials);
      });

      it('Cannot create modification without update:event access', async () => {
        const createData = await getModificationData();
        await appInstance
          .post(getShortUrl(localScenario, localEvent))
          .send(createData)
          .expect(EXPECTED_REJECT_STATUS);
      });

      it('Cannot update modification without update:event access', async () => {
        const updateData = await getModificationData(
          reuseTypeMode(modification),
        );
        await appInstance
          .patch(getLongUrl(localScenario, localEvent, modification))
          .send(updateData)
          .expect(EXPECTED_REJECT_STATUS);
      });

      it('Cannot delete modification without update:event access', async () => {
        await appInstance
          .delete(getLongUrl(localScenario, localEvent, modification))
          .expect(EXPECTED_REJECT_STATUS);
      });
    });
  });

  describe('Copy modification', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns copy of the original event', async () => {
      const modification = await modificationFactory(
        { authorUserId: baseUser.id },
        { attachedEventModel: baseEvent },
      );
      const modBody = Formatter.modificationModelToResponse(modification);
      const expectedBody = {
        ...modBody,
        name: `(copy) ${modBody.name}`,
      };

      const response = await appInstance
        .post(
          `/scenarios/${baseScenario.itemId}/events/${baseEvent.itemId}/modifications/${modification.itemId}/copy`,
        )
        .expect(201);

      const responseResource = response.body.modification;

      expect(responseResource).toMatchObject(expectedBody);
    });

    it('Can not by copied by unauthorized user', async () => {
      const { rawCredentials: anotherUserCredentials } = await userFactory();
      const modification = await modificationFactory(
        { authorUserId: baseUser.id },
        { attachedEventModel: baseEvent },
      );

      await appInstance.sessionLogin(anotherUserCredentials);
      await appInstance
        .post(
          `/scenarios/${baseScenario.itemId}/events/${baseEvent.itemId}/modifications/${modification.itemId}/copy`,
        )
        .expect(403);
    });
  });
});
