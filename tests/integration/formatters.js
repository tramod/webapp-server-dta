class Formatter {
  static scenarioModelToResponse({
    id,
    itemId,
    name,
    description,
    dateFrom,
    dateTo,
    authorUserId,
  } = {}) {
    return {
      id: id ?? itemId,
      name,
      description,
      dateFrom: dateFrom !== null ? new Date(dateFrom).toISOString() : null,
      dateTo: dateTo !== null ? new Date(dateTo).toISOString() : null,
      author: authorUserId,
    };
  }

  static eventModelToResponse({
    id,
    name,
    description,
    dateFrom,
    dateTo,
    authorUserId,
    included = true,
  } = {}) {
    return {
      ...(id && { id }),
      name,
      description,
      dateFrom: dateFrom !== null ? new Date(dateFrom).toISOString() : null,
      dateTo: dateTo !== null ? new Date(dateTo).toISOString() : null,
      authorUserId,
      included,
    };
  }

  static modificationModelToResponse(modification) {
    const { id, name, dateFrom, dateTo, type, mode, description, note } =
      modification;
    return {
      ...modification.data,
      ...(id && { id }),
      name,
      description,
      note,
      type,
      mode,
      dateFrom: dateFrom !== null ? new Date(dateFrom).toISOString() : null,
      dateTo: dateTo !== null ? new Date(dateTo).toISOString() : null,
    };
  }

  static userModelToResponse(
    { id, username, email, roles } = {},
    { name } = {},
  ) {
    return {
      id,
      username,
      email,
      roles,
      organization: name || null,
    };
  }

  static trafficModelModelToResponse({
    id,
    name,
    type,
    endPoint,
    isDefault,
    isValid,
    isBroken,
    timestamp,
  } = {}) {
    return {
      id,
      name,
      type,
      isDefault,
      endPoint,
      isValid,
      isBroken,
      timestamp: new Date(timestamp).toISOString(),
    };
  }
}

export default Formatter;
