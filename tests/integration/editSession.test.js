import { beforeEach, describe, expect, it } from '@jest/globals';

import {
  getAppInstance,
  truncateDB,
  waitForServer,
} from './helpers/general.js';
import { expectNotFoundError } from './helpers/errorResponses.js';
import { routesTestUnit } from './helpers/utils.js';
import {
  restoreAxiosMock,
  resetAxiosRequestHistory,
  mockPostTrafficJob,
  mockGetTrafficJobStatus,
  mockGetModelInfo,
} from './helpers/mockAxios.js';
import { reuseTypeMode, reuseModData } from './helpers/modifications.js';
import {
  userFactory,
  scenarioFactory,
  eventFactory,
  modificationFactory,
  getEventData,
  getModificationData,
} from '../../db/factories/index.js';
import scenarioService from '../../app/services/scenarios.js';
import editSessionService from '../../app/services/editSession.js';

let appInstance;
let baseUser;
let baseUserCredentials;
let baseScenario;
let baseScenarioId;
let baseEvents;
let baseEvent;

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: baseUser, rawCredentials: baseUserCredentials } =
    await userFactory());
  await appInstance.sessionLogin(baseUserCredentials);
  ({ scenario: baseScenario, events: baseEvents } = await scenarioFactory(
    {
      authorUserId: baseUser.id,
      events: [
        { included: true, modifications: [{ type: 'node', mode: 'new' }] },
      ],
      modelType: 'DTA',
    },
    { withTrafficModel: true },
  ));
  [baseEvent] = baseEvents;
  baseScenarioId = baseScenario.itemId;
});

afterEach(() => {
  resetAxiosRequestHistory();
});

// Taken from app implementation, should by integration
async function createEditSession(source, user) {
  const session = await editSessionService.initEditSession({
    source,
    user,
  });
  return session;
}

beforeAll(() => {
  mockPostTrafficJob();
  mockGetTrafficJobStatus();
  mockGetModelInfo({ type: 'link' });
  mockGetModelInfo({ type: 'node' });
});

afterAll(() => {
  restoreAxiosMock();
});

describe('Edit session', () => {
  describe('Edit session active', () => {
    it('Scenario operations protected by edit session', async () => {
      const eventId = baseEvent.itemId;
      const modification = await modificationFactory({
        eventRowId: eventId,
      });
      const modId = modification.itemId;

      const modificationData = await getModificationData();
      const protectedRoutes = [
        {
          url: `/scenarios/${baseScenarioId}?useChanged=true`,
          methodName: 'get',
        },
        {
          url: `/scenarios/${baseScenarioId}/events?useChanged=true`,
          methodName: 'get',
        },
        {
          url: `/scenarios/${baseScenarioId}`,
          methodName: 'patch',
          data: { name: 'test' },
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}?useChanged=true`,
          methodName: 'get',
        },
        {
          url: `/scenarios/${baseScenarioId}/events`,
          methodName: 'post',
          data: { name: 'test' },
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}`,
          methodName: 'delete',
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}`,
          methodName: 'patch',
          data: { name: 'test' },
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}/modifications/${modId}?useChanged=true`,
          methodName: 'get',
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}/modifications`,
          methodName: 'post',
          data: modificationData,
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}/modifications/${modId}`,
          methodName: 'delete',
        },
        {
          url: `/scenarios/${baseScenarioId}/events/${eventId}/modifications/${modId}`,
          methodName: 'patch',
          data: { ...modificationData, name: 'test' },
        },
        {
          url: `/scenarios/${baseScenarioId}/extra-feature-modifications?useChanged=true`,
          methodName: 'get',
        },
      ];

      await routesTestUnit({
        appInstance,
        cases: protectedRoutes,
        testFn: expectNotFoundError,
        testOptions: {
          customExpectFn: (response) =>
            expect(response.body.code).toBe('EDIT_NOT_FOUND'),
        },
      });
    });
  });

  describe('Start edit session', () => {
    it('Creates new edit session', async () => {
      const { body } = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/enter`)
        .expect(200);
      expect(body.status).toBe('EDIT_ENTERED');
      expect(body.editSession.hasChange).toBe(false);
      expect(body.editSession.hasComputableChange).toBe(false);
    });

    it('Reuse users session if it is already active (although undecided)', async () => {
      await createEditSession(baseScenario, baseUser);

      const { body } = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/enter`)
        .expect(200);
      expect(body.status).toBe('EDIT_ENTERED');
      expect(body.editSession.hasChange).toBe(false);
      expect(body.editSession.hasComputableChange).toBe(false);
    });

    it('Reuse inactive undecided session', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'completed',
        userId: null,
      }); // Simulate undecided inactive state

      const { body } = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/enter`)
        .expect(200);
      expect(body.status).toBe('EDIT_ENTERED');
      expect(body.editSession.hasChange).toBe(true);
      expect(body.editSession.hasComputableChange).toBe(true);
    });

    it('Cannot start edit session when other user is editing', async () => {
      const { user: otherUser } = await userFactory();
      const { scenario: otherScenario } = await scenarioFactory(
        {
          authorUserId: otherUser.id,
          scenarioAccesses: [{ userId: baseUser.id, level: 'editor' }],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );
      const otherScenarioId = otherScenario.itemId;
      await createEditSession(otherScenario, otherUser);

      const response = await appInstance
        .post(`/scenarios/${otherScenarioId}/edit/enter`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_OCCUPIED');
    });

    it('Start new edit session when previous inactive without computable change', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        hasChange: true,
        userId: null,
      }); // Simulate non-computable inactive state

      const { body } = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/enter`)
        .expect(200);
      expect(body.status).toBe('EDIT_ENTERED');
      expect(body.editSession.hasChange).toBe(false);
      expect(body.editSession.hasComputableChange).toBe(false);
    });

    it('Edit session start is authorization protected', async () => {
      const { rawCredentials: otherUserCredentials } = await userFactory();
      await appInstance.sessionLogin(otherUserCredentials);
      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/enter`)
        .expect(403);
    });

    it('Can enter edit session undecided as editor', async () => {
      const { user: editor, rawCredentials: editorCredentials } =
        await userFactory();
      const { scenario: testScenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          scenarioAccesses: [{ userId: editor.id, level: 'editor' }],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );

      const session = await createEditSession(testScenario, baseUser);
      await session.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'completed',
      }); // Simulate undecided active state
      await appInstance
        .post(`/scenarios/${testScenario.itemId}/edit/leave`)
        .expect(200); // Properly leave session

      await appInstance.sessionLogin(editorCredentials);
      const response = await appInstance
        .post(`/scenarios/${testScenario.itemId}/edit/enter`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_ENTERED');
    });

    it('Cannot enter edit session undecided as inserter', async () => {
      const { user: inserter, rawCredentials: inserterCredentials } =
        await userFactory();
      const { scenario: testScenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          scenarioAccesses: [{ userId: inserter.id, level: 'inserter' }],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );

      const session = await createEditSession(testScenario, baseUser);
      await session.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'completed',
      }); // Simulate undecided active state
      await appInstance
        .post(`/scenarios/${testScenario.itemId}/edit/leave`)
        .expect(200); // Properly leave session

      await appInstance.sessionLogin(inserterCredentials);
      const response = await appInstance
        .post(`/scenarios/${testScenario.itemId}/edit/enter`)
        .expect(403);
      expect(response.body.code).toBe('NOT_AUTHORIZED');
    });
  });

  describe('Compute edit session', () => {
    it('Cannot compute unchanged or non-computable changed edit session', async () => {
      await createEditSession(baseScenario, baseUser);
      let response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_NOT_COMPUTABLE');

      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: 'changed' })
        .expect(200);
      response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_NOT_COMPUTABLE');
    });

    it('Can compute edit session after computable change', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      expect(session.hasComputableChange).toBe(false);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
        .send({ included: false })
        .expect(200);
      await session.reload();
      expect(session.hasComputableChange).toBe(true);
      expect(session.computationState).toBe('notStarted');

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_COMPUTATION_STARTED');
      expect(response.body.editSession.computationState).toBe('inProgress');
      await session.reload();
      expect(session.computationState).toBe('inProgress');
    });

    it('Can set saveOnComputed flag on computing edit session', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
        .send({ included: false })
        .expect(200);
      await session.reload();
      expect(session.saveOnComputed).toBe(false);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute?saveOnComputed=true`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_COMPUTATION_STARTED');
      expect(response.body.editSession.saveOnComputed).toBe(true);
      await session.reload();
      expect(session.saveOnComputed).toBe(true);
      await waitForServer(100); // Let edit session process finish, would be better to create test-only cancel fn?
    });
  });

  describe('Save edit session', () => {
    it('Can save edit session with non-computable changed', async () => {
      await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: 'changed' })
        .expect(200);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_SAVED');
      expect(response.body.editSession.hasChange).toBe(false);

      const updatedScenario = await scenarioService.getScenario(
        baseScenarioId,
        baseUser.id,
        { isItemId: true },
      );
      expect(updatedScenario.name).toBe('changed');

      // 'New' session should be active after normal save
      const session = await editSessionService.getEditSession(baseScenarioId, {
        validOnly: true,
      });
      expect(session).toBeDefined();
      expect(session.updatedRowId).toBeDefined();
    });

    it('Cannot save edit session with computable change yet to be computed', async () => {
      await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
        .send({ included: false })
        .expect(200);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_NOT_COMPUTED');
    });

    it('Can save edit session with computable change after computation', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
        .send({ included: false })
        .expect(200);

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(200);
      await waitForServer(20);
      await session.reload();
      expect(session.hasChange).toBe(true);
      expect(session.computationState).toBe('computed');

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_SAVED');
      expect(response.body.editSession.hasChange).toBe(false);
      expect(response.body.editSession.computationState).toBe('notStarted');

      const updatedScenario = await scenarioService.getScenario(
        baseScenarioId,
        baseUser.id,
        { isItemId: true, eventId: baseEvent.itemId },
      );
      expect(updatedScenario.events[0].included).toBe(false);
    });

    it('Can save edit session after computation with warning', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      const modData = await getModificationData({
        type: 'node',
        mode: 'new',
      }); // unconnected new node -> should generate warning
      await appInstance
        .post(
          `/scenarios/${baseScenarioId}/events/${baseEvent.itemId}/modifications`,
        )
        .send(modData)
        .expect(201);

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(200);
      await waitForServer(100);
      await session.reload();
      expect(session.hasChange).toBe(true);
      expect(session.computationState).toBe('computed');
      expect(session.computationWarning).toBe(true);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_SAVED');
      expect(response.body.editSession.hasChange).toBe(false);
      expect(response.body.editSession.computationState).toBe('notStarted');
      expect(response.body.editSession.computationWarning).toBe(true);
    });

    it('Cannot save edit session if computation got error', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        computationState: 'error',
        hasComputableChange: true,
        hasChange: true,
      }); // Simulate computation state
      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_COMPUTATION_ERROR');
    });

    it('Cannot save edit session if there are no changes', async () => {
      await createEditSession(baseScenario, baseUser);
      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_NOT_CHANGED');
    });

    it('Cannot save edit session if computation is running', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        computationState: 'inProgress',
        hasComputableChange: true,
        hasChange: true,
      }); // Simulate computation state

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(400);
      expect(response.body.code).toBe('EDIT_NOT_COMPUTED');
    });

    it('Keep original creation date for updated scenario', async () => {
      await createEditSession(baseScenario, baseUser);
      const originalCreatedAt = baseScenario.createdAt.toISOString();
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: 'changed' })
        .expect(200);

      const resource = await appInstance
        .get(`/scenarios/${baseScenarioId}?useChanged=true`)
        .expect(200);
      expect(resource.body.scenario.createdAt).toBe(originalCreatedAt);

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      const afterSave = await appInstance
        .get(`/scenarios/${baseScenarioId}?useChanged=true`)
        .expect(200);
      expect(afterSave.body.scenario.createdAt).toBe(originalCreatedAt);
    });

    it('Can save changed scenario as copy', async () => {
      await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: 'changed' })
        .expect(200);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save?saveAsCopy=true`)
        .expect(201);
      expect(response.body.status).toBe('EDIT_SAVED_COPY');
      expect(response.body.editSession.hasChange).toBe(true);
      expect(response.body.editSession.scenarioId).not.toBe(
        response.body.scenario.id,
      );
      expect(response.body.scenario.name).toBe('(copy) changed');
    });

    it('Can save edit session as copy after computation', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
        .send({ included: false })
        .expect(200);

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/compute`)
        .expect(200);
      await waitForServer(20);

      await session.reload();
      expect(session.hasChange).toBe(true);
      expect(session.computationState).toBe('computed');

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save?saveAsCopy=true`)
        .expect(201);
      expect(response.body.status).toBe('EDIT_SAVED_COPY');
      expect(response.body.editSession.hasChange).toBe(true);
      expect(response.body.editSession.computationState).toBe('computed');

      await appInstance
        .get(`/scenarios/${response.body.scenario.id}`)
        .expect(200);
    });
  });

  describe('Leave edit session', () => {
    it('Can leave edit session without any changes, session will be removed', async () => {
      await createEditSession(baseScenario, baseUser);
      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/leave`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_LEFT');
      expect(response.body.editSession).toBeUndefined();
    });

    it('Can leave edit session without undecided changes', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        hasChange: true,
      }); // Simulate changed-undecided state
      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/leave`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_LEFT');
      expect(response.body.editSession).toBeUndefined();
    });

    it('Can leave edit session with undecided changes, session will keep alive', async () => {
      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'computed',
      }); // Simulate changed-undecided state
      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/leave`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_LEFT');
      expect(response.body.editSession).toBeDefined();
    });
  });

  describe('Cancel edit session changes', () => {
    it('Can cancel edit session changes and keep session active', async () => {
      const originalName = baseScenario.name;
      const changedName = `${originalName}changed`;
      await createEditSession(baseScenario, baseUser);
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: changedName })
        .expect(200);

      const response = await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/cancel`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_CANCELED_CHANGES');
      expect(response.body.editSession).toBeDefined();

      const updatedScenario = await scenarioService.getScenario(
        baseScenarioId,
        baseUser.id,
        { isItemId: true },
      );
      expect(updatedScenario.name).toBe(originalName);

      // Session should be active
      const session = await editSessionService.getEditSession(baseScenarioId, {
        validOnly: true,
      });
      expect(session).toBeDefined();
    });
  });

  describe('Delete scenario in edit session', () => {
    it('Can delete scenario if edit session is not active', async () => {
      await appInstance.delete(`/scenarios/${baseScenarioId}`).expect(200);
    });
    it('Cannot delete scenario if edit session active', async () => {
      await createEditSession(baseScenario, baseUser);
      await appInstance.delete(`/scenarios/${baseScenarioId}`).expect(400);
    });
  });

  describe('Fetch methods in edit session', () => {
    it('Fetch method still returns last save until edit session is done', async () => {
      await createEditSession(baseScenario, baseUser);
      const originalName = baseScenario.name;
      const updateName = 'changed';
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: updateName })
        .expect(200);

      const resource = await appInstance
        .get(`/scenarios/${baseScenarioId}`)
        .expect(200);
      expect(resource.body.scenario.name).toBe(originalName);
      expect(resource.body.scenario.name).not.toBe(updateName);

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      const savedResource = await appInstance
        .get(`/scenarios/${baseScenarioId}`)
        .expect(200);
      expect(savedResource.body.scenario.name).not.toBe(originalName);
      expect(savedResource.body.scenario.name).toBe(updateName);
    });

    it('Fetch method returns updated with query param', async () => {
      await createEditSession(baseScenario, baseUser);
      const originalName = baseScenario.name;
      const updateName = 'changed';
      await appInstance
        .patch(`/scenarios/${baseScenarioId}`)
        .send({ name: updateName })
        .expect(200);

      const resource = await appInstance
        .get(`/scenarios/${baseScenarioId}?useChanged=true`)
        .expect(200);
      expect(resource.body.scenario.name).not.toBe(originalName);
      expect(resource.body.scenario.name).toBe(updateName);
    });
  });

  describe('Edit session states', () => {
    const getShortModUrl = (scenario, event) =>
      `/scenarios/${scenario?.itemId ?? scenario}/events/${
        event?.itemId ?? event
      }/modifications`;
    const getLongModUrl = (scenario, event, modification) =>
      `${getShortModUrl(scenario, event)}/${
        modification?.itemId ?? modification
      }`;

    describe('Makes non-computable changes', () => {
      it('Update scenario', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .patch(`/scenarios/${baseScenarioId}`)
          .send({ name: 'updated' })
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('CreateEvent not included', async () => {
        const { user: inserter, rawCredentials: inserterCredentials } =
          await userFactory();
        const { scenario } = await scenarioFactory({
          authorUserId: baseUser.id,
          scenarioAccesses: [{ userId: inserter.id, level: 'inserter' }],
        });
        const scenarioId = scenario.itemId;
        const session = await createEditSession(scenario, inserter);
        expect(session.hasChange).toBe(false);
        await appInstance.sessionLogin(inserterCredentials);

        const eventData = await getEventData({ authorUserId: inserter.id });

        const { body } = await appInstance
          .post(`/scenarios/${scenarioId}/events`)
          .send(eventData)
          .expect(201);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      // Event itself hasn't any computable property
      it('CreateEvent: included', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const eventData = await getEventData({ authorUserId: baseUser.id });
        const { body } = await appInstance
          .post(`/scenarios/${baseScenarioId}/events`)
          .send(eventData)
          .expect(201);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('DeleteEvent: not included', async () => {
        const { event: notIncludedEvent } = await eventFactory(
          { authorUserId: baseUser.id, included: false },
          { attachedScenarioModel: baseScenario },
        );
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .delete(
            `/scenarios/${baseScenarioId}/events/${notIncludedEvent.itemId}`,
          )
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('UpdateEvent: not included', async () => {
        const { event: notIncludedEvent } = await eventFactory(
          { authorUserId: baseUser.id, included: false },
          { attachedScenarioModel: baseScenario },
        );
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const updateData = await getEventData({ authorUserId: baseUser.id });
        const { body } = await appInstance
          .patch(
            `/scenarios/${baseScenarioId}/events/${notIncludedEvent.itemId}`,
          )
          .send(updateData)
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('UpdateEvent: included - change base props', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const updateData = await getEventData({ authorUserId: baseUser.id });
        const { body } = await appInstance
          .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
          .send({
            name: updateData.name,
            description: updateData.desctiption,
            note: updateData.note,
          })
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('UpdateEvent: included - change included prop', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
          .send({ included: false })
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });

      it('UpdateEvent: included changed without modification in event', async () => {
        const { event: eventWoModification } = await eventFactory(
          { authorUserId: baseUser.id, included: false },
          { attachedScenarioModel: baseScenario },
        );
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .patch(
            `/scenarios/${baseScenarioId}/events/${eventWoModification.itemId}`,
          )
          .send({ included: true })
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('DeleteEvent: included without modifications', async () => {
        const { event: eventWoModification } = await eventFactory(
          { authorUserId: baseUser.id, included: true },
          { attachedScenarioModel: baseScenario },
        );
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .delete(
            `/scenarios/${baseScenarioId}/events/${eventWoModification.itemId}`,
          )
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('Create modification: event not included', async () => {
        const { event: notIncludedEvent } = await eventFactory(
          { authorUserId: baseUser.id, included: false },
          { attachedScenarioModel: baseScenario },
        );
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const modification = await getModificationData();
        const { body } = await appInstance
          .post(getShortModUrl(baseScenario, notIncludedEvent))
          .send(modification)
          .expect(201);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('DeleteModification: event not included', async () => {
        const { event: notIncludedEvent, modifications } = await eventFactory(
          {
            authorUserId: baseUser.id,
            included: false,
            modifications: [{}],
          },
          { attachedScenarioModel: baseScenario },
        );
        const [modification] = modifications;

        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .delete(getLongModUrl(baseScenario, notIncludedEvent, modification))
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('UpdateModification: event not included', async () => {
        const { event: notIncludedEvent, modifications } = await eventFactory(
          {
            authorUserId: baseUser.id,
            included: false,
            modifications: [{}],
          },
          { attachedScenarioModel: baseScenario },
        );
        const [modification] = modifications;

        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const updateData = await getModificationData(
          reuseTypeMode(modification),
        );
        const { body } = await appInstance
          .patch(getLongModUrl(baseScenario, notIncludedEvent, modification))
          .send(updateData)
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });

      it('UpdateModification: non computable change', async () => {
        // TODO: this test only passes with mode: existing - is it intended?
        const modification = await modificationFactory(
          {},
          { attachedEventModel: baseEvent },
        );

        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .patch(getLongModUrl(baseScenario, baseEvent, modification))
          .send({ ...reuseModData(modification), name: 'abcd' })
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(false);
      });
    });

    describe('Makes computable changes', () => {
      it('DeleteEvent: included', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .delete(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });

      it('UpdateEvent: included changed', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const updateData = { included: false };
        const { body } = await appInstance
          .patch(`/scenarios/${baseScenarioId}/events/${baseEvent.itemId}`)
          .send(updateData)
          .expect(200);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });

      it('Create modification: event included', async () => {
        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const modification = await getModificationData();
        const { body } = await appInstance
          .post(getShortModUrl(baseScenario, baseEvent))
          .send(modification)
          .expect(201);
        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });

      it('DeleteModification: event not included', async () => {
        const modification = await modificationFactory(
          {},
          { attachedEventModel: baseEvent },
        );

        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .delete(getLongModUrl(baseScenario, baseEvent, modification))
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });

      it('UpdateModification: computable change in included event', async () => {
        // TODO: this test only passes with mode: existing - is it intended?
        const modification = await modificationFactory(
          {},
          { attachedEventModel: baseEvent },
        );

        const session = await createEditSession(baseScenario, baseUser);
        expect(session.hasChange).toBe(false);

        const { body } = await appInstance
          .patch(getLongModUrl(baseScenario, baseEvent, modification))
          .send({ ...reuseModData(modification), dateTo: new Date() })
          .expect(200);

        expect(body.editSession.hasChange).toBe(true);
        expect(body.editSession.hasComputableChange).toBe(true);
      });
    });
  });

  describe('Running edit session', () => {
    it('Cannot delete scenario while edit session is active', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      await createEditSession(scenario, baseUser);

      const response = await appInstance
        .delete(`/scenarios/${scenario.itemId}`)
        .expect(400);

      expect(response.body.code).toBe('EDIT_SESSION_ACTIVE');
    });

    it('Model sections are not changed with non-computable change save', async () => {
      // Fixes bug when section codelist got lost
      const testSections = [
        {
          key: 'TEST',
          dateFrom: null,
          dateTo: null,
          isValid: true,
          codeList: { link: {}, node: {} },
        },
      ];
      const { scenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          events: [
            {
              included: true,
              modifications: [{ type: 'node', mode: 'new' }],
            },
          ],
          modelSections: testSections.map((section) => ({
            ...section,
            hash: 'hash',
          })),
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );
      await createEditSession(scenario, baseUser);

      await appInstance
        .patch(`/scenarios/${scenario.itemId}`)
        .send({ note: 'changed' })
        .expect(200);
      const savedSession = await appInstance
        .post(`/scenarios/${scenario.itemId}/edit/save`)
        .expect(200);
      expect(savedSession.body.status).toBe('EDIT_SAVED');

      const scenarioAfterSave = await appInstance
        .get(`/scenarios/${scenario.itemId}?useChanged=true`)
        .expect(200);
      const { modelSections } = scenarioAfterSave.body.scenario;
      expect(modelSections).toEqual(testSections);
    });
  });

  describe('Direct editing scenario with running edit session', () => {
    it('Can update publication props while edit session is running', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id, modelType: 'DTA' },
        { withTrafficModel: true },
      );
      await createEditSession(scenario, baseUser);

      // update name the common way first
      await appInstance
        .patch(`/scenarios/${scenario.itemId}`)
        .send({ name: 'changed' })
        .expect(200);

      // update publication props directly
      await appInstance
        .patch(`/scenarios/${scenario.itemId}/public`)
        .send({
          isPublic: !scenario.isPublic,
          isInPublicList: !scenario.isInPublicList,
        })
        .expect(200);

      // save edit session
      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/edit/save`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_SAVED');

      // get updated scenario
      const updatedScenario = await scenarioService.getScenario(
        scenario.itemId,
        baseUser.id,
        { isItemId: true },
      );

      expect(updatedScenario.name).toBe('changed');
      expect(updatedScenario.isPublic).toBe(!scenario.isPublic);
      expect(updatedScenario.isInPublicList).toBe(!scenario.isInPublicList);
    });

    it('Can update user access while edit session is running', async () => {
      const { user: collabUser } = await userFactory();
      const { scenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          scenarioAccesses: [{ level: 'inserter', userId: collabUser.id }],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );
      await createEditSession(scenario, baseUser);

      // update name the common way first
      await appInstance
        .patch(`/scenarios/${scenario.itemId}`)
        .send({ name: 'changed' })
        .expect(200);

      // delete user access directly
      await appInstance
        .delete(`/scenarios/${scenario.itemId}/access/${collabUser.id}`)
        .expect(200);

      // save edit session
      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/edit/save`)
        .expect(200);
      expect(response.body.status).toBe('EDIT_SAVED');

      // get updated scenario
      const updatedScenario = await scenarioService.getScenario(
        scenario.itemId,
        baseUser.id,
        { isItemId: true, allAccesses: true },
      );

      expect(updatedScenario.name).toBe('changed');
      expect(updatedScenario.scenarioAccesses.length).toBe(1);
    });
  });

  describe('Get edit sessions', () => {
    let secondBaseScenario;
    let otherUser;
    let otherUserSharedScenario;
    let otherUserPrivateScenario;

    beforeEach(async () => {
      ({ scenario: secondBaseScenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      ));
      ({ user: otherUser } = await userFactory());
      ({ scenario: otherUserSharedScenario } = await scenarioFactory(
        {
          authorUserId: otherUser.id,
          scenarioAccesses: [{ userId: baseUser.id, level: 'editor' }],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      ));
      ({ scenario: otherUserPrivateScenario } = await scenarioFactory(
        {
          authorUserId: otherUser.id,
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      ));
    });

    const findSession = (sessions, findScenario, findUser) =>
      sessions.find(
        ({ userId, scenarioId }) =>
          userId === findUser && scenarioId === findScenario,
      );

    it('Provides only active edit session with access in overview', async () => {
      await createEditSession(baseScenario, baseUser);
      await createEditSession(secondBaseScenario, baseUser);
      await createEditSession(otherUserSharedScenario, otherUser);
      await createEditSession(otherUserPrivateScenario, otherUser);

      const { body } = await appInstance
        .get(`/scenarios/edit/overview`)
        .expect(200);

      expect(body.length).toBe(3);
      const ids = body.map(({ scenarioId }) => scenarioId);
      expect(ids).toContain(baseScenario.itemId);
      expect(ids).toContain(secondBaseScenario.itemId);
      expect(ids).toContain(otherUserSharedScenario.itemId);
      expect(ids).not.toContain(otherUserPrivateScenario.itemId);

      expect(
        findSession(body, otherUserSharedScenario.itemId, otherUser.id),
      ).toBeTruthy();
      expect(
        findSession(body, otherUserPrivateScenario.itemId, otherUser.id),
      ).toBeFalsy();
    });

    it('Provides correct state for sessions', async () => {
      const inProgressSession = await createEditSession(baseScenario, baseUser);
      const computedSession = await createEditSession(
        secondBaseScenario,
        baseUser,
      );
      const errorSession = await createEditSession(
        otherUserSharedScenario,
        baseUser,
      );

      await inProgressSession.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'inProgress',
      }); // Simulate in progress state
      await computedSession.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'computed',
      }); // Simulate in computed state
      await errorSession.update({
        hasChange: true,
        hasComputableChange: true,
        computationState: 'error',
      }); // Simulate in error state

      const { body } = await appInstance
        .get(`/scenarios/edit/overview`)
        .expect(200);
      expect(
        findSession(body, baseScenario.itemId, baseUser.id).computationState,
      ).toBe('inProgress');
      expect(
        findSession(body, secondBaseScenario.itemId, baseUser.id)
          .computationState,
      ).toBe('computed');
      expect(
        findSession(body, otherUserSharedScenario.itemId, baseUser.id)
          .computationState,
      ).toBe('error');
    });

    it('Provides empty array without sessions or with invalid sessions', async () => {
      const { body: noSessions } = await appInstance
        .get(`/scenarios/edit/overview`)
        .expect(200);
      expect(noSessions.length).toBe(0);

      const session = await createEditSession(baseScenario, baseUser);
      await session.update({
        userId: null,
      }); // Simulate exited state

      const { body: onlyInvalid } = await appInstance
        .get(`/scenarios/edit/overview`)
        .expect(200);
      expect(onlyInvalid.length).toBe(0);
    });
  });
});
