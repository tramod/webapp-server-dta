import { beforeAll, expect, it, jest } from '@jest/globals';

import {
  getAppInstance,
  truncateDB,
  waitForServer,
} from './helpers/general.js';
import { reuseModData } from './helpers/modifications.js';
import {
  restoreAxiosMock,
  resetAxiosRequestHistory,
  axiosMock,
  mockGetTurnRestrictionByNode,
} from './helpers/mockAxios.js';
import { expectInvalidInputError } from './helpers/errorResponses.js';
import {
  userFactory,
  scenarioFactory,
  eventFactory,
  modificationFactory,
  getEventData,
  getScenarioData,
  getModificationData,
  trafficModelFactory,
} from '../../db/factories/index.js';
import Formatter from './formatters.js';
import scenarioService from '../../app/services/scenarios.js';
import modificationService from '../../app/services/modification.js';
import { resetModelItemsCache } from '../../app/services/modelAPI.js';
import config, { setConfig } from '../../config/env.js';
import { Scenario, Event } from '../../db/index.js';

let appInstance;
let baseUser;
let baseUserCredentials;

const editModeDisableSetting = config.TM_EDIT_MODE_DISABLE;
beforeAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', true);
});
afterAll(() => {
  setConfig('TM_EDIT_MODE_DISABLE', editModeDisableSetting);
  restoreAxiosMock();
});

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: baseUser, rawCredentials: baseUserCredentials } =
    await userFactory());
});
afterEach(() => {
  resetAxiosRequestHistory();
  resetModelItemsCache();
});

// TODO Update after access system implemented
// TODO Improve test cases build with factories
describe('Scenarios', () => {
  describe('Protected routes', () => {
    it('Protected routes are login only', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const scenarioId = scenario.itemId;

      const protectedRoutes = [
        { route: '/scenarios', method: 'post' },
        { route: `/scenarios/${scenarioId}`, method: 'delete' },
        { route: `/scenarios/${scenarioId}`, method: 'patch' },
        { route: `/scenarios/${scenarioId}/public`, method: 'patch' },
        { route: `/scenarios/${scenarioId}/events`, method: 'post' },
        { route: `/scenarios/${scenarioId}/events`, method: 'delete' },
        { route: `/scenarios/${scenarioId}/copy`, method: 'post' },
        { route: `/scenarios/${scenarioId}/import-events`, method: 'post' },
      ];

      function expectStatusOnRoutes(status, testInstance, routes) {
        return Promise.all(
          routes.map(async (entry) => {
            await testInstance[entry.method](entry.route).expect(status);
          }),
        );
      }

      await expectStatusOnRoutes(401, appInstance, protectedRoutes);

      await appInstance.sessionLogin(baseUserCredentials);
      // Test only getter after login
      await expectStatusOnRoutes(
        200,
        appInstance,
        protectedRoutes.filter((route) => route.method === 'get'),
      );
    });
  });

  describe('GetAll', () => {
    it('Returns users scenarios and all shared', async () => {
      const { user: otherUser, rawCredentials: otherUserCredentials } =
        await userFactory();
      const sc1 = await scenarioFactory({}, { withRandomAccess: true });
      const sc2 = await scenarioFactory({}, { withRandomAccess: true });
      const sc3 = await scenarioFactory({}, { withRandomAccess: true });

      const expectedScenarioIds = (userId) =>
        [sc1, sc2, sc3]
          .filter(
            (item) =>
              (item.scenario.isPublic && item.scenario.isInPublicList) ||
              item.accesses.some((a) => a.userId === userId),
          )
          .map((item) => item.scenario.itemId);

      await appInstance.sessionLogin(baseUserCredentials);
      const response = await appInstance.get('/scenarios');
      const returnedIds =
        response.body.scenarios?.map((entry) => entry.id) || [];
      expect(returnedIds.sort()).toEqual(expectedScenarioIds(baseUser.id));

      await appInstance.sessionLogin(otherUserCredentials);
      const otherResponse = await appInstance.get('/scenarios');
      const otherReturnedIds =
        otherResponse.body.scenarios?.map((entry) => entry.id) || [];
      expect(otherReturnedIds.sort()).toEqual(
        expectedScenarioIds(otherUser.id),
      );
    });
  });

  describe('GetScenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns scenario by id', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}`)
        .expect(200);
      const expectedBody = {
        ...Formatter.scenarioModelToResponse(scenario),
      };
      const responseResource = response.body.scenario;
      expect(responseResource).toMatchObject(expectedBody);
      expect(responseResource).not.toHaveProperty('rowId');
      expect(responseResource).not.toHaveProperty('itemId');
      expect(responseResource).not.toHaveProperty('events');
      expect(responseResource).toHaveProperty('createdAt');
      expect(responseResource).toHaveProperty('updatedAt');
    });

    it('Returns scenario with author username as owner field', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}`)
        .expect(200);

      const responseResource = response.body.scenario;
      expect(responseResource).toHaveProperty('owner');
      expect(responseResource.owner).toEqual(baseUser.username);
    });

    it('Returns scenario with all events and modifications', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id },
        { withRandomEvents: true, randomEventsCount: 3 },
      );

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}?withEvents=true`)
        .expect(200);

      const responseScenario = response.body.scenario;
      expect(responseScenario).toHaveProperty('events');

      const responseEvents = response.body.scenario.events;

      responseEvents.forEach((ev) => {
        expect(ev).toHaveProperty('modifications');
      });
    });

    it('Fails when scenario does not exist', async () => {
      const SCENARIO_NOT_EXISTS_ID = 1;
      await appInstance.get(`/scenarios/${SCENARIO_NOT_EXISTS_ID}`).expect(404);
    });

    it('Fails when scenario id is not valid', async () => {
      await appInstance.get(`/scenarios/abc`).expect(400);
      await appInstance.get(`/scenarios/@1`).expect(400);
    });

    it('Available only to users with access', async () => {
      const { user: inserterUser, rawCredentials: inserterCredentials } =
        await userFactory();
      const { user: viewerUser, rawCredentials: viewerCredentials } =
        await userFactory();
      const { rawCredentials: noAccessUserCredentials } = await userFactory();

      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: false,
        scenarioAccesses: [
          { userId: inserterUser.id, level: 'inserter' },
          { userId: viewerUser.id, level: 'viewer' },
        ],
      });

      await appInstance.get(`/scenarios/${scenario.itemId}`).expect(200);

      await appInstance.sessionLogin(inserterCredentials);
      await appInstance.get(`/scenarios/${scenario.itemId}`).expect(200);

      await appInstance.sessionLogin(viewerCredentials);
      await appInstance.get(`/scenarios/${scenario.itemId}`).expect(200);

      await appInstance.sessionLogin(noAccessUserCredentials);
      // Resource fetcher passed but auth check blocks
      await appInstance.get(`/scenarios/${scenario.itemId}`).expect(403);
    });
  });

  describe('CreateScenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Creates scenario instance and assign id', async () => {
      await trafficModelFactory();
      const scenario = await getScenarioData({
        authorUserId: baseUser.id,
        dateFrom: null, // scenario can not be created with dates
        dateTo: null, // scenario can not be created with dates
      });
      const EXPECTED_ID = 1;

      const response = await appInstance
        .post(`/scenarios`)
        .send(scenario)
        .expect(201);

      const returnedId = response.body.scenario.id;
      expect(returnedId).toEqual(EXPECTED_ID);

      const getResponse = await appInstance
        .get(`/scenarios/${returnedId}`)
        .expect(200);

      expect(getResponse.body.scenario).toMatchObject(
        Formatter.scenarioModelToResponse({ ...scenario, id: returnedId }),
      );
    });

    it('Creates scenario instance together with events', async () => {
      await trafficModelFactory();
      const scenario = await getScenarioData({
        authorUserId: baseUser.id,
        dateFrom: null,
        dateTo: null,
      });
      const events = [
        await getEventData({ authorUserId: baseUser.id }),
        await getEventData({ authorUserId: baseUser.id }),
      ];

      const createResponse = await appInstance
        .post(`/scenarios`)
        .expect(201)
        .send({ ...scenario, events });
      const SCENARIO_ID = createResponse.body.scenario.id;

      const getResponse = await appInstance
        .get(`/scenarios/${SCENARIO_ID}/events`)
        .expect(200);
      expect(getResponse.body.events.length).toBe(events.length);
      const factoryEventNames = events.map((event) => event.name).sort();
      const responseEventNames = getResponse.body.events
        .map((event) => event.name)
        .sort();
      expect(responseEventNames).toEqual(factoryEventNames);
    });

    const validationCases = [
      ['Misses mandatory param', { names: 'someDescription' }, ['name']],
      [
        'Invalid params',
        {
          name: '',
          description: 123,
          events: {},
        },
        null,
      ],
      [
        'Invalid events param',
        {
          name: 'correct',
          events: [
            {
              name: '',
              description: 123,
              included: 'truethy',
            },
          ],
        },
        null,
      ],
    ];
    validationCases[1][2] = Object.keys(validationCases[1][1]);
    validationCases[2][2] = Object.keys(validationCases[2][1].events[0]).map(
      (entry) => `events[0].${entry}`,
    );

    test.each(validationCases)(
      'Fails with invalid input: %s',
      async (_testName, data, invalidFields) => {
        await expectInvalidInputError(appInstance, {
          url: '/scenarios',
          data,
          invalidFields,
        });
      },
    );

    it('Fails creating with non-existing model type', async () => {
      const scenario = await getScenarioData({
        authorUserId: baseUser.id,
        dateFrom: null,
        dateTo: null,
        modelType: 'DTA',
      });

      const response = await appInstance
        .post(`/scenarios`)
        .send(scenario)
        .expect(400);

      expect(response.body.error).toEqual(
        'No DTA model is available on the server!',
      );
    });
  });

  describe('Update scenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Can be updated by authorized user', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const scenarioData = await getScenarioData({ authorUserId: baseUser.id });
      const updatedData = {
        name: scenarioData.name,
        description: scenarioData.description,
      };

      const response = await appInstance
        .patch(`/scenarios/${scenario.itemId}`)
        .send(updatedData)
        .expect(200);

      const updatedScenario = response.body.scenario;

      for (const [key, value] of Object.entries(updatedData)) {
        expect(value).toEqual(updatedScenario[key]);
      }
    });

    it('Can not be updated by unauthorized user', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const { rawCredentials: noAccessUserCredentials } = await userFactory();

      await appInstance.sessionLogin(noAccessUserCredentials);

      await appInstance.patch(`/scenarios/${scenario.itemId}`);
      expect(403);
    });
  });

  describe('Update scenario publication', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Can be updated by authorized user', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const updatedData = {
        isPublic: !scenario.isPublic,
        isInPublicList: !scenario.isInPublicList,
      };

      const response = await appInstance
        .patch(`/scenarios/${scenario.itemId}/public`)
        .send(updatedData)
        .expect(200);

      const updatedScenario = response.body.scenario;

      for (const [key, value] of Object.entries(updatedData)) {
        expect(value).toEqual(updatedScenario[key]);
      }
    });

    it('Can not be updated by unauthorized user', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const { rawCredentials: noAccessUserCredentials } = await userFactory();

      await appInstance.sessionLogin(noAccessUserCredentials);

      await appInstance.patch(`/scenarios/${scenario.itemId}/public`);
      expect(403);
    });
  });

  describe('Scenario - date updates', () => {
    const DATES_TIMEOUT = 100;
    let baseScenario;

    beforeEach(async () => {
      ({ scenario: baseScenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      ));
      await appInstance.sessionLogin(baseUserCredentials);
    });
    beforeAll(async () => setConfig('TM_EDIT_MODE_DISABLE', false));
    afterAll(() => setConfig('TM_EDIT_MODE_DISABLE', true));

    it('Scenarios dates are updated both in Model and API', async () => {
      const { event } = await eventFactory(
        { included: false },
        { attachedScenarioModel: baseScenario },
      );
      const modification = await getModificationData({ type: 'link' });
      await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

      await appInstance
        .post(
          `/scenarios/${baseScenario.itemId}/events/${event.itemId}/modifications`,
        )
        .send(modification)
        .expect(201);

      await appInstance
        .post(`/scenarios/${baseScenario.itemId}/edit/save`)
        .expect(200);

      await waitForServer(DATES_TIMEOUT);
      // ! Rarely fails because of bad timing

      // Updated in DB
      const updatedScenario = await Scenario.findOne({
        where: { itemId: baseScenario.itemId },
      });
      expect(updatedScenario.dateFrom).toStrictEqual(modification.dateFrom);
      expect(updatedScenario.dateTo).toStrictEqual(modification.dateTo);

      // Also updated in API Response
      const scenarioFromApi = await appInstance
        .get(`/scenarios/${baseScenario.itemId}`)
        .expect(200);
      expect(scenarioFromApi.body.scenario.dateFrom).toBe(
        modification.dateFrom.toISOString(),
      );
      expect(scenarioFromApi.body.scenario.dateTo).toBe(
        modification.dateTo.toISOString(),
      );
    });

    it('Scenario dates are null with null date event', async () => {
      const { event, modifications } = await eventFactory(
        { included: false, modifications: [{ type: 'node', mode: 'new' }] },
        { attachedScenarioModel: baseScenario },
      );
      await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);
      const [modification] = modifications;

      expect(baseScenario.dateFrom).not.toBe(null);
      expect(baseScenario.dateTo).not.toBe(null);

      const modificationUpdate = {
        ...reuseModData(modification),
        dateFrom: null,
        dateTo: null,
      };

      await appInstance
        .patch(
          `/scenarios/${baseScenario.itemId}/events/${event.itemId}/modifications/${modification.itemId}`,
        )
        .send(modificationUpdate)
        .expect(200);

      await appInstance
        .post(`/scenarios/${baseScenario.itemId}/edit/save`)
        .expect(200);

      await waitForServer(DATES_TIMEOUT);
      // ! Rarely fails because of bad timing

      const updatedScenario = await Scenario.findOne({
        where: { itemId: baseScenario.itemId },
      });
      expect(updatedScenario.dateFrom).toBe(null);
      expect(updatedScenario.dateTo).toBe(null);
    });

    it('Scenario dates are picked correctly from multiple modifications in multiple events', async () => {
      const { modifications: ev1mods } = await eventFactory(
        { included: false, modifications: [{}, {}] },
        { attachedScenarioModel: baseScenario },
      );

      const { modifications: ev2mods } = await eventFactory(
        { included: false, modifications: [{}, {}] },
        { attachedScenarioModel: baseScenario },
      );
      await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

      const allMods = [...ev1mods, ...ev2mods];

      const [firstDateFrom] = await allMods
        .map((mod) => mod.dateFrom)
        .sort((a, b) => a - b); // First should be first

      const [lastDateTo] = allMods
        .map((mod) => mod.dateTo)
        .sort((a, b) => b - a); // Last should be first

      // update anything to enable saving
      await appInstance
        .patch(`/scenarios/${baseScenario.itemId}`)
        .send({ description: 'test' })
        .expect(200);

      await appInstance
        .post(`/scenarios/${baseScenario.itemId}/edit/save`)
        .expect(200);

      // ! Rarely fails because of bad timing
      await waitForServer(DATES_TIMEOUT);

      const updatedScenario = await Scenario.findOne({
        where: { itemId: baseScenario.itemId },
      });
      expect(updatedScenario.dateFrom).toStrictEqual(firstDateFrom);
      expect(updatedScenario.dateTo).toStrictEqual(lastDateTo);
    });

    describe('Scenario dates update called after', () => {
      const updateSpy = jest.spyOn(scenarioService, 'deepUpdateScenarioDates');
      beforeEach(async () => updateSpy.mockClear());
      afterAll(async () => updateSpy.mockRestore());

      it('saving scenario session', async () => {
        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);
        // update anything to enable saving
        await appInstance
          .patch(`/scenarios/${baseScenario.itemId}`)
          .send({ description: 'test' })
          .expect(200);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
        });

        expect(updateSpy).toHaveBeenCalled();
        // expect scenario updatedAt date to be updated (increased since exact date is difficult to predict)
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
      });

      it('event create', async () => {
        const event = await getEventData({ authorUserId: baseUser.id });
        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/events`)
          .send(event)
          .expect(201);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
        });

        expect(updateSpy).toHaveBeenCalled();
        // expect scenario updatedAt date to be updated
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
      });

      it('modification update', async () => {
        const { event, modifications } = await eventFactory(
          { included: false, modifications: [{ type: 'link' }] },
          { attachedScenarioModel: baseScenario },
        );
        const [modification] = modifications;
        const modificationUpdate = {
          ...reuseModData(modification),
          dateFrom: new Date(),
          dateTo: new Date(),
        };
        await baseScenario.reload();

        updateSpy.mockClear(); // Update spy as it was called inside factory
        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

        await appInstance
          .patch(
            `/scenarios/${baseScenario.itemId}/events/${event.itemId}/modifications/${modification.itemId}`,
          )
          .send(modificationUpdate)
          .expect(200);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
          include: [{ model: Event }],
        });
        const savedEvent = savedScenario.events[0];

        expect(updateSpy).toHaveBeenCalled();
        // expect scenario updatedAt date to be updated
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
        // expect event updatedAt date to be updated
        expect(savedEvent.updatedAt > event.updatedAt).toBeTruthy();
      });

      it('modification delete', async () => {
        const { event, modifications } = await eventFactory(
          { included: false, modifications: [{ type: 'link' }] },
          { attachedScenarioModel: baseScenario },
        );
        const [modification] = modifications;
        updateSpy.mockClear(); // Update spy as it was called inside factory
        await baseScenario.reload();

        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

        await appInstance
          .delete(
            `/scenarios/${baseScenario.itemId}/events/${event.itemId}/modifications/${modification.itemId}`,
          )
          .expect(200);
        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
          include: [{ model: Event }],
        });
        const savedEvent = savedScenario.events[0];

        expect(updateSpy).toHaveBeenCalled();
        // expect scenario updatedAt date to be updated
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
        // expect event updatedAt date to be updated
        expect(savedEvent.updatedAt > event.updatedAt).toBeTruthy();
      });

      it('event delete', async () => {
        const { event } = await eventFactory(
          { included: false },
          { attachedScenarioModel: baseScenario },
        );
        await baseScenario.reload();
        updateSpy.mockClear(); // Update spy as it was called inside factory
        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

        await appInstance
          .delete(`/scenarios/${baseScenario.itemId}/events/${event.itemId}`)
          .expect(200);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
        });

        expect(updateSpy).toHaveBeenCalled();
        // expect scenario updatedAt date to be updated
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
      });

      it('scenario update but keeping event updatedAt date', async () => {
        const { event } = await eventFactory(
          { included: false, modifications: [{ type: 'link' }] },
          { attachedScenarioModel: baseScenario },
        );
        updateSpy.mockClear(); // Update spy as it was called inside factory
        await appInstance.post(`/scenarios/${baseScenario.itemId}/edit/enter`);

        await appInstance
          .patch(`/scenarios/${baseScenario.itemId}`)
          .send({ description: 'test' })
          .expect(200);

        expect(updateSpy).not.toHaveBeenCalled();

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
          include: [{ model: Event }],
        });
        const savedEvent = savedScenario.events[0];

        expect(updateSpy).toHaveBeenCalled();
        expect(savedScenario.updatedAt > baseScenario.updatedAt).toBeTruthy();
        expect(
          savedEvent.updatedAt.getTime() === event.updatedAt.getTime(),
        ).toBeTruthy();

        await appInstance
          .patch(`/scenarios/${baseScenario.itemId}/events/${event.itemId}`)
          .send({ description: 'test' })
          .expect(200);

        await appInstance
          .post(`/scenarios/${baseScenario.itemId}/edit/save`)
          .expect(200);

        const savedScenario2 = await Scenario.findOne({
          where: { itemId: baseScenario.itemId },
          include: [{ model: Event }],
        });
        const savedEvent2 = savedScenario2.events[0];

        expect(updateSpy).toHaveBeenCalled();
        expect(savedScenario2.updatedAt > savedScenario.updatedAt).toBeTruthy();
        expect(savedEvent2.updatedAt > savedEvent.updatedAt).toBeTruthy();
      });
    });
  });

  describe('Copy scenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Returns copy of the original scenario', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id, modelType: 'DTA' },
        { withTrafficModel: true },
      );
      const scenarioBody = Formatter.scenarioModelToResponse(scenario);
      const expectedBody = {
        ...scenarioBody,
        id: scenarioBody.id + 1,
        name: `(copy) ${scenarioBody.name}`,
      };

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/copy`)
        .expect(201);

      const responseResource = response.body.scenario;

      expect(responseResource).toMatchObject(expectedBody);
    });

    it('Scenario copy has new created at date', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id, modelType: 'DTA' },
        { withTrafficModel: true },
      );
      const originalCreatedAt = scenario.createdAt;

      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/copy`)
        .expect(201);
      const scenarioCopy = response.body.scenario;
      expect(scenarioCopy.createdAt).not.toBe(originalCreatedAt.toISOString());
    });

    it('Scenario copy removes possible public access', async () => {
      const { scenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          isPublic: true,
          isInPublicList: true,
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );
      const response = await appInstance
        .post(`/scenarios/${scenario.itemId}/copy`)
        .expect(201);
      const scenarioCopy = response.body.scenario;

      expect(scenario.isPublic).toBe(true);
      expect(scenario.isInPublicList).toBe(true);
      expect(scenarioCopy.isPublic).toBe(false);
      expect(scenarioCopy.isInPublicList).toBe(false);
    });
  });

  describe('Delete scenario', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Can be deleted by authorized user', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id, modelType: 'DTA' },
        { withTrafficModel: true },
      );

      await appInstance.delete(`/scenarios/${scenario.itemId}`).expect(200);

      await waitForServer(10);
      const deletedScenario = await scenarioService.scenarioModel.findByPk(
        scenario.rowId,
      );
      expect(deletedScenario).toEqual(null);
    });

    it('Can not be deleted by user without permissions', async () => {
      const { scenario } = await scenarioFactory({ authorUserId: baseUser.id });
      const { rawCredentials: noAccessUserCredentials } = await userFactory();

      await appInstance.sessionLogin(noAccessUserCredentials);
      await appInstance.delete(`/scenarios/${scenario.itemId}`).expect(403);
    });
  });

  describe('Get scenario node transit', () => {
    beforeEach(async () => appInstance.sessionLogin(baseUserCredentials));

    it('Request node transit data from scenario and returns in expected format', async () => {
      const NODE_ID = 1;
      const NODE_TRANSIT_DATA = {
        node: 1,
        linkFrom: 2,
        linkTo: 3,
        capacity: 4,
      };
      const { scenario, events } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          events: [
            {
              authorUserId: baseUser.id,
              included: true,
            },
          ],
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );

      const [event] = events;
      const modification = await modificationFactory(
        {
          type: 'node',
          mode: 'existing',
          data: NODE_TRANSIT_DATA,
        },
        {
          attachedEventModel: event,
        },
      );
      const formattedMod = Formatter.modificationModelToResponse(modification);
      const EXPECTED_RESPONSE = [
        {
          ...NODE_TRANSIT_DATA,
          isBaseModel: false,
          eventId: event.itemId,
          eventName: event.name,
          eventIncluded: true,
          dateFrom: formattedMod.dateFrom,
          dateTo: formattedMod.dateTo,
          value: NODE_TRANSIT_DATA.cost,
        },
      ];

      mockGetTurnRestrictionByNode({ nodeId: NODE_ID });
      const localFindSpy = jest.spyOn(
        modificationService,
        'getScenarioNodeTransit',
      );

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/node-transit/${NODE_ID}`)
        .expect(200);
      expect(localFindSpy).toHaveBeenCalled();
      expect(response.body).toEqual(EXPECTED_RESPONSE);
      localFindSpy.mockRestore();
    });

    it('Request node data also from baseModel and return them in correct format (inc. cost/value)', async () => {
      const { scenario } = await scenarioFactory(
        { authorUserId: baseUser.id, modelType: 'DTA' },
        { withTrafficModel: true },
      );
      const NODE_ID = 1;
      const MODEL_API_RESPONSE = [
        { node_id: NODE_ID, from_edge_id: 4, to_edge_id: 5, capacity: 1 },
        { node_id: NODE_ID, from_edge_id: 6, to_edge_id: 7, capacity: 100 },
      ];
      const EXPECTED_RESPONSE = [
        {
          node: NODE_ID,
          linkFrom: 4,
          linkTo: 5,
          value: 3600,
          isBaseModel: true,
          eventId: null,
        },
        {
          node: NODE_ID,
          linkFrom: 6,
          linkTo: 7,
          value: -1,
          isBaseModel: true,
          eventId: null,
        },
      ];

      mockGetTurnRestrictionByNode({
        nodeId: NODE_ID,
        response: MODEL_API_RESPONSE,
      });
      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/node-transit/${NODE_ID}`)
        .expect(200);

      expect(axiosMock.mock.history.get.length).toEqual(1);
      expect(response.body).toEqual(EXPECTED_RESPONSE);
    });

    it('Returns empty array if nothing provided by any source', async () => {
      const NODE_ID = 1;
      const { scenario } = await scenarioFactory(
        {
          authorUserId: baseUser.id,
          modelType: 'DTA',
        },
        { withTrafficModel: true },
      );
      mockGetTurnRestrictionByNode({
        nodeId: NODE_ID,
      });

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/node-transit/${NODE_ID}`)
        .expect(200);
      expect(response.body).toEqual([]);
    });
  });

  describe('Get scenario extra feature modifications', () => {
    it('returns included extra feature modifications for logged user', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: false,
        events: [
          {
            included: true,
            modifications: [
              { mode: 'new' },
              { mode: 'new' },
              { mode: 'existing' },
            ],
          },
          {
            included: false,
            modifications: [
              { mode: 'new' },
              { mode: 'new' },
              { mode: 'existing' },
            ],
          },
        ],
      });

      await appInstance.sessionLogin(baseUserCredentials);

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/extra-feature-modifications`)
        .expect(200);

      expect(response.body).toHaveProperty('modifications');
      const responseModifications = response.body.modifications;

      expect(responseModifications.length).toBe(2);
    });

    it('returns all new modifications for logged user', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: false,
        events: [
          {
            included: true,
            modifications: [
              { mode: 'new' },
              { mode: 'new' },
              { mode: 'existing' },
            ],
          },
          {
            included: false,
            modifications: [
              { mode: 'new' },
              { mode: 'new' },
              { mode: 'existing' },
            ],
          },
        ],
      });

      await appInstance.sessionLogin(baseUserCredentials);

      const response = await appInstance
        .get(
          `/scenarios/${scenario.itemId}/extra-feature-modifications?useChanged=true`,
        )
        .expect(200);

      expect(response.body).toHaveProperty('modifications');
      const responseModifications = response.body.modifications;

      expect(responseModifications.length).toBe(4);
    });

    it('returns included new modifications in public scenario', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: true,
        events: [
          { included: true, modifications: [{ mode: 'new' }] },
          { included: false, modifications: [{ mode: 'new' }] },
        ],
      });

      const response = await appInstance
        .get(`/scenarios/${scenario.itemId}/extra-feature-modifications`)
        .expect(200);

      expect(response.body).toHaveProperty('modifications');
      const responseModifications = response.body.modifications;

      expect(responseModifications.length).toBe(1);
    });

    it('returns 403 for unatuhorized user', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: false,
        events: [
          { included: true, modifications: [{ mode: 'new' }] },
          { included: false, modifications: [{ mode: 'new' }] },
        ],
      });

      await appInstance
        .get(`/scenarios/${scenario.itemId}/extra-feature-modifications`)
        .expect(403);
    });

    it('returns 404 with no new modifications', async () => {
      const { scenario } = await scenarioFactory({
        authorUserId: baseUser.id,
        isPublic: false,
        events: [{ included: true, modifications: [{ mode: 'existing' }] }],
      });

      await appInstance.sessionLogin(baseUserCredentials);

      await appInstance
        .get(`/scenarios/${scenario.itemId}/extra-feature-modifications`)
        .expect(404);
    });

    it('returns 400 with invalid params', async () => {
      await appInstance.sessionLogin(baseUserCredentials);

      await appInstance
        .get(`/scenarios/invalid-sc-id/extra-feature-modifications`)
        .expect(400);
    });
  });
});
