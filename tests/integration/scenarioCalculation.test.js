import { afterEach, beforeEach, describe, expect, it } from '@jest/globals';
import { startOfHour, addHours, addDays } from 'date-fns';

import {
  getAppInstance,
  truncateDB,
  waitForServer,
} from './helpers/general.js';
import {
  restoreAxiosMock,
  resetAxiosRequestHistory,
  mockPostTrafficJob,
  mockGetTrafficJobStatus,
  mockDeleteTrafficCache,
  mockGetLink,
  mockAllGetLink,
  mockAllGetNode,
  mockGetModelInfo,
  axiosMock,
} from './helpers/mockAxios.js';
import {
  userFactory,
  scenarioFactory,
  modificationFactory,
} from '../../db/factories/index.js';
import { ModelSection } from '../../db/index.js';
import editSessionService from '../../app/services/editSession.js';
import { resetModelItemsCache } from '../../app/services/modelAPI.js';
import config from '../../config/env.js';

let appInstance;
let baseUser;
let baseUserCredentials;
let baseScenario;
let baseScenarioId;
let baseEvents;
let baseEvent;
let editSession;

const MODEL_SECTION_PREFIX = config.TM_MODEL_CACHE_PREFIX;
const SERVER_PROCESSING_TIMEOUT = 100;

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user: baseUser, rawCredentials: baseUserCredentials } =
    await userFactory());
  await appInstance.sessionLogin(baseUserCredentials);
  ({ scenario: baseScenario, events: baseEvents } = await scenarioFactory(
    {
      authorUserId: baseUser.id,
      events: [{ included: true }],
      modelType: 'DTA',
    },
    { withTrafficModel: true },
  ));
  [baseEvent] = baseEvents;
  baseScenarioId = baseScenario.itemId;
});

afterEach(() => {
  resetAxiosRequestHistory();
  resetModelItemsCache();
});

// Taken from app implementation, should by integration
async function createEditSession(source, user) {
  const session = await editSessionService.initEditSession({
    source,
    user,
  });
  return session;
}

async function simulateComputationRequired(session) {
  const sessionWithUpdated = await editSessionService.addUpdatedToSession(
    session,
  );
  await sessionWithUpdated.update({
    hasComputableChange: true,
    hasChange: true,
  });
  return sessionWithUpdated;
}

const requestEditSessionComputation = async (scenarioId) =>
  appInstance.post(`/scenarios/${scenarioId}/edit/compute`).expect(200);

const requestEditSessionScenario = async (scenarioId) =>
  appInstance.get(`/scenarios/${scenarioId}?useChanged=true`).expect(200);

const applyMocks = ({ jobResponse, jobStatusResponse } = {}) => {
  mockPostTrafficJob({ response: jobResponse });
  mockGetTrafficJobStatus({ response: jobStatusResponse });
  mockDeleteTrafficCache();
  mockGetModelInfo({ type: 'link' });
  mockGetModelInfo({ type: 'node' });
};

describe('Scenario calculation (via editSession compute)', () => {
  describe('Common calculation outputs', () => {
    let modification;
    beforeEach(async () => {
      modification = await modificationFactory(
        { type: 'node', mode: 'existing' },
        { attachedEventModel: baseEvent },
      );

      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);
    });

    afterEach(() => restoreAxiosMock);

    it('Calculation requests traffic job and awaits result, modelSection given with scenario', async () => {
      const JOB_ID_USED = 'a1b2';
      applyMocks({ jobResponse: { jobId: JOB_ID_USED } });
      mockAllGetLink();
      mockAllGetNode();
      await requestEditSessionComputation(baseScenarioId);
      const modificationDateFromRounded = startOfHour(modification.dateFrom);

      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(1);
      const postData = JSON.parse(axiosMock.mock.history.post[0].data);
      const requestedKeyName = postData.cacheName;
      expect(axiosMock.mock.history.get.length).toBe(1 + 2 + 1 + 2); // two model infos + job status + get links/node
      expect(
        axiosMock.mock.history.get[
          axiosMock.mock.history.get.length - 1
        ].url.includes(JOB_ID_USED),
      ).toBeTruthy();

      await editSession.reload();
      expect(editSession.computationState).toBe('computed');

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(1);
      const [modelSection] = modelSections;
      expect(modelSection.key.startsWith(MODEL_SECTION_PREFIX)).toBe(true);
      expect(modelSection.key).toBe(requestedKeyName);
      expect(new Date(modelSection.dateFrom)).toEqual(
        modificationDateFromRounded,
      );
    });

    it('Removes calculation of deleted scenario', async () => {
      applyMocks();
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      const dbModelSections = await ModelSection.findAll();
      expect(dbModelSections.length).toBe(1);
      const dbModelSectionKey = dbModelSections[0].key;

      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/save`)
        .expect(200);
      await appInstance
        .post(`/scenarios/${baseScenarioId}/edit/leave`)
        .expect(200);
      await appInstance.delete(`/scenarios/${baseScenarioId}`).expect(200);

      await waitForServer(50); // Slight wait as modelSections are removed after scenario delete
      expect(axiosMock.mock.history.delete.length).toBe(1);
      expect(
        axiosMock.mock.history.delete[0].url.includes(dbModelSectionKey),
      ).toBeTruthy();
      const dbModelSectionsAfterDelete = await ModelSection.findAll();
      expect(dbModelSectionsAfterDelete.length).toBe(0);
    });

    it('Await traffic job result until modelAPI is done', async () => {
      applyMocks({ jobStatusResponse: { status: 'RUNNING' } });
      mockAllGetLink();
      mockAllGetNode();
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(20);
      const dbModelSectionsUnfinished = await ModelSection.findAll();
      expect(dbModelSectionsUnfinished.length).toBe(0);

      mockGetTrafficJobStatus({ response: { status: 'FINISHED' } });
      await waitForServer(2500);
      const dbModelSectionsFinished = await ModelSection.findAll();
      expect(dbModelSectionsFinished.length).toBe(1);
      expect(axiosMock.mock.history.get.length).toBe(2 + 2 + 3);
      await waitForServer(100);
    });
  });

  describe('Multiple modifications calculation', () => {
    it('Two modifications with gap test, model sections dont have guaranteed order', async () => {
      const datesFrom = Array.from(Array(2), (_, x) =>
        addHours(startOfHour(new Date()), 2 * x),
      );
      const datesTo = Array.from(Array(2), (_, x) =>
        addHours(startOfHour(new Date()), 2 * x + 1),
      );
      for (let i = 0; i < 2; i++) {
        // eslint-disable-next-line no-await-in-loop
        await modificationFactory(
          {
            type: 'node',
            mode: 'existing',
            dateFrom: datesFrom[i],
            dateTo: datesTo[i],
          },
          { attachedEventModel: baseEvent },
        );
      }
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockAllGetLink();
      mockAllGetNode();
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(2);
      expect(axiosMock.mock.history.get.length).toBe(2 + 2 + 6); // 2 model infos + 2 job status + get links/nodes

      await editSession.reload();
      expect(editSession.computationState).toBe('computed');

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(2);
      const expectedDatePairs = [
        [datesFrom[0].toISOString(), datesTo[0].toISOString()],
        [datesFrom[1].toISOString(), datesTo[1].toISOString()],
      ];
      for (let i = 0; i < modelSections.length; i++) {
        const { key, dateFrom, dateTo } = modelSections[i];
        const modelSectionDates = [dateFrom, dateTo];
        expect(key.startsWith(MODEL_SECTION_PREFIX)).toBe(true);
        expect(expectedDatePairs).toContainEqual(modelSectionDates);
      }
    });

    it('Two modifications without date restriction test', async () => {
      const secondDateFrom = startOfHour(new Date());
      const firstDateTo = addHours(startOfHour(new Date()), 24);

      await modificationFactory(
        {
          type: 'node',
          mode: 'existing',
          dateFrom: null,
          dateTo: firstDateTo,
        },
        { attachedEventModel: baseEvent },
      );
      await modificationFactory(
        {
          type: 'node',
          mode: 'existing',
          dateFrom: secondDateFrom,
          dateTo: null,
        },
        { attachedEventModel: baseEvent },
      );

      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockAllGetNode();
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(3);
      expect(axiosMock.mock.history.get.length).toBe(2 + 3 + 6);

      await editSession.reload();
      expect(editSession.computationState).toBe('computed');

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(3);

      const expectedDatePairs = [
        [null, secondDateFrom.toISOString()],
        [secondDateFrom.toISOString(), firstDateTo.toISOString()],
        [firstDateTo.toISOString(), null],
      ];
      for (let i = 0; i < modelSections.length; i++) {
        const modelSection = modelSections[i];
        const { dateFrom, dateTo, key } = modelSection;
        expect(key.startsWith(MODEL_SECTION_PREFIX)).toBe(true);
        const modelSectionDates = [dateFrom, dateTo];
        expect(expectedDatePairs).toContainEqual(modelSectionDates);
      }
    });

    it('merges two modification duplicates, model items are cached and is fetched once', async () => {
      const dateFrom = startOfHour(new Date());
      const dateTo = addHours(startOfHour(new Date()), 24);
      const LINK_ID = 10;
      await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom,
          dateTo,
          data: { linkId: [LINK_ID], speed: 50, capacity: 500 },
        },
        { attachedEventModel: baseEvent },
      );
      await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom,
          dateTo,
          data: { linkId: [LINK_ID], speed: 0, capacity: 1000 },
        },
        { attachedEventModel: baseEvent },
      );
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockGetLink({ linkId: LINK_ID });
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(1);
      const postData = JSON.parse(axiosMock.mock.history.post[0].data);
      expect(postData.update.length).toBe(1);
      expect(postData.update[0].capacity).toBe(500);
      expect(postData.update[0].cost).toBe(1000);
      expect(axiosMock.mock.history.get.length).toBe(2 + 1 + 1); // 2 model infos + 1 get link (cached for second time) + 1 get traffic status

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(1);
    });
    // TODO test params are correctly built with all typeModes mods
    it('reuse calculation of same situations', async () => {
      const dateFromLong = startOfHour(new Date());
      const dateToLong = addDays(dateFromLong, 3);
      const dateFromShort = addDays(dateFromLong, 1);
      const dateToShort = addDays(dateFromLong, 2);
      const LINK_ID_LONG = 10;
      const LINK_ID_SHORT = 20;
      await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom: dateFromLong,
          dateTo: dateToLong,
          data: { linkId: [LINK_ID_LONG], speed: 50, capacity: 500 },
        },
        { attachedEventModel: baseEvent },
      );
      await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom: dateFromShort,
          dateTo: dateToShort,
          data: { linkId: [LINK_ID_SHORT], speed: 0, capacity: 1000 },
        },
        { attachedEventModel: baseEvent },
      );
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockGetLink({ linkId: LINK_ID_LONG });
      mockGetLink({ linkId: LINK_ID_SHORT });
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(2);

      await editSession.reload();
      expect(editSession.computationState).toBe('computed');

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(3);
    });

    it('return warnings from calculation when modification could not connect on network', async () => {
      const dateFrom = startOfHour(new Date());
      const dateTo = addDays(dateFrom, 3);
      const LINK_ID = 10;
      await modificationFactory(
        {
          type: 'link',
          mode: 'existing',
          dateFrom,
          dateTo,
          data: { linkId: [LINK_ID], speed: 50, capacity: 500 },
        },
        { attachedEventModel: baseEvent },
      );
      await modificationFactory(
        {
          type: 'node',
          mode: 'new',
          dateFrom,
          dateTo,
          data: { coordinates: [0, 0] },
        },
        { attachedEventModel: baseEvent },
      );
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockGetLink({ linkId: LINK_ID });
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(1);

      await editSession.reload();
      expect(editSession.computationWarning).toBe(true);
    });
  });

  describe('Model API features usage', () => {
    it('uses modelAPI features to calculate modification of existing mode', async () => {
      const modification = await modificationFactory(
        { type: 'link', mode: 'existing' },
        { attachedEventModel: baseEvent },
      );
      const linkId = modification.data.link;
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);

      applyMocks();
      mockGetLink({ linkId });
      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.get.length).toBe(2 + 2 + 1); // 2 model infos + 1 get link + 1 get traffic status + get nodes
      expect(
        axiosMock.mock.history.get[0].url.includes('edges') &&
          axiosMock.mock.history.get[0].url.includes(linkId),
      ).toBeTruthy();
    });

    it('Calculation assigns new features id base on modelInfo, extra features codeList is returned with modelSection', async () => {
      const modification = await modificationFactory(
        { type: 'link', mode: 'new' },
        { attachedEventModel: baseEvent },
      );
      const isTwoWay = modification.data.twoWay;
      editSession = await createEditSession(baseScenario, baseUser);
      editSession = await simulateComputationRequired(editSession);
      applyMocks();

      await requestEditSessionComputation(baseScenarioId);
      await waitForServer(SERVER_PROCESSING_TIMEOUT);
      expect(axiosMock.mock.history.post.length).toBe(1);
      const postData = JSON.parse(axiosMock.mock.history.post[0].data);
      const { updateEdges } = postData;
      expect(updateEdges.length).toBe(isTwoWay ? 2 : 1);
      expect(updateEdges[0].edge_id).toBe(11); // 10 maximalLinkId mocked by default

      const calculatedScenario = await requestEditSessionScenario(
        baseScenarioId,
      );
      const { modelSections } = calculatedScenario.body.scenario;
      expect(modelSections.length).toBe(1);
      const { codeList } = modelSections[0];
      expect(Object.keys(codeList.link).length).toBe(isTwoWay ? 2 : 1);
      const possibleExtraIds = [
        `extra-${modification.itemId}`,
        `extra-${modification.itemId}-b`,
      ];
      expect(possibleExtraIds.includes(codeList.link[11])).toBeTruthy();
    });
  });
});

/*
  4] It handles calculations of multiple modifications 
*/
// TODO multiple calculations test - implementation might change soon
// TODO Calculation fail tests (not properly handled yet)
