import { getAppInstance, truncateDB } from './helpers/general.js';
import { userFactory } from '../../db/factories/index.js';
import config from '../../config/env.js';

let appInstance;
let user;
let rawCredentials;

beforeEach(async () => {
  await truncateDB();
  appInstance = getAppInstance();
  ({ user, rawCredentials } = await userFactory());
});

describe('Auth routes', () => {
  describe('Login', () => {
    const expectedCookieName =
      config.TM_APP_NAMESPACE ?? config.TM_MODEL_DTA_NAME;

    it('Implements cookie session login', async () => {
      const res = await appInstance
        .post('/auth/login')
        .send(rawCredentials)
        .expect(200);
      expect(res.header['set-cookie'][0]).toContain(`${expectedCookieName}=`);
    });

    it('Login possible also with email', async () => {
      const res = await appInstance
        .post('/auth/login')
        .send(rawCredentials)
        .expect(200);
      expect(res.header['set-cookie'][0]).toContain(`${expectedCookieName}=`);
    });

    it('Returns logged user information after redirect, without secrets', async () => {
      const res = await appInstance.post('/auth/login').send(rawCredentials);

      expect(res.body).toHaveProperty('username', user.username);
      expect(res.body).not.toHaveProperty('password');
      expect(res.body).toHaveProperty('id', user.id);
      expect(res.body).toHaveProperty('roles', user.roles);
      expect(res.body).toHaveProperty('rights');
    });

    it('Send error on faulty user credentials', async () => {
      await appInstance
        .post('/auth/login')
        .send({
          username: user.username,
          password: 'faulty-password',
        })
        .expect(401);

      await appInstance
        .post('/auth/login')
        .send({
          username: 'missing-user',
          password: rawCredentials.password,
        })
        .expect(401);

      const invalidUsername = {
        username: 'sho',
        password: rawCredentials.password,
      };
      await appInstance.post('/auth/login').send(invalidUsername).expect(400);

      const invalidWhitespaceUsername = {
        username: ' sho  ',
        password: rawCredentials.password,
      };
      await appInstance
        .post('/auth/login')
        .send(invalidWhitespaceUsername)
        .expect(400);

      const invalidPassword = {
        username: user.username,
        password: 'short',
      };
      await appInstance.post('/auth/login').send(invalidPassword).expect(400);
    });
  });

  describe('Logout', () => {
    it('Clears cookie on logout', async () => {
      await appInstance.sessionLogin(rawCredentials);
      const logoutRes = await appInstance.post('/auth/logout').expect(200);
      expect(logoutRes.header['set-cookie'][0]).toContain('tm.api=;');
    });
  });

  describe('Authentication system', () => {
    it('Enables protected route only after authentication', async () => {
      await appInstance.post('/scenarios').expect(401);
      await appInstance.sessionLogin(rawCredentials);
      // We still does not pass next middleware (validation)
      await appInstance.post('/scenarios').expect(400);
    });

    it('Protect routes after user logout', async () => {
      await appInstance.sessionLogin(rawCredentials);
      await appInstance.post('/auth/logout');

      await appInstance.post('/scenarios').expect(401);
    });
  });
});
