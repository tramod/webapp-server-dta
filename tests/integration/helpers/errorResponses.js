const requestWithBody = async (
  appInstance,
  methodName,
  url,
  body,
  statusCode,
) => appInstance[methodName](url).send(body).expect(statusCode);

const request = async (appInstance, methodName, url, statusCode) =>
  appInstance[methodName](url).expect(statusCode);

export const expectInvalidInputError = async (
  appInstance,
  options = {},
  testOptions,
) =>
  expectErrorResponse(
    appInstance,
    { statusCode: 400, ...options },
    testOptions,
  );

export const expectNotFoundError = async (
  appInstance,
  options = {},
  testOptions,
) =>
  expectErrorResponse(
    appInstance,
    { statusCode: 404, ...options },
    testOptions,
  );

async function expectErrorResponse(
  appInstance,
  { data, statusCode, invalidFields, url, methodName = 'post' } = {},
  { customExpectFn } = {},
) {
  if (!appInstance || !url || (methodName === 'post' && !data))
    throw Error('Test helper input invalid');
  const response = data
    ? await requestWithBody(appInstance, methodName, url, data, statusCode)
    : await request(appInstance, methodName, url, statusCode);

  if (customExpectFn) customExpectFn(response);

  if (invalidFields && invalidFields.length > 0) {
    const invalidProperties = response.body.error.map((err) => err.param);
    // Error fields mismatch
    expect(invalidFields.sort()).toEqual(invalidProperties.sort());
  }
}
