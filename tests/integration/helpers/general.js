import request from 'supertest';
import app, { sessionStore } from '../../../app.js';
import models from '../../../db/index.js';

async function truncateDB() {
  return Promise.all(
    Object.keys(models).map((key) => {
      if (['sequelize', 'Sequelize'].includes(key)) return null;
      return models[key].truncate({ restartIdentity: true, cascade: true });
    }),
  );
}

function getAppInstance(sourceApp = app) {
  const agentInstance = request.agent(sourceApp);

  agentInstance.sessionLogin = async function sessionLogin(credentials) {
    await this.sessionLogout();
    const response = await this.post('/auth/login').send(credentials);
    if (response.statusCode !== 200)
      throw Error(`Test login: Session login failed`);
  };

  agentInstance.sessionLogout = async function sessionLogout() {
    await this.post('/auth/logout');
  };

  return agentInstance;
}

async function waitForServer(timeoutMs) {
  return new Promise((resolve) => setTimeout(resolve, timeoutMs));
}

// Sync session store
sessionStore.sync();

export { getAppInstance, truncateDB, waitForServer };
