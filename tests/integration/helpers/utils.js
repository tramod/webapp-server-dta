import { expectInvalidInputError } from './errorResponses.js';

/**
 * Invalid params specific test unit, should be used on single request
 * @param {object} options
 * @param {object} options.appInstance
 * @param {array} options.cases List of test cases, expected entry [testname, data array/object, invalidFields]
 * @param {string} options.methodName MethodName for request
 * @param {string} options.url Static request url
 * @param {function} options.getUrlFn Dynamic request url (gets case data array/object as parameter)
 * @param {object} options.body Static request body
 * @param {function} options.getBodyFn Dynamic request body  (gets case data array/object as parameter)
 */
export const invalidParamsTestUtil = async ({
  methodName = 'get',
  appInstance,
  cases,
  url,
  getUrlFn,
  body,
  getBodyFn,
} = {}) => {
  const results = await Promise.all(
    cases.map(async ([testName, params, invalidFields]) => {
      try {
        const data = getBodyFn ? getBodyFn(params) : body;
        await expectInvalidInputError(appInstance, {
          url: url || getUrlFn(params),
          methodName,
          invalidFields,
          data,
        });
        return null;
      } catch (error) {
        return { testName, error };
      }
    }),
  );
  parseResults(results);
};

/**
 * Apply testFn on multiple items of array in parallel (without DB cleaning)
 * @param appInstance
 * @param {function} testFn Test function, with appInstance, requestOptions, testOptions params. ErrorResponses supported
 * @param {array} cases List of test cases, these are passed to testFn as requestOptions
 * @param {object} testOptions Common test options passed into testFn
 */
export const routesTestUnit = async ({
  appInstance,
  cases,
  testFn,
  testOptions,
} = {}) => {
  const results = await Promise.all(
    cases.map(async (requestOptions) => {
      try {
        await testFn(appInstance, requestOptions, testOptions);
        return null;
      } catch (error) {
        return {
          testName: requestOptions.testName || requestOptions.url,
          error,
        };
      }
    }),
  );
  parseResults(results);
};

function parseResults(results) {
  const badResults = results.filter((result) => result !== null);
  if (badResults.length > 0) {
    for (const result of badResults) {
      // eslint-disable-next-line no-console
      console.error(result.error);
    }
    const testNames = badResults.reduce(
      (string, { testName }) => `${string}${testName},`,
      'Failed cases: ',
    );
    throw new Error(testNames);
  }
}
