import MockAdapter from 'axios-mock-adapter';
import { getAPIInstance } from '../../../app/services/modelAPI.js';

class AxiosMock {
  constructor() {
    this.mock = null;
    this.activeTM = null;
    this.mocks = [];
  }

  findMock(modelId) {
    return this.mocks.find((mock) => mock.id === modelId);
  }

  registerInstance(trafficModel, { force, delay = 0 } = {}) {
    const registeredMock = this.findMock(trafficModel.id);
    if (registeredMock && !force) return;
    if (registeredMock) {
      this.removeMock(trafficModel);
    }
    const api = getAPIInstance(trafficModel);
    const mock = new MockAdapter(api.axios, {
      delayResponse: delay,
      onNoMatch: 'throwException',
    });
    this.mocks.push({ instance: mock, id: trafficModel.id, trafficModel });
  }

  setInstance(trafficModel) {
    const mock = this.findMock(trafficModel.id);
    if (!mock) throw Error('Axios mock api instance not found');
    this.mock = mock.instance;
    this.activeTM = mock.trafficModel;
  }

  removeMock(trafficModel) {
    const mockToRemove = this.findMock(trafficModel.id);
    if (!mockToRemove) return;
    mockToRemove.instance.restore();
    this.mocks = this.mocks.filter((m) => m.id !== mockToRemove.id);
  }
}

const axiosMock = new AxiosMock();

export { axiosMock };

export const resetAxiosRequestHistory = (all = false) => {
  if (all) axiosMock.mocks.forEach((mock) => mock.instance.resetHistory());
  else axiosMock.mock.resetHistory();
};

export const restoreAxiosMock = (all = false) => {
  if (all) axiosMock.mocks.forEach((mock) => mock.instance.restore());
  else axiosMock.mock.restore();
};

export const mockGetTurnRestrictionByNode = ({
  response = [],
  code = 200,
  nodeId = 1,
} = {}) => {
  axiosMock.mock
    .onGet(
      `${axiosMock.activeTM.endPoint}/trs/${axiosMock.activeTM.name}/${nodeId}`,
    )
    .reply(code, response);
};

const getLinkUrl = () =>
  new RegExp(
    `${axiosMock.activeTM.endPoint}/edges/${axiosMock.activeTM.name}/[\\w\\d-]*`,
  );
const getNodeUrl = () =>
  new RegExp(
    `${axiosMock.activeTM.endPoint}/nodes/${axiosMock.activeTM.name}/[\\w\\d-]*`,
  );

export const mockAllGetLink = ({
  response = {
    type: 'Feature',
    properties: {
      edge_id: 1,
      source: 1753,
      target: 1754,
      capacity: 1000.0,
      cost: 0.00530268531,
      speed: 19.9999275,
      type: 0,
    },
    geometry: {
      type: 'LineString',
      coordinates: [
        [13.3798724949666, 49.7544716669304],
        [13.381328663724, 49.7546099757768],
      ],
    },
  },
  code = 200,
} = {}) => {
  axiosMock.mock.onGet(getLinkUrl()).reply(code, response);
};

export const mockAllGetNode = ({ response = [], code = 200 } = {}) => {
  axiosMock.mock.onGet(getNodeUrl()).reply(code, response);
};

export const mockGetLink = ({
  response = {
    type: 'Feature',
    properties: {
      edge_id: 1,
      source: 1753,
      target: 1754,
      capacity: 1000.0,
      cost: 0.00530268531,
      speed: 19.9999275,
      type: 0,
    },
    geometry: {
      type: 'LineString',
      coordinates: [
        [13.3798724949666, 49.7544716669304],
        [13.381328663724, 49.7546099757768],
      ],
    },
  },
  code = 200,
  linkId = 1,
} = {}) => {
  axiosMock.mock
    .onGet(
      `${axiosMock.activeTM.endPoint}/edges/${axiosMock.activeTM.name}/${linkId}`,
    )
    .reply(code, response);
};

export const mockGetGenerator = ({
  response = {
    properties: {
      zone_id: 1,
      node_id: 2,
      trips: 15,
      incoming_trips: 15,
      outgoing_trips: 15,
    },
  },
  code = 200,
  generatorId = 1,
} = {}) => {
  axiosMock.mock
    .onGet(
      `${axiosMock.activeTM.endPoint}/zones/${axiosMock.activeTM.name}/${generatorId}`,
    )
    .reply(code, response);
};

export const mockGetModelInfo = ({
  code = 200,
  type = 'link',
  returnedNumber = 10,
} = {}) => {
  const requestType = type === 'link' ? 'edges' : 'nodes';
  const response = {
    ...(type === 'link' && { maximalEdgeId: returnedNumber }),
    ...(type === 'node' && { maximalNodeId: returnedNumber }),
  };
  axiosMock.mock
    .onGet(
      `${axiosMock.activeTM.endPoint}/${requestType}/${axiosMock.activeTM.name}/info`,
    )
    .reply(code, response);
};

export const mockPostTrafficJob = ({
  response = { jobId: 1 },
  code = 200,
} = {}) =>
  axiosMock.mock
    .onPost(`${axiosMock.activeTM.endPoint}/jobs/${axiosMock.activeTM.name}`)
    .reply(code, response);

const killTrafficJobUrl = () =>
  new RegExp(
    `${axiosMock.activeTM.endPoint}/jobs/${axiosMock.activeTM.name}/[a-z0-1]\\w*`,
  );
export const mockKillTrafficJob = ({ code = 200 } = {}) =>
  axiosMock.mock.onDelete(killTrafficJobUrl()).reply(code);

const getTrafficJobUrl = () =>
  new RegExp(
    `${axiosMock.activeTM.endPoint}/jobs/${axiosMock.activeTM.name}/[a-z0-1]\\w*/status`,
  );
export const mockGetTrafficJobStatus = ({
  response = { status: 'FINISHED' },
  code = 200,
} = {}) => axiosMock.mock.onGet(getTrafficJobUrl()).reply(code, response);

const deleteTrafficCacheUrl = () =>
  new RegExp(
    `${axiosMock.activeTM.endPoint}/caches/${axiosMock.activeTM.name}/[\\w\\d-]*`,
  );
export const mockDeleteTrafficCache = ({ response, code = 200 } = {}) =>
  axiosMock.mock.onDelete(deleteTrafficCacheUrl()).reply(code, response);

export const mockLoadModel = ({ response, code = 200 } = {}) =>
  axiosMock.mock
    .onGet(
      `${axiosMock.activeTM.endPoint}/models/${axiosMock.activeTM.name}/load`,
    )
    .reply(code, response);
