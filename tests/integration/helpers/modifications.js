// eslint-disable-next-line import/prefer-default-export
export const reuseTypeMode = (modification) => {
  const { mode, type } = modification;
  return { mode, type };
};

export const reuseModData = (modification) => {
  const modData = { ...modification.get(), ...modification.data };
  delete modData.data;
  delete modData.createdAt;
  delete modData.updatedAt;
  delete modData.typeMode;
  delete modData.rowId;
  delete modData.itemId;
  delete modData.eventRowId;
  return modData;
};
