import '../config/env.js';
import { closeDbConnection } from '../db/index.js';
import { getTrafficModelData } from '../db/factories/index.js';
import { axiosMock } from './integration/helpers/mockAxios.js';

beforeAll(async () => {
  const trafficModelData = await getTrafficModelData({ id: 1 });
  axiosMock.registerInstance(trafficModelData);
  axiosMock.setInstance(trafficModelData);
});

afterAll(() => closeDbConnection());
