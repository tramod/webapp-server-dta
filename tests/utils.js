// eslint-disable-next-line import/prefer-default-export
export async function waitForServer(timeoutMs) {
  return new Promise((resolve) => setTimeout(resolve, timeoutMs));
}
