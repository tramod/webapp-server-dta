export default {
  testEnvironment: 'node',
  transform: {},
  testMatch: ['**/integration/**/?(*.)+(spec|test).[jt]s'],
  setupFilesAfterEnv: ['./tests/jest.setup.js'],
  verbose: true,
};
