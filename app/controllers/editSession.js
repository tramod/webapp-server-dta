import scenarioService from '../services/scenarios.js';
import scenarioCopyService from '../services/scenariosCopy.js';
import editSessionService from '../services/editSession.js';
import {
  formatScenarioResponse,
  areModelSectionsValid,
} from '../services/formatters.js';
import { TmNotAuthorized, TmErrorGeneral } from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import config from '../../config/env.js';
import { configEvents } from '../services/eventBus.js';
import logger from '../utils/logger.js';

// constant in normal mode, can be changed only in tests
let EDIT_MODE_ACTIVE = !config.TM_EDIT_MODE_DISABLE;

configEvents.on('CONFIG:CHANGED:TM_EDIT_MODE_DISABLE', (value) => {
  EDIT_MODE_ACTIVE = !value;
});

const logMessages = {
  STARTED: 'Scenario edit session started',
  RESTORED: 'Scenario edit session restored',
  COMPUTE_START: 'Scenario edit session computation requested',
  CANCEL: 'Scenario edit session changes canceled',
  SAVE_COPY: 'Scenarios edit session saved in copy',
};
const activity = new ActivityLog({
  logger,
  messages: logMessages,
  scope: 'EDIT_SESSION',
});

export const enterEditSession = async (req, res) => {
  const { scenarioId } = req.params;
  const { user } = req;
  const { ability } = req.$tm;
  if (!EDIT_MODE_ACTIVE)
    throw new TmErrorGeneral('Edit session mode not active on server', {
      customCode: 'EDIT_MODE_DISABLED',
      level: 'warn',
    });

  const scenario = await scenarioService.getScenario(scenarioId, user.id, {
    allEvents: true, // TODO  delete?
    allCalculations: true,
    trafficModel: true,
  });

  if (!scenario || ability.cannot('edit:mode', scenario.formatForAbility()))
    throw new TmNotAuthorized(
      'UnAuthorized to enter scenario edit mode',
      getLogParams(req),
    );

  const hasModelSectionsValid = areModelSectionsValid(
    scenario.scenarioModelSections,
  );
  const isTrafficModelValid = scenario.trafficModel.isValid;
  if (!hasModelSectionsValid || !isTrafficModelValid)
    throw new TmErrorGeneral(
      'Cannot start scenario edit session while scenario model refresh is running',
      {
        customCode: 'EDIT_MODEL_REFRESH',
      },
      getLogParams(req),
    );

  const oldSession = await editSessionService.getEditSession(scenarioId, {
    includeUpdated: false,
  });
  if (oldSession) {
    // Old invalid session, remove and pass
    if (!oldSession.isValid())
      await editSessionService.removeSession(oldSession);
    // Session inactive or active by requesting user
    else if (!oldSession.isActive() || user.id === oldSession.userId) {
      // Edit:mode:return (editor only) should be enough, as inserter can make only non-computable changes, which are save/cancel
      if (ability.cannot('edit:mode:return', scenario.formatForAbility()))
        throw new TmNotAuthorized(
          'UnAuthorized to enter scenarios existing edit mode',
          getLogParams(req),
        );

      activity.logReq(req, 'RESTORED', { id: oldSession.id });
      await editSessionService.setEditSessionUser({
        session: oldSession,
        user,
      });
      return res.json({
        status: 'EDIT_ENTERED',
        message: 'Edit session activated and entered',
        editSession: oldSession.toLocalPropObject(),
      });
    } else
      throw new TmErrorGeneral(
        'Edit session already activated by other user',
        {
          customCode: 'EDIT_OCCUPIED',
        },
        getLogParams(req, {
          otherUser: oldSession.userId,
          editId: oldSession.id,
        }),
      ); // Other user currently editing
  }

  const newSession = await editSessionService.initEditSession({
    source: scenario,
    user,
  });
  activity.logReq(req, 'STARTED', { id: newSession.id });
  return res.json({
    status: 'EDIT_ENTERED',
    message: 'Edit session created and entered',
    editSession: newSession.toLocalPropObject(),
  });
};

export const computeEditSession = async (req, res) => {
  const { editSession } = req.$tm;
  const { saveOnComputed } = req.query;

  if (!editSession.hasComputableChange)
    throw new TmErrorGeneral(
      'There are no changes to compute in edit session',
      {
        customCode: 'EDIT_NOT_COMPUTABLE',
      },
      getLogParams(req, { editId: editSession.id }),
    );

  if (saveOnComputed) await editSession.update({ saveOnComputed });
  const sessionComputing = await editSessionService.computeSession(editSession);

  activity.logReq(req, 'COMPUTE_START');
  return res.json({
    message: 'Scenario is being computed',
    status: 'EDIT_COMPUTATION_STARTED',
    editSession: sessionComputing.toLocalPropObject(),
  });
};

export const saveEditSession = async (req, res) => {
  const { editSession } = req.$tm;
  const { user } = req;
  const { leaveOnSaved, saveAsCopy } = req.query;

  if (!editSession.hasChange)
    throw new TmErrorGeneral(
      'Cannot save edit session if nothing changed, cancel/close to proceed.',
      {
        customCode: 'EDIT_NOT_CHANGED',
      },
      getLogParams(req, { editId: editSession.id }),
    );

  if (editSession.computationState === 'error')
    throw new TmErrorGeneral(
      'Error happened while computing session, cannot save changes',
      {
        customCode: 'EDIT_COMPUTATION_ERROR',
      },
      getLogParams(req, { editId: editSession.id }),
    );

  if (
    editSession.hasComputableChange &&
    ['notStarted', 'inProgress'].includes(editSession.computationState)
  )
    throw new TmErrorGeneral(
      'Cannot save this scenario without finished computation',
      {
        customCode: 'EDIT_NOT_COMPUTED',
      },
      getLogParams(req, { editId: editSession.id }),
    );

  if (saveAsCopy) {
    const changedScenario = await scenarioService.getScenario(
      editSession.updatedRowId,
      user.id,
      {
        isItemId: false,
      },
    );

    const copiedScenario = await scenarioCopyService.copyScenario({
      scenario: changedScenario,
      user,
      withEvents: true,
      setItemId: true,
      markCopy: true,
    });

    activity.logReq(req, 'SAVE_COPY', { newItem: copiedScenario.itemId });
    return res.status(201).json({
      message: 'Scenario successfully saved as copy, edit session is running',
      status: 'EDIT_SAVED_COPY',
      editSession: editSession.toLocalPropObject(),
      scenario: formatScenarioResponse(copiedScenario, user),
    });
  }

  const afterSave = await editSessionService.saveSession(editSession, {
    leaveOnSaved,
  });
  return res.json({
    message: 'Scenario successfully saved',
    status: 'EDIT_SAVED',
    editSession: afterSave.toLocalPropObject(),
  });
};

export const leaveEditSession = async (req, res) => {
  const { editSession } = req.$tm;
  const session = await editSessionService.leaveSession(editSession);
  return res.json({
    message: 'Scenario edit session left by user',
    status: 'EDIT_LEFT',
    ...(session && { editSession: session.toLocalPropObject() }),
  });
};

export const cancelEditSessionChanges = async (req, res) => {
  const { editSession } = req.$tm;

  const afterCancel = await editSessionService.cancelSessionChanges(
    editSession,
  );
  activity.logReq(req, 'CANCEL');
  return res.json({
    message: 'Scenario edit session changes removed',
    status: 'EDIT_CANCELED_CHANGES',
    editSession: afterCancel.toLocalPropObject(),
  });
};

export const getEditSessionState = async (req, res) => {
  const { editSession } = req.$tm;
  return res.json({
    editSession: editSession.toLocalPropObject(),
  });
};

export const getUsersEditSessions = async (req, res) => {
  const { user } = req;
  const sessions = await editSessionService.getSessionsByUser(user);
  res.json(sessions.map((session) => session.toLocalPropObject()));
};
