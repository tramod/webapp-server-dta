import modificationService from '../services/modification.js';
import scenarioCopyService from '../services/scenariosCopy.js';
import editSessionService from '../services/editSession.js';
import { TmNotFoundError, TmErrorGeneral } from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import {
  formatModificationResponse,
  formatEditSessionResponse,
  formatTrafficModelResponse,
} from '../services/formatters.js';
import logger from '../utils/logger.js';

const logMessages = {
  CREATED: 'Modification created',
  DELETED: 'Modification deleted',
  UPDATED: 'Modification updated',
  COPY: 'Modification copy created',
};
const activity = new ActivityLog({
  logger,
  messages: logMessages,
  scope: 'MODIFICATION',
});

export const createModification = async (req, res) => {
  const { body: resource } = req;
  const { scenario, editSession } = req.$tm;

  const event = scenario.events[0];
  const dependentMods = await modificationService.getDependentMods(
    scenario,
    [resource],
    { dateRange: [resource.dateFrom, resource.dateTo] },
  );
  if (dependentMods.length) {
    throw new TmErrorGeneral(
      'Can not be created due to dependent modifications.',
      {
        statusCode: 400,
        customCode: 'DEPENDENCIES_ERROR',
        data: dependentMods,
      },
      getLogParams(req),
    );
  }

  const modification = await modificationService.createModification(
    event,
    resource,
  );
  await editSessionService.checkChanges(editSession, event.included);

  const formatted = formatModificationResponse(modification);
  activity.logReq(req, 'CREATED', { item: formatted });
  res.status(201).json({
    modification: formatted,
    ...formatEditSessionResponse({
      editSession,
      isComputableChange: event.included,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const deleteModification = async (req, res) => {
  const { scenario, editSession } = req.$tm;
  const { deleteDependencies = false } = req.query;

  const event = scenario.events[0];
  const modification = event.modifications[0];
  const deletedModifications = [];
  let isComputableChange = event.included;

  const dependentModsIds = await modificationService.getDependentMods(
    scenario,
    [modification],
  );
  if (!deleteDependencies && dependentModsIds.length)
    throw new TmErrorGeneral(
      `Can not be deleted! Dependent modifications: ${dependentModsIds}`,
      { statusCode: 400, customCode: 'DEPENDENCIES_ERROR' },
      getLogParams(req),
    );

  if (deleteDependencies && dependentModsIds.length) {
    const { deletedMods, isRemovedIncludedMod } =
      await modificationService.removeModsByIds(scenario, dependentModsIds);

    if (isRemovedIncludedMod) isComputableChange = true;
    deletedModifications.push(...deletedMods);
  }

  await modificationService.deleteModification(modification, event);
  deletedModifications.push({
    evId: event.itemId,
    modId: modification.itemId,
    mode: modification.mode,
    type: modification.type,
  });
  await editSessionService.checkChanges(editSession, isComputableChange);

  activity.logReq(req, 'DELETED', deletedModifications);
  res.json({
    deletedModifications,
    ...formatEditSessionResponse({ editSession, isComputableChange }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getModificationsByEvent = async (req, res) => {
  const { scenario } = req.$tm;
  const { modifications } = scenario.events[0];
  if (!modifications || modifications.length === 0)
    throw new TmNotFoundError('No modifications found', getLogParams(req));

  res.json({
    modifications: formatModificationResponse(modifications),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getModificationById = async (req, res) => {
  const { scenario } = req.$tm;
  const modification = scenario.events[0].modifications[0];
  if (!modification)
    throw new TmNotFoundError('No modification found', getLogParams(req));
  res.json({
    modification: formatModificationResponse(modification),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const updateModification = async (req, res) => {
  const { body: resource } = req;
  const { editSession, scenario } = req.$tm;
  const event = scenario.events[0];
  const eventIncluded = event.included;
  const modification = event.modifications[0];

  // if changing date -> check connected modifications not in that date range
  if (resource.dateFrom !== undefined || resource.dateTo !== undefined) {
    const dependentMods = await modificationService.getDependentMods(
      scenario,
      [
        {
          itemId: modification.itemId,
          type: modification.type,
          mode: modification.mode,
          ...modification.data,
          ...resource,
        },
      ],
      { dateRange: [resource.dateFrom, resource.dateTo] },
    );

    if (dependentMods.length) {
      throw new TmErrorGeneral(
        'Can not be updated due to dependent modifications.',
        {
          statusCode: 400,
          customCode: 'DEPENDENCIES_ERROR',
          data: dependentMods,
        },
        getLogParams(req),
      );
    }
  }

  const {
    modification: updatedModification,
    isComputableChange,
    changed,
  } = await modificationService.updateModification(
    event,
    modification,
    resource,
  );

  await editSessionService.checkChanges(
    editSession,
    isComputableChange && eventIncluded,
  );
  const { oldData, newData, fieldsChanged } = changed;
  activity.logReq(req, 'UPDATED', { oldData, newData });
  res.json({
    modification: formatModificationResponse(updatedModification),
    ...formatEditSessionResponse({
      isComputableChange,
      fieldsChanged,
      editSession,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const copyModification = async (req, res) => {
  const { scenario, editSession } = req.$tm;
  const event = scenario.events[0];
  const eventIncluded = event.included;
  const modification = event.modifications[0];

  const copiedModification = await scenarioCopyService.copyModification({
    modification,
    event,
    // since we are adding new modification to the same event we can not clone the original item ID
    withOriginalItemId: false,
    markCopy: true, // labels the copy by updating its name with '(copy)' string
  });

  await editSessionService.checkChanges(editSession, eventIncluded);
  activity.logReq(req, 'COPY', { newItem: copiedModification.itemId });
  res.status(201).json({
    modification: formatModificationResponse(copiedModification),
    ...formatEditSessionResponse({
      editSession,
      isComputableChange: eventIncluded,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};
