import trafficModelsService from '../services/trafficModels.js';
import { formatTrafficModelResponse } from '../services/formatters.js';
import { getLogParams } from '../utils/logger-utils.js';
import { TmNotFoundError, TmNotAuthorized } from '../utils/error.js';

export const getTrafficModels = async (req, res) => {
  const models = await trafficModelsService.getTrafficModels();
  if (!models || models.length === 0)
    throw new TmNotFoundError('No models found', getLogParams(req));
  res.json({ trafficModels: formatTrafficModelResponse(models) });
};

export const getTrafficModel = async (req, res) => {
  const model = req.$tm.trafficModel;
  if (!model) throw new TmNotFoundError('No models found', getLogParams(req));
  res.json({ trafficModel: formatTrafficModelResponse(model) });
};

export const refreshModel = async (req, res) => {
  const { ability, trafficModel } = req.$tm;

  if (ability.cannot('model:refresh'))
    throw new TmNotAuthorized(
      `UnAuthorized to refresh model`,
      getLogParams(req),
    );
  if (!trafficModel)
    throw new TmNotFoundError('Model not found', getLogParams(req));

  const status = await trafficModelsService.refreshModel(trafficModel);
  res.json({ trafficModel: formatTrafficModelResponse(trafficModel), status });
};
