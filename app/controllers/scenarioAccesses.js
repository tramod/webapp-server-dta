import scenarioAccessService from '../services/scenarioAccesses.js';
import { TmNotFoundError } from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import logger from '../utils/logger.js';

const logMessages = {
  CREATED: 'User access to scenario created',
  DELETED: 'User access to scenario  deleted',
  UPDATED: 'User access to scenario  updated',
};
const activity = new ActivityLog({
  logger,
  messages: logMessages,
  scope: 'SCENARIO_ACCESS',
});

export const getScenarioAccesses = async (req, res) => {
  const { scenario } = req.$tm;
  const accesses = scenario.scenarioAccesses || [];
  if (!accesses.length) {
    throw new TmNotFoundError(
      'No user accesses found to this scenario',
      getLogParams(req),
    );
  }
  res.json({ accesses });
};

export const createScenarioAccess = async (req, res) => {
  const { userId } = req.params;
  const { body: resource } = req;
  const { scenario } = req.$tm;
  const createdAccess = await scenarioAccessService.createScenarioAccess(
    scenario.rowId,
    userId,
    resource,
  );
  activity.logReq(req, 'CREATED', { level: createdAccess.level });
  res.status(201).json({ access: createdAccess });
};

export const updateScenarioAccessByUserId = async (req, res) => {
  const { userId } = req.params;
  const { body: resource } = req;
  const { scenario } = req.$tm;
  const updatedAccess =
    await scenarioAccessService.updateScenarioAccessByUserId(
      scenario.rowId,
      userId,
      resource,
    );
  activity.logReq(req, 'UPDATED', { level: updatedAccess.level });
  res.json({ access: updatedAccess });
};

export const deleteScenarioAccessByUserId = async (req, res) => {
  const { userId } = req.params;
  const { scenario } = req.$tm;
  await scenarioAccessService.deleteScenarioAccessByUserId(
    scenario.rowId,
    userId,
  );
  activity.logReq(req, 'DELETED');
  res.sendStatus(200);
};
