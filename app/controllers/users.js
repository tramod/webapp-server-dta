import usersService from '../services/users.js';
import { formatUserResponse } from '../services/formatters.js';
import { TmNotFoundError, TmNotAuthorized } from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import logger from '../utils/logger.js';

const PASSWORD_RESET_TOKEN_TYPE = 'reset';
const PASSWORD_CREATE_TOKEN_TYPE = 'create';
const RESET_TOKEN_EXPIRATION_TIME = 1000 * 60 * 60; // 1 hour
const CREATE_TOKEN_EXPIRATION_TIME = 1000 * 60 * 60 * 24 * 3; // 3 days
const ADMIN_USER_ROLE = 100;

const logMessages = {
  CREATED: 'User created',
  DELETED: 'User deleted',
  UPDATED: 'User updated',
};
const activity = new ActivityLog({
  logger,
  messages: logMessages,
  scope: 'USER',
});
const hideSensitives = (
  { roles, id, username, organizationId },
  showId = true,
) => ({
  roles,
  ...(showId && { id }),
  username,
  organizationId,
});

export const getUsers = async (req, res) => {
  const { hiddenFields } = req.query;

  if (hiddenFields) {
    const { user: loggedUser } = req;
    const loggedUserRoles = loggedUser.roles || [];
    if (!loggedUserRoles.includes(ADMIN_USER_ROLE))
      throw new TmNotAuthorized(
        'Not authorized to access secret fields for users.',
        getLogParams(req),
      );
  }

  const users = await usersService.getUsers(hiddenFields);
  if (!users || users.length === 0)
    throw new TmNotFoundError('[GetUsers]No users found');

  res.json({ users: users.map((user) => formatUserResponse(user)) });
};

export const getUser = async (req, res) => {
  const { user: loggedUser } = req;
  const { userId } = req.params;
  const loggerUserRoles = loggedUser.roles || [];
  const loggedUserId = loggedUser.id.toString();

  // TODO update authorized check to abilities
  if (
    parseInt(loggedUserId, 10) !== parseInt(userId, 10) &&
    !loggerUserRoles.includes(ADMIN_USER_ROLE)
  )
    throw new TmNotAuthorized(
      'Not authorized to access user.',
      getLogParams(req),
    );

  const user = await usersService.getUser({ id: userId });

  if (!user) throw new TmNotFoundError('No user found', getLogParams(req));

  res.json({ user: formatUserResponse(user) });
};

export const createUser = async (req, res) => {
  const { user: loggedUser, body: resource } = req;
  const loggerUserRoles = loggedUser.roles || [];
  const { organization: orgName } = resource;

  if (!loggerUserRoles.includes(ADMIN_USER_ROLE))
    throw new TmNotAuthorized(
      'Not authorized to create new user.',
      getLogParams(req),
    );

  const organization = await usersService.getOrCreateOrganization({
    name: orgName,
  });

  const newUser = await usersService.createUser(organization, resource);
  activity.logReq(req, 'CREATED', { user: hideSensitives(newUser) });
  res.status(201).json({
    user: formatUserResponse(newUser, organization || {}),
  });
};

export const updateUser = async (req, res) => {
  const { body: resource, user: loggedUser } = req;
  const { userId } = req.params;
  const { newPassword, oldPassword, organization: orgName, email } = resource;
  const loggerUserRoles = loggedUser.roles || [];
  const loggedUserId = loggedUser.id.toString();

  if (
    parseInt(loggedUserId, 10) !== parseInt(userId, 10) &&
    !loggerUserRoles.includes(ADMIN_USER_ROLE)
  )
    throw new TmNotAuthorized(
      'Not authorized to update user.',
      getLogParams(req),
    );

  if (newPassword && parseInt(loggedUserId, 10) !== parseInt(userId, 10)) {
    throw new TmNotAuthorized(
      'Not authorized to change password for another user.',
      getLogParams(req),
    );
  }

  const user = await usersService.getUser({ id: userId });
  if (!user) throw new TmNotFoundError('No user found', getLogParams(req));

  if (newPassword || (email && email !== user.email)) {
    const credentials = await usersService.getCredentials({
      userId,
      password: oldPassword,
    });

    if (!credentials)
      throw new TmNotAuthorized(
        'Current password is not correct.',
        getLogParams(req),
      );

    if (newPassword) await credentials.update({ password: newPassword });
  }

  const organization = await usersService.getOrCreateOrganization({
    name: orgName,
  });

  const updatedUser = await usersService.updateUser(
    user,
    organization,
    resource,
  );

  activity.logReq(req, 'UPDATED', { user: hideSensitives(updatedUser, false) });
  res.json({
    user: formatUserResponse(updatedUser, organization || {}),
  });
};

export const deleteUser = async (req, res) => {
  const { userId } = req.params;
  const { user: loggedUser } = req;
  const loggerUserRoles = loggedUser.roles || [];

  if (!loggerUserRoles.includes(ADMIN_USER_ROLE))
    throw new TmNotAuthorized(
      'Not authorized to delete user.',
      getLogParams(req),
    );

  const user = await usersService.getUser({ id: userId });
  if (!user) throw new TmNotFoundError('No user found', getLogParams(req));
  await usersService.deleteUser(user);
  activity.logReq(req, 'DELETED');
  return res.sendStatus(200);
};

export const getOrganizations = async (req, res) => {
  const organizations = await usersService.getOrganizations();
  if (!organizations || organizations.length === 0)
    throw new TmNotFoundError('No organizations found', getLogParams(req));

  res.json({ organizations });
};

export const requestPasswordReset = async (req, res) => {
  // TODO rate limit this, after production session store decision
  const { email, subject, body, type } = req.body;

  const user = await usersService.getUserByUsernameOrEmail(email, {
    matchUsername: false,
  });

  if (user) {
    // Fake email is correct so its harder to get private info if email is registered
    const token = await usersService.createToken({
      userId: user.id,
      type:
        type === 'create'
          ? PASSWORD_CREATE_TOKEN_TYPE
          : PASSWORD_RESET_TOKEN_TYPE,
      expiration:
        type === 'create'
          ? CREATE_TOKEN_EXPIRATION_TIME
          : RESET_TOKEN_EXPIRATION_TIME,
    });

    await usersService.sendTokenLinkViaEmail(user, token, subject, body);
  }
  res.sendStatus(200);
};

export const resetPassword = async (req, res) => {
  const { userId, token } = req.params;
  const { email, password } = req.body;

  const user = await usersService.getUserByUsernameOrEmail(email, {
    matchUsername: false,
  });
  if (!user || user.id !== userId)
    throw new TmNotFoundError(
      'No user found or reset token has expired',
      getLogParams(req),
    );

  const tokenModel = await usersService.getValidToken({ userId, token });
  if (!tokenModel)
    throw new TmNotAuthorized(
      'No user found or reset token has expired',
      getLogParams(req),
    );

  await usersService.updateCredentials({
    userId,
    password,
  });
  await tokenModel.destroy();

  // TODO send confirm reset email

  res.sendStatus(200);
};
