import { aliases } from '../services/authorization.js';
import { formatUserResponse } from '../services/formatters.js';

export const login = async (req, res) => {
  res.redirect('/user');
};

export const logout = async (req, res, next) => {
  req.logout((err) => {
    if (err) {
      return next(err);
    }
    return res
      .status(200)
      .clearCookie('tm.api', {
        path: '/',
      })
      .send();
  });
};

export const getLoggedUser = async (req, res) => {
  const userResponse = formatUserResponse(req.user);
  const { rules } = req.$tm.ability;
  res.json({ ...userResponse, rights: { rules, aliases } });
};
