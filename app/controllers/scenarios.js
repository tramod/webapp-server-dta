import scenarioService from '../services/scenarios.js';
import scenarioCopyService from '../services/scenariosCopy.js';
import eventService from '../services/events.js';
import modificationService from '../services/modification.js';
import editSessionService from '../services/editSession.js';
import {
  getModelNodeTransit as getModelNodeTransitFromAPI,
  formatModelNodeTransitRules,
} from '../services/modelAPI.js';
import {
  formatScenarioResponse,
  formatEditSessionResponse,
  formatModificationResponse,
  formatTrafficModelResponse,
  areModelSectionsValid,
} from '../services/formatters.js';
import { TmNotFoundError, TmErrorGeneral } from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import config from '../../config/env.js';
import { configEvents } from '../services/eventBus.js';
import logger from '../utils/logger.js';

// constant in normal mode, should be changed only in tests
let EDIT_MODE_ACTIVE = !config.TM_EDIT_MODE_DISABLE;

configEvents.on('CONFIG:CHANGED:TM_EDIT_MODE_DISABLE', (value) => {
  EDIT_MODE_ACTIVE = !value;
});

const logMessages = {
  CREATED: 'Scenario created',
  DELETED: 'Scenario deleted',
  UPDATED: 'Scenario updated',
  PUBLIC_UPDATED: 'Scenario published state updated',
  COPY: 'Scenario copy created',
};
const activity = new ActivityLog({
  logger,
  messages: logMessages,
  scope: 'SCENARIO',
});

export const createScenario = async (req, res) => {
  // TODO Authorize with global right - everyone registered should be able?
  const { user, body: resource } = req;
  const { modelType = 'DTA' } = resource; // Old client compat

  // Simplified scenario creating with just modelType string - using default model id
  const trafficModel = await scenarioService.models.TrafficModel.findOne({
    where: { type: modelType, isDefault: true },
  });
  if (!trafficModel) {
    throw new TmErrorGeneral(
      `No ${modelType} model is available on the server!`,
    );
  }
  const trafficModelId = trafficModel.id;

  // TODO should be probably done in one transaction when re-implemented
  const scenario = await scenarioService.createScenario({
    ...resource,
    user,
    trafficModelId,
  });
  // TODO probably remove?
  if (resource.events) {
    await Promise.all(
      resource.events.map(async (event) => {
        await eventService.createEvent(scenario, {
          ...event,
          user,
        });
      }),
    );
  }

  const formatted = formatScenarioResponse(scenario, user);
  activity.logReq(req, 'CREATED', { item: formatted });
  res.status(201).json({
    scenario: formatted,
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getScenarios = async (req, res) => {
  const { modelType } = req.query;
  const scenarios = await scenarioService.getScenarios(req.user, {
    modelType,
    withModelSections: true,
  });
  if (!scenarios || scenarios.length === 0)
    throw new TmNotFoundError('No scenarios found', getLogParams(req));
  res.json({
    scenarios: formatScenarioResponse(scenarios, req.user, {
      formatSections: false,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const deleteScenario = async (req, res) => {
  const { scenarioId } = req.params;
  const { scenario } = req.$tm;

  if (EDIT_MODE_ACTIVE) {
    const oldSession = await editSessionService.getEditSession(scenarioId, {
      includeUpdated: false,
    });

    if (oldSession) {
      if (oldSession.isActive())
        throw new TmErrorGeneral(
          'Cannot delete scenario while there is active edit session',
          {
            customCode: 'EDIT_SESSION_ACTIVE',
          },
          getLogParams(req, { otherUser: oldSession.user }),
        );
      else await editSessionService.removeSession(oldSession);
    }
  }

  await scenarioService.deleteScenario(scenario);
  activity.logReq(req, 'DELETED');
  return res.sendStatus(200);
};

export const getScenarioById = async (req, res) => {
  const { user } = req;
  const { withEvents } = req.query;
  const { scenario } = req.$tm;
  res.json({
    scenario: formatScenarioResponse(scenario, user, { withEvents }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const updateScenario = async (req, res) => {
  const { body: resource, user } = req;
  const { scenario, editSession } = req.$tm;

  const { scenario: updated, changed } = await scenarioService.updateScenario(
    scenario,
    resource,
  );
  await editSessionService.checkChanges(editSession);

  const { oldData, newData, fieldsChanged } = changed;
  activity.logReq(req, 'UPDATED', { oldData, newData });
  res.json({
    scenario: formatScenarioResponse(updated, user),
    ...formatEditSessionResponse({ fieldsChanged, editSession }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const updateScenarioPublication = async (req, res) => {
  const { body: resource, user } = req;
  const { scenario } = req.$tm;
  const { isPublic, isInPublicList } = resource;

  await scenario.update({ isPublic, isInPublicList });

  activity.logReq(req, 'PUBLIC_UPDATED', { isPublic, isInPublicList });
  res.json({
    scenario: formatScenarioResponse(scenario, user),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const copyScenario = async (req, res) => {
  const { user } = req;
  const { scenario: originalScenario } = req.$tm;

  const hasModelSectionsValid = areModelSectionsValid(
    originalScenario.scenarioModelSections,
  );
  if (!hasModelSectionsValid || !originalScenario.trafficModel.isValid)
    throw new TmErrorGeneral(
      'Cannot copy scenario while scenario model refresh is running',
      {
        customCode: 'SCENARIO_MODEL_REFRESH',
      },
      getLogParams(req),
    );

  const copiedScenario = await scenarioCopyService.copyScenario({
    scenario: originalScenario,
    user,
    withEvents: true,
    copyAuthor: false,
    withAccesses: false, // only sets author as 'editor'
    setItemId: true, // makes the scenario 'published' immediately,
    markCopy: true, // labels the copy by updating its name with '(copy)' string
  });

  activity.logReq(req, 'COPY', { newItem: copiedScenario.itemId });
  res.status(201).json({
    scenario: formatScenarioResponse(copiedScenario, user, {
      modelSections: originalScenario.scenarioModelSections,
      hasModelSectionsValid,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getScenarioNodeTransit = async (req, res) => {
  const { scenario } = req.$tm;
  const { nodeId } = req.params;
  const { isExtraNode } = req.query;

  const getBaseModelNT = async () => {
    if (isExtraNode) return [];
    const baseModelNT = await getModelNodeTransitFromAPI({
      id: nodeId,
      trafficModel: scenario.trafficModel,
    });
    return formatModelNodeTransitRules(baseModelNT, scenario.trafficModel.type);
  };

  const [baseModelNT, scenarioNT] = await Promise.all([
    getBaseModelNT(),
    modificationService.getScenarioNodeTransit(scenario, nodeId),
  ]);

  res.status(200).json([...baseModelNT, ...scenarioNT]);
};

export const getExtraFeatureModifications = async (req, res) => {
  const { scenario } = req.$tm;
  const { useChanged } = req.query;
  const extraFeatures = await modificationService.getModificationsByScenario(
    scenario.rowId,
    { includedOnly: !useChanged, newOnly: true },
  );

  if (!extraFeatures || extraFeatures.length === 0)
    throw new TmNotFoundError('No extra features found', getLogParams(req));
  res.status(200).json({
    modifications: formatModificationResponse(extraFeatures),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};
