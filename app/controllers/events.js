import scenarioCopyService from '../services/scenariosCopy.js';
import eventService from '../services/events.js';
import editSessionService from '../services/editSession.js';
import modificationService from '../services/modification.js';
import { scenarioMock } from '../services/authorization.js';
import {
  formatEventResponse,
  formatEditSessionResponse,
  formatTrafficModelResponse,
} from '../services/formatters.js';
import { formatEventForAbility } from '../services/utils.js';
import {
  TmNotFoundError,
  TmNotAuthorized,
  TmErrorGeneral,
} from '../utils/error.js';
import { getLogParams, ActivityLog } from '../utils/logger-utils.js';
import TmLogger from '../utils/logger.js';

const logMessages = {
  CREATED: 'Event created',
  DELETED: 'Event deleted',
  UPDATED: 'Event updated',
  COPY: 'Event copy created',
};
const activity = new ActivityLog({
  logger: TmLogger,
  messages: logMessages,
  scope: 'EVENT',
});

export const createEvent = async (req, res) => {
  const { user, body: resource } = req;
  const { ability, scenario, editSession } = req.$tm;

  const canInclude = ability.can(
    'create:event:included',
    scenario.formatForAbility(),
  );
  const included =
    canInclude && resource.included !== undefined
      ? resource.included
      : canInclude;

  const event = await eventService.createEvent(scenario, {
    ...resource,
    user,
    included,
  });
  // event is computable change only with modifications
  await editSessionService.checkChanges(editSession);

  const formatted = formatEventResponse(event, user);
  activity.logReq(req, 'CREATED', { item: formatted });
  res.status(201).json({
    event: formatted,
    ...formatEditSessionResponse({ editSession, isComputableChange: included }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const deleteEvent = async (req, res) => {
  const { scenario, editSession } = req.$tm;
  const { deleteDependencies = false } = req.query;

  const event = scenario.events[0];
  const modificationCount = await event.countModifications();
  let isComputableChange = event.included && modificationCount > 0;
  const deletedModifications = [];

  const eventModifications = await event.getModifications();
  const dependentModsIds = await modificationService.getDependentMods(
    scenario,
    eventModifications,
    { deletingEventId: event.rowId },
  );

  if (!deleteDependencies && dependentModsIds.length)
    throw new TmErrorGeneral(
      `Can not be deleted! Dependent modifications: ${dependentModsIds}`,
      { statusCode: 400, customCode: 'DEPENDENCIES_ERROR' },
      getLogParams(req),
    );

  if (deleteDependencies && dependentModsIds.length) {
    const { deletedMods, isRemovedIncludedMod } =
      await modificationService.removeModsByIds(scenario, dependentModsIds);

    if (isRemovedIncludedMod) isComputableChange = true;
    deletedModifications.push(...deletedMods);
  }

  await eventService.deleteEvent(scenario, event);
  deletedModifications.push(
    ...eventModifications.map((mod) => ({
      evId: event.itemId,
      modId: mod.itemId,
      mode: mod.mode,
      type: mod.type,
    })),
  );

  await editSessionService.checkChanges(editSession, isComputableChange);
  activity.logReq(req, 'DELETED', { deletedModifications });
  res.json({
    deletedModifications,
    ...formatEditSessionResponse({ editSession, isComputableChange }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

const parseDeleteEventsResult = (results) => {
  let deleteSuccess = false;
  let deleteError = false;
  let fatalError = false;

  const response = results.map(({ status, id, error = {} }) => {
    const { statusCode, code, message } = error;
    if (status) deleteSuccess = true;
    else deleteError = true;
    if (message && !statusCode) fatalError = error;
    else if (message) TmLogger.logOperational(error);
    return {
      status,
      id,
      ...(message && {
        error: { statusCode, code, message },
      }),
    };
  });

  let statusCode;
  if (deleteSuccess && deleteError) statusCode = 207;
  else if (deleteSuccess) statusCode = 200;
  else statusCode = 400;
  return { statusCode, response, fatalError };
};

// ? DEPRECATED?
export const deleteEvents = async (req, res) => {
  const { ability, scenario, editSession } = req.$tm;
  const scenarioId = scenario.rowId;
  const { events } = scenario;
  if (!events || events.length === 0)
    throw new TmNotFoundError('No events found', getLogParams(req));

  let isComputableChange = false;
  const deleteEventIds = req.query.ids;
  const eventsMap = new Map(events.map((event) => [event.itemId, event]));
  const deleteResults = await Promise.all(
    deleteEventIds.map(async (id) => {
      if (!eventsMap.has(id)) {
        const error = new TmNotFoundError(
          '[DeleteEvent]Event is not included in scenario',
        );
        return { id, status: false, error };
      }
      const event = eventsMap.get(id);
      if (
        ability.cannot(
          'delete:event',
          scenarioMock(scenarioId, {
            events: [formatEventForAbility(event)],
            level: scenario.get('level'),
          }),
        )
      ) {
        const error = new TmNotAuthorized('UnAuthorized to delete event', {
          ...req.params,
          user: req.user?.id,
          eventId: id,
        });
        return { id, status: false, error };
      }

      if (!isComputableChange && event.included) isComputableChange = true;
      try {
        await eventService.deleteEvent(scenario, event, true);
        return { id, status: true };
      } catch (error) {
        return { id, status: false, error };
      }
    }),
  );
  const { statusCode, response, fatalError } =
    parseDeleteEventsResult(deleteResults);
  await editSessionService.checkChanges(editSession, isComputableChange);
  res.status(statusCode).json({
    results: response,
    ...formatEditSessionResponse({ editSession, isComputableChange }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });

  if (fatalError) throw fatalError;
};

export const getEventsByScenario = async (req, res) => {
  const { scenario } = req.$tm;
  const { events } = scenario;
  if (!events || events.length === 0)
    throw new TmNotFoundError('No events found', getLogParams(req));

  res.json({
    events: formatEventResponse(events),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getEventById = async (req, res) => {
  const { scenario } = req.$tm;
  const event = scenario.events[0];
  res.json({
    event: formatEventResponse(event),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const updateEvent = async (req, res) => {
  const { body: resource } = req;
  const { editSession, ability, scenario } = req.$tm;
  const { updateDependencies = false } = req.query;

  if (
    resource.included !== undefined &&
    ability.cannot('update:event:included', scenario.formatForAbility())
  )
    throw new TmNotAuthorized(
      'Unauthorized to update event included',
      getLogParams(req),
    );

  const event = scenario.events[0];
  const wasIncluded = event.included;
  const updatedEvents = [];

  if (resource.included !== undefined) {
    const eventModifications = await event.getModifications();
    const dependentMods = await modificationService.getDependentMods(
      scenario,
      eventModifications,
      {
        ...(!resource.included && { excludingEventId: event.rowId }),
        ...(resource.included && { includingEventId: event.rowId }),
      },
    );

    if (!updateDependencies && dependentMods.length) {
      const inclusionPhrase = (bool) => (bool ? 'included' : 'excluded');
      throw new TmErrorGeneral(
        `Can not be ${inclusionPhrase(
          resource.included,
        )}! Dependent ${inclusionPhrase(
          !resource.included,
        )} modifications: ${dependentMods}`,
        { statusCode: 400, customCode: 'DEPENDENCIES_ERROR' },
        getLogParams(req),
      );
    }

    if (updateDependencies && dependentMods.length) {
      const updatedEventIds = await eventService.updateEventsByModIds(
        scenario,
        dependentMods,
        resource.included,
      );
      updatedEvents.push(...updatedEventIds);
    }
  }

  const {
    event: updatedEvent,
    isComputableChange,
    changed,
  } = await eventService.updateEvent(scenario, event, resource);
  updatedEvents.push(updatedEvent.itemId);
  const { oldData, newData, fieldsChanged } = changed;
  await editSessionService.checkChanges(
    editSession,
    isComputableChange &&
      (wasIncluded || (!wasIncluded && fieldsChanged.includes('included'))) &&
      (await event.countModifications()) > 0,
  );

  activity.logReq(req, 'UPDATED', { oldData, newData, updatedEvents });
  res.json({
    event: formatEventResponse(updatedEvent),
    updatedEvents,
    ...formatEditSessionResponse({
      isComputableChange,
      fieldsChanged,
      editSession,
    }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const copyEvent = async (req, res) => {
  const { user } = req;
  const { editSession, ability, scenario } = req.$tm;

  const event = scenario.events[0];
  // event is always included if it can be included
  const included = ability.can(
    'create:event:included',
    scenario.formatForAbility(),
  );

  const copiedEvent = await scenarioCopyService.copyEvent({
    event,
    scenario,
    user,
    withModifications: true,
    // since we are adding new event to the same scenario we can not clone the original item ID
    withOriginalItemId: false,
    includedOverride: included,
    markCopy: true, // labels the copy by updating its name with '(copy)' string
  });
  await editSessionService.checkChanges(editSession, included);

  activity.logReq(req, 'COPY', { newItem: copiedEvent.itemId });
  res.status(201).json({
    event: formatEventResponse(copiedEvent, user, { withModifications: true }),
    ...formatEditSessionResponse({ editSession, isComputableChange: included }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const getImportableEventsToScenario = async (req, res) => {
  const { scenario } = req.$tm;
  const { user } = req;

  const events = await eventService.getImportableEventsToScenario(
    scenario.rowId,
    user.id,
    { modelType: scenario.trafficModel?.type || null },
  );

  if (!events || events.length === 0)
    throw new TmNotFoundError('No events found', getLogParams(req));

  res.json({
    events: formatEventResponse(events),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};

export const importEvents = async (req, res) => {
  const { user, body } = req;
  const { events } = body;
  const { editSession, ability, scenario } = req.$tm;

  const eventsToBeImported = await eventService.getImportableEventsToScenario(
    scenario.rowId,
    user.id,
    {
      idPairs: events.map((ev) => `${ev.scenarioId}-${ev.eventId}`),
      modelType: scenario.trafficModel?.type || null,
    },
  );

  if (eventsToBeImported.length !== events.length)
    throw new TmNotFoundError('Imported events not found', getLogParams(req));

  // event is always included if it can be included
  const included = ability.can(
    'create:event:included',
    scenario.formatForAbility(),
  );

  const importedEvents = [];

  // TOOD: optimize this?
  await Promise.all(
    eventsToBeImported.map(async (ev) => {
      const copiedEvent = await scenarioCopyService.copyEvent({
        event: ev,
        scenario,
        user,
        withModifications: true,
        // since we are importing event from another scenario we can not clone the original item ID to avoid duplicates
        withOriginalItemId: false,
        includedOverride: included,
        markCopy: true,
        markString: '(import)',
      });

      importedEvents.push(
        formatEventResponse(copiedEvent, user, { withModifications: true }),
      );
    }),
  );

  await editSessionService.checkChanges(editSession, included);
  activity.logReq(req, 'IMPORT EVENTS', { newItems: importedEvents });
  res.status(201).json({
    events: importedEvents,
    ...formatEditSessionResponse({ editSession, isComputableChange: included }),
    trafficModel: formatTrafficModelResponse(req.$tm.trafficModel),
  });
};
