export default (sequelize, { Model, DataTypes }) => {
  class UserToken extends Model {
    static associate(models) {
      UserToken.belongsTo(models.User);
    }
  }
  UserToken.init(
    {
      token: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      expire: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'userToken',
      underscored: true,
    },
  );
  return UserToken;
};
