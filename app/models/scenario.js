import { formatScenarioForAbility } from '../services/utils.js';

export default (sequelize, { Model, DataTypes, Op }) => {
  class Scenario extends Model {
    static associate(models) {
      Scenario.belongsTo(models.User, {
        foreignKey: 'authorUserId',
      });
      Scenario.hasMany(models.Event, {
        onDelete: 'CASCADE',
        hooks: true,
      });
      Scenario.belongsToMany(models.User, {
        through: models.ScenarioAccess,
        as: 'authorizedUsers',
      });
      Scenario.hasMany(models.ScenarioAccess);
      Scenario.hasOne(models.EditSession, {
        as: 'source',
        sourceKey: 'itemId',
        foreignKey: 'sourceItemId',
      });
      Scenario.hasOne(models.EditSession, {
        as: 'updated',
        sourceKey: 'rowId',
        foreignKey: 'updatedRowId',
      });
      Scenario.belongsToMany(models.ModelSection, {
        through: { model: models.ScenarioModelSection, unique: false },
      });
      Scenario.hasMany(models.ScenarioModelSection);
      Scenario.belongsTo(models.TrafficModel);
    }

    static registerScopes(models) {
      // Scope to detect if user has access level in scenario, but passes without access (auth should be done elsewhere, depending on publicity)
      Scenario.addScope(
        'hasUserAccess',
        ({ userId, required = false, levels } = {}) => ({
          include: {
            model: models.ScenarioAccess,
            attributes: [],
            required,
            where: {
              userId,
              ...(levels && { level: { [Op.in]: levels } }),
            },
          },
          attributes: {
            include: [
              [Scenario.sequelize.col('scenarioAccesses.level'), 'level'],
            ],
          },
        }),
      );
      Scenario.addScope('isPublic', () => ({
        where: {
          isPublic: {
            [Op.is]: true,
          },
        },
      }));
      Scenario.addScope('isInPublicList', () => ({
        where: {
          isInPublicList: {
            [Op.is]: true,
          },
        },
      }));
    }

    formatForAbility() {
      // Cache expected to use only for one controller fn, for longer lifetime this should not be cached
      if (this.abilityFormatted) return this.abilityFormatted;
      const formatted = formatScenarioForAbility(this);
      this.abilityFormatted = formatted;
      return formatted;
    }
  }
  Scenario.init(
    {
      rowId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      note: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      dateFrom: {
        type: DataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
      dateTo: {
        type: DataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
      itemId: {
        type: DataTypes.INTEGER,
        unique: true,
      },
      isPublic: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      isInPublicList: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      // Added manually, as we need custom handling
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: 'scenario',
      underscored: true,
      timestamps: true,
      createdAt: false, // Sequelize handling disabled
      indexes: [
        {
          name: 'itemId',
          unique: true,
          fields: ['itemId'],
        },
      ],
      defaultScope: {
        where: {
          itemId: {
            [Op.not]: null,
          },
        },
      },
    },
  );
  return Scenario;
};
