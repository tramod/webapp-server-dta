import argon2 from 'argon2';

// Reassigning is documented pattern inside sequelize hooks
async function hashPassword(credential) {
  if (!credential.changed('password')) return;
  // eslint-disable-next-line no-param-reassign
  credential.password = await argon2.hash(credential.password);
}

export default (sequelize, { Model, DataTypes }) => {
  class Credential extends Model {
    static associate(models) {
      Credential.belongsTo(models.User);
    }

    async isPasswordValid(passwordToValidate) {
      return argon2.verify(this.password, passwordToValidate);
    }
  }
  Credential.init(
    {
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'credential',
      underscored: true,
    },
  );

  Credential.beforeCreate(hashPassword);
  Credential.beforeUpdate(hashPassword);

  return Credential;
};
