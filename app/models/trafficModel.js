export default (sequelize, { Model, DataTypes }) => {
  class TrafficModel extends Model {
    static associate(models) {
      TrafficModel.hasMany(models.Scenario);
      TrafficModel.hasMany(models.ModelSection);
    }
  }
  TrafficModel.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      endPoint: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      endPointApiKey: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: DataTypes.ENUM('DTA'),
        allowNull: false,
      },
      isDefault: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      isValid: {
        type: DataTypes.BOOLEAN,
        default: false,
        allowNull: false,
      },
      isBroken: {
        type: DataTypes.BOOLEAN,
        default: false,
        allowNull: false,
      },
      timestamp: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'trafficModel',
      underscored: true,
    },
  );
  return TrafficModel;
};
