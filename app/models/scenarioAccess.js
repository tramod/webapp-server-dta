export default (sequelize, { Model, DataTypes }) => {
  class ScenarioAccess extends Model {
    static associate(models) {
      ScenarioAccess.belongsTo(models.Scenario);
      ScenarioAccess.belongsTo(models.User);
    }
  }
  ScenarioAccess.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      level: {
        type: DataTypes.ENUM('editor', 'inserter', 'viewer'),
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'scenarioAccess',
      underscored: true,
    },
  );
  return ScenarioAccess;
};
