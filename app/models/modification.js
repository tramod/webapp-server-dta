export default (sequelize, { Model, DataTypes }) => {
  class Modification extends Model {
    static associate(models) {
      Modification.belongsTo(models.Event);
    }
  }
  Modification.init(
    {
      rowId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      itemId: {
        type: DataTypes.INTEGER,
      },
      mode: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      typeMode: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${this.type}-${this.mode}`;
        },
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: '',
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      note: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      dateFrom: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
      dateTo: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
      data: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: 'modification',
      underscored: true,
    },
  );
  return Modification;
};
