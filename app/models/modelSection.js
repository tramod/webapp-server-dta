export default (sequelize, { Model, DataTypes }) => {
  class ModelSection extends Model {
    static associate(models) {
      ModelSection.belongsToMany(models.Scenario, {
        through: { model: models.ScenarioModelSection, unique: false },
      });
      ModelSection.hasMany(models.ScenarioModelSection);
      ModelSection.belongsTo(models.TrafficModel);
    }
  }

  // TODO change PK to key + trafficModelId?
  // Or change key naming eg. PREFIX-MODELID-MAINPART
  ModelSection.init(
    {
      key: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
      },
      hash: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      isValid: {
        type: DataTypes.BOOLEAN,
        default: true,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'modelSection',
      underscored: true,
    },
  );
  return ModelSection;
};
