export default (sequelize, { Model, DataTypes }) => {
  class ScenarioModelSection extends Model {
    static associate(models) {
      ScenarioModelSection.belongsTo(models.Scenario);
      ScenarioModelSection.belongsTo(models.ModelSection);
    }
  }
  ScenarioModelSection.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      codeList: {
        type: DataTypes.JSON,
        allowNull: true,
      },
      dateFrom: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      dateTo: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'scenarioModelSection',
      underscored: true,
      updatedAt: false,
    },
  );
  return ScenarioModelSection;
};
