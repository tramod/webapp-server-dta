export default (sequelize, { Model, DataTypes }) => {
  class Organization extends Model {
    static associate(models) {
      Organization.hasMany(models.User);
    }
  }
  Organization.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: 'organization',
      underscored: true,
    },
  );
  return Organization;
};
