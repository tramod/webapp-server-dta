export default (sequelize, { Model, DataTypes }) => {
  class User extends Model {
    static associate(models) {
      User.hasOne(models.Credential);
      User.hasMany(models.Scenario, {
        foreignKey: 'authorUserId',
      });
      User.hasMany(models.Event, {
        foreignKey: 'authorUserId',
      });

      User.belongsToMany(models.Scenario, {
        through: models.ScenarioAccess,
        as: 'authorizedScenarios',
      });
      User.hasMany(models.ScenarioAccess);

      User.hasMany(models.EditSession);

      User.belongsTo(models.Organization);
    }
  }
  User.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      roles: {
        type: DataTypes.STRING,
        allowNull: true,
        get() {
          const string = this.getDataValue('roles');
          if (string === undefined) return undefined; // Fix for excluded
          return string
            ? string.split('_').map((value) => parseInt(value, 10))
            : null;
        },
        set(value) {
          this.setDataValue(
            'roles',
            Array.isArray(value) ? value.join('_') : value,
          );
        },
      },
    },
    {
      sequelize,
      modelName: 'user',
      underscored: true,
    },
  );
  return User;
};
