export default (sequelize, { Model, DataTypes, Op }) => {
  class Event extends Model {
    static associate(models) {
      Event.belongsTo(models.User, { foreignKey: 'authorUserId' });
      Event.belongsTo(models.Scenario);
      Event.hasMany(models.Modification, {
        onDelete: 'CASCADE',
        hooks: true,
      });
    }

    static registerScopes() {
      Event.addScope('isInScenario', (scenarioId) => ({
        where: { scenarioRowId: scenarioId },
      }));
      Event.addScope('isNotInScenario', (scenarioId) => ({
        where: {
          scenarioRowId: {
            [Op.not]: scenarioId,
          },
        },
      }));
    }
  }
  Event.init(
    {
      rowId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      itemId: {
        type: DataTypes.INTEGER,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      included: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      note: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      dateFrom: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
      dateTo: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
    },
    {
      sequelize,
      modelName: 'event',
      underscored: true,
    },
  );
  return Event;
};
