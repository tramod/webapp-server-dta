import { sub, add } from 'date-fns';

const EDIT_SESSION_MIN_TIMEOUT = 30;

export default (sequelize, { Model, DataTypes }) => {
  class EditSession extends Model {
    static associate(models) {
      EditSession.belongsTo(models.Scenario, {
        as: 'source',
        targetKey: 'itemId',
        foreignKey: 'sourceItemId',
      });
      EditSession.belongsTo(models.Scenario, {
        as: 'updated',
        targetKey: 'rowId',
        foreignKey: 'updatedRowId',
      });
      EditSession.belongsTo(models.User);
    }

    static registerScopes(models, { Op }) {
      EditSession.addScope('isUndecided', {
        where: {
          computationState: { [Op.not]: 'notStarted' },
        },
      });

      EditSession.addScope('isActive', () => ({
        where: {
          [Op.and]: [
            {
              userId: { [Op.not]: null },
            },
            {
              updatedAt: {
                [Op.gte]: sub(Date.now(), {
                  minutes: EDIT_SESSION_MIN_TIMEOUT,
                }),
              },
            },
          ],
        },
      }));

      EditSession.addScope('isValid', () => ({
        where: {
          [Op.or]: [
            { computationState: { [Op.not]: 'notStarted' } },
            {
              [Op.and]: [
                {
                  userId: { [Op.not]: null },
                },
                {
                  updatedAt: {
                    [Op.gte]: sub(Date.now(), {
                      minutes: EDIT_SESSION_MIN_TIMEOUT,
                    }),
                  },
                },
              ],
            },
          ],
        },
      }));
    }

    toLocalPropObject() {
      return {
        scenarioId: this.sourceItemId,
        userId: this.userId,
        hasChange: this.hasChange,
        hasComputableChange: this.hasComputableChange,
        computationState: this.computationState,
        computationTimestamp: this.computationTimestamp,
        computationWarning: this.computationWarning,
        saveOnComputed: this.saveOnComputed,
      };
    }

    isActive() {
      if (!this.userId) return false;
      const updatedBuffer = add(this.updatedAt, {
        minutes: EDIT_SESSION_MIN_TIMEOUT,
      });
      const now = new Date();
      return updatedBuffer - now > 0;
    }

    isUndecided() {
      return this.computationState !== 'notStarted';
    }

    isValid() {
      return this.isUndecided() || this.isActive();
    }

    async updateTimestamp() {
      this.changed('updatedAt', true);
      await this.update({
        updatedAt: new Date(),
      });
    }
  }
  EditSession.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      hasChange: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      hasComputableChange: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      saveOnComputed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      computationState: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'notStarted',
      },
      computationTimestamp: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      computationWarning: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: 'editSession',
      underscored: true,
    },
  );
  return EditSession;
};
