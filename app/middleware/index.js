import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import session from 'express-session';
import sequelizeStoreModule from 'connect-session-sequelize';
import * as rTracer from 'cls-rtracer';
import passport from './passport.js';
import httpLogger from './http-logger.js';
import logger from '../utils/logger.js';
import config from '../../config/env.js';
import decorateRequest from './decorateRequest.js';

const SequelizeStore = sequelizeStoreModule(session.Store);
const { NODE_ENV } = process.env;

const useCors = (app) => {
  const corsOptions = {
    origin: [],
    credentials: true,
  };

  app.use(cors(corsOptions));
};

const useBodyParser = (app) => {
  app.use(
    express.urlencoded({
      extended: false,
      limit: '10mb',
    }),
  );
  app.use(express.json({ limit: '10mb' }));
};

const useHelmet = (app) => {
  app.set('x-powered-by', false);
  // Helmet set up for API server, in case of static/app routes should be changed, some protection done on nginx level
  app.use(helmet.frameguard());
  app.use(helmet.hsts());
  app.use(helmet.permittedCrossDomainPolicies());
};

const SESSION_EXPIRATION = 60 * 60 * 1000;

const createSessionStore = (db) =>
  new SequelizeStore({
    db,
    expiration: SESSION_EXPIRATION,
  });

const createSessionConfig = (sessionStore) => ({
  name: parseCookieName(config.TM_APP_NAMESPACE, config.TM_MODEL_DTA_NAME),
  // Secret should be loaded from env, and periodically changed - old one should be in array
  secret: parseSessionSecret(config.TM_SESSION_SECRET),
  resave: false,
  saveUninitialized: true,
  rolling: true,
  cookie: {
    maxAge: SESSION_EXPIRATION,
    ...(NODE_ENV === 'production' && { secure: true }),
  },
  store: sessionStore,
});

const useTrustProxy = (app) => {
  if (NODE_ENV === 'production')
    app.set('trust proxy', config.TM_APP_PROXY ?? 1); // Expected to be used at least behind one proxy in production
};

const useSession = (app, db) => {
  const sessionStore = createSessionStore(db);
  app.use(session(createSessionConfig(sessionStore)));
  return sessionStore;
};

const usePassport = (app) => {
  app.use(passport.initialize());
  app.use(passport.session());
};

const useHttpLogger = (app) => {
  app.use(httpLogger);
};

function parseSessionSecret(secretConfig) {
  const isConfigValid =
    typeof secretConfig === 'string' && secretConfig[0] !== ',';
  if (!isConfigValid)
    logger.warn(
      'TM_SESSION_SECRET missing in config. Express initialized with default value',
    );
  return isConfigValid ? secretConfig.split(',') : 'dummy_secret';
}

function parseCookieName(cookieConfig, modelNameConfig) {
  const isConfigValid = typeof cookieConfig === 'string';
  if (!isConfigValid)
    logger.warn(
      'TM_APP_NAMESPACE missing in config. Cookie initialized with model name value',
    );
  return isConfigValid ? cookieConfig : modelNameConfig;
}

const useDecoratedRequest = (app) => {
  app.use(decorateRequest);
};

const useReqId = (app) => {
  app.use(rTracer.expressMiddleware());
};

export {
  useTrustProxy,
  useCors,
  useBodyParser,
  useSession,
  usePassport,
  useHttpLogger,
  useHelmet,
  useDecoratedRequest,
  useReqId,
};
