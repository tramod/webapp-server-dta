import scenarioServiceInstance from '../services/scenarios.js';
import { TmNotFoundError } from '../utils/error.js';
import { getLogParams } from '../utils/logger-utils.js';

/*
  Prefetch scenario with all nested associations based on request url and optional params
  Automatically parses request params ids for scenario/event/modification
  All events, modifications (of event) or all accesses might be requested from router
  Expose fetched scenario on req.$tm object
*/
const resourceFetchWrapper = (scenarioService) =>
  function paramMiddleware({
    allEvents = false,
    allModifications = false,
    allAccesses = false,
    allCalculations = false,
    trafficModel = false,
  } = {}) {
    return async function resourceFetchFn(req, res, next) {
      // TODO Rework later when it is more clear when itemed scenario and when updated scenario is needed
      const { scenarioId, evtId: eventId, modId: modificationId } = req.params;
      if (!scenarioId) return next();

      const { withEvents: withEventsQueryParam } = req.query;
      const userId = req.user?.id ?? null;
      const { isEditSession, fetchScenarioId, fetchScenarioIsItemId } = req.$tm;
      const isItemId = isEditSession ? fetchScenarioIsItemId : true;
      const usedId = isEditSession ? fetchScenarioId : scenarioId;

      const scenario = await scenarioService.getScenario(usedId, userId, {
        eventId,
        modificationId,
        allEvents: withEventsQueryParam ?? allEvents,
        allModifications: withEventsQueryParam ?? allModifications,
        allAccesses,
        allCalculations,
        trafficModel,
        isItemId,
      });

      if (!scenario) {
        const message = `Scenario ${
          eventId ? `with event${modificationId ? 'and modification' : ''}` : ''
        } not found.`;
        throw new TmNotFoundError(message, getLogParams(req));
      }

      // TODO might be stored in DB also
      if (isEditSession) scenario.sourceId = scenarioId;

      req.$tm.scenario = scenario;

      return next();
    };
  };

const resourceFetch = resourceFetchWrapper(scenarioServiceInstance);

export default resourceFetch;
export { resourceFetchWrapper };
