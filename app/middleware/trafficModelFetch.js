import trafficModelServiceInstance from '../services/trafficModels.js';

const getTrafficModelFromReqScenario = (scenario = {}, { type, id } = {}) => {
  const { trafficModel } = scenario;
  if (trafficModel && (trafficModel.type === type || trafficModel.id === id))
    return scenario.trafficModel;
  return null;
};

const getModelTypeOrId = (req) => ({
  type: req.params.modelType ?? req.body.modelType ?? req.query.modelType,
  id: req.$tm?.scenario?.trafficModelId,
});

const trafficModelFetchWrapper = (trafficModelService) =>
  async function resourceFetchFn(req, res, next) {
    const modelTypeOrId = getModelTypeOrId(req);

    const trafficModel =
      getTrafficModelFromReqScenario(req.$tm.scenario, modelTypeOrId) ??
      (await trafficModelService.getTrafficModel(modelTypeOrId));

    req.$tm.trafficModel = trafficModel;

    return next();
  };

const trafficModelFetch = trafficModelFetchWrapper(trafficModelServiceInstance);

export default trafficModelFetch;
export { trafficModelFetchWrapper };
