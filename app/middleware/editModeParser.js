import editSessionServiceInstance from '../services/editSession.js';
import { TmErrorGeneral } from '../utils/error.js';
import configInstance from '../../config/env.js';

const editModeParserWrapper = (editSessionService, config) =>
  async function editModeParserFn(req, res, next) {
    const { scenarioId } = req.params;
    const { useChanged } = req.query;
    // Don't see option to cache and observe dynamic changes in function closure
    const EDIT_MODE_ACTIVE = !config.TM_EDIT_MODE_DISABLE;
    const IS_EDIT_SESSION_FETCH =
      req.method === 'GET' && req.route.path.includes('/edit');
    if (
      !EDIT_MODE_ACTIVE ||
      (!useChanged && req.method === 'GET' && !IS_EDIT_SESSION_FETCH)
    ) {
      req.$tm.isEditSession = false;
      return next();
    }

    const validOnly = !IS_EDIT_SESSION_FETCH;
    let session = await editSessionService.getEditSession(scenarioId, {
      includeUpdated: false,
      validOnly,
      user: req.user,
    });
    if (!session)
      throw new TmErrorGeneral(
        "Could not found active user's edit session for scenario",
        {
          customCode: 'EDIT_NOT_FOUND',
          statusCode: 404,
          isOperational: true,
        },
      );

    const IS_SCENARIO_CHANGING =
      req.method !== 'GET' && !req.route.path.includes('/edit');
    if (
      session.isUndecided() &&
      session.computationState !== 'error' &&
      IS_SCENARIO_CHANGING
    )
      throw new TmErrorGeneral(
        'Could not change scenario while computation is running or undecided',
        {
          customCode: 'EDIT_WAITING',
          statusCode: 404,
          isOperational: true,
        },
      );
    if (!session.updatedRowId && IS_SCENARIO_CHANGING) {
      session = await editSessionService.addUpdatedToSession(session);
    }

    const fetchScenarioId = session.updatedRowId ?? session.sourceItemId;
    const fetchScenarioIsItemId = !session.updatedRowId;
    // Force session updatedAt update on every interaction
    await session.updateTimestamp();

    req.$tm = {
      ...req.$tm,
      isEditSession: true,
      editSession: session,
      fetchScenarioId,
      fetchScenarioIsItemId,
    };

    return next();
  };

const editModeParser = editModeParserWrapper(
  editSessionServiceInstance,
  configInstance,
);

export default editModeParser;
export { editModeParserWrapper };
