export default function decorateRequest(req, res, next) {
  req.$tm = {};
  next();
}
