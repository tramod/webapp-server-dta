import { defineAbilityFor, abilityCache } from '../services/authorization.js';

const abilityProviderWrapper = (cache, createAbility) =>
  async function abilityProviderFn(req, res, next) {
    const userId = req.user?.id ?? 'no-user';
    let ability;
    if (cache.has(userId)) ability = cache.get(userId);
    else {
      ability = await createAbility(req.user);
      cache.set(userId, ability);
    }
    req.$tm.ability = ability;
    return next();
  };

const abilityProvider = abilityProviderWrapper(abilityCache, defineAbilityFor);

export default abilityProvider;
export { abilityProviderWrapper };
