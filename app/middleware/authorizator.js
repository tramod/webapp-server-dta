import { TmNotAuthorized, TmErrorGeneral } from '../utils/error.js';
import { getLogParams } from '../utils/logger-utils.js';

function authorizationMiddleware({ abilityKey, errorMsg = '' } = {}) {
  return function authorizationFn(req, res, next) {
    const { ability, scenario } = req.$tm;
    if (!ability)
      throw new TmErrorGeneral('Ability key not provided for auth check', {
        level: 'error',
        isOperational: false,
      });
    if (ability.cannot(abilityKey, scenario.formatForAbility()))
      throw new TmNotAuthorized(
        `UnAuthorized to ${errorMsg}`,
        getLogParams(req),
      );

    return next();
  };
}

export default authorizationMiddleware;
