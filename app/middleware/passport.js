import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { User } from '../../db/index.js';
import usersService from '../services/users.js';
import logger from '../utils/logger.js';

passport.use(
  new LocalStrategy(async (username, password, done) => {
    const user = await usersService.getUserByUsernameOrEmail(username);
    if (!user || !user.credential)
      return invalidLogin(done, 'User not found or not fully created');

    const isValid = await user.credential.isPasswordValid(password);
    if (!isValid) return invalidLogin(done, 'Password invalid');

    return done(null, user);
  }),
);

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser(async (id, cb) => {
  const user = await User.findOne({
    where: { id },
  });
  // Fire user getters
  cb(null, user);
});

function invalidLogin(done, reason) {
  logger.logOperational(`LoginFailed: ${reason}`);
  done(null, false, { message: 'Invalid authentication' });
}

export default passport;
