import expressValidator from 'express-validator';
import {
  id,
  queryToBool,
  bodyNameString,
  bodyDescriptionString,
  bodyNoteString,
} from './utils.js';

const { body, param, query, oneOf } = expressValidator;

const scenarioId = id({ key: 'scenarioId' });
const eventId = id({ key: 'evtId' });

const idsToArray = ({ key = 'ids', method = param } = {}) =>
  method(key, 'Ids are string of ids')
    .notEmpty()
    .bail()
    .customSanitizer((value) => value.split(','))
    .customSanitizer((value) => [...new Set(value)]); // Remove duplicates
const idsAreNumber = ({ key = 'ids', method = param } = {}) =>
  method(`${key}.*`, 'Ids are string of ids').isInt().toInt();
const eventIds = (options) => [idsToArray(options), idsAreNumber(options)];

const eventParameters = (prefix = '') => [
  bodyNameString({ key: `${prefix}name` }),
  bodyDescriptionString(`${prefix}description`),
  bodyNoteString(`${prefix}note`),
  body(`${prefix}included`, 'Boolean state of event inclusion')
    .optional()
    .isBoolean(),
];

export default {
  events: {
    create: [
      ...eventParameters(),
      body('name', 'Name is mandatory').exists(),
      scenarioId,
    ],
    delete: [scenarioId, eventId],
    getter: [scenarioId, eventId, queryToBool('useChanged')],
    deleteMany: [scenarioId, eventIds({ method: query })],
    update: [
      ...eventParameters(),
      oneOf(
        [
          body('name').exists(),
          body('description').exists(),
          body('note').exists(),
          body('included').exists(),
        ],
        'There must be something to update',
      ),
      scenarioId,
      eventId,
    ],
    import: [
      body('events').isArray({ min: 1 }),
      scenarioId,
      id({ key: 'events.*.scenarioId', method: body }),
      id({ key: 'events.*.eventId', method: body }),
    ],
  },
};

export { eventParameters };
