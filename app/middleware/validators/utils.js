import expressValidator from 'express-validator';

const { param, query, body, oneOf } = expressValidator;

const id = ({ key, method = param } = {}) =>
  method(key, 'Id is integer number').isInt().toInt();

const queryToBool = (key) => query(key).toBoolean(true);

const bodyNameString = ({ key = 'name', notEmpty = true } = {}) =>
  body(key, 'Name is non-empty string')
    .optional()
    .isString()
    .bail()
    .trim()
    .if(() => notEmpty === true)
    .notEmpty();

const bodyDescriptionString = (key = 'description') =>
  body(key, 'Optional string description').optional().isString().trim();

const bodyNoteString = (key = 'note') =>
  body(key, 'Optional string note').optional().isString().trim();

const bodyScenarioAccessLevelString = (key = 'level') => {
  const allowedLevelValues = ['viewer', 'inserter', 'editor'];

  return body(key, `${key} param is not in the correct format`)
    .isIn(allowedLevelValues)
    .withMessage(
      `${key} param can only take values: ${allowedLevelValues.join(' | ')}`,
    );
};

const extraIdRegExp = /^extra-\d+(-b)*$/;
const featureId = ({ key, method = body } = {}) =>
  oneOf([
    method(key).isInt({ min: 0 }).toInt(),
    method(key).matches(extraIdRegExp),
  ]);
// Wildcard with oneOf does not work https://github.com/express-validator/express-validator/issues/950
const featureIdArray = (property) =>
  body(property)
    .custom(
      (features) =>
        Array.isArray(features) &&
        features.length > 0 &&
        features.every((feature) =>
          feature && typeof feature === 'string'
            ? feature.match(extraIdRegExp)
            : Number.isInteger(feature) && feature > 0,
        ),
    )
    .bail()
    .customSanitizer((features) =>
      features.map((feature) => {
        const integer = parseInt(feature, 10);
        return integer || feature;
      }),
    );

const isNull = (key) => body(key).custom((value) => value === null);
const oneOfIsNotNull = (keys) =>
  oneOf(
    keys.map((key) => body(key).exists({ checkNull: true })),
    `One of ${keys.join(',')} has to be defined`,
  );

export {
  id,
  queryToBool,
  bodyNameString,
  bodyDescriptionString,
  bodyNoteString,
  bodyScenarioAccessLevelString,
  featureId,
  featureIdArray,
  isNull,
  oneOfIsNotNull,
};
