import expressValidator from 'express-validator';
import { id, queryToBool } from './utils.js';
import usersService from '../../services/users.js';

const { body, param } = expressValidator;
const userId = id({ key: 'userId' });
const uniqueEmail = () =>
  body('email')
    .isEmail()
    .bail()
    .custom((value, { req }) =>
      usersService
        .getUser({
          email: value,
          ...(req.body.id && { id: { [usersService.op.not]: req.body.id } }),
        })
        .then((user) => {
          if (user) return Promise.reject();
          return true;
        }),
    )
    .withMessage('E-mail must be valid and unique.');
const uniqueUsername = () =>
  body('username')
    .isString()
    .notEmpty()
    .bail()
    .not()
    .isEmail()
    .withMessage('Username cannot have email format.')
    .bail()
    .custom((value, { req }) =>
      usersService
        .getUser({
          username: value,
          ...(req.body.id && { id: { [usersService.op.not]: req.body.id } }),
        })
        .then((user) => {
          if (user) return Promise.reject();
          return true;
        }),
    )
    .withMessage('Username must be unique.');

export default {
  users: {
    get: [userId],
    getAll: [queryToBool('hiddenFields')],
    create: [uniqueEmail(), uniqueUsername()],
    update: [
      userId,
      uniqueEmail().optional(),
      uniqueUsername().optional(),
      body('newPassword')
        .custom((value) => !value || value.length >= 8)
        .withMessage('New password must be at least 8 characters long.'),
    ],
    delete: [userId],
    password: {
      request: [
        body('email', 'E-mail is mandatory.').isEmail(),
        body('subject', 'Subject is mandatory.').isString(),
        body('body', 'Body is mandatory.').isString(),
        body('type', 'Type is mandatory.').isString(),
      ],
      reset: [
        userId,
        param('token', 'Token is mandatory.').isString(),
        body('email', 'E-mail is mandatory.').isEmail(),
        body('password', 'Password must be at least 8 characters long.')
          .isString()
          .bail()
          .isLength({ min: 8 }),
      ],
    },
  },
};
