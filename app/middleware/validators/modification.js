import expressValidator from 'express-validator';
import {
  id,
  queryToBool,
  featureId,
  featureIdArray,
  bodyNameString,
  bodyDescriptionString,
  bodyNoteString,
  isNull,
  oneOfIsNotNull,
} from './utils.js';

const { body, oneOf } = expressValidator;

const scenarioId = id({ key: 'scenarioId' });
const eventId = id({ key: 'evtId' });
const modificationId = id({ key: 'modId' });

const commonParameters = [
  bodyNameString({ notEmpty: false }),
  bodyDescriptionString(),
  bodyNoteString(),
  oneOf(
    [isNull('dateFrom'), body('dateFrom').isISO8601({ strict: true })],
    'dateFrom is valid date in ISO format or null',
  ),
  oneOf(
    [isNull('dateTo'), body('dateTo').isISO8601({ strict: true })],
    'dateTo is valid date in ISO format or null',
  ),
  body('type')
    .isIn(['link', 'node', 'generator'])
    .withMessage('Type must be one of link|node|generator'),
  body('mode')
    .isIn(['new', 'existing'])
    .withMessage('Mode must be one of new|existing'),
];

export default {
  modifications: {
    getter: [scenarioId, eventId, modificationId, queryToBool('useChanged')],
    create: [scenarioId, eventId, ...commonParameters],
    update: [scenarioId, eventId, modificationId, ...commonParameters],
    delete: [scenarioId, eventId, modificationId],
  },
};

const speedValidation = (prop = 'speed') =>
  body(prop).isFloat({ min: 0 }).toFloat();
const speedNullableValidation = (prop = 'speed') =>
  oneOf([speedValidation(prop), isNull(prop)]);

const capacityValidation = (prop = 'capacity') =>
  body(prop).isInt({ min: 0 }).toInt();
const capacityNullableValidation = (prop = 'capacity') =>
  oneOf([capacityValidation(prop), isNull(prop)]);

const lanesValidation = (prop = 'lanes') =>
  body(prop).isInt({ min: 1, max: 6 }).toInt();
const lanesNullableValidation = (prop = 'lanes') =>
  oneOf([lanesValidation(prop), isNull(prop)]);

export async function modificationTypeValidator(req, res, next) {
  const validations = [];
  const isNew = req.params.modId === undefined;

  const { type, mode } = isNew
    ? req.body
    : req.$tm.scenario.events[0].modifications[0];
  const typeMode = `${type}-${mode}`;

  if (!isNew) {
    validations.push(
      body('type')
        .equals(type)
        .withMessage('Cannot change modification type in update'),
      body('mode')
        .equals(mode)
        .withMessage('Cannot change modification mode in update'),
    );
  }

  if (typeMode === 'link-existing') {
    validations.push(
      featureIdArray('linkId'),
      speedNullableValidation(),
      capacityNullableValidation(),
    );
    validations.push(lanesNullableValidation());
    const calcProps = ['speed', 'capacity', 'lanes'];
    validations.push(oneOfIsNotNull(calcProps));
  } else if (typeMode === 'link-new') {
    const isTwoWay = req.body.twoWay === true;
    validations.push(
      speedValidation(),
      capacityValidation(),
      body('twoWay').default(false).isBoolean(),
      lanesValidation(),
      ...(isTwoWay
        ? [speedValidation('twoWaySpeed'), capacityValidation('twoWayCapacity')]
        : []),
      ...(isTwoWay ? [lanesValidation('twoWayLanes')] : []),
      featureId({ key: 'source' }),
      featureId({ key: 'target' }),
      body('coordinates')
        .isArray({ min: 2 })
        .withMessage('Coordinates must array of at least 2 entries'), // first nesting level -  at least two entries so it is line
      body('coordinates.*')
        .isArray({ min: 2, max: 2 })
        .withMessage(
          'Coordinates inner array must contain precisely two float numbers',
        ), // second nesting level - array of two entries, x/y coords
      body('coordinates.*.*')
        .isFloat()
        .toFloat()
        .withMessage(
          'Coordinates inner array must contain precisely two float numbers',
        ),
    );
  } else if (typeMode === 'node-existing') {
    validations.push(
      capacityValidation(),
      featureId({ key: 'node' }),
      featureId({ key: 'linkFrom' }),
      featureId({ key: 'linkTo' }),
    );
  } else if (typeMode === 'node-new') {
    validations.push(
      body('node').isArray({ min: 2, max: 2 }),
      body('node.*').isFloat().toFloat(),
    );
  } else if (
    typeMode === 'generator-new' ||
    typeMode === 'generator-existing'
  ) {
    if (typeMode === 'generator-new')
      validations.push(
        body('generator').isArray({ min: 2, max: 2 }),
        body('generator.*').isFloat().toFloat(),
      );
    else
      validations.push(
        body('generator').isArray({ min: 1, max: 1 }),
        featureIdArray('generator'),
      );

    validations.push(
      body('nodes').isArray({ min: 1 }),
      featureIdArray('nodes'),
      body('inTraffic').isFloat().toFloat(),
      body('outTraffic').isFloat().toFloat(),
    );
  }

  await Promise.all(validations.map((validation) => validation.run(req)));
  return next();
}
