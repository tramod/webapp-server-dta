import { id, queryToBool } from './utils.js';

const scenarioId = () => id({ key: 'scenarioId' });

export default {
  editSessions: {
    compute: [scenarioId(), queryToBool('saveOnComputed')],
    save: [
      scenarioId(),
      queryToBool('saveOnComputed'),
      queryToBool('leaveOnSaved'),
      queryToBool('saveAsCopy'),
    ],
    id: [scenarioId()],
    cancel: [scenarioId(), queryToBool('restart')],
  },
};
