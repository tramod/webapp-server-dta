import expressValidator from 'express-validator';

const { body, oneOf } = expressValidator;

const bodyEmail = (key = 'email') => body(key).isEmail().normalizeEmail();
const bodyUsername = (key = 'username') =>
  body(key).isString().bail().trim().isLength({ min: 4 });
const bodyPassword = (key = 'password') =>
  body(key).isString().bail().isLength({ min: 8 });

export default {
  auth: {
    login: [
      oneOf(
        [bodyEmail('username'), bodyUsername()],
        'Can be either email or username string of minimal length 4',
      ),
      bodyPassword(),
    ],
  },
};
