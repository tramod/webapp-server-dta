import expressValidator from 'express-validator';

const { body, param } = expressValidator;

export default {
  trafficModel: {
    getOne: [param('modelType').isString().bail().isIn(['DTA'])],
    refresh: [body('modelType').isString().bail().isIn(['DTA'])],
  },
};
