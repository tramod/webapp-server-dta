import { id, bodyScenarioAccessLevelString } from './utils.js';

const scenarioId = id({ key: 'scenarioId' });
const userId = id({ key: 'userId' });

export default {
  scenarioAccess: {
    getter: [scenarioId],
    create: [scenarioId, userId, bodyScenarioAccessLevelString()],
    update: [scenarioId, userId, bodyScenarioAccessLevelString()],
    delete: [scenarioId, userId],
  },
};
