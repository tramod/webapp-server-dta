import expressValidator from 'express-validator';

import {
  id,
  queryToBool,
  bodyNameString,
  bodyDescriptionString,
  bodyNoteString,
  featureId,
} from './utils.js';
import { eventParameters } from './event.js';

const { body, oneOf, param } = expressValidator;

const scenarioId = id({ key: 'scenarioId' });

const scenarioParameters = [
  bodyNameString(),
  bodyDescriptionString(),
  bodyNoteString(),
];

export default {
  scenarios: {
    create: [
      ...scenarioParameters,
      body('name', 'Name is mandatory').exists(),
      body('events').optional().isArray({ min: 1 }),
      ...eventParameters('events.*.'),
      body('events.*.name', 'Event name is mandatory').exists(),
      body('modelType').optional().isIn(['DTA']),
    ],
    delete: [scenarioId],
    get: [scenarioId, queryToBool('useChanged')],
    getAll: [param('modelType').optional().isIn(['DTA'])],
    update: [
      ...scenarioParameters,
      oneOf(
        [
          body('name').exists(),
          body('description').exists(),
          body('note').exists(),
          body('isPublic').exists(),
          body('isInPublicList').exists(),
        ],
        'There must be something to update',
      ),
      scenarioId,
    ],
    updatePublication: [
      scenarioId,
      body('isPublic').exists(),
      body('isInPublicList').exists(),
    ],
    nodeTransit: [scenarioId, featureId({ key: 'nodeId', method: param })],
    extraFeatureModifications: [scenarioId],
  },
};
