import AuthValidator from './auth.js';
import ScenarioValidator from './scenario.js';
import EventValidator from './event.js';
import ModificationValidator from './modification.js';
import EditSessionValidator from './editSession.js';
import ScenarioAccessValidator from './scenarioAccess.js';
import UserValidator from './user.js';
import TrafficModelValidator from './trafficModel.js';

export default {
  ...AuthValidator,
  ...ScenarioValidator,
  ...EventValidator,
  ...ModificationValidator,
  ...EditSessionValidator,
  ...ScenarioAccessValidator,
  ...UserValidator,
  ...TrafficModelValidator,
};
