import logger from '../utils/logger.js';

export default function isAuthenticated(req, res, next) {
  if (req.user) next();
  else {
    logger.logOperational(`Path ${req.path}, user not authenticated`);
    res.status(401).json({
      code: 'AUTHENTICATION_INVALID',
      error: 'Not authenticated to access this route',
    });
  }
}
