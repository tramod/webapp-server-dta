import validatorError from './validatorError.js';
import validations from './validators/index.js';

function getValidator(validatorPath) {
  if (typeof validatorPath !== 'string')
    throw Error(`ValidatorPath ${validatorPath}: not found or incorrect`);
  return parseValidator(validatorPath);
}

function parseValidator(path) {
  const parts = path.split('.');
  const selectedValidator = parts.reduce(
    (validationsLevel, key) => validationsLevel[key] ?? {},
    validations,
  );

  if (Array.isArray(selectedValidator)) return selectedValidator;
  throw Error(`ValidatorPath ${path}: not found or incorrect`);
}

export default function validator(validatorPath, validatorFn) {
  return [
    getValidator(validatorPath),
    ...(validatorFn ? [validatorFn] : []),
    validatorError,
  ];
}

export { validatorError, getValidator as getValidatorByPath };
