import morgan from 'morgan';
import logger from '../utils/logger.js';

// MORGAN (HTTP) LOGGER
// TODO Review HTTP logging later
morgan.token('user', (req) => (req.user ? `user:${req.user.id}` : req.ip));

const format =
  ':user ":method :url" :status (:res[content-length]) :response-time ms';

const httpLogger = morgan(format, { stream: logger.pass });

export default httpLogger;
