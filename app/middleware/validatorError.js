import expressValidator from 'express-validator';
import logger from '../utils/logger.js';
import { getLogParams } from '../utils/logger-utils.js';

const { validationResult } = expressValidator;

const errorFormatter = ({ msg, param, value, nestedErrors }) => ({
  msg,
  param: nestedErrors ? nestedErrors[0].param : param,
  value: nestedErrors ? nestedErrors[0].value : value,
});

const PRIVATE_FIELDS = ['username', 'email', 'password'];

export default function validatorErrorMiddleware(req, res, next) {
  const errors = validationResult(req).formatWith(errorFormatter);
  if (errors.isEmpty()) return next();

  const extractedErrors = errors.array();
  for (const error of extractedErrors) {
    if (PRIVATE_FIELDS.includes(error.param)) delete error.value;
  }
  logger.warn({
    code: 'INPUT_INVALID',
    error: extractedErrors,
    message: 'ValidationError',
    req: getLogParams(req),
  });
  return res.status(400).json({
    code: 'INPUT_INVALID',
    error: extractedErrors,
  });
}
