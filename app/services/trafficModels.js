import pLimit from 'p-limit';

import { TrafficModel } from '../../db/index.js';
import {
  calculateDefaultCache,
  getTrafficCache,
  deleteTrafficCache,
} from './modelAPI.js';
import TmLogger from '../utils/logger.js';
import scenarioCalculationService, {
  modelSectionService,
} from './calculations/index.js';

const refreshModelLimit = pLimit(1);

class TrafficModelService {
  constructor({ model, logger, modelSection, scenarioCalculation } = {}) {
    this.model = model;
    this.logger = logger;
    this.modelSection = modelSection;
    this.scenarioCalculation = scenarioCalculation;
  }

  async getTrafficModels() {
    return this.model.findAll();
  }

  // Temp helper to get one of two models by type
  // Better querying needed in future
  async getTrafficModel({ type, id }) {
    if (!type && !id) return null;
    return this.model.findOne({
      where: { ...(type && { type }), ...(id && { id }) },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async setTrafficModelValidity(
    trafficModel,
    { isValid = true, isBroken = false } = {},
  ) {
    await trafficModel.update({
      isValid,
      isBroken,
      timestamp: isValid ? new Date() : null,
    });
    this.logger.verbose(
      `TrafficModel: ${trafficModel.type}: Valid status: ${isValid}, broken status: ${isBroken}`,
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async checkBaseModel(trafficModel) {
    this.logger.verbose('Checking default cache existence');
    if (!trafficModel.isValid) return false;
    try {
      await getTrafficCache({ trafficModel });
      return true;
    } catch (error) {
      if (trafficModel.isValid)
        this.setTrafficModelValidity(trafficModel, { isValid: false });
      return false;
    }
  }

  async calculateBaseModel(trafficModel) {
    this.logger.verbose('Calculating default cache on model server');
    try {
      await calculateDefaultCache({ trafficModel });
      this.setTrafficModelValidity(trafficModel, { isValid: true });
      return true;
    } catch (error) {
      this.logger.warn(
        `TrafficModel: ${trafficModel.id} could not calculate base`,
      );
      this.logger.debug(error);
      return false;
    }
  }

  async checkOrCalculateBaseModel(trafficModel) {
    const hasBaseModel = await this.checkBaseModel(trafficModel);
    if (hasBaseModel) return true;
    return this.calculateBaseModel(trafficModel);
  }

  async removeBaseModel(trafficModel) {
    try {
      await getTrafficCache({ trafficModel });
      await deleteTrafficCache({ trafficModel });
    } catch (e) {
      // TODO Check if typeof expected error
      this.logger.verbose(e);
    } finally {
      this.logger.verbose('Default cache does not exist on server');
      await this.setTrafficModelValidity(trafficModel, { isValid: false });
    }
  }

  async refreshModel(trafficModel) {
    this.logger.verbose(
      `TrafficModel: ${trafficModel.type}: Refresh scheduling`,
    );
    await this.setTrafficModelValidity(trafficModel, { isValid: false });
    await this.modelSection.markTrafficModelSectionsInvalid({ trafficModel });
    refreshModelLimit(() => this.scheduledModelRefresh(trafficModel));
    return true;
  }

  async scheduledModelRefresh(trafficModel) {
    this.logger.verbose(`TrafficModel: ${trafficModel.type}: Refresh started`);
    const hasBaseModel = await this.calculateBaseModel(trafficModel);
    await this.setTrafficModelValidity(trafficModel, {
      isValid: hasBaseModel,
      isBroken: !hasBaseModel,
    });
    if (!hasBaseModel) return;
    this.logger.verbose(
      `TrafficModel: ${trafficModel.type}: Base model refreshed`,
    );

    const scenarios = await trafficModel.getScenarios();
    await Promise.allSettled(
      scenarios.map(async (scenario) => {
        await this.scenarioCalculation.calculateScenario(scenario, () => {});
      }),
    );

    this.logger.verbose(`TrafficModel: ${trafficModel.type}: Refresh finished`);
    this.logger.info(`TrafficModel: ${trafficModel.type}: Refresh success`);
  }
}

const trafficModelServiceInstance = new TrafficModelService({
  model: TrafficModel,
  logger: TmLogger,
  modelSection: modelSectionService,
  scenarioCalculation: scenarioCalculationService,
});

export default trafficModelServiceInstance;
export { TrafficModelService };
