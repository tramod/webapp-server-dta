/* eslint-disable class-methods-use-this */
import QueryTypes from 'sequelize';
import { deleteTrafficCache } from '../modelAPI.js';

// TODO remove later, replaceAll shipped in Node v15
if (!String.prototype.replaceAll) {
  // eslint-disable-next-line no-extend-native, func-names
  String.prototype.replaceAll = function (str, newStr) {
    // If a regex pattern
    if (
      Object.prototype.toString.call(str).toLowerCase() === '[object regexp]'
    ) {
      return this.replace(str, newStr);
    }

    // If a string
    return this.replace(new RegExp(str, 'g'), newStr);
  };
}

class ModelSectionService {
  constructor({ ModelSection, ScenarioModelSection, db, Op } = {}) {
    this.ScenarioModelSection = ScenarioModelSection;
    this.ModelSection = ModelSection;
    this.db = db;
    this.Op = Op;
  }

  async createModelSection({ key, hash, trafficModelId } = {}) {
    return this.ModelSection.create({
      key,
      hash,
      trafficModelId,
      isValid: true,
    });
  }

  async linkScenarioModelSection(
    scenarioInstance,
    modelSectionInstance,
    { dateFrom, dateTo, codeList } = {},
  ) {
    await scenarioInstance.createScenarioModelSection({
      dateFrom,
      dateTo,
      codeList,
      modelSectionKey: modelSectionInstance.key,
    });
  }

  async getModelSectionByHash(hashCode) {
    return this.ModelSection.findOne({
      where: { hash: hashCode, isValid: true },
    });
  }

  async removeScenariosModelSections(scenario, { timeStamp } = {}) {
    await this.ScenarioModelSection.destroy({
      where: {
        scenarioRowId: scenario.rowId,
        ...(timeStamp && { createdAt: { [this.Op.lte]: timeStamp } }),
      },
    });
  }

  async countScenarios(modelSection) {
    return modelSection.countScenarios({
      scope: false,
    });
  }

  async getLoneModelSections(trafficModel) {
    const schemaString =
      process.env.TM_DB_SCHEMA !== undefined
        ? `"${process.env.TM_DB_SCHEMA}".`
        : '';
    const trafficModelJoin = trafficModel
      ? ` INNER JOIN ${schemaString}"traffic_models" AS "trafficModel" ON "modelSection"."traffic_model_id" = "trafficModel"."id" `
      : ' ';
    const trafficModelWhere = trafficModel
      ? ` AND "trafficModel"."id"=${trafficModel.id}`
      : '';
    const queryString = `SELECT * FROM ${schemaString}"model_sections" AS "modelSection"${trafficModelJoin}LEFT JOIN ${schemaString}"scenario_model_sections" AS "scenarioModelSection" ON "modelSection"."key" = "scenarioModelSection"."model_section_key" WHERE "scenarioModelSection"."model_section_key" IS NULL${trafficModelWhere}`;
    // Manual select as sequelize way to do this was not found
    const [loneModelSections] = await this.db.query(queryString, {
      type: QueryTypes.SELECT,
    });
    return loneModelSections;
  }

  async getInvalidModelSections(trafficModel) {
    return this.ModelSection.findAll({
      where: {
        trafficModelId: trafficModel.id,
        isValid: false,
      },
    });
  }

  async clearModelSection({ modelSection, trafficModel } = {}) {
    await deleteTrafficCache({
      trafficModel,
      cacheName: modelSection.key,
    });
    await this.ModelSection.destroy({
      where: { key: modelSection.key },
    });
  }

  async markTrafficModelSectionsInvalid({ trafficModel } = {}) {
    await this.ModelSection.update(
      {
        isValid: false,
      },
      {
        where: {
          trafficModelId: trafficModel.id,
        },
      },
    );
  }
}

// eslint-disable-next-line import/prefer-default-export
export { ModelSectionService };
