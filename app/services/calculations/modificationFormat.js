import { startOfHour, addHours } from 'date-fns';
import pLimit from 'p-limit';
import { modificationToApiItemTransformers } from './modificationToApiTransformers.js';
import {
  getModelLink,
  getModelNode,
  getModelNodeTransit,
  getModelGenerator,
} from '../modelAPI.js';

const modelApiFeatureDefs = {
  modificationProps: {
    'link-new': [
      { type: 'node', prop: 'source' },
      { type: 'node', prop: 'target' },
    ],
    'link-existing': [{ type: 'link', prop: 'linkId' }],
    'node-new': [],
    'node-existing': [
      { type: 'node', prop: 'node' },
      { type: 'nodeTransit', prop: 'node' },
      { type: 'link', prop: 'linkFrom' },
      { type: 'link', prop: 'linkTo' },
    ],
    'generator-new': [{ type: 'node', prop: 'nodes' }],
    'generator-existing': [
      { type: 'generator', prop: 'generator' },
      { type: 'node', prop: 'nodes' },
    ],
  },
  fetchFunctions: {
    link: getModelLink,
    node: getModelNode,
    nodeTransit: getModelNodeTransit,
    generator: getModelGenerator,
  },
};

const MAX_CONCURRENT_FETCHES = 20;
const modelApiFetchLimit = pLimit(MAX_CONCURRENT_FETCHES);
const modelApiRunningFetches = {};

export default class ScenariosModificationsFormat {
  constructor({ modificationModel, eventModel } = {}) {
    this.modificationModel = modificationModel;
    this.eventModel = eventModel;
  }

  async getModificationItems({ scenario, trafficModel } = {}) {
    const modifications = await this.getScenariosModifications(scenario);
    const supportedModifications = filterSupportedModifications(
      modifications,
      trafficModel.type,
    );
    return transformModifications({
      modifications: supportedModifications,
      trafficModel,
    });
  }

  // Maybe could be in mod service?
  async getScenariosModifications(scenario, includedOnly = true) {
    const modifications = await this.modificationModel.findAll({
      include: [
        {
          model: this.eventModel,
          required: true,
          where: {
            scenarioRowId: scenario.rowId,
            ...(includedOnly && { included: true }),
          },
        },
      ],
      order: [
        // In case values get more complex, JS sort on results might be required
        ['mode', 'DESC'], // mode new existing DESC, need new first
        ['type', 'DESC'], // type node link generator DESC, need node first, other irrelevant
      ],
    });
    return modifications;
  }
}

function filterSupportedModifications(modifications, modelType = 'DTA') {
  return modifications.filter(
    ({ typeMode }) => typeMode in modificationToApiItemTransformers[modelType],
  );
}

const ceilHour = (dateTime) => {
  const hourStart = startOfHour(dateTime);
  if (hourStart.valueOf() === dateTime.valueOf()) return hourStart;
  return addHours(hourStart, 1);
};
async function transformModifications({ modifications, trafficModel }) {
  return Promise.all(
    modifications.map(async (modification) => {
      const {
        typeMode,
        dateFrom: rawDateFrom,
        dateTo: rawDateTo,
      } = modification;
      const dateFrom = rawDateFrom !== null ? startOfHour(rawDateFrom) : null;
      const dateTo = rawDateTo !== null ? ceilHour(rawDateTo) : null;
      const originalFeatures = await getOriginalFeatures({
        modification,
        scenarioModificationsList: modifications,
        trafficModel,
      });
      const transformer =
        modificationToApiItemTransformers[trafficModel.type][typeMode];
      const apiParams = transformer(modification, originalFeatures);
      return {
        dateFrom,
        dateTo,
        typeMode,
        apiParams,
      };
    }),
  );
}

async function getOriginalFeatures({
  modification,
  scenarioModificationsList,
  trafficModel,
} = {}) {
  const featureList = parseFeaturesTypeAndId(modification);
  const items = await Promise.all(
    featureList.map(async ({ type, id }) => {
      const key = `${type}:${id}`;
      try {
        const isApiItem = typeof id === 'number';
        const feature = isApiItem
          ? await fetchModelApiFeature({ trafficModel, type, id, key })
          : getFeatureFromModifications(scenarioModificationsList, id);

        return { key, feature };
      } catch (e) {
        return { key };
      }
    }),
  );
  const itemsHash = items.reduce((hash, { key, feature }) => {
    // eslint-disable-next-line no-param-reassign
    hash[key] = feature;
    return hash;
  }, {});
  return itemsHash;
}

function parseFeaturesTypeAndId(modification) {
  const { typeMode } = modification;
  const properties = modelApiFeatureDefs.modificationProps[typeMode];
  return properties.reduce((items, { type, prop }) => {
    const idOrIds = modification.data[prop];
    const idsArray = Array.isArray(idOrIds) ? idOrIds : [idOrIds];
    const typedValues = idsArray.map((id) => ({
      type,
      id,
    }));
    items.push(...typedValues);
    return items;
  }, []);
}

async function fetchModelApiFeature({ trafficModel, type, id, key } = {}) {
  if (modelApiRunningFetches[key]) return modelApiRunningFetches[key];
  const fetchFn = modelApiFeatureDefs.fetchFunctions[type];
  if (!fetchFn) return null;

  const fetchInstance = modelApiFetchLimit(() => fetchFn({ trafficModel, id }));
  modelApiRunningFetches[key] = fetchInstance;
  const result = await fetchInstance;

  delete modelApiRunningFetches[key];
  return result;
}

function getFeatureFromModifications(modifications, featureId) {
  const idToFind = parseInt(featureId.match(/\d+/)[0], 10); // two-way links are from one mod
  return modifications.find((orgMod) => orgMod.itemId === idToFind);
}
