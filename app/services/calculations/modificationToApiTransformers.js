import lineLengthModule from '@turf/length';
import { lineString as createLineStringFeature } from '@turf/helpers';
import { toWgs84 } from '@turf/projection';

// Move to mod transform utils
// eslint-disable-next-line import/prefer-default-export
export const modificationToApiItemTransformers = {
  DTA: {
    'link-new': linkNewDTATransformer,
    'link-existing': linkExistingDTATransformer,
    'node-new': nodeNewDTATransformer,
    'node-existing': nodeExistingDTATransformer,
  },
};

const lineLength =
  typeof lineLengthModule === 'function'
    ? lineLengthModule
    : lineLengthModule.default; // Fix for tests where import was incorrect

function hasOriginalFeature(features, item) {
  return getOriginalFeature(features, item) !== undefined;
}

function getOriginalFeature(features, { type, id }) {
  return features[`${type}:${id}`];
}

function parseLinkProps({
  modData,
  linkData,
  linkId,
  propNames = ['capacity', 'speed'],
} = {}) {
  const isAPILink = typeof linkId === 'number';
  const isTwoWay = !isAPILink && linkId.endsWith('-b');
  return propNames.reduce((props, name) => {
    // eslint-disable-next-line no-param-reassign
    props[name] =
      modData[name] === null
        ? getPropFromLink({
            linkData,
            isAPILink,
            isTwoWay,
            name,
          })
        : modData[name];
    return props;
  }, {});
}

const getTwoWayProp = (prop) =>
  `twoWay${prop.charAt(0).toUpperCase()}${prop.slice(1)}`;
function getPropFromLink({ linkData, isAPILink, isTwoWay, name } = {}) {
  if (isAPILink) return linkData.properties[name];
  if (isTwoWay) return linkData.data[getTwoWayProp(name)];
  return linkData.data[name];
}

function linkNewDTATransformer(modification, features) {
  const {
    source,
    target,
    speed,
    capacity,
    lanes,
    twoWay,
    twoWaySpeed,
    twoWayCapacity,
    twoWayLanes,
  } = modification.data;
  const linkId = `extra-${modification.itemId}`;
  const hasValidNodes =
    hasOriginalFeature(features, { type: 'node', id: source }) &&
    hasOriginalFeature(features, { type: 'node', id: target });
  if (!hasValidNodes)
    return [
      {
        param: null,
        paramId: linkId,
      },
    ];

  const featureLength = getModificationLinkLength(modification);
  // TODO review when model API calc is fixed, rounding might not be needed
  const roundedLength = parseFloat(featureLength.toFixed(3));
  const linkParams = [
    {
      param: {
        edgeId: linkId,
        speed: restrictDTAParams(speed), // Temp? hack
        capacity: restrictDTAParams(capacity), // Temp? hack
        lanes,
        fromNodeId: source,
        toNodeId: target,
        length: roundedLength,
      },
      paramId: linkId,
    },
  ];
  if (twoWay) {
    const twoWayLinkId = `extra-${modification.itemId}-b`;
    linkParams.push({
      param: {
        edgeId: twoWayLinkId,
        speed: restrictDTAParams(twoWaySpeed), // Temp? hack
        capacity: restrictDTAParams(twoWayCapacity), // Temp? hack
        lanes: twoWayLanes,
        fromNodeId: target,
        toNodeId: source,
        length: roundedLength,
      },
      paramId: twoWayLinkId,
    });
  }
  return linkParams;
}

function linkExistingDTATransformer(modification, features) {
  return modification.data.linkId.map((linkId) => {
    const originalFeature = getOriginalFeature(features, {
      type: 'link',
      id: linkId,
    });
    if (!originalFeature)
      return {
        param: null,
        paramId: `${linkId}`,
      };

    const { capacity, speed, lanes } = parseLinkProps({
      modData: modification.data,
      linkData: originalFeature,
      linkId,
      propNames: ['capacity', 'speed', 'lanes'],
    });

    // Closed links are better modelled as deleted edge
    if (capacity === 0 || speed === 0) {
      return {
        param: {
          type: 'deleteEdge',
          edgeId: linkId,
        },
        paramId: `${linkId}`,
      };
    }

    return {
      param: {
        edgeId: linkId,
        speed: restrictDTAParams(speed), // Temp? hack
        capacity: restrictDTAParams(capacity), // Temp? hack
        lanes,
      },
      paramId: `${linkId}`,
    };
  });
}

// Disallow zero value params
function restrictDTAParams(value) {
  return value >= 1 ? value : 1;
}

function getModificationLinkLength(modification) {
  const { coordinates } = modification.data;
  const jsonFeature = createLineStringFeature(coordinates);
  const wgsFeature = toWgs84(jsonFeature); // Convert to correct geojson projection
  return lineLength(wgsFeature);
}

function nodeNewDTATransformer(modification) {
  const nodeId = `extra-${modification.itemId}`;
  return [
    {
      param: {
        nodeId,
        ctrlType: 'none',
      },
      paramId: nodeId,
    },
  ];
}

function nodeExistingDTATransformer(modification, features) {
  const { node, linkFrom, linkTo, capacity } = modification.data;

  const hasValidItems =
    hasOriginalFeature(features, { type: 'node', id: node }) &&
    hasOriginalFeature(features, { type: 'link', id: linkFrom }) &&
    hasOriginalFeature(features, { type: 'link', id: linkTo });
  const originalNode = getOriginalFeature(features, { type: 'node', id: node });
  const isUpdatable =
    capacity === 0 ||
    (originalNode.properties.ctrl_type !== 'none' &&
      originalNode.properties.use_movement === true);
  if (!isUpdatable || !hasValidItems)
    return [
      {
        param: null,
        paramId: `${node}:${linkFrom}:${linkTo}`,
      },
    ];

  const item = {
    nodeId: node,
    sourceEdge: linkFrom,
    targetEdge: linkTo,
    capacity,
  };
  return [
    {
      param: item,
      paramId: `${node}:${linkFrom}:${linkTo}`,
    },
  ];
}
