// eslint-disable-next-line import/prefer-default-export
export function mergeModelParamItems(aItem, bItem, modelType = 'STA') {
  const mergeFn = getMergeFn(aItem, modelType);
  return mergeFn(aItem, bItem);
}

function getMergeFn(paramItem, modelType) {
  const paramType = paramItem.typeMode.split('-')[0];
  const key = `${modelType}-${paramType}`;
  switch (key) {
    case 'DTA-link':
      return mergeDTALinkDuplicates;
    case 'DTA-node':
      return mergeDTANodeDuplicates;
    default:
      return null;
  }
}

const getLower = (a, b, key) => (a[key] < b[key] ? a[key] : b[key]);
const getHigher = (a, b, key) => (a[key] > b[key] ? a[key] : b[key]);
const oneOfIs = (a, b, value, key) => a[key] === value || b[key] === value;
const bothAre = (a, b, value, key) => a[key] === value && b[key] === value;

function mergeDTALinkDuplicates(a, b) {
  if (oneOfIs(a.param, b.param, 'deleteEdge', 'type'))
    return {
      ...a,
      param: {
        edgeId: a.param.edgeId,
        type: 'deleteEdge',
      },
    };
  if (bothAre(a, b, 'link-existing', 'typeMode'))
    return mergeParamUpdate(a, {
      capacity: getLower(a.param, b.param, 'capacity'),
      speed: getLower(a.param, b.param, 'speed'),
      lanes: getLower(a.param, b.param, 'lanes'),
    });
  const { newL, edit } =
    a.typeMode === 'link-existing'
      ? { newL: b, edit: a }
      : { newL: a, edit: b };
  const { capacity, speed, lanes } = edit.param;
  return mergeParamUpdate(newL, {
    capacity,
    speed,
    lanes,
  });
}

function mergeDTANodeDuplicates(a, b) {
  /*
    Most restrictive wins
    -1 is unrestricted, so takes lowest chance
  */
  const capacityFn = oneOfIs(a.param, b.param, -1, 'capacity')
    ? getHigher
    : getLower;
  const capacity = capacityFn(a.param, b.param, 'capacity');
  return mergeParamUpdate(a, {
    capacity,
  });
}

function mergeParamUpdate(source, paramUpdate) {
  return {
    ...source,
    param: {
      ...source.param,
      ...paramUpdate,
    },
  };
}
