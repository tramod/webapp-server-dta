import pLimit from 'p-limit';
import config from '../../../config/env.js';
import TrafficJobHandler from './trafficJobHandler.js';

// Prefix to identify this app caches on model server
const SECTION_KEY_PREFIX = config.TM_MODEL_CACHE_PREFIX;
const MAX_DTA_THREADS = config.TM_MODEL_DTA_MAX_THREADS;
const RESTART_TIMEOUT_MS = 30 * 1000; // 30s
const MAX_HANDLER_RETRIES = 5;

export default class TrafficJobManager {
  constructor(
    { modelSectionService, logger, trafficModel } = {},
    {
      maxThreads,
      modelSectionKeyPrefix = SECTION_KEY_PREFIX,
      maxRetries = MAX_HANDLER_RETRIES,
    } = {},
  ) {
    this.queue = [];
    this.jobs = {};
    this.scenarioHashes = {};
    this.jobThreads = Array.from(
      {
        length: maxThreads || MAX_DTA_THREADS,
      },
      () =>
        new TrafficJobHandler(
          {
            logger,
            modelSectionService,
            modelSectionKeyPrefix,
          },
          { maxRetries },
        ),
    );
    this.threadsFull = false;
    this.threadsRestartTimeout = null;
    this.modelSectionService = modelSectionService;
    this.trafficModel = trafficModel;
    this.cleanUpLimit = pLimit(1);
  }

  async calculate({ hash, params, trafficModel, scenarioId } = {}) {
    if (trafficModel.id !== this.trafficModel.id)
      throw Error('TrafficJobManager received unknown traffic model calc');
    return new Promise((resolve, reject) => {
      const onSuccess = (modelSection) => resolve(modelSection);
      const onError = (err) => reject(err);
      const callbacks = { onSuccess, onError, scenarioId };

      if (this.jobs[hash]) {
        this.jobs[hash].callbacks.push(callbacks);
        this.jobs[hash].scenarios.add(scenarioId);
      } else {
        const jobDef = {
          hash,
          params,
          callbacks: [callbacks],
          trafficModel,
          scenarios: new Set([scenarioId]),
        };
        this.jobs[hash] = jobDef;
        this.queue.push(hash);
      }
      if (!this.scenarioHashes[scenarioId])
        this.scenarioHashes[scenarioId] = new Set([hash]);
      else this.scenarioHashes[scenarioId].add(hash);
      this.trafficJob();
    });
  }

  cancel({ scenarioId }) {
    if (
      !this.scenarioHashes[scenarioId] ||
      this.scenarioHashes[scenarioId].size === 0
    )
      return;
    const hashes = Array.from(this.scenarioHashes[scenarioId]);

    const jobsToCancel = [];
    for (const hash of hashes) {
      const jobDef = this.jobs[hash];
      if (jobDef && jobDef.scenarios.size > 1) {
        for (const { onError, scenarioId: callbackId } of jobDef.callbacks) {
          if (scenarioId === callbackId) onError('Traffic job cancelled'); // job error
        }
        jobDef.callbacks = jobDef.callbacks.filter(
          (cb) => cb.scenarioId !== scenarioId,
        );
        jobDef.scenarios.delete(scenarioId);
      } else jobsToCancel.push(jobDef);
    }

    const [queued, inHandle] = jobsToCancel.reduce(
      (result, job) => {
        const subResult = this.queue.includes(job.hash) ? result[0] : result[1];
        subResult.push(job);
        return result;
      },
      [[], []],
    );

    const queuedHashes = queued.map((job) => job.hash);
    this.queue = this.queue.filter((hash) => !queuedHashes.includes(hash));
    for (const job of queued) {
      for (const { onError } of job.callbacks) {
        onError('Traffic job cancelled');
      }
      delete this.jobs[job.hash];
    }

    for (const job of inHandle) {
      const handler = this.getHandlerByHash(job.hash);
      if (handler) handler.cancelJob();
      // this.jobs cleanup/callback calls to be done by trafficJob() flow
      else this.logger.verbose('No handler found?');
    }
    delete this.scenarioHashes[scenarioId];
  }

  async trafficJob() {
    if (this.queue.length === 0) return;
    const handler = this.getEmptyHandler();
    if (!handler) return; // All handlers running or ModelAPI jobs count reached

    const jobHash = this.queue.shift();
    const jobDef = this.jobs[jobHash];
    handler.registerJob(jobDef);
    this.trafficJob(); // JobHandler occupied - can start another

    const jobResult = await handler.handleJob();
    // Job result - 'error'/'thread-limit'(aka modelAPIJobs overflow)/<object>modelSection(aka success)
    if (jobResult === 'thread-limit') {
      this.startRestartTimeout();
      this.queue.unshift(jobHash);
      return;
    }
    for (const { onSuccess, onError } of jobDef.callbacks) {
      if (typeof jobResult === 'object') onSuccess(jobResult);
      else if (jobResult === 'cancelled') onError('Traffic job cancelled');
      else onError('Traffic job error'); // job error
    }
    this.clearAndStartNextJob(jobDef);
  }

  async clearDeprecatedJobs() {
    if (this.areHandlersEmpty() && this.areJobsEmpty()) {
      await this.cleanUpLimit(() => this.clearUnlinkedJobsLimited());
    }
  }

  // async clearInvalidJobs() {
  //   const invalidSections =
  //     await this.modelSectionService.getInvalidModelSections(this.trafficModel);
  //   await Promise.allSettled(
  //     invalidSections.map(async (section) => this.clearModelSection(section)),
  //   );
  // }

  async clearUnlinkedJobsLimited() {
    const unlinkedSections =
      await this.modelSectionService.getLoneModelSections(this.trafficModel);
    await Promise.allSettled(
      unlinkedSections.map(async (section) => this.clearModelSection(section)),
    );
  }

  async clearModelSection(modelSection) {
    try {
      await this.modelSectionService.clearModelSection({
        modelSection,
        trafficModel: this.trafficModel,
      });
    } catch (error) {
      this.logger.warn({
        message: 'Could not remove model section',
        key: modelSection.key,
      });
    }
  }

  getEmptyHandler() {
    return (
      !this.threadsFull &&
      this.jobThreads.find((handler) => handler.isWaiting === true)
    );
  }

  areHandlersEmpty() {
    return this.jobThreads.every((handler) => handler.isWaiting === true);
  }

  getHandlerByHash(hash) {
    return this.jobThreads.find((handler) => handler.currentJob?.hash === hash);
  }

  areJobsEmpty() {
    return Object.keys(this.jobs).length === 0;
  }

  startRestartTimeout() {
    this.threadsFull = true;
    this.threadsRestartTimeout = setTimeout(() => {
      this.clearRestartTimeout();
      this.trafficJob();
    }, RESTART_TIMEOUT_MS);
  }

  clearRestartTimeout() {
    this.threadsFull = false;
    clearTimeout(this.threadsRestartTimeout);
  }

  clearAndStartNextJob(jobDef) {
    jobDef.scenarios.forEach((scenarioId) => {
      if (this.scenarioHashes[scenarioId])
        this.scenarioHashes[scenarioId].delete(jobDef.hash);
    });
    delete this.jobs[jobDef.hash];
    this.clearRestartTimeout();
    this.trafficJob();
  }
}
