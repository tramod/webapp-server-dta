import { customAlphabet } from 'nanoid';
import { postTrafficJob, awaitTrafficJobResult } from '../modelAPI.js';

const generateID = customAlphabet(
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz',
  12,
);

export default class TrafficJobHandler {
  constructor(
    { logger, modelSectionService, modelSectionKeyPrefix } = {},
    { maxRetries = 1 } = {},
  ) {
    // State
    this.currentJob = null;
    this.isWaiting = true;
    this.retry = 0;
    this.cancelCall = null;
    // Deps
    this.modelSectionService = modelSectionService;
    this.modelSectionKeyPrefix = modelSectionKeyPrefix;
    this.logger = logger;
    this.maxRetries = maxRetries;
  }

  registerJob(jobDef) {
    if (!this.isWaiting) throw new Error('JobHandler already has job!');
    this.isWaiting = false;
    this.currentJob = jobDef;
  }

  async handleJob() {
    const key = this.generateModelSectionKey();
    const { params, hash, trafficModel } = this.currentJob;
    const jobResult = await this.handleWithRetries({
      key,
      params,
      trafficModel,
    });
    if (jobResult !== 'done') {
      return jobResult;
    }
    const modelSection = await this.modelSectionService.createModelSection({
      key,
      hash,
      trafficModelId: trafficModel.id,
    });
    this.clearHandler();
    return modelSection;
  }

  cancelJob() {
    if (typeof this.cancelCall === 'function') return this.cancelCall();
    this.cancelCall = true; // Most likely in posting phase, which is not cancellable
    return true;
  }

  async handleWithRetries({ key, params, trafficModel } = {}) {
    try {
      const jobId = await postTrafficJob({
        cacheName: key,
        trafficParams: params,
        trafficModel,
      });
      if (!jobId) {
        this.clearHandler();
        return 'thread-limit';
      }
      if (this.cancelCall) throw new Error('cancelled');
      const { waitForResult, cancel } = awaitTrafficJobResult({
        jobId,
        trafficModel,
      });
      this.cancelCall = cancel;
      await waitForResult;
      return 'done';
    } catch (error) {
      if (error.message === 'cancelled') {
        this.clearHandler();
        return error.message;
      }
      this.retry += 1;
      // TODO check error cause before retrying
      if (this.retry <= this.maxRetries) return this.retryJob({ key, params });
      this.logger.warn(error);
      this.clearHandler();
      return 'error';
    }
  }

  async retryJob(options) {
    this.logger.verbose(
      `RETRY ${options.key} n${this.retry}. WAITING FOR ${2 ** this.retry}s`,
    );
    await wait(2 ** this.retry * 1000);
    return this.handleWithRetries(options);
  }

  clearHandler() {
    this.currentJob = null;
    this.retry = 0;
    this.isWaiting = true;
    this.cancelCall = null;
  }

  generateModelSectionKey() {
    return `${this.modelSectionKeyPrefix}-${generateID()}`;
  }
}

function wait(ms) {
  return new Promise((res) => {
    setTimeout(() => res(), ms);
  });
}
