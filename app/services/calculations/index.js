import { ScenarioCalculationService } from './scenarioCalculation.js';
import { ModelSectionService } from './modelSection.js';
import TrafficJobManager from './trafficJobManager.js';
import {
  Event,
  Modification,
  ModelSection,
  ScenarioModelSection,
  Op,
  db,
} from '../../../db/index.js';
import TmLogger from '../../utils/logger.js';

export { ScenarioCalculationService, ModelSectionService };
export * from './modificationToApiTransformers.js';
export * from './mergers.js';

const modelSectionService = new ModelSectionService({
  ModelSection,
  ScenarioModelSection,
  db,
  Op,
});

const scenarioCalculationService = new ScenarioCalculationService({
  eventModel: Event,
  modificationModel: Modification,
  modelSectionService,
  logger: TmLogger,
});

export default scenarioCalculationService;
export { modelSectionService, TrafficJobManager };
