import { mergeModelParamItems } from './mergers.js';

const modelTypeConfig = {
  DTA: {
    newLinkParamProp: 'update',
    typeModesParamProp: {
      'link-new': 'addEdge',
      'link-existing': 'updateEdge', // can be overriden to deleteEdge
      'node-new': 'addNode',
      'node-existing': 'updateMovement',
    },
    newLinkNetworkProp: {
      sourceNode: 'fromNodeId',
      targetNode: 'toNodeId',
      edge: 'edgeId',
    },
    createModelApiParams: () => ({
      update: [],
    }),
    extraIdProps: {
      'link-existing': [{ key: 'edgeId', list: 'link' }],
      'node-new': [{ key: 'nodeId', list: 'node' }],
      'node-existing': [
        { key: 'nodeId', list: 'node' },
        { key: 'sourceEdge', list: 'link' },
        { key: 'targetEdge', list: 'link' },
      ],
    },
    sortModelParams: sortDTAModelParams,
    checkModelParamsEmpty: (modelParams) => modelParams.update.length === 0,
  },
};

export default class ModelSectionFormat {
  constructor(modelType = 'DTA') {
    this.modelType = modelType;
    this.config = modelTypeConfig[modelType];
  }

  createModelSectionApiParams({
    dateInterval,
    modifications,
    extraIdBase,
  } = {}) {
    const modsInSection = filterModificationsInSection({
      dateInterval,
      modifications,
    });
    if (modsInSection.length === 0) return { apiParams: null };

    const { paramItems, itemsByParamId, missingItems } =
      this.buildParamItems(modsInSection);
    // Mutates paramItems map
    this.mergeParamItemDuplicates(paramItems, itemsByParamId);

    const { codeList, newLinkApiParams, newLinkFaultyApiParams } =
      this.buildNewLinksNetwork({
        paramItems,
        extraIdBase,
      });
    const { apiParams, faultyParams } = this.buildModelParams(
      paramItems,
      codeList.server,
    );

    faultyParams.push(...newLinkFaultyApiParams, ...missingItems);
    apiParams[this.config.newLinkParamProp].push(...newLinkApiParams);

    this.config.sortModelParams(apiParams); // Return model params with consistent array ordering
    const hasParamsEmpty = this.config.checkModelParamsEmpty(apiParams);
    return {
      apiParams,
      codeList: codeList.client,
      faultyParams,
      hasParamsEmpty,
    };
  }

  // eslint-disable-next-line class-methods-use-this
  buildParamItems(modifications) {
    let paramIndex = 0;
    return modifications.reduce(
      (output, modificationData) => {
        const { typeMode, apiParams } = modificationData;
        for (const { param, paramId } of apiParams) {
          const paramItem = {
            param,
            paramId,
            typeMode,
            apiProperty: this.config.typeModesParamProp[typeMode],
          };
          if (!param) output.missingItems.push(paramItem);
          else {
            output.paramItems.set(paramIndex, paramItem);
            const paramsByIdList = output.itemsByParamId.get(paramId) ?? [];
            paramsByIdList.push(paramIndex);
            output.itemsByParamId.set(paramId, paramsByIdList);
            paramIndex += 1;
          }
        }
        return output;
      },
      {
        paramItems: new Map(),
        itemsByParamId: new Map(),
        missingItems: [],
      },
    );
  }

  mergeParamItemDuplicates(paramItems, itemsByParamId) {
    const toUpdate = [];
    const toRemove = [];

    for (const itemsIds of itemsByParamId.values()) {
      if (itemsIds.length > 1) {
        let targetItem = paramItems.get(itemsIds[0]);
        for (let i = 1; i < itemsIds.length; i++) {
          const sourceItem = paramItems.get(itemsIds[i]);
          targetItem = mergeModelParamItems(
            targetItem,
            sourceItem,
            this.modelType,
          );
        }

        if (targetItem)
          toUpdate.push({
            key: itemsIds[0],
            item: targetItem,
          });
        toRemove.push(...itemsIds);
      }
    }

    for (const removeItem of toRemove) {
      paramItems.delete(removeItem);
    }
    for (const { key, item } of toUpdate) {
      paramItems.set(key, item);
    }
  }

  buildNewLinksNetwork({ paramItems, extraIdBase } = {}) {
    const codeList = getExtraCodeListBase();
    const extraId = { node: extraIdBase.node + 1, link: extraIdBase.link + 1 };
    const apiParams = [];
    const faultyApiParams = [];

    let remainingLinkItems = Array.from(paramItems.values()).filter(
      ({ typeMode }) => typeMode === 'link-new',
    );
    let connectableLinks;

    while (remainingLinkItems.length > 0) {
      ({ remainingLinkItems, connectableLinks } = getConnectableLinks(
        remainingLinkItems,
        codeList,
        this.config.newLinkNetworkProp,
      ));

      // TODO optimize extraId translations in one cycle
      if (connectableLinks.length > 0) {
        connectableLinks.sort((a, b) =>
          linkSortFunction(
            a,
            b,
            codeList.server.node,
            this.config.newLinkNetworkProp,
          ),
        );
        for (const { param, apiProperty } of connectableLinks) {
          const paramCopy = { ...param };
          createOrUpdateExtraId({
            type: 'node',
            extraId,
            param: paramCopy,
            property: this.config.newLinkNetworkProp.sourceNode,
            codeList,
          });
          createOrUpdateExtraId({
            type: 'node',
            extraId,
            param: paramCopy,
            property: this.config.newLinkNetworkProp.targetNode,
            codeList,
          });
          createExtraId({
            type: 'link',
            extraId,
            param: paramCopy,
            property: this.config.newLinkNetworkProp.edge,
            codeList,
          });
          if (this.modelType === 'DTA') {
            paramCopy.type = apiProperty;
          }
          apiParams.push(paramCopy);
          // TODO Remove edgeId in DTA mode???
        }
      } else {
        faultyApiParams.push(...remainingLinkItems);
        remainingLinkItems = [];
      }
    }

    return {
      codeList,
      newLinkApiParams: apiParams,
      newLinkFaultyApiParams: faultyApiParams,
    };
  }

  buildModelParams(paramItems, codeList) {
    const apiParams = this.config.createModelApiParams();
    const faultyParams = [];

    const paramItemsWithoutLinkNew = Array.from(paramItems.values()).filter(
      ({ typeMode }) => typeMode !== 'link-new',
    );

    for (const paramItem of paramItemsWithoutLinkNew) {
      const { apiProperty, param, typeMode } = paramItem;
      const paramCopy = { ...param };
      const extraIdProps = this.getExtraIdProps(paramCopy, typeMode);
      let connectionSuccess = true;
      while (extraIdProps.length > 0 && connectionSuccess) {
        const { key, list } = extraIdProps.shift();
        const localId = paramCopy[key];
        const apiId = codeList[list][localId];
        if (apiId) paramCopy[key] = apiId;
        else connectionSuccess = false;
      }
      if (connectionSuccess) {
        paramCopy.type = paramCopy.type ?? apiProperty;
        apiParams.update.push(paramCopy);
      } else faultyParams.push(paramItem);
    }

    return { apiParams, faultyParams };
  }

  getExtraIdProps(param, typeMode) {
    return this.config.extraIdProps[typeMode].reduce(
      (extraIdProps, property) => {
        if (typeof param[property.key] !== 'number')
          extraIdProps.push(property);
        return extraIdProps;
      },
      [],
    );
  }
}

function filterModificationsInSection({ modifications, dateInterval }) {
  const [dateFrom, dateTo] = dateInterval;
  return modifications.filter(
    (modification) =>
      (modification.dateFrom === null ||
        (dateFrom !== null && modification.dateFrom <= dateFrom)) &&
      (modification.dateTo === null ||
        (dateTo !== null && modification.dateTo >= dateTo)),
  );
}

function getExtraCodeListBase() {
  return {
    server: { node: {}, link: {} },
    client: { node: {}, link: {} },
  };
}

function getConnectableLinks(linkParamItems, codeList, propNames) {
  return linkParamItems.reduce(
    (acc, paramItem) => {
      const isConnectable = isLinkConnectable(
        paramItem.param,
        codeList.server,
        propNames,
      );
      if (isConnectable) acc.connectableLinks.push(paramItem);
      else acc.remainingLinkItems.push(paramItem);
      return acc;
    },
    {
      remainingLinkItems: [],
      connectableLinks: [],
    },
  );
}

function isLinkConnectable(param, codeList, propNames) {
  return (
    isLinksNodeConnectable(param[propNames.sourceNode], codeList) ||
    isLinksNodeConnectable(param[propNames.targetNode], codeList)
  );
}

function isLinksNodeConnectable(nodeId, codeList) {
  return typeof nodeId === 'number' || codeList.node[nodeId] !== undefined;
}

const isNumber = (val) => typeof val === 'number';
const translateExtraNodeId = (id, nodeCodeList) =>
  isNumber(id) || !nodeCodeList[id] ? id : nodeCodeList[id];

function linkSortFunction(a, b, nodeCodeList, propNames) {
  const aSource = translateExtraNodeId(
    a.param[propNames.sourceNode],
    nodeCodeList,
  );
  const bSource = translateExtraNodeId(
    b.param[propNames.sourceNode],
    nodeCodeList,
  );
  if ((isNumber(aSource) || isNumber(bSource)) && aSource !== bSource) {
    if (isNumber(aSource) && isNumber(bSource)) return aSource - bSource;
    if (isNumber(aSource)) return -1;
    return 1;
  }
  const aTarget = translateExtraNodeId(
    a.param[propNames.targetNode],
    nodeCodeList,
  );
  const bTarget = translateExtraNodeId(
    b.param[propNames.targetNode],
    nodeCodeList,
  );
  if ((isNumber(aTarget) || isNumber(bTarget)) && aTarget !== bTarget) {
    if (isNumber(aTarget) && isNumber(bTarget)) return aTarget - bTarget;
    if (isNumber(aTarget)) return -1;
    return 1;
  }
  return 0;
}

function createOrUpdateExtraId({ type, extraId, param, property, codeList }) {
  const localId = param[property];
  if (!isNumber(localId)) {
    const apiId = codeList.server[type][localId];
    // eslint-disable-next-line no-param-reassign
    if (apiId) param[property] = apiId;
    else
      createExtraId({
        type,
        extraId,
        param,
        property,
        codeList,
        value: localId,
      });
  }
}

// Mutates extraId, param, codeList
function createExtraId({ type, extraId, param, property, codeList, value }) {
  const localId = value ?? param[property];
  const apiId = extraId[type];
  // eslint-disable-next-line no-param-reassign
  codeList.server[type][localId] = apiId;
  // eslint-disable-next-line no-param-reassign
  codeList.client[type][apiId] = localId;
  // eslint-disable-next-line no-param-reassign
  extraId[type] += 1;
  // eslint-disable-next-line no-param-reassign
  param[property] = apiId;
}

function sortDTAModelParams(modelParams) {
  modelParams.update.sort(sortDTAModelParamsFn);
}

const sortDTAModelParamsType = {
  addNode: {
    order: 1,
    fn: (a, b) => a.nodeId - b.nodeId,
  },
  addEdge: {
    order: 2,
    fn: (a, b) => a.edgeId - b.edgeId,
  },
  deleteEdge: {
    order: 3,
    fn: (a, b) => a.edgeId - b.edgeId,
  },
  updateEdge: {
    order: 4,
    fn: (a, b) => a.edgeId - b.edgeId,
  },
  updateMovement: {
    order: 5,
    fn: (a, b) => {
      const nodeDiff = a.nodeId - b.nodeId;
      if (nodeDiff !== 0) return nodeDiff;
      const fromDiff = a.sourceEdge - b.sourceEdge;
      if (fromDiff !== 0) return fromDiff;
      const toDiff = a.targetEdge - b.targetEdge;
      return toDiff;
    },
  },
};

function sortDTAModelParamsFn(a, b) {
  const aOrder = sortDTAModelParamsType[a.type].order;
  const bOrder = sortDTAModelParamsType[b.type].order;
  if (aOrder !== bOrder) return aOrder - bOrder;
  return sortDTAModelParamsType[a.type].fn(a, b);
}
