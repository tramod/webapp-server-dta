/* eslint-disable class-methods-use-this */
import hasher from 'node-object-hash';
import ScenariosModificationsFormat from './modificationFormat.js';
import ModelSectionFormat from './modelSectionFormat.js';
import TrafficJobManager from './trafficJobManager.js';
import { getModelInfos } from '../modelAPI.js';

class ScenarioCalculationService {
  constructor({
    eventModel,
    modificationModel,
    modelSectionService,
    logger,
  } = {}) {
    this.modelSectionService = modelSectionService;
    this.jobManagers = {};
    this.scenariosModificationsFormat = new ScenariosModificationsFormat({
      modificationModel,
      eventModel,
    });
    this.DTAModelSectionFormat = new ModelSectionFormat('DTA');
    this.logger = logger;
    this.hasher = hasher({ sort: false, coerce: false });
  }

  async calculateScenarioCb(scenario, onCalculationEnd) {
    try {
      const results = await this.calculateScenario(scenario);
      const hasWarning = results.some((result) => result.warning === true);
      onCalculationEnd({ status: true, hasWarning });
    } catch (error) {
      this.logger.warn(error);
      onCalculationEnd({ status: false });
    }
  }

  async cancelCalculation(scenario) {
    const trafficModel =
      scenario.trafficModel || (await scenario.getTrafficModel());
    const jobManager = this.getJobManager(trafficModel);
    jobManager.cancel({ scenarioId: scenario.rowId });
  }

  getJobManager(trafficModel) {
    if (!this.jobManagers[trafficModel.id]) {
      this.jobManagers[trafficModel.id] = new TrafficJobManager({
        modelSectionService: this.modelSectionService,
        logger: this.logger,
        trafficModel,
      });
    }
    return this.jobManagers[trafficModel.id];
  }

  async cleanUpJobs(trafficModel) {
    const jobManager = this.getJobManager(trafficModel);
    jobManager.clearDeprecatedJobs();
  }

  async calculateScenario(scenario) {
    const timeStamp = new Date();
    const trafficModel =
      scenario.trafficModel || (await scenario.getTrafficModel());
    const modificationsData =
      await this.scenariosModificationsFormat.getModificationItems({
        scenario,
        trafficModel,
      });
    const sectionIntervals =
      this.getScenarioModelSectionIntervals(modificationsData);
    const extraIdBase = await getModelInfos({ trafficModel });

    const jobManager = this.getJobManager(trafficModel);
    const calculationSections = await Promise.all(
      sectionIntervals.map(async (dateInterval) =>
        this.calculateScenarioModelSection({
          scenario,
          dateInterval,
          extraIdBase,
          modifications: modificationsData,
          trafficModel,
          jobManager,
        }),
      ),
    );

    await this.modelSectionService.removeScenariosModelSections(scenario, {
      timeStamp,
    });
    // ! Still can introduce race condition between section linking and clearing
    // ModelSection can be removed by mistake then when multiple calculations occur
    // TODO Rewrite with transaction when sqlite support is dropped
    jobManager.clearDeprecatedJobs();
    return calculationSections;
  }

  getScenarioModelSectionIntervals(modifications) {
    const { dates, freeFrom, freeTo } =
      this.getDatesFromModificationsData(modifications);
    const uniqueDates = this.filterUniqueDates(dates);
    const sortedDates = [
      ...(freeFrom ? [null] : []),
      ...this.sortDates(uniqueDates),
      ...(freeTo ? [null] : []),
    ];

    const dateIntervals = [];
    for (let i = 0; i < sortedDates.length - 1; i++) {
      dateIntervals.push([sortedDates[i], sortedDates[i + 1]]);
    }
    return dateIntervals;
  }

  getDatesFromModificationsData(modificationsData) {
    return modificationsData.reduce(
      (output, modification) => {
        const { dateFrom, dateTo } = modification;
        if (dateFrom) output.dates.push(dateFrom);
        if (dateTo) output.dates.push(dateTo);
        return {
          dates: output.dates,
          freeFrom: output.freeFrom || dateFrom === null,
          freeTo: output.freeTo || dateTo === null,
        };
      },
      { dates: [], freeFrom: false, freeTo: false },
    );
  }

  filterUniqueDates(dates) {
    return Array.from(new Set(dates.map((date) => date.valueOf()))).map(
      (timeStamp) => new Date(timeStamp),
    );
  }

  sortDates(dates) {
    return dates.sort((a, b) => a - b);
  }

  async calculateScenarioModelSection({
    scenario,
    dateInterval,
    extraIdBase,
    modifications,
    trafficModel,
    jobManager,
  } = {}) {
    const modelSectionFormat = this.DTAModelSectionFormat;
    const { apiParams, codeList, faultyParams, hasParamsEmpty } =
      modelSectionFormat.createModelSectionApiParams({
        dateInterval,
        modifications,
        extraIdBase,
      });

    if (apiParams && !hasParamsEmpty) {
      const hash = this.hasher.hash(apiParams);
      let modelSection = await this.modelSectionService.getModelSectionByHash(
        hash,
      );
      if (!modelSection)
        modelSection = await jobManager.calculate({
          hash,
          params: apiParams,
          trafficModel,
          scenarioId: scenario.rowId,
        });
      await this.modelSectionService.linkScenarioModelSection(
        scenario,
        modelSection,
        {
          dateFrom: dateInterval[0],
          dateTo: dateInterval[1],
          codeList,
        },
      );
    }

    const warning = !!faultyParams && faultyParams.length > 0;
    return { status: true, warning };
  }
}

// eslint-disable-next-line import/prefer-default-export
export { ScenarioCalculationService };
