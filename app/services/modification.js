/* eslint-disable class-methods-use-this */
import DBModels, { Op as OpSource } from '../../db/index.js';
import eventServiceInstance from './events.js';
import TmLogger from '../utils/logger.js';
import { TmErrorGeneral, TmNotFoundError } from '../utils/error.js';
import { resourcePatcher, getChangedModelValues } from './utils.js';

// New options should requires changes in validator and factories
const MODIFICATIONS_TYPES = ['link', 'node', 'generator'];
const MODIFICATIONS_MODES = ['existing', 'new'];
const MODIFICATION_PROPS = {
  general: [
    'name',
    'description',
    'note',
    'dateFrom',
    'dateTo',
    'type',
    'mode',
  ],
  'link-existing': ['linkId', 'capacity', 'speed', 'lanes'],
  'link-new': [
    'source',
    'target',
    'coordinates',
    'capacity',
    'speed',
    'lanes',
    'twoWay',
    'twoWayCapacity',
    'twoWaySpeed',
    'twoWayLanes',
  ],
  'node-existing': ['node', 'linkFrom', 'linkTo', 'cost', 'capacity'],
  'node-new': ['node'],
  'generator-new': ['generator', 'nodes', 'inTraffic', 'outTraffic'],
  'generator-existing': ['generator', 'nodes', 'inTraffic', 'outTraffic'],
};

const COMMON_FIELDS_COMPUTE = ['dateFrom', 'dateTo'];
// ! Currently we do not detect which of 'data' properties changed, but all of them are computable so watching whole data prop could be enough
// const variants = Object.keys(MODIFICATION_PROPS).slice(1);
// const FIELDS_COMPUTE = variants.reduce(
//   (map, variant) => ({
//     ...map,
//     [variant]: [...COMMON_FIELDS_COMPUTE, ...MODIFICATION_PROPS[variant]],
//   }),
//   {},
// );
const FIELDS_COMPUTE = [...COMMON_FIELDS_COMPUTE, 'data'];
class ModificationService {
  constructor({ models, eventService, logger, Op } = {}) {
    this.models = models;
    this.eventService = eventService;
    this.Op = Op;
    this.logger = logger;
  }

  async createModification(
    event,
    props,
    { provideItemId, rowItemId = true } = {},
  ) {
    const typeMode = getTypeMode(props);
    const values = {
      ...parseModificationProps(typeMode, { ...props.data, ...props }),
      ...(provideItemId && {
        itemId: provideItemId,
        ...(!props.name && { name: `${props.type}-${provideItemId}` }),
      }),
      eventRowId: event.rowId,
    };

    const modification = await this.models.Modification.create(values);

    if (!provideItemId && rowItemId)
      await modification.update({
        itemId: modification.rowId,
        ...(!props.name && { name: `${props.type}-${modification.rowId}` }),
      });

    return modification;
  }

  async deleteModification(modification, parentEvent = null) {
    await modification.destroy();
    // mark event as updated
    if (parentEvent) {
      parentEvent.changed('updatedAt', true);
      await parentEvent.update({ updatedAt: new Date() });
    }
  }

  async getModificationsByEvent(event) {
    return event.getModifications();
  }

  async getModificationsByEvents(events) {
    const eventRowIds = events.map((event) => event.rowId);
    return this.models.Modification.findAll({
      where: {
        eventRowId: { [this.Op.in]: eventRowIds },
      },
    });
  }

  async getModificationById(event, itemId) {
    const modification = await this.models.Modification.findOne({
      where: { itemId, eventRowId: event.rowId },
    });
    return modification;
  }

  async updateModification(event, modificationOrId, props) {
    const modification =
      modificationOrId ||
      (await this.getModificationById(event, modificationOrId));
    // TODO Check if needed / remove or fix logging
    if (!modification)
      throw new TmNotFoundError('Modification not found in event/scenario');

    const typeMode = getTypeMode(modification);
    const values = parseModificationProps(typeMode, {
      ...modification.data, // Pass old modification data so they are merged with updated parts
      ...props,
    });
    resourcePatcher(modification, values);
    const changed = getChangedModelValues(modification);
    await modification.save();

    const isComputableChange = changed.fieldsChanged
      ? changed.fieldsChanged.filter((x) => FIELDS_COMPUTE.includes(x)).length >
        0
      : false;

    return {
      modification,
      isComputableChange,
      changed,
    };
  }

  async getScenarioNodeTransit(scenario, nodeId) {
    const scenarioEventsRowIds = scenario.events.map((event) => event.rowId);

    const nodeModifications = await this.models.Modification.findAll({
      where: {
        type: 'node',
        mode: 'existing',
        eventRowId: { [this.Op.in]: scenarioEventsRowIds },
      },
    });
    // Filter correct node by JS, as sql filter introduced type mismatch if both base model nodes and extra nodes were combined
    const modsOnTargetNode = nodeModifications.filter(
      (mod) => mod.data.node === nodeId,
    );

    if (modsOnTargetNode.length === 0) return modsOnTargetNode;

    const modificationsEventRowIds = modsOnTargetNode.map(
      (item) => item.eventRowId,
    );
    const eventsMapByRowId = scenario.events
      .filter((event) => modificationsEventRowIds.includes(event.rowId))
      .reduce((map, event) => {
        map.set(event.rowId, event);
        return map;
      }, new Map());

    return modsOnTargetNode.map((modification) => {
      const event = eventsMapByRowId.get(modification.eventRowId);
      return {
        eventId: event.itemId,
        eventName: event.name,
        eventIncluded: event.included,
        ...modification.data,
        dateFrom: modification.dateFrom,
        dateTo: modification.dateTo,
        isBaseModel: false,
        value: modification.data.capacity,
      };
    });
  }

  async getModificationsByScenario(
    scenarioRowId,
    {
      includedOnly = false,
      excludedOnly = false,
      newOnly = false,
      modIds = [],
      ignoredEvent = null,
    } = {},
  ) {
    return this.models.Modification.findAll({
      where: {
        ...(modIds.length && { item_id: { [this.Op.in]: modIds } }),
        ...(newOnly && { mode: 'new' }),
      },
      include: {
        model: this.models.Event,
        required: true,
        attributes: ['itemId', 'included'],
        where: {
          scenarioRowId,
          ...(ignoredEvent && { row_id: { [this.Op.not]: ignoredEvent } }),
          ...(includedOnly && { included: true }),
          ...(excludedOnly && { included: false }),
        },
      },
      attributes: [
        [this.models.Sequelize.literal('"event"."item_id"'), 'eventId'],
        [this.models.Sequelize.literal('"event"."included"'), 'included'],
        'rowId',
        'itemId',
        ['item_id', 'id'],
        'dateFrom',
        'dateTo',
        'data',
        'mode',
        'type',
      ],
    });
  }

  async getDependentMods(
    scenario,
    modifications,
    {
      excludingEventId = null,
      includingEventId = null,
      deletingEventId = null,
      dateRange = [],
    } = {},
  ) {
    const scenarioMods =
      (await this.getModificationsByScenario(scenario.rowId, {
        ignoredEvent: excludingEventId || includingEventId || deletingEventId,
        includedOnly: excludingEventId ? true : null,
        excludedOnly: includingEventId ? true : null,
      })) || [];

    const scenarioModsDependencies = scenarioMods.map((mod) => {
      const rawMod = mod.toJSON();
      return {
        id: rawMod.id,
        dependencies: parseModificationExtrasDependencies(rawMod),
        ...(dateRange.length && {
          dateFrom: rawMod.dateFrom,
          dateTo: rawMod.dateTo,
        }),
      };
    });

    const dependentMods = [];
    let dependencyFunction;
    if (includingEventId) dependencyFunction = getParentDepModIds;
    else if (dateRange.length)
      dependencyFunction = getParentAndChildDepModIdsInRange;
    else dependencyFunction = getChildrenDepModIds;

    modifications.forEach((mod) => {
      const modData = mod.data ? mod.toJSON() : { ...mod, data: mod };
      dependentMods.push(
        ...dependencyFunction({
          dependencyList: scenarioModsDependencies,
          modificationId: mod.itemId,
          dateRange: dateRange.length ? dateRange : null,
          defaultDepMods:
            includingEventId || dateRange.length
              ? parseModificationExtrasDependencies(modData)
              : null,
        }),
      );
    });

    return [...new Set([...dependentMods])];
  }

  async removeModsByIds(scenario, modIds) {
    const deletedMods = [];
    let isRemovedIncludedMod = false;

    const modifications = await this.getModificationsByScenario(
      scenario.rowId,
      { modIds },
    );

    /* eslint-disable no-await-in-loop */
    for (const mod of modifications) {
      await this.deleteModification(mod);

      if (mod.included) isRemovedIncludedMod = true;
      deletedMods.push({
        evId: mod.event.itemId,
        modId: mod.itemId,
        type: mod.type,
        mode: mod.mode,
      });
    }

    return { deletedMods, isRemovedIncludedMod };
  }
}

const modificationServiceInstance = new ModificationService({
  models: DBModels,
  eventService: eventServiceInstance,
  Op: OpSource,
  logger: TmLogger,
});

export default modificationServiceInstance;
export { ModificationService };

function getTypeMode(props, required = true) {
  if (!required && (!props.type || !props.mode)) return null;
  if (!MODIFICATIONS_TYPES.includes(props.type))
    throw new TmErrorGeneral(`Invalid modification type ${props.type}`);
  if (!MODIFICATIONS_MODES.includes(props.mode))
    throw new TmErrorGeneral(`Invalid modification mode ${props.mode}`);
  return `${props.type}-${props.mode}`;
}

function parseModificationProps(typeMode, props) {
  const dataProp = MODIFICATION_PROPS[typeMode].reduce((data, key) => {
    // eslint-disable-next-line no-param-reassign
    if (typeof props[key] !== 'undefined') data[key] = props[key];
    return data;
  }, {});
  return MODIFICATION_PROPS.general.reduce(
    (parsed, key) => {
      // eslint-disable-next-line no-param-reassign
      if (typeof props[key] !== 'undefined') parsed[key] = props[key];
      return parsed;
    },
    { data: dataProp },
  );
}

export function parseModificationExtrasDependencies(modification) {
  const { type, mode, data } = modification;
  const typeMode = `${type}-${mode}`;
  const dependencies = [];

  const addExtrasAsDependencies = (ids) => {
    ids.forEach((id) => {
      if (typeof id === 'string') dependencies.push(id.split('-')[1]);
    });
  };

  switch (typeMode) {
    case 'link-existing':
      addExtrasAsDependencies(data.linkId);
      break;
    case 'link-new':
      addExtrasAsDependencies([data.source, data.target]);
      break;
    case 'node-existing':
      addExtrasAsDependencies([data.node, data.linkFrom, data.linkTo]);
      break;
    case 'node-new':
      break;
    case 'generator-existing':
      addExtrasAsDependencies([...data.generator, ...data.nodes]);
      break;
    case 'generator-new':
      addExtrasAsDependencies(data.nodes);
      break;
    default:
      break;
  }

  return dependencies;
}

export function getChildrenDepModIds(
  { dependencyList, modificationId },
  allNestedDependentModIds = new Set(),
) {
  const dpendantMods = dependencyList.flatMap((mod) =>
    mod.dependencies.includes(modificationId.toString()) ? mod.id : [],
  );

  dpendantMods.forEach((depModId) => {
    allNestedDependentModIds.add(depModId);

    getChildrenDepModIds(
      { dependencyList, modificationId: depModId },
      allNestedDependentModIds,
    );
  });

  return [...allNestedDependentModIds];
}

export function getParentDepModIds(
  { dependencyList, modificationId, defaultDepMods = null },
  allNestedDependingModIds = new Set(),
) {
  const dpendingMods = defaultDepMods
    ? defaultDepMods.filter((modId) =>
        dependencyList.find((mod) => mod.id.toString() === modId.toString()),
      )
    : dependencyList.find(
        (mod) => mod.id.toString() === modificationId.toString(),
      ).dependencies;

  dpendingMods.forEach((depModId) => {
    allNestedDependingModIds.add(depModId);

    getParentDepModIds(
      { dependencyList, modificationId: depModId },
      allNestedDependingModIds,
    );
  });

  return [...allNestedDependingModIds];
}

export function getParentAndChildDepModIdsInRange({
  dependencyList,
  modificationId = null,
  defaultDepMods = [],
  dateRange = [],
}) {
  const parseDate = (date) => (date ? new Date(date) : null);
  const modDateFrom = parseDate(dateRange[0]);
  const modDateTo = parseDate(dateRange[1]);
  const depMods = [];

  const dpendingMods = defaultDepMods.map((modId) =>
    dependencyList.find((mod) => mod.id.toString() === modId.toString()),
  );

  const dpendantMods = modificationId
    ? dependencyList.flatMap((mod) =>
        mod.dependencies.includes(modificationId.toString()) ? mod : [],
      )
    : [];

  dpendingMods.forEach((mod) => {
    const depDateFrom = parseDate(mod.dateFrom);
    const depDateTo = parseDate(mod.dateTo);

    if (
      (depDateFrom && (!modDateFrom || depDateFrom > modDateFrom)) ||
      (depDateTo && (!modDateTo || depDateTo < modDateTo))
    ) {
      depMods.push({ ...mod, dependency: 'parent' });
    }
  });

  dpendantMods.forEach((mod) => {
    const depDateFrom = parseDate(mod.dateFrom);
    const depDateTo = parseDate(mod.dateTo);

    if (
      (modDateFrom && (!depDateFrom || depDateFrom < modDateFrom)) ||
      (modDateTo && (!depDateTo || depDateTo > modDateTo))
    ) {
      depMods.push({ ...mod, dependency: 'child' });
    }
  });

  return depMods;
}
