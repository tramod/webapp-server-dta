import { subject } from '@casl/ability';

export function isEventInScenario(scenario, event) {
  return event.scenarioRowId === scenario.rowId;
}

export function resourcePatcher(resource, values) {
  for (const [key, value] of Object.entries(values)) {
    if (value !== undefined)
      // eslint-disable-next-line no-param-reassign
      resource[key] = value;
  }
}

export function getChangedModelValues(model, fields) {
  const fieldsChanged = fields || model.changed();
  const respObject = {
    oldData: {},
    newData: {},
    fieldsChanged,
  };
  if (!fieldsChanged) return respObject;
  return fieldsChanged.reduce((data, field) => {
    // eslint-disable-next-line no-param-reassign
    data.newData[field] = model.get(field);
    // eslint-disable-next-line no-param-reassign
    data.oldData[field] = model.previous(field);
    return data;
  }, respObject);
}

// Parse scenario model instance and returns raw object compatible with casl ability functions
export function formatScenarioForAbility(scenario) {
  const rawScenario = scenario.toJSON();
  if (!rawScenario.level) rawScenario.level = scenario.get('level');
  if (scenario.events)
    rawScenario.events = scenario.events.map(formatEventForAbility);
  rawScenario.modelName = 'scenario';
  return subject('scenario', rawScenario);
}

export function formatEventForAbility(event) {
  return event.toJSON();
}

export function getDateRange(items = []) {
  let dateFrom = null;
  let dateTo = null;
  const count = items.length;

  if (count > 0) {
    const first = items[0];
    dateFrom = first.dateFrom !== null ? new Date(first.dateFrom) : null;
    dateTo = first.dateTo !== null ? new Date(first.dateTo) : null;
  }

  if (count > 1) {
    let i = 1;
    while (i < count && (dateFrom !== null || dateTo !== null)) {
      const item = items[i];
      if (dateFrom !== null) {
        if (item.dateFrom === null) dateFrom = null;
        else {
          const itemDateFrom = new Date(item.dateFrom);
          dateFrom = dateFrom < itemDateFrom ? dateFrom : itemDateFrom;
        }
      }
      if (dateTo !== null) {
        if (item.dateTo === null) dateTo = null;
        else {
          const itemDateTo = new Date(item.dateTo);
          dateTo = dateTo > itemDateTo ? dateTo : itemDateTo;
        }
      }
      i += 1;
    }
  }

  return { dateFrom, dateTo };
}

export function getLatestUpdatedAtDate(originalModel, childModels = []) {
  let lastUpdatedAt = originalModel.updatedAt;
  let updateDesired = false;

  childModels.forEach((model) => {
    if (model.updatedAt > lastUpdatedAt) {
      lastUpdatedAt = model.updatedAt;
      updateDesired = true;
    }
  });

  if (updateDesired) {
    // mark as changed, so the next update is possible in case no other fields were changed
    originalModel.changed('updatedAt', true);
  }

  return lastUpdatedAt;
}
