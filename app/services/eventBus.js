/* eslint-disable import/prefer-default-export */
import { EventEmitter } from 'events';

const configEvents = new EventEmitter();

export { configEvents };
