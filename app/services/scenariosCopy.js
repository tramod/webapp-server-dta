/* eslint-disable class-methods-use-this */
// Helper class to avoid cyclic dependency
import DBModels from '../../db/index.js';
import scenarioServiceInstance from './scenarios.js';
import eventServiceInstance from './events.js';
import modificationServiceInstance from './modification.js';

// TODO Refactor copies into bulks where possible

class ScenarioCopyService {
  constructor({
    scenarioService,
    eventService,
    modificationService,
    models,
  } = {}) {
    this.scenarioService = scenarioService;
    this.eventService = eventService;
    this.modificationService = modificationService;
    this.models = models;
  }

  async copyScenario({
    scenario,
    user,
    withEvents = true,
    withAccesses = false,
    withCalculations = true,
    copyAuthor = false,
    setItemId = false,
    copyCreatedAt = false,
    copyUpdatedAt = false,
    markCopy = false,
  } = {}) {
    const scenarioUser = copyAuthor ? { id: scenario.authorUserId } : user;
    const duplicate = await this.scenarioService.createScenario(
      {
        ...scenario.get(),
        user: scenarioUser,
        ...(markCopy && { name: `(copy) ${scenario.name}` }),
        ...(!withAccesses && { isPublic: false, isInPublicList: false }),
        ...(!copyCreatedAt && { createdAt: null }),
        ...(!copyUpdatedAt && { updatedAt: null }),
      },
      { rowItemId: setItemId },
    );

    if (withEvents) {
      await duplicate.update({
        dateFrom: scenario.dateFrom,
        dateTo: scenario.dateTo,
      });
      await this.copyScenarioInnerItems({
        sourceScenario: scenario,
        targetScenario: duplicate,
        ...(!copyAuthor && { userOverride: user }),
      });
    }

    if (withAccesses)
      await this.copyAccesses({
        sourceScenario: scenario,
        targetScenario: duplicate,
      });

    if (withCalculations) {
      const scenarioModelSections = await scenario.getScenarioModelSections();
      const toInsert = scenarioModelSections.map(
        ({ modelSectionKey, dateFrom, dateTo, codeList }) => ({
          modelSectionKey,
          dateFrom,
          dateTo,
          codeList,
          scenarioRowId: duplicate.rowId,
        }),
      );
      await this.models.ScenarioModelSection.bulkCreate(toInsert);
    }

    return duplicate;
  }

  async copyScenarioInnerItems({
    sourceScenario,
    targetScenario,
    userOverride,
  } = {}) {
    const events = sourceScenario.events || (await sourceScenario.getEvents());
    const duplicateEvents = await this.batchCopyEvents({
      events,
      scenario: targetScenario,
      userOverride,
    });

    // Is items order guaranteed to be same?
    const eventRowIdMap = {};
    for (let i = 0; i < events.length; i++) {
      const oldRowId = events[i].rowId;
      const newRowId = duplicateEvents[i].rowId;
      eventRowIdMap[oldRowId] = newRowId;
    }

    const modifications =
      await this.modificationService.getModificationsByEvents(events);
    await this.batchCopyModifications({
      modifications,
      eventRowIdMap,
    });
  }

  async copyAccesses({ sourceScenario, targetScenario }) {
    // delete any old accesses first (at least the author has always access)
    await this.models.ScenarioAccess.destroy({
      where: { scenarioRowId: targetScenario.rowId },
    });
    // copy original accesses
    const users = await sourceScenario.getAuthorizedUsers();
    const toInsert = users.reduce((values, userToCopy) => {
      const { level, userId } = userToCopy.scenarioAccess;
      values.push({ level, userId, scenarioRowId: targetScenario.rowId });
      return values;
    }, []);
    await this.models.ScenarioAccess.bulkCreate(toInsert);
  }

  // Optimized variant for copy scenario
  // Assumes whole scenario copy -> keeps item ids, copies with modifications
  async batchCopyEvents({ events, scenario, userOverride } = {}) {
    const toInsert = events.reduce((values, eventToCopy) => {
      const eventUser = !userOverride
        ? { id: eventToCopy.authorUserId }
        : userOverride;
      const item = {
        ...eventToCopy.get(),
        scenarioRowId: scenario.rowId,
        authorUserId: eventUser.id,
      };
      delete item.rowId;
      // delete item.updatedAt; // we intentionally keep original createdAt/updatedAt in the event copies
      values.push(item);
      return values;
    }, []);
    return this.models.Event.bulkCreate(toInsert);
  }

  async batchCopyModifications({
    modifications,
    eventRowIdMap,
    withOriginalItemId = true,
  } = {}) {
    const toInsert = modifications.reduce((values, toCopy) => {
      const item = {
        ...toCopy.get(),
        eventRowId: eventRowIdMap[toCopy.eventRowId],
      };
      delete item.rowId;
      // delete item.updatedAt; // we intentionally keep original createdAt/updatedAt in the modification copies
      values.push(item);
      return values;
    }, []);
    const copies = await this.models.Modification.bulkCreate(toInsert);

    if (!withOriginalItemId) {
      await Promise.all(
        copies.map(async (modCopy) => {
          await modCopy.update({ itemId: modCopy.rowId });
        }),
      );
    }

    return copies;
  }

  async copyEvent({
    event,
    scenario,
    user,
    withModifications = true,
    withOriginalItemId = true,
    includedOverride = null,
    markCopy = false,
    markString = '(copy)',
  }) {
    const copiedEvent = await this.eventService.createEvent(
      scenario,
      {
        ...event.get(),
        user,
        ...(markCopy && { name: `${markString} ${event.name}` }),
        included: includedOverride ?? event.included,
      },
      {
        provideItemId: withOriginalItemId ? event.itemId : false,
      },
    );

    if (!withModifications) {
      // event without modifications has null dates
      await copiedEvent.update({
        dateFrom: null,
        dateTo: null,
      });
    }

    if (withModifications) {
      const modifications =
        await this.modificationService.getModificationsByEvent(event);
      const eventRowIdMap = {
        [event.rowId]: copiedEvent.rowId,
      };

      const copiedModifications = await this.batchCopyModifications({
        modifications,
        eventRowIdMap,
        withOriginalItemId,
      });
      copiedEvent.modifications = copiedModifications;
    }

    return copiedEvent;
  }

  async copyModification({
    modification,
    event,
    withOriginalItemId = true,
    markCopy = false,
  }) {
    const copiedModification =
      await this.modificationService.createModification(
        event,
        {
          ...modification.get(),
          ...(markCopy &&
            modification.name && { name: `(copy) ${modification.name}` }),
        },
        {
          provideItemId: withOriginalItemId ? modification.itemId : null,
        },
      );

    return copiedModification;
  }
}

const scenarioCopyServiceInstance = new ScenarioCopyService({
  scenarioService: scenarioServiceInstance,
  eventService: eventServiceInstance,
  modificationService: modificationServiceInstance,
  models: DBModels,
});

export default scenarioCopyServiceInstance;
export { ScenarioCopyService };
