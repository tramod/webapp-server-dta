/* eslint-disable class-methods-use-this */
import { ScenarioAccess } from '../../db/index.js';

class ScenarioAccessService {
  constructor({ scenarioAccessModel } = {}) {
    this.scenarioAccessModel = scenarioAccessModel;
  }

  async createScenarioAccess(scenarioId, userId, { level } = {}) {
    return this.scenarioAccessModel.create({
      scenarioRowId: scenarioId,
      userId,
      level,
    });
  }

  async updateScenarioAccessByUserId(scenarioId, userId, { level } = {}) {
    const accessToBeUpdated = await this.scenarioAccessModel.findOne({
      where: { scenarioRowId: scenarioId, userId },
    });

    return accessToBeUpdated.update({ level });
  }

  async deleteScenarioAccessByUserId(scenarioId, userId) {
    const accessToBeDeleted = await this.scenarioAccessModel.findOne({
      where: { scenarioRowId: scenarioId, userId },
    });

    return accessToBeDeleted.destroy();
  }
}

const scenarioServiceInstance = new ScenarioAccessService({
  scenarioAccessModel: ScenarioAccess,
});

export default scenarioServiceInstance;
export { ScenarioAccessService };
