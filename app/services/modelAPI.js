import axios from 'axios';
import axiosRetry from 'axios-retry';
import LRU from 'lru-cache';
import pLimit from 'p-limit';

import config from '../../config/env.js';
import { TmError3rdParty, TmErrorGeneral } from '../utils/error.js';
import TmLogger from '../utils/logger.js';

const { TM_MODEL_CACHE_PREFIX } = config;
const MAX_CONCURRENT_FETCHES = 10;
const MAX_CONCURRENT_ACTIONS_DTA = 1;

export const DEFAULT_TRAFFIC_CACHE = `${TM_MODEL_CACHE_PREFIX}-default`;

const axiosApis = {};
const getAPIInstance = (trafficModel) => {
  const modelId = trafficModel.id;
  if (!axiosApis[modelId]) axiosApis[modelId] = buildAxiosApi(trafficModel);
  return axiosApis[modelId];
};

function buildAxiosApi({ name, endPoint, endPointApiKey } = {}) {
  const axiosAPI = axios.create({
    baseURL: endPoint,
    timeout: 3000,
    headers: {
      'X-API-KEY': endPointApiKey,
      'accept-encoding': '*', // TODO AXIOS bug temp fix
    },
  });

  axiosAPI.interceptors.response.use(null, (error) => {
    const { method, url, params } = error.config;
    const errConfig = {
      message: `${method} request failed`,
      method,
      url,
      params,
      retryCount: error['axios-retry']?.retryCount,
      code: error.code,
      type: 'MODEL_API_ERROR',
      ...(method === 'post' &&
        url === `/jobs/${name}` && {
          cacheName: error.config.data.cacheName,
        }),
    };
    TmLogger.verbose(errConfig);
    if (error.response) {
      const { data } = error.response;
      TmLogger.verbose(data);
    }
    return Promise.reject(error);
  });

  axiosRetry(axiosAPI, {
    retries: 3,
    retryDelay: axiosRetry.exponentialDelay,
    shouldResetTimeout: true,
    retryCondition: (error) =>
      axiosRetry.isNetworkOrIdempotentRequestError(error) ||
      error.code === 'ECONNABORTED',
  });

  return {
    axios: axiosAPI,
    fetchLimit: pLimit(MAX_CONCURRENT_FETCHES),
    actionsLimit: pLimit(MAX_CONCURRENT_ACTIONS_DTA),
  };
}

// TODO clear in case of non-restart reload
const modelItemsCache = new LRU({
  max: 250,
  ttl: 1000 * 60 * 60 * 24,
});

export const resetModelItemsCache = () => modelItemsCache.clear(); // Export for testing

export { axiosApis, getAPIInstance };

const getTrafficJobBaseParams = () => ({
  numOfTimeIntervals: 12,
  fanStep: 10000,
  flowThreshold: 2,
  mixtureFlowThreshold: 0.02,
  day: 0,
  maximalIteration: 1,
  gradientStep: 0.1,
});

export const postTrafficJob = async ({
  trafficModel,
  cacheName = DEFAULT_TRAFFIC_CACHE,
  trafficParams = {},
} = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/jobs/${trafficModel.name}`;
  try {
    const promise = api.actionsLimit(() =>
      api.axios.post(url, {
        ...getTrafficJobBaseParams(),
        cacheName,
        ...trafficParams,
      }),
    );
    const response = await promise;
    const { jobId } = response.data;
    TmLogger.verbose({
      message: 'Job posted',
      type: 'MODEL_API_SUCCESS',
      cacheName,
      jobId,
    });
    return jobId; // Too many jobs -> undefined
  } catch (error) {
    throw new TmError3rdParty(
      'Could not start traffic job [ModelApiError]',
      error,
    );
  }
};

export const getTrafficJobStatus = async ({ trafficModel, jobId } = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/jobs/${trafficModel.name}/${jobId}/status`;
  try {
    const promise = api.fetchLimit(() => api.axios.get(url));
    const response = await promise;
    if (response.data.status === 'ERROR') {
      TmLogger.verbose(response.data);
      throw new TmErrorGeneral('Traffic job returned error');
    }
    TmLogger.verbose({
      message: 'Job status',
      type: 'MODEL_API_SUCCESS',
      jobId,
      status: response.data.status,
    });
    return response.data.status;
  } catch (error) {
    if (error instanceof TmErrorGeneral) throw error;
    throw new TmError3rdParty(
      'Could not get traffic job status [ModelApiError]',
      error,
    );
  }
};

export const killTrafficJob = async ({ trafficModel, jobId } = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/jobs/${trafficModel.name}/${jobId}`;
  try {
    const promise = api.actionsLimit(() => api.axios.delete(url));
    const response = await promise;
    TmLogger.verbose(JSON.stringify(response.data));
    TmLogger.verbose({
      message: 'Job killed',
      type: 'MODEL_API_SUCCESS',
      jobId,
    });
    return;
  } catch (error) {
    TmLogger.verbose(error);
    throw new TmError3rdParty(
      'Could not get kill traffic job [ModelApiError]',
      error,
    );
  }
};

const TRAFFIC_JOB_RESULT_DELAY = 2 * 1000;
export const awaitTrafficJobResult = ({ jobId, trafficModel } = {}) => {
  let cancelled = false;
  let reject;
  let timeout;

  const cancel = () => {
    cancelled = true;
    if (timeout) clearTimeout(timeout);
    killTrafficJob({ trafficModel, jobId });
    if (typeof reject === 'function') reject(new Error('cancelled'));
  };

  const waitForResult = new Promise((resolve, _reject) => {
    reject = _reject;

    async function jobResultFn() {
      try {
        const result = await getTrafficJobStatus({ jobId, trafficModel });
        if (cancelled) return;
        if (result === 'FINISHED') resolve();
        else timeout = setTimeout(jobResultFn, 10 * TRAFFIC_JOB_RESULT_DELAY);
      } catch (error) {
        if (cancelled) {
          TmLogger.warn(error);
          return;
        }
        reject(error);
      }
    }

    jobResultFn();
  });

  return { cancel, waitForResult };
};

export const deleteTrafficCache = async ({
  trafficModel,
  cacheName = DEFAULT_TRAFFIC_CACHE,
} = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/caches/${trafficModel.name}/${cacheName}`;
  try {
    const promise = api.actionsLimit(() => api.axios.delete(url));
    await promise;
    TmLogger.verbose({
      message: 'Job cache removed',
      type: 'MODEL_API_SUCCESS',
      cacheName,
    });
    return true;
  } catch (error) {
    throw new TmError3rdParty(
      'Could not delete traffic cache [ModelApiError]',
      error,
    );
  }
};

export const getTrafficCacheList = async ({ trafficModel } = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/caches/${trafficModel.name}`;
  try {
    const { data } = await api.axios.get(url);
    TmLogger.verbose({
      message: 'Model cache list fetched',
      type: 'MODEL_API_SUCCESS',
    });
    return data;
  } catch (error) {
    throw new TmError3rdParty(`Could not load cache list from server`, error);
  }
};
export const getTrafficCache = async ({
  trafficModel,
  cacheKey = DEFAULT_TRAFFIC_CACHE,
} = {}) => {
  const api = getAPIInstance(trafficModel);
  const url = `/caches/${trafficModel.name}/${cacheKey}`;
  try {
    const { data } = await api.axios.get(url);
    TmLogger.verbose({
      message: 'Model cache fetched',
      type: 'MODEL_API_SUCCESS',
    });
    return data;
  } catch (error) {
    throw new TmError3rdParty(`Could not load cache from server`, error);
  }
};

const modelItemPathNames = {
  link: 'edges',
  node: 'nodes',
  nodeTransit: 'trs',
  generator: 'zones',
};

const modelItemNames = {
  link: 'link',
  nodeTransit: 'node transit',
  generator: 'generator',
};

const getModelItem = async ({ trafficModel, type, id, url } = {}) => {
  const cacheKey = `${trafficModel.id}:${type}:${id}`;
  const cachedItem = modelItemsCache.get(cacheKey);
  if (cachedItem) return cachedItem;
  const api = getAPIInstance(trafficModel);
  try {
    const resolvedUrl =
      url ?? `/${modelItemPathNames[type]}/${trafficModel.name}/${id}`;
    const promise = api.fetchLimit(() => api.axios.get(resolvedUrl));
    const { data } = await promise;
    modelItemsCache.set(cacheKey, data);
    TmLogger.verbose({
      message: 'Model item fetched',
      type: 'MODEL_API_SUCCESS',
      modelType: type,
      id,
    });
    return data;
  } catch (error) {
    throw new TmError3rdParty(
      `Could not load model ${modelItemNames[type]} from server`,
      error,
    );
  }
};

export const getModelLink = async ({ trafficModel, id } = {}) =>
  getModelItem({ type: 'link', trafficModel, id });
export const getModelGenerator = async ({ trafficModel, id } = {}) =>
  getModelItem({ type: 'generator', trafficModel, id });
export const getModelNode = async ({ trafficModel, id } = {}) =>
  getModelItem({ type: 'node', trafficModel, id });

export const getModelDTANodeTransit = async ({ trafficModel, id } = {}) => {
  const url = `/movements/${trafficModel.name}/node/${id}`;
  const data = await getModelItem({
    type: 'nodeTransit',
    trafficModel,
    id,
    url,
  });
  return data.features;
};

export const getModelNodeTransit = async ({ trafficModel, id } = {}) =>
  getModelDTANodeTransit({ trafficModel, id });

// No caching, as responses are large
const getModelItems = async ({ trafficModel, type } = {}) => {
  const api = getAPIInstance(trafficModel);
  try {
    const url = `/${modelItemPathNames[type]}/${trafficModel.name}`;
    const promise = api.fetchLimit(() => api.axios.get(url));
    const { data } = await promise;
    TmLogger.verbose({
      message: 'Model item fetched',
      type: 'MODEL_API_SUCCESS',
      modelType: type,
    });
    return data;
  } catch (error) {
    throw new TmError3rdParty(
      `Could not load model ${modelItemNames[type]} from server`,
      error,
    );
  }
};

export const getModelLinks = async ({ trafficModel } = {}) =>
  getModelItems({ type: 'link', trafficModel });
export const getModelGenerators = async ({ trafficModel } = {}) =>
  getModelItems({ type: 'generator', trafficModel });
export const getModelNodes = async ({ trafficModel } = {}) =>
  getModelItems({ type: 'node', trafficModel });

const getModelInfo = async ({ type, trafficModel } = {}) => {
  const cacheKey = `info:${trafficModel.id}:${type}`;
  const cachedItem = modelItemsCache.get(cacheKey);
  if (cachedItem) return cachedItem;
  const api = getAPIInstance(trafficModel);
  const url = `/${type}/${trafficModel.name}/info`;
  try {
    const promise = api.fetchLimit(() => api.axios.get(url));
    const { data } = await promise;
    modelItemsCache.set(cacheKey, data);
    TmLogger.verbose({
      message: 'Model info fetched',
      type: 'MODEL_API_SUCCESS',
      info: type,
    });
    return data;
  } catch (error) {
    throw new TmError3rdParty(
      `Could not load model info for ${type} from server`,
      error,
    );
  }
};

const maxPropertyName = {
  DTA: {
    edge: 'lastId',
    node: 'lastId',
  },
};

export const getModelInfos = async ({ trafficModel } = {}) => {
  const edgeInfo = await getModelInfo({ type: 'edges', trafficModel });
  const nodeInfo = await getModelInfo({ type: 'nodes', trafficModel });
  return {
    link: edgeInfo[maxPropertyName[trafficModel.type].edge],
    node: nodeInfo[maxPropertyName[trafficModel.type].node],
  };
};

const nodeTransitFormats = {
  DTA: (rules) =>
    rules.map((item) => {
      // eslint-disable-next-line camelcase
      const { node_id, ib_edge_id, ob_edge_id, capacity } = item.properties;
      return {
        // eslint-disable-next-line camelcase
        node: node_id,
        // eslint-disable-next-line camelcase
        linkFrom: ib_edge_id,
        // eslint-disable-next-line camelcase
        linkTo: ob_edge_id,
        value: capacity,
        isBaseModel: true,
        eventId: null, // easier filtering on client, remove if not needed
      };
    }),
};
export const formatModelNodeTransitRules = (rules, modelType = 'STA') =>
  nodeTransitFormats[modelType](rules);

export const calculateDefaultCache = async ({ trafficModel } = {}) => {
  const jobId = await postTrafficJob({ trafficModel });
  const { waitForResult } = awaitTrafficJobResult({ jobId, trafficModel });
  await waitForResult;
};
