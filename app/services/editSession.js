/* eslint-disable class-methods-use-this */
import {
  Scenario,
  EditSession,
  TrafficModel,
  Op as OpSource,
  db as dbSource,
} from '../../db/index.js';
import TmLogger from '../utils/logger.js';
import { ActivityLog } from '../utils/logger-utils.js';
import scenarioServiceInstance from './scenarios.js';
import scenarioCopyServiceInstance from './scenariosCopy.js';
import scenarioCalculationService from './calculations/index.js';

class EditSessionService {
  constructor({
    scenarioModel,
    editSessionModel,
    trafficModelModel,
    Op,
    db,
    logger,
    scenarioService,
    scenarioCopyService,
    scenarioCalculation,
  } = {}) {
    this.scenarioModel = scenarioModel;
    this.editSessionModel = editSessionModel;
    this.trafficModelModel = trafficModelModel;
    this.scenarioService = scenarioService;
    this.scenarioCopyService = scenarioCopyService;
    this.scenarioCalculation = scenarioCalculation;
    this.Op = Op;
    this.db = db;
    this.logger = logger;

    this.resettableState = {
      hasChange: false,
      hasComputableChange: false,
      computationState: 'notStarted',
      saveOnComputed: false,
    };
    this.defaultState = {
      ...this.resettableState,
      computationTimestamp: null,
      computationWarning: false,
    };

    this.activity = new ActivityLog({
      logger,
      messages: {
        COMPUTED: 'Scenario edit session computed',
        SAVED: 'Scenario edit session saved',
        LEFT: 'Scenario edit session left',
        REMOVED: 'Scenario edit session removed',
      },
      scope: 'EDIT_SESSION',
    });
  }

  async initEditSession({ source, user } = {}) {
    const session = await this.editSessionModel.create({
      userId: user.id,
      sourceItemId: source.itemId,
    });
    return session;
  }

  async setEditSessionUser({ session, user } = {}) {
    await session.update({ userId: user ? user.id : null });
  }

  async getEditSession(
    scenarioId,
    {
      requireUpdated = false,
      includeUpdated = true,
      validOnly = false,
      user = null,
    } = {},
  ) {
    const [editSession] = await this.editSessionModel.findAll({
      where: {
        sourceItemId: scenarioId,
        ...(user && {
          userId: user.id,
        }),
      },
      ...(includeUpdated && {
        include: [
          {
            model: this.scenarioModel.scope(),
            as: 'updated',
            required: requireUpdated,
          },
        ],
      }),
    });

    if (editSession && validOnly)
      return editSession.isValid() ? editSession : null;
    return editSession;
  }

  async getSessionsByUser(user) {
    const scenarios = await this.scenarioModel
      .scope('defaultScope', {
        method: [
          'hasUserAccess',
          {
            userId: user.id,
            required: true,
            levels: ['editor', 'inserter'],
          },
        ],
      })
      .findAll();
    const scenariosId = scenarios.map((scenario) => scenario.itemId);
    const sessions = await this.editSessionModel.scope('isValid').findAll({
      where: {
        sourceItemId: {
          [this.Op.in]: scenariosId,
        },
      },
    });
    return sessions;
  }

  async removeSession(session, updated) {
    await this.removeUpdatedFromSession(session, updated);
    this.activity.log('REMOVED', {
      scenarioId: session.sourceItemId,
      id: session.id,
    });
    await session.destroy();
  }

  async computeSession(session) {
    if (!session.hasComputableChange) return session;
    const scenarioToCompute = await session.getUpdated({
      scope: false,
      include: {
        model: this.trafficModelModel,
      },
    });

    await session.update({
      computationState: 'inProgress',
      computationTimestamp: new Date(),
    });

    const postCalculateCallback = (result) =>
      this.resolveSessionComputed(session, result);
    this.scenarioCalculation.calculateScenarioCb(
      scenarioToCompute,
      postCalculateCallback,
    );

    return session;
  }

  async resolveSessionComputed(session, result) {
    try {
      await session.reload();
      if (session.computationState !== 'inProgress')
        throw Error('Session not waiting for computation');
    } catch (error) {
      this.logger.verbose(
        `Could not resolve session computation, scenario ${session.sourceItemId}`,
      );
      return;
    }

    if (session.computationState === 'notStarted')
      this.logger.debug(
        `Session of scenario ${session.sourceItemId} computation previously cancelled`,
      );
    else if (result.status === true) {
      await session.update({
        ...(!session.saveOnComputed && { computationState: 'computed' }),
        computationWarning: result.hasWarning,
        computationTimestamp: new Date(),
      });
      this.activity.log('COMPUTED', {
        scenarioId: session.sourceItemId,
        id: session.id,
      });
      if (session.saveOnComputed)
        await this.saveSession(session, { forceSave: true });
    } else {
      this.logger.error(
        `Session of scenario ${session.sourceItemId} computation failed`,
      );
      await session.update({
        computationState: 'error',
        computationTimestamp: null,
      });
    }
  }

  async saveSession(session, { leaveOnSaved = false, forceSave = false } = {}) {
    if (!session.hasChange) return session;
    if (
      !forceSave &&
      session.hasComputableChange &&
      session.computationState !== 'computed'
    )
      return session;

    const source = await session.getSource();
    const updated = await session.getUpdated({ scope: false });
    const { itemId } = source;
    const { isPublic, isInPublicList } = source;

    try {
      const result = await this.db.transaction(async () => {
        await session.setSource(null); // Somehow worked without this before
        await source.update({ itemId: null });
        // set publication props manually since they could be changed during opened edit session
        await updated.update({ itemId, isPublic, isInPublicList });
        // copy user accesses manually since they could be changed during opened edit session
        await this.scenarioCopyService.copyAccesses({
          sourceScenario: source,
          targetScenario: updated,
        });
        await session.update({ sourceItemId: itemId, updatedRowId: null });
        await this.scenarioService.deepUpdateScenarioDates(updated);

        this.activity.log('SAVED', {
          scenarioId: itemId,
          id: session.id,
        });
        if (session.userId && !leaveOnSaved) {
          await session.update({ ...this.resettableState });
          return session;
        }
        await this.removeSession(session);
        return null;
      });
      this.scenarioService.deleteScenario(source); // Deprecated scenario delete can be done out of transaction
      return result;
    } catch (error) {
      this.logger.error(`Could not save session ${itemId}`);
      return null;
    }
  }

  async leaveSession(session) {
    await this.setEditSessionUser({
      session,
    });
    if (!session.isValid()) {
      await this.removeSession(session);
      return null;
    }
    this.activity.log('LEFT', {
      scenarioId: session.sourceItemId,
      id: session.id,
    });
    return session;
  }

  async cancelSessionChanges(session) {
    if (session.computationState === 'inProgress') {
      const scenarioInCompute = await session.getUpdated({
        scope: false,
        include: {
          model: this.trafficModelModel,
        },
      });
      await this.scenarioCalculation.cancelCalculation(scenarioInCompute);
    }
    await this.removeUpdatedFromSession(session);
    await session.update({ ...this.defaultState });
    return session;
  }

  async setChanged(session, hasComputableChange = false) {
    await session.update({
      hasChange: true,
      hasComputableChange,
    });
  }

  async checkChanges(session, isComputableChange = false) {
    if (!session) return;
    if (
      session.hasComputableChange &&
      session.computationState === 'notStarted'
    )
      return;
    await this.setChanged(session, isComputableChange);
  }

  async createUpdatedCopy(scenario, user) {
    return this.scenarioCopyService.copyScenario({
      scenario,
      user,
      copyAuthor: true,
      withAccesses: true,
      copyCreatedAt: true,
      copyUpdatedAt: true,
    });
  }

  async addUpdatedToSession(session, source) {
    const user = { id: session.userId };
    const sourceToCopy = source ?? (await session.getSource());
    const updated = await this.createUpdatedCopy(sourceToCopy, user);
    await session.setUpdated(updated);
    return session;
  }

  async removeUpdatedFromSession(session, updated) {
    const scenarioToRemove =
      updated ?? (await session.getUpdated({ scope: false }));
    if (scenarioToRemove) {
      await session.setUpdated(null);
      this.scenarioService.deleteScenario(scenarioToRemove);
    }
  }

  async restartSessionsInProgress() {
    const sessionsInProgress = await this.editSessionModel.findAll({
      where: { computationState: 'inProgress' },
    });
    await Promise.all(
      sessionsInProgress.map(async (session) => {
        this.computeSession(session);
      }),
    );
  }

  async touchSessions() {
    const sessions = await this.editSessionModel.findAll();
    await Promise.all(
      sessions.map(async (session) => {
        await session.updateTimestamp();
      }),
    );
  }
}

const editSessionServiceInstance = new EditSessionService({
  Op: OpSource,
  db: dbSource,
  scenarioModel: Scenario,
  editSessionModel: EditSession,
  trafficModelModel: TrafficModel,
  scenarioService: scenarioServiceInstance,
  scenarioCopyService: scenarioCopyServiceInstance,
  scenarioCalculation: scenarioCalculationService,
  logger: TmLogger,
});

export default editSessionServiceInstance;
export { EditSessionService };
