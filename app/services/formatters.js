const getArray = (resource, isArray) =>
  isArray ?? Array.isArray(resource) ? resource : [resource];

export function formatScenarioResponse(
  scenarios,
  user,
  {
    withEvents = false,
    modelSections = null,
    formatSections = true,
    hasModelSectionsValid,
  } = {},
) {
  const isArray = Array.isArray(scenarios);

  const formatted = getArray(scenarios, isArray).map((entry) => {
    const raw = entry.toJSON();
    if (raw.scenarioAccesses) {
      raw.level = raw.scenarioAccesses[0].level;
      delete raw.scenarioAccesses;
    }
    const providedModelSections =
      raw.scenarioModelSections || entry.scenarioModelSections || modelSections;
    if (providedModelSections) {
      raw.hasModelSectionsValid =
        hasModelSectionsValid ?? areModelSectionsValid(providedModelSections);
      if (formatSections)
        raw.modelSections = formatModelSections(providedModelSections);
    }
    delete raw.scenarioModelSections;
    if (withEvents) {
      raw.events = formatEventResponse(entry.events, user, {
        withModifications: true,
      });
    } else {
      delete raw.events;
    }
    delete raw.rowId;
    raw.id = raw.itemId ?? entry.sourceId;
    delete raw.itemId;
    delete raw.source;
    raw.isShared = raw.level && user.id !== raw.authorUserId;
    raw.author = raw.authorUserId;

    const owner = raw.user || user; // if no user is associated yet -> assume the current user is the owner
    raw.owner = owner.username || 'n/a';
    delete raw.user;

    return raw;
  });
  return isArray ? formatted : formatted[0];
}

const getModelSectionValidity = (ms) =>
  ms.isValid ?? ms.modelSection?.isValid ?? false;
export function areModelSectionsValid(modelSections) {
  return modelSections.every((ms) => getModelSectionValidity(ms) === true);
}

function formatModelSections(modelSections) {
  if (modelSections.length === 0) return [];
  const isInverted = modelSections[0].dateFrom !== undefined;
  if (isInverted)
    return modelSections.map(
      ({ dateFrom, dateTo, modelSectionKey, codeList, modelSection }) => ({
        dateFrom,
        dateTo,
        key: modelSectionKey,
        codeList,
        isValid: modelSection.isValid,
      }),
    );

  return modelSections.map(({ key, isValid, scenarioModelSection }) => ({
    dateFrom: scenarioModelSection.dateFrom,
    dateTo: scenarioModelSection.dateTo,
    key,
    codeList: scenarioModelSection.codeList,
    isValid,
  }));
}

export function formatEventResponse(
  events,
  user = {},
  { withModifications = false } = {},
) {
  const isArray = Array.isArray(events);
  const formatted = getArray(events, isArray).map((entry) => {
    const raw = entry.toJSON();
    if (withModifications) {
      raw.modifications = formatModificationResponse(entry.modifications);
    } else {
      delete raw.modifications;
    }
    delete raw.rowId;
    raw.id = raw.itemId;
    delete raw.itemId;

    const owner = raw.user || user; // if no user is associated yet -> assume the current user is the owner
    raw.owner = owner.username || 'n/a';
    delete raw.user;

    return raw;
  });
  return isArray ? formatted : formatted[0];
}

export function formatEditSessionResponse({
  fieldsChanged,
  isComputableChange = false,
  editSession,
} = {}) {
  return {
    ...(fieldsChanged && { fieldsChanged }),
    isComputableChange,
    ...(editSession && { editSession: editSession.toLocalPropObject() }),
  };
}

export function formatModificationResponse(modifications) {
  const isArray = Array.isArray(modifications);
  const formatted = getArray(modifications, isArray).map((modification) => {
    const rawJSON = modification.toJSON();
    const rawFlat = {
      id: rawJSON.itemId,
      ...rawJSON.data,
      ...rawJSON,
    };
    delete rawFlat.rowId;
    delete rawFlat.itemId;
    delete rawFlat.eventRowId;
    delete rawFlat.data;
    return rawFlat;
  });
  return isArray ? formatted : formatted[0];
}

export function formatTrafficModelResponse(trafficModels) {
  if (!trafficModels) return null;
  const isArray = Array.isArray(trafficModels);
  const formatted = getArray(trafficModels, isArray).map((trafficModel) => {
    const rawFlat = {
      ...trafficModel.toJSON(),
    };
    delete rawFlat.endPointApiKey;
    delete rawFlat.createdAt;
    delete rawFlat.updatedAt;
    return rawFlat;
  });
  return isArray ? formatted : formatted[0];
}

export function formatUserResponse(
  { email, id, roles, username, organization },
  { name: orgNameOverride } = {},
) {
  const orgName = organization ? organization.name : null;

  return {
    email,
    id,
    roles,
    username,
    organization: orgNameOverride !== undefined ? orgNameOverride : orgName,
  };
}
