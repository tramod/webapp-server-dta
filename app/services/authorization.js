import {
  AbilityBuilder,
  Ability,
  subject,
  createAliasResolver,
} from '@casl/ability';

import { ScenarioAccess as ScenarioAccessModel } from '../../db/index.js';

const GTE_VIEWER = { level: { $in: ['viewer', 'inserter', 'editor'] } };
const GTE_INSERTER = { level: { $in: ['inserter', 'editor'] } };
const GTE_EDITOR = { level: { $in: ['editor'] } };
class AbilityCache {
  constructor({ ScenarioAccess } = {}) {
    this.ScenarioAccess = ScenarioAccess;
    // TODO replace by proper caching implementation - LRU/Session
    this.abilityCache = new Map();
    // this.initModelHooks(); Access fetch not needed for abilities
  }

  initModelHooks() {
    this.ScenarioAccess.afterBulkCreate((accesses) => {
      for (const access of accesses) {
        this.delete(access.userId);
      }
    });
  }

  has(userId) {
    return this.abilityCache.has(userId);
  }

  get(userId) {
    return this.abilityCache.get(userId);
  }

  set(userId, ability) {
    return this.abilityCache.set(userId, ability);
  }

  delete(userId) {
    return this.abilityCache.delete(userId);
  }
}

const abilityCache = new AbilityCache({
  ScenarioAccess: ScenarioAccessModel,
});

export { abilityCache, AbilityCache };

// Alias -> action keys mapping
export const aliases = {
  getterAlias: ['enter', 'get'],
  editorAlias: [
    'update',
    'delete',
    'enter',
    'edit:mode',
    'edit:mode:return',
    'create:event:included',
    'update:event',
    'update:event:included',
    'delete:event',
  ],
  inserterAlias: ['enter', 'edit:mode', 'create:event'],
  inserterOwnsAlias: ['enter', 'update:event', 'delete:event'],
};

const resolveAction = createAliasResolver(aliases);

const ADMIN_USER_ROLE = 100;
const rolePermissions = {
  [ADMIN_USER_ROLE](user, can) {
    can('model:refresh');
  },
};

export async function defineAbilityFor(userModel) {
  const { can, build } = new AbilityBuilder(Ability);

  // TODO Add global rights

  // NOTE: action keys should be used for ability checks, see aliases mapping
  can('getterAlias', 'scenario', { isPublic: true }); // Getter should work also when scenario is public

  if (userModel) {
    can('getterAlias', 'scenario', { ...GTE_VIEWER });
    can('editorAlias', 'scenario', { ...GTE_EDITOR });
    can('inserterAlias', 'scenario', { ...GTE_INSERTER });
    // Expect scenario with single nested event - fetched by getWithEvent(s)
    can('inserterOwnsAlias', 'scenario', {
      ...GTE_INSERTER,
      events: {
        $elemMatch: {
          included: false,
          authorUserId: userModel.id,
        },
      },
    });

    const userRoles = userModel.roles;
    if (Array.isArray(userRoles)) {
      for (const userRole of userRoles) {
        if (typeof rolePermissions[userRole] === 'function')
          rolePermissions[userRole](userModel, can);
      }
    }
  }

  return build({ resolveAction });
}

// Can be used only with simple authorization logic
export const scenarioMock = (id, params) =>
  subject('scenario', { id: parseInt(id, 10), ...params });
