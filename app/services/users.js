/* eslint-disable class-methods-use-this */
import {
  User,
  UserToken,
  Credential,
  Organization,
  Op,
  Sequelize,
} from '../../db/index.js';
import { TmErrorGeneral } from '../utils/error.js';
import sendEmail from '../utils/sendEmail.js';
import TmLogger from '../utils/logger.js';
import config from '../../config/env.js';

class UserService {
  constructor({
    userModel,
    userTokenModel,
    credentialModel,
    organizationModel,
    op,
    logger,
  } = {}) {
    this.userModel = userModel;
    this.userTokenModel = userTokenModel;
    this.credentialModel = credentialModel;
    this.organizationModel = organizationModel;
    this.op = op;
    this.logger = logger;
  }

  async getUsers(fullResponse = false) {
    return this.userModel.findAll({
      include: this.organizationModel,
      ...(!fullResponse && {
        attributes: {
          exclude: ['email', 'roles'],
        },
      }),
    });
  }

  async getUser(props) {
    return this.userModel.findOne({
      where: { ...props },
      include: this.organizationModel,
      logging: () =>
        this.logger.debug('[SELECT REDACTED] Fetching user by props'),
    });
  }

  // By default first lookup is case insensitive, in case multiple users found, only case sensitive email can be used
  // Username restricted elsewhere so it cannot have email form
  async getUserByUsernameOrEmail(
    input,
    { matchCase = false, matchUsername = true } = {},
  ) {
    const emailWhere = matchCase
      ? {
          email: input,
        }
      : Sequelize.where(
          Sequelize.fn('lower', Sequelize.col('email')),
          input.toLowerCase(),
        );
    const users = await User.findAll({
      where: {
        [Op.or]: [
          emailWhere,
          ...(matchUsername
            ? [
                {
                  username: input,
                },
              ]
            : []),
        ],
      },
      include: Credential,
      logging: () =>
        this.logger.debug('[SELECT REDACTED] Fetching user by username/email'),
    });

    if (users.length === 0) return null;
    if (users.length === 1) return users[0];
    return users.find((user) => user.email === input);
  }

  async createUser(organization, { username, email, roles }) {
    const user = await this.userModel.create({
      username,
      email,
      roles,
    });
    await user.setOrganization(organization?.id || null);

    return user;
  }

  async updateUser(user, organization, { username, email, roles }) {
    await user.setOrganization(organization?.id || null);

    return user.update({
      username,
      email,
      roles,
    });
  }

  async deleteUser(user) {
    return user.destroy();
  }

  async getOrganizations() {
    return this.organizationModel.findAll();
  }

  async getOrCreateOrganization({ name }) {
    if (!name) return null;

    const [organization] = await Organization.findOrCreate({
      where: { name },
      defaults: { name },
    });

    return organization;
  }

  async getCredentials({ userId, password }) {
    if (!password) return null;

    const credentials = await this.credentialModel.findOne({
      where: { userId },
    });
    const isValid = await credentials.isPasswordValid(password);

    return isValid ? credentials : null;
  }

  async createToken({ userId, type, expiration }) {
    // TODO: maybe use crypto or something?
    const randomString = () => Math.random().toString(36).substr(2);
    const expirationDate = () => Date.now() + expiration;

    return this.userTokenModel.create({
      userId,
      type,
      token: randomString() + randomString(),
      expire: expirationDate(),
    });
  }

  async getValidToken({ userId, token }) {
    return this.userTokenModel.findOne({
      where: {
        userId,
        token,
        expire: {
          [this.op.gte]: Date.now(),
        },
      },
    });
  }

  async updateCredentials({ userId, password }) {
    const existingCredentials = await this.credentialModel.findOne({
      where: {
        userId,
      },
    });

    if (existingCredentials) return existingCredentials.update({ password });

    return this.credentialModel.create({
      userId,
      password,
    });
  }

  async sendTokenLinkViaEmail({ id, email }, { token }, subject, body) {
    if (config.TM_EMAIL_DISABLE) return;

    const resetLink = `${process.env.TM_HOST}/user/${id}/password-reset/${token}`;
    const regex = /\[LINK\]|\[EMAIL\]/g;
    const replacements = { '[LINK]': resetLink, '[EMAIL]': email };
    const html = body.replace(regex, (m) => replacements[m]);

    const sendingError = await sendEmail({
      email,
      subject,
      html,
    });
    if (sendingError)
      throw new TmErrorGeneral('[RequestPasswordReset]Email sending failed', {
        statusCode: 502,
      });
  }
}

const userServiceInstance = new UserService({
  userModel: User,
  userTokenModel: UserToken,
  credentialModel: Credential,
  organizationModel: Organization,
  op: Op,
  logger: TmLogger,
});

export default userServiceInstance;
export { UserService };
