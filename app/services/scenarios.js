/* eslint-disable class-methods-use-this */
import DBModels, { Op } from '../../db/index.js';
import TmLogger from '../utils/logger.js';
import {
  resourcePatcher,
  getChangedModelValues,
  getDateRange,
  getLatestUpdatedAtDate,
} from './utils.js';
import scenarioCalculationService, {
  modelSectionService,
} from './calculations/index.js';

class ScenarioService {
  constructor({
    models,
    scenarioCalculation,
    modelSection,
    dbOperators,
    logger,
  } = {}) {
    this.scenarioModel = models.Scenario;
    this.models = models;
    this.scenarioCalculation = scenarioCalculation;
    this.modelSection = modelSection;
    this.Op = dbOperators;
    this.logger = logger;
  }

  async createScenario(
    {
      name,
      description,
      note,
      user,
      isPublic,
      isInPublicList,
      createdAt,
      trafficModelId,
    } = {},
    { provideItemId, rowItemId = true } = {},
  ) {
    // TODO CREATE WITH ACCESS
    const scenario = await this.scenarioModel.create(
      {
        name,
        description,
        note,
        isPublic,
        isInPublicList,
        ...(provideItemId && { itemId: provideItemId }),
        ...(trafficModelId && { trafficModelId }),
        authorUserId: user.id,
        scenarioAccesses: [{ level: 'editor', userId: user.id }],
        createdAt: createdAt ?? new Date(),
      },
      { include: [this.models.ScenarioAccess] },
    );
    if (!provideItemId && rowItemId)
      await scenario.update({ itemId: scenario.rowId });
    return scenario;
    // Inputs should be validated, so error here is unexpected and should be handled downstream
    // Where content cannot be easily validated, errors should be catch
  }

  async deleteScenario(scenario) {
    const trafficModel =
      scenario.trafficModel || (await scenario.getTrafficModel());
    await this.modelSection.removeScenariosModelSections(scenario); // Await, this is one quick DB OP, and necessary for calc cleanup
    scenario.destroy(); // Dont await destroy, for large scenario takes long time to remove all associations
    this.scenarioCalculation.cleanUpJobs(trafficModel);
  }

  async getScenarios(
    user,
    { withModelSections = false, modelType = null } = {},
  ) {
    // Custom logic without CASL
    return this.scenarioModel
      .scope('defaultScope', {
        method: ['hasUserAccess', { userId: user?.id ?? null }],
      })
      .findAll({
        where: {
          [Op.or]: [
            ...(user
              ? [{ '$scenarioAccesses.level$': { [Op.not]: null } }]
              : []),
            { isInPublicList: true, isPublic: true },
          ],
        },
        ...(withModelSections && {
          order: [[this.models.ScenarioModelSection, 'dateFrom', 'ASC']],
        }),
        include: [
          ...(modelType
            ? [
                {
                  model: this.models.TrafficModel,
                  where: { type: modelType },
                  required: true, // only scenarios with matching modelType are returned
                  attributes: [], // no attributes needed at this point
                },
              ]
            : []),
          ...(withModelSections
            ? [
                {
                  model: this.models.ScenarioModelSection,
                  include: {
                    model: this.models.ModelSection,
                  },
                },
              ]
            : []),
          {
            model: this.models.User,
            attributes: ['username', 'email'],
          },
        ],
      });
  }

  async getScenario(
    scenarioId,
    userId,
    {
      eventId,
      allEvents,
      modificationId,
      allModifications,
      allAccesses,
      allCalculations,
      trafficModel,
      isItemId = true,
    } = {},
  ) {
    const idKey = isItemId ? 'itemId' : 'rowId';
    const [scenario] = await this.scenarioModel
      .scope({ method: ['hasUserAccess', { userId }] })
      .findAll({
        where: { [idKey]: scenarioId },
        include: buildScenarioIncludes({
          eventId,
          allEvents,
          modificationId,
          allModifications,
          allAccesses,
          trafficModel,
          models: this.models,
        }),
      }); // Somehow findOne produces non working (and more complex) sql commmand, while this work
    if (!scenario) return null;

    if (allCalculations) {
      scenario.scenarioModelSections = await scenario.getModelSections();
    }

    if (allAccesses)
      scenario.scenarioAccesses = await scenario.getScenarioAccesses(); // Accesses include overwritten by 'hasUserAccess' scope so must be called individually if used here :(
    return scenario;
  }

  async updateScenario(
    scenario,
    { name, description, note, isPublic, isInPublicList } = {},
  ) {
    const values = { name, description, note, isPublic, isInPublicList };
    resourcePatcher(scenario, values);
    const changed = getChangedModelValues(scenario);
    await scenario.save();
    return { scenario, changed };
  }

  async deepUpdateScenarioDates(scenario) {
    const events = (await scenario.getEvents()) || [];

    await Promise.all(
      events.map(async (ev) => {
        const modifications = (await ev.getModifications()) || [];
        const { dateFrom, dateTo } = getDateRange(modifications);
        const updatedAt = getLatestUpdatedAtDate(ev, modifications);
        await ev.update({ dateFrom, dateTo, updatedAt });
      }),
    );

    const { dateFrom: scDateFrom, dateTo: scDateTo } = getDateRange(events);
    const scUpdatedAt = getLatestUpdatedAtDate(scenario, events);
    await scenario.update({
      dateFrom: scDateFrom,
      dateTo: scDateTo,
      updatedAt: scUpdatedAt,
    });
  }

  async getScenarioWithUserAccesses(scenarioId, { isItemId = true } = {}) {
    const idKey = isItemId ? 'itemId' : 'rowId';
    const [scenario] = await this.scenarioModel.findAll({
      where: { [idKey]: scenarioId },
      // include all the user accesses to this scenario
      include: [this.models.ScenarioAccess],
    });

    return scenario;
  }
}

const scenarioServiceInstance = new ScenarioService({
  models: DBModels,
  dbOperators: Op,
  scenarioCalculation: scenarioCalculationService,
  modelSection: modelSectionService,
  logger: TmLogger,
});

export default scenarioServiceInstance;
export { ScenarioService };

function buildScenarioIncludes({
  eventId,
  allEvents,
  modificationId,
  allModifications,
  allAccesses,
  trafficModel,
  models,
}) {
  return [
    ...(eventId || allEvents
      ? [
          {
            model: models.Event,
            ...(eventId && {
              where: {
                itemId: eventId,
              },
            }),
            include: [
              {
                model: models.User,
                attributes: ['username', 'email'],
              },
              ...(modificationId || allModifications
                ? [
                    {
                      model: models.Modification,
                      ...(modificationId && {
                        where: {
                          itemId: modificationId,
                        },
                      }),
                    },
                  ]
                : []),
            ],
          },
        ]
      : []),
    ...(allAccesses ? [{ model: models.ScenarioAccess }] : []),
    ...(trafficModel ? [{ model: models.TrafficModel }] : []),
    {
      model: models.User,
      attributes: ['username', 'email'],
    },
  ];
}
