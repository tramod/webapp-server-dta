import { getTrafficCacheList } from './modelAPI.js';
import editSessionService from './editSession.js';
import trafficModelService from './trafficModels.js';
import config from '../../config/env.js';
import logger from '../utils/logger.js';

class AppStartUp {
  constructor({
    app,
    db,
    sessionStore,
    editSession = editSessionService,
    trafficModel = trafficModelService,
  } = {}) {
    this.expressApp = app;
    this.db = db;
    this.sessionStore = sessionStore;
    this.editSessionService = editSession;
    this.trafficModelService = trafficModel;
  }

  async init(onSuccess) {
    logger.info('Starting server init sequence');

    await this.isDbAvailable();

    const trafficModels = await this.db.models.trafficModel.findAll();
    await Promise.allSettled(
      trafficModels.map(async (model) => {
        await this.checkOrCalculateDefaultCache(model);
        await this.checkModelSectionCachesExist(model);
      }),
    );
    await this.reviveEditSessions();

    logger.info('Finished server init sequence');
    onSuccess({ config, logger });
  }

  listen(...args) {
    this.expressApp.listen(...args);
  }

  async isDbAvailable() {
    await this.db.authenticate();
    logger.verbose('DB connection validated');

    // No store model extension needed now, can be only synced
    await this.sessionStore.sync();
    logger.verbose('Session data synchronized');
  }

  // eslint-disable-next-line class-methods-use-this
  async checkOrCalculateDefaultCache(trafficModel) {
    this.trafficModelService.checkOrCalculateBaseModel(trafficModel);
  }

  async checkModelSectionCachesExist({ trafficModel }) {
    logger.verbose('Checking existence of local model section caches');
    const { TM_MODEL_CACHE_PREFIX } = config;
    const localModelSections = await this.db.models.modelSection.findAll({
      where: { trafficModelId: trafficModel.id },
    });
    const serverModelSections = await getTrafficCacheList({ trafficModel });

    const localCacheNames = localModelSections.map((item) => item.key);
    const serverCacheNames = serverModelSections.reduce((items, item) => {
      const [prefix] = item.cacheName.split('-');
      if (prefix === TM_MODEL_CACHE_PREFIX) items.push(item.cacheName);
      return items;
    }, []);

    const localCacheMissing = localCacheNames.filter(
      (name) => !serverCacheNames.includes(name),
    );

    if (localCacheMissing.length === 0)
      logger.verbose('All model section caches available');
    else
      logger.warn(
        `Local model sections caches not found ${localCacheMissing.join(', ')}`,
      );
  }

  async reviveEditSessions() {
    logger.verbose('Reviving edit session still in progress');
    await this.editSessionService.touchSessions();
    await this.editSessionService.restartSessionsInProgress();
  }
}

// eslint-disable-next-line import/prefer-default-export
export { AppStartUp };
