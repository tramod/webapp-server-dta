/* eslint-disable class-methods-use-this */
import DBModels, { Op as OpSource } from '../../db/index.js';
import scenarioServiceInstance from './scenarios.js';
import TmLogger from '../utils/logger.js';
import { TmNotFoundError } from '../utils/error.js';
import {
  resourcePatcher,
  getChangedModelValues,
  isEventInScenario,
} from './utils.js';

const EVENT_FIELDS_COMPUTE = ['included'];

class EventService {
  constructor({ models, scenarioService, logger, Op } = {}) {
    this.models = models;
    this.scenarioService = scenarioService;
    this.Op = Op;
    this.logger = logger;
  }

  async createEvent(
    scenario,
    { name, description, note, dateFrom, dateTo, user, included = true } = {},
    { provideItemId, rowItemId = true } = {},
  ) {
    const event = await this.models.Event.create({
      name,
      description,
      note,
      dateFrom,
      dateTo,
      ...(provideItemId && { itemId: provideItemId }),
      authorUserId: user.id,
      scenarioRowId: scenario.rowId,
      included,
    });

    if (!provideItemId && rowItemId)
      await event.update({ itemId: event.rowId });

    // TODO event candidate
    return event;
  }

  async deleteEvent(scenario, event) {
    // TODO Check if needed / remove or fix logging
    if (!isEventInScenario(scenario, event))
      throw new TmNotFoundError(
        '[DeleteEvent]Event is not included in scenario',
      );
    await event.destroy();
  }

  async getEventsByScenario(scenarioRowId) {
    return this.models.Event.scope({
      method: ['isInScenario', scenarioRowId],
    }).findAll();
  }

  async getEventById(scenarioRowId, eventId) {
    return this.models.Event.scope({
      method: ['isInScenario', scenarioRowId],
    }).findOne({ where: { itemId: eventId } });
  }

  async getScenarioEventsByModifications(scenarioRowId, modificationIds) {
    return this.models.Event.findAll({
      where: { scenario_row_id: scenarioRowId },
      include: {
        model: this.models.Modification,
        required: true,
        attributes: [],
        where: { item_id: { [this.Op.in]: modificationIds } },
      },
    });
  }

  async updateEvent(
    scenario,
    event,
    { name, description, note, dateFrom, dateTo, included } = {},
  ) {
    if (
      !isEventInScenario(scenario, event) /* (await scenario.hasEvent(event)) */
    )
      // Somehow has stopped working, while generated sql seems fine in executor
      throw new TmNotFoundError('[UpdateEvent]Event not in scenario');
    const values = { name, description, note, dateFrom, dateTo, included };
    resourcePatcher(event, values);
    const changed = getChangedModelValues(event);
    await event.save();

    const isComputableChange = changed.fieldsChanged
      ? changed.fieldsChanged.filter((x) => EVENT_FIELDS_COMPUTE.includes(x))
          .length > 0
      : false;

    return {
      event,
      isComputableChange,
      changed,
    };
  }

  async updateEventsByModIds(scenario, modIds, inclusion) {
    const updatedEventIds = [];

    const dependentScenarioEvents = await this.getScenarioEventsByModifications(
      scenario.rowId,
      modIds,
    );

    /* eslint-disable no-await-in-loop */
    for (const scEvent of dependentScenarioEvents) {
      scEvent.included = inclusion;
      await scEvent.save();
      updatedEventIds.push(scEvent.itemId);
      // TODO: how to utilize event service update method? What is expected as 'event' parameter? :-O
      // await eventService.updateEvent(scenario, depEvent, resource);
      // no need to track isComputableChange here since changing event inclusion with mods should allways be computable change
    }

    return updatedEventIds;
  }

  async getImportableEventsToScenario(
    scenarioRowId,
    userId,
    { idPairs = [], modelType = null } = {},
  ) {
    const seq = this.models.sequelize;
    return this.models.Event.scope({
      method: ['isNotInScenario', scenarioRowId],
    }).findAll({
      attributes: {
        include: [
          [seq.col('scenario.name'), 'scenarioName'],
          [seq.col('scenario.item_id'), 'scenarioId'],
        ],
      },
      where: {
        ...(idPairs.length && [
          seq.where(
            seq.fn(
              'concat',
              seq.col('scenario.item_id'),
              '-',
              seq.col('event.item_id'),
            ),
            { [this.Op.in]: idPairs },
          ),
        ]),
        [this.Op.or]: [
          { '$scenario.is_in_public_list$': true },
          { '$scenario->scenarioAccesses.level$': { [this.Op.not]: null } }, // any existing user access = at least viewer
        ],
      },
      include: [
        {
          model: this.models.Scenario,
          required: true,
          attributes: [],
          include: [
            ...(modelType
              ? [
                  {
                    model: this.models.TrafficModel,
                    where: { type: modelType },
                    required: true, // only scenarios with matching modelType are returned
                    attributes: [],
                  },
                ]
              : []),
            {
              model: this.models.ScenarioAccess,
              required: false,
              where: { userId },
              attributes: [],
            },
          ],
        },
      ],
    });
  }
}

const eventServiceInstance = new EventService({
  models: DBModels,
  scenarioService: scenarioServiceInstance,
  Op: OpSource,
  logger: TmLogger,
});

export default eventServiceInstance;
export { EventService };
