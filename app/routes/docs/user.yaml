tags:
  - name: User
    description: Operations on the User model
components:
  parameters:
    UserId:
      in: path
      name: userId
      description: Id of User model
      schema:
        type: integer
      required: true
      example: 1
    Token:
      in: path
      name: token
      description: User token
      schema:
        type: string
      required: true
      example: 4pon99kq9mu3eqysf2i3
    HiddenFields:
      in: query
      name: hiddenFields
      description: When true, fetch method returns hidden fields for items (if authorized)
      schema:
        type: boolean
  requestBodies:
    User:
      description: User parameters
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/User'
    RequestPasswordReset:
      description: Parameters needed for sending password reset link
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/PasswordRequest'
    ResetPassword:
      description: Parameters needed for reseting user password
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/PasswordReset'
  responses:
    Users:
      description: List of fetched users
      content:
        application/json:
          schema:
            type: object
            properties:
              users:
                $ref: '#/components/schemas/Users'
    User:
      description: User model
      content:
        application/json:
          schema:
            type: object
            properties:
              user:
                $ref: '#/components/schemas/User'
    Organizations:
      description: List of all user organizations
      content:
        application/json:
          schema:
            type: object
            properties:
              organizations:
                $ref: '#/components/schemas/Organization'
    EmailNotSent:
      description: >
        Email was not sent successfuly.
    TokenInvalid:
      description: >
        Provided token is invalid or expired.
  schemas:
    Users:
      type: array
      description: List of users
      items:
        $ref: '#/components/schemas/User'
    User:
      type: object
      properties:
        id:
          type: integer
        email:
          type: string
          description: (restricted) If available to the user
        username:
          type: string
        role:
          type: array
          description: (restricted) List of authorization role codes
          items:
            type: string
        organization:
          type: string
      required:
        - id
        - email
    Organization:
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
      required:
        - id
        - name
    Email:
      type: object
      properties:
        email:
          type: string
      required:
        - email
    PasswordRequest:
      type: object
      properties:
        email:
          type: string
        subject:
          type: string
        body:
          type: string
        type:
          type: string
      required:
        - email
        - subject
        - body
        - type
    PasswordReset:
      type: object
      properties:
        email:
          type: string
        password:
          type: string
      required:
        - email
        - password
