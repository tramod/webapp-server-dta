import express from 'express';
import {
  createModification,
  getModificationsByEvent,
  getModificationById,
  updateModification,
  deleteModification,
  copyModification,
} from '../controllers/modifications.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator, { validatorError } from '../middleware/validator.js';
import { modificationTypeValidator } from '../middleware/validators/modification.js';
import abilityProvider from '../middleware/abilityProvider.js';
import resourceFetch from '../middleware/resourceFetch.js';
import trafficModelFetch from '../middleware/trafficModelFetch.js';
import editModeParser from '../middleware/editModeParser.js';
import isAuthorized from '../middleware/authorizator.js';

const router = express.Router();

const middlewareChain = ({
  validatorChain,
  resourceFetchOptions,
  afterFetchValidator,
  skipEditMode = false,
  skipAuth = false,
  authOptions,
} = {}) => [
  ...(skipAuth ? [] : [isAuthenticated]),
  ...(validatorChain || []),
  abilityProvider,
  ...(skipEditMode ? [] : [editModeParser]),
  resourceFetch(resourceFetchOptions),
  ...(afterFetchValidator ? [afterFetchValidator, validatorError] : []),
  trafficModelFetch,
  ...(authOptions ? [isAuthorized(authOptions)] : []),
];
/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications:
 *   get:
 *     summary: Get modifications from event/scenario pair
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/EditMode'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Modifications'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/events/:evtId/modifications',
  middlewareChain({
    validatorChain: validator('events.getter'),
    resourceFetchOptions: { allModifications: true },
    skipAuth: true,
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get modifications from scenario',
    },
  }),
  getModificationsByEvent,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications/{modificationId}:
 *   get:
 *     summary: Get modification (by modificationId) from event/scenario pair (by scenarioId + eventId)
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/ModificationId'
 *       - $ref: '#/components/parameters/EditMode'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Modification'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/events/:evtId/modifications/:modId',
  middlewareChain({
    validatorChain: validator('modifications.getter'),
    skipAuth: true,
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get modification from scenario',
    },
  }),
  getModificationById,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications:
 *   post:
 *     summary: Create modification object in event/scenario (by eventId + scenarioId)
 *     description: >
 *       Requires modification type property to be specified
 *       Requires editor or inserter access level to event.
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/Modification'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/ModificationCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events/:evtId/modifications',
  middlewareChain({
    validatorChain: validator('modifications.create'),
    afterFetchValidator: modificationTypeValidator,
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'change modifications in scenario',
    },
    resourceFetchOptions: { trafficModel: true },
  }),
  createModification,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications/{modificationId}:
 *   post:
 *     summary: Update modification (by modificationId) in event/scenario (by eventId + scenarioId)
 *     description: >
 *       Replace modification data with data send in request (full entry expected, mode and type cannot be changed).
 *       Requires editor or inserter access level to event.
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/ModificationId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/Modification'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ModificationUpdated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events/:evtId/modifications/:modId',
  middlewareChain({
    validatorChain: validator('modifications.update'),
    afterFetchValidator: modificationTypeValidator,
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'update modifications in scenario',
    },
    resourceFetchOptions: { trafficModel: true },
  }),
  updateModification,
);

// Deprecated
router.patch(
  '/:scenarioId/events/:evtId/modifications/:modId',
  middlewareChain({
    validatorChain: validator('modifications.update'),
    afterFetchValidator: modificationTypeValidator,
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'update modifications in scenario',
    },
    resourceFetchOptions: { trafficModel: true },
  }),
  updateModification,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications/{modificationId}:
 *   delete:
 *     summary: Delete modification (by modificationId) from event/scenario (by eventId + scenarioId)
 *     description: >
 *       Removes modification from event.
 *       Requires editor or inserter access level to event.
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/ModificationId'
 *       - $ref: '#/components/parameters/DeleteDependencies'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ModificationDeleted'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:scenarioId/events/:evtId/modifications/:modId',
  middlewareChain({
    validatorChain: validator('modifications.delete'),
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'delete modifications in scenario',
    },
  }),
  deleteModification,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/modifications/{modificationId}/copy:
 *   post:
 *     summary: Copy modification (by modificationId) in event/scenario (by eventId/scenarioId)
 *     description: >
 *       Copy modification by modification ID
 *       Requires inserter access level to the event
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Modification]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/ModificationId'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/ModificationCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events/:evtId/modifications/:modId/copy',
  middlewareChain({
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'copy modifications in scenario',
    },
  }),
  copyModification,
);

export default router;
