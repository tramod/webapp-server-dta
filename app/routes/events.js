import express from 'express';
import {
  createEvent,
  getEventsByScenario,
  getEventById,
  deleteEvent,
  deleteEvents,
  updateEvent,
  copyEvent,
  getImportableEventsToScenario,
  importEvents,
} from '../controllers/events.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';
import abilityProvider from '../middleware/abilityProvider.js';
import resourceFetch from '../middleware/resourceFetch.js';
import trafficModelFetch from '../middleware/trafficModelFetch.js';
import editModeParser from '../middleware/editModeParser.js';
import isAuthorized from '../middleware/authorizator.js';

const router = express.Router();

const middlewareChain = ({
  validatorChain,
  resourceFetchOptions,
  skipEditMode = false,
  skipAuth = false,
  authOptions,
} = {}) => [
  ...(skipAuth ? [] : [isAuthenticated]),
  ...(validatorChain || []),
  abilityProvider,
  ...(skipEditMode ? [] : [editModeParser]),
  resourceFetch(resourceFetchOptions),
  trafficModelFetch,
  ...(authOptions ? [isAuthorized(authOptions)] : []),
];

/**
 * @openapi
 * /scenarios/{scenarioId}/events:
 *   get:
 *     summary: Get events from scenario (by scenarioId)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EditMode'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Events'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/events',
  middlewareChain({
    validatorChain: validator('scenarios.get'),
    resourceFetchOptions: { allEvents: true },
    skipAuth: true,
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get events from scenario',
    },
  }),
  getEventsByScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}:
 *   get:
 *     summary: Get event (by eventId) from parent scenario (by scenarioId)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/EditMode'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Event'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/events/:evtId',
  middlewareChain({
    validatorChain: validator('events.getter'),
    skipAuth: true,
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get event from scenario',
    },
  }),
  getEventById,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events:
 *   post:
 *     summary: Create event object in scenario (by scenarioId)
 *     description: >
 *       User info automatically taken from session. Requires editor or inserter access level.
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/Event'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/EventCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events',
  middlewareChain({
    validatorChain: validator('events.create'),
    authOptions: {
      abilityKey: 'create:event',
      errorMsg: 'create event in scenario',
    },
  }),
  createEvent,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}:
 *   delete:
 *     summary: Delete event (by eventId) from scenario (by scenarioId)
 *     description: >
 *       Removes event from scenario, if no other scenario found for event, event removed completely.
 *       Requires editor or inserter access level (inserter can delete only own event not included in scenario).
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/DeleteDependencies'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EventDeleted'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:scenarioId/events/:evtId',
  middlewareChain({
    validatorChain: validator('events.delete'),
    authOptions: {
      abilityKey: 'delete:event',
      errorMsg: 'delete event',
    },
  }),
  deleteEvent,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events:
 *   delete:
 *     summary: Delete multiple events from scenario (by scenarioId)
 *     description: >
 *       Removes events from scenario, if no other scenario found for them, they are removed completely.
 *       If event is not part of scenario, error 404 is returned.
 *       If some of events is not found mixed response is returned.
 *       Requires editor or inserter access level (inserter can delete only own event not included in scenario).
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/QueryEventIds'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/BulkDeleteSuccess'
 *       '207':
 *         $ref: '#/components/responses/BulkDeleteMixed'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:scenarioId/events',
  middlewareChain({
    validatorChain: validator('events.deleteMany'),
    resourceFetchOptions: { allEvents: true },
  }),
  deleteEvents,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}:
 *   post:
 *     summary: Update event (by eventId) in scenario (by scenarioId)
 *     description: >
 *       Replace event data with data send in request.
 *       Requires editor or inserter access level (inserter can update only own event not included in scenario).
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *       - $ref: '#/components/parameters/UpdateDependencies'
 *     requestBody:
 *       $ref: '#/components/requestBodies/Event'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EventUpdated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events/:evtId',
  middlewareChain({
    validatorChain: validator('events.update'),
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'update event',
    },
  }),
  updateEvent,
);

// Deprecated
router.patch(
  '/:scenarioId/events/:evtId',
  middlewareChain({
    validatorChain: validator('events.update'),
    authOptions: {
      abilityKey: 'update:event',
      errorMsg: 'update event',
    },
  }),
  updateEvent,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/events/{evtId}/copy:
 *   post:
 *     summary: Copy event (by eventId) in the scenario (by scenarioId)
 *     description: >
 *       Copies event by event ID
 *       Requires inserter access level
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EventId'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/EventCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/events/:evtId/copy',
  middlewareChain({
    authOptions: {
      abilityKey: 'create:event',
      errorMsg: 'copy event',
    },
  }),
  copyEvent,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/importable-events:
 *   get:
 *     summary: Get list of importable events to scenario (by scenarioId)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Events'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/importable-events',
  middlewareChain({
    validatorChain: validator('scenarios.get'),
    authOptions: {
      abilityKey: 'create:event',
      errorMsg: 'get importable events',
    },
    resourceFetchOptions: { trafficModel: true },
  }),
  getImportableEventsToScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/import-events:
 *   post:
 *     summary: Import events (by imported scenario and event ids) in the scenario (by scenarioId)
 *     tags: [Event]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/ImportedEvents'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/EventImported'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/import-events',
  middlewareChain({
    validatorChain: validator('events.import'),
    authOptions: {
      abilityKey: 'create:event',
      errorMsg: 'import event',
    },
    resourceFetchOptions: { trafficModel: true },
  }),
  importEvents,
);

export default router;
