import express from 'express';
import {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  getOrganizations,
  requestPasswordReset,
  resetPassword,
} from '../controllers/users.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';

const router = express.Router();

/**
 * @openapi
 * /users:
 *   get:
 *     summary: Get all registered users
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/HiddenFields'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Users'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get('/', [isAuthenticated, validator('users.getAll')], getUsers);

/**
 * @openapi
 * /users/organizations:
 *   get:
 *     summary: Get all available user organizations
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Organizations'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get('/organizations', [isAuthenticated], getOrganizations);

/**
 * @openapi
 * /users/{userId}:
 *   get:
 *     summary: Get user (by userId)
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/UserId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/User'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get('/:userId', [isAuthenticated, validator('users.get')], getUser);

/**
 * @openapi
 * /users:
 *   post:
 *     summary: Create new user
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     requestBody:
 *       $ref: '#/components/requestBodies/User'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/User'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 */
router.post('/', [isAuthenticated, validator('users.create')], createUser);

/**
 * @openapi
 * /users/{userId}:
 *   patch:
 *     summary: Update user (by userId)
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/UserId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/User'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/User'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.patch(
  '/:userId',
  [isAuthenticated, validator('users.update')],
  updateUser,
);

/**
 * @openapi
 * /users/{userId}:
 *   delete:
 *     summary: Delete user (by userId)
 *     tags: [User]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/UserId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:userId',
  [isAuthenticated, validator('users.delete')],
  deleteUser,
);

/**
 * @openapi
 * /users/password-reset/:
 *   post:
 *     summary: Send password reset link
 *     tags: [User]
 *     requestBody:
 *       $ref: '#/components/requestBodies/RequestPasswordReset'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '502':
 *         $ref: '#/components/responses/EmailNotSent'
 */
router.post(
  '/password-reset',
  [validator('users.password.request')],
  requestPasswordReset,
);

/**
 * @openapi
 * /users/{userId}/password-reset/{token}:
 *   post:
 *     summary: Reset user password
 *     tags: [User]
 *     parameters:
 *       - $ref: '#/components/parameters/UserId'
 *       - $ref: '#/components/parameters/Token'
 *     requestBody:
 *        $ref: '#/components/requestBodies/ResetPassword'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '403':
 *         $ref: '#/components/responses/TokenInvalid'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:userId/password-reset/:token',
  [validator('users.password.reset')],
  resetPassword,
);

export default router;
