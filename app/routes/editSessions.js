import express from 'express';
import {
  enterEditSession,
  computeEditSession,
  saveEditSession,
  getEditSessionState,
  leaveEditSession,
  cancelEditSessionChanges,
  getUsersEditSessions,
} from '../controllers/editSession.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';
import abilityProvider from '../middleware/abilityProvider.js';
import editModeParser from '../middleware/editModeParser.js';

const router = express.Router();

const middlewareChainNonFetch = (validatorMiddleware) => [
  isAuthenticated,
  ...(validatorMiddleware || []),
  abilityProvider,
];
const middlewareChainEdits = (validatorMiddleware) => [
  isAuthenticated,
  ...(validatorMiddleware || []),
  abilityProvider,
  editModeParser,
];

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/enter:
 *   post:
 *     summary: Enter edit session of scenario (if app editMode enabled)
 *     description: >
 *       Edit session can be entered if none is valid (inserter/editor), or valid scenario is not
 *       active (has no user, applies only to editor). (400: EDIT_OCCUPIED).
 *       If scenarios traffic model is refreshing, enter is impossible. (400: EDIT_MODEL_REFRESH)
 *       If valid edit session is found, it is entered same way as if new is created (status: EDIT_ENTERED).
 *       User have to has at least inserter access level to create new, editor level to enter valid existing.
 *       Edit session entered means requesting user is linked to it.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EditActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ActionRejectedOrParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/edit/enter',
  middlewareChainNonFetch(validator('editSessions.id')),
  enterEditSession,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/compute:
 *   post:
 *     summary: Compute edit session of scenario (if app editMode enabled)
 *     description: >
 *       There must be active user's scenario edit session with computable changes (400: EDIT_NOT_COMPUTABLE).
 *       Necessary for saving session with computable changes.
 *       On success (status: EDIT_COMPUTATION_STARTED)
 *       Auto-save operation might be requested with compute.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/SaveOnComputed'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EditActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ActionRejectedOrParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/edit/compute',
  middlewareChainEdits(validator('editSessions.compute')),
  computeEditSession,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/save:
 *   post:
 *     summary: Save edit session of scenario (if app editMode enabled)
 *     description: >
 *       There must be active user's scenario edit session ready to save.
 *       It has to have changes and if computable, then computation must be finished.
 *       (400:EDIT_NOT_CHANGED/EDIT_NOT_COMPUTED/EDIT_COMPUTATION_ERROR).
 *       Activity of editSession depends on leaveOnSave param. Status EDIT_SAVED_COPY returned in both cases.
 *       Scenario copy can be saved in request, which does not affect edit session itself.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/LeaveOnSave'
 *       - $ref: '#/components/parameters/SaveAsCopy'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EditActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ActionRejectedOrParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/edit/save',
  middlewareChainEdits(validator('editSessions.save')),
  saveEditSession,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit:
 *   get:
 *     summary: GET state of edit session (if app editMode enabled)
 *     description: >
 *       Returns edit session meta object if found (can be also inactive closed session).
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EditSession'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/edit',
  middlewareChainEdits(validator('editSessions.id')),
  getEditSessionState,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/overview:
 *   get:
 *     summary: GET state of edit sessions available to user
 *     description: >
 *       Returns edit sessions info for scenario user can enter as editor or inserter.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/EditSessions'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 */
router.get('/edit/overview', middlewareChainNonFetch(), getUsersEditSessions);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/leave:
 *   post:
 *     summary: Leave edit session of scenario (user is unlinked)
 *     description: >
 *       Edit session can be left at any moment, and its state change to inactive.
 *       If edit session has computable changes, and isUndecided (computation is started) then its preserved.
 *       In other cases session is automatically cancelled.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ActionRejectedOrParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/edit/leave',
  middlewareChainEdits(validator('editSessions.id')),
  leaveEditSession,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/edit/cancel:
 *   post:
 *     summary: Cancel edit session changes of scenario (if app editMode enabled)
 *     description: >
 *       Deletes all unsaved changes of edit session (status: EDIT_CANCELED_CHANGES).
 *       If computation is in progress, its also cancelled.
 *     tags: [Edit session]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ActionRejectedOrParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/edit/cancel',
  middlewareChainEdits(validator('editSessions.cancel')),
  cancelEditSessionChanges,
);

export default router;
