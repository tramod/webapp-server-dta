import express from 'express';
import {
  getScenarioAccesses,
  createScenarioAccess,
  updateScenarioAccessByUserId,
  deleteScenarioAccessByUserId,
} from '../controllers/scenarioAccesses.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';
import abilityProvider from '../middleware/abilityProvider.js';
import resourceFetch from '../middleware/resourceFetch.js';
import isAuthorized from '../middleware/authorizator.js';

const router = express.Router();

const middlewareChain = ({
  validatorChain,
  resourceFetchOptions,
  authOptions,
} = {}) => [
  isAuthenticated,
  ...(validatorChain || []),
  abilityProvider,
  resourceFetch(resourceFetchOptions),
  ...(authOptions ? [isAuthorized(authOptions)] : []),
];

/**
 * @openapi
 * /scenarios/{scenarioId}/access:
 *   get:
 *     summary: Get every user access to the scenario (by scenarioId)
 *     description: >
 *       Returns scenario accesses for every user.
 *       Requires editor access level.
 *     tags: [Scenario Access]
 *     security:
 *       - tmAuth: []
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ScenarioAccesses'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/access',
  middlewareChain({
    validatorChain: validator('scenarioAccess.getter'),
    resourceFetchOptions: { allAccesses: true },
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'get accesses to scenario',
    },
  }),
  getScenarioAccesses,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/access/{userId}:
 *   post:
 *     summary: Create new access for specific user (by userId) and scenario (by scenarioId)
 *     description: >
 *       Creates new user access for the scenario.
 *       Requires editor access level.
 *     tags: [Scenario Access]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/UserId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/ScenarioAccess'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/ScenarioAccessCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId/access/:userId',
  middlewareChain({
    validatorChain: validator('scenarioAccess.create'),
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'create user access to scenario',
    },
  }),
  createScenarioAccess,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/access/{userId}:
 *   patch:
 *     summary: Update user access (by userId) in the scenario (by scenarioId)
 *     description: >
 *       Updates user access level to the scenario.
 *       Requires editor access level.
 *     tags: [Scenario Access]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/UserId'
 *     requestBody:
 *       $ref: '#/components/requestBodies/ScenarioAccess'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ScenarioAccessUpdated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.patch(
  '/:scenarioId/access/:userId',
  middlewareChain({
    validatorChain: validator('scenarioAccess.update'),
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'update user access to scenario',
    },
  }),
  updateScenarioAccessByUserId,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/access/{userId}:
 *   delete:
 *     summary: Delete user access (by userId) from the scenario (by scenarioId)
 *     description: >
 *       Removes user access from the scenario.
 *       Requires editor access level
 *     tags: [Scenario Access]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/UserId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:scenarioId/access/:userId',
  middlewareChain({
    validatorChain: validator('scenarioAccess.delete'),
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'delete user access to scenario',
    },
  }),
  deleteScenarioAccessByUserId,
);

export default router;
