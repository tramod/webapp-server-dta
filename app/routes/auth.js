import express from 'express';

import { logout, getLoggedUser } from '../controllers/auth.js';
import passport from '../middleware/passport.js';
import abilityProvider from '../middleware/abilityProvider.js';
import validator from '../middleware/validator.js';
import isAuthenticated from '../middleware/authenticator.js';

const router = express.Router();

/**
 * @openapi
 * /auth/login:
 *   post:
 *     summary: Login user to application API
 *     tags: [Auth]
 *     requestBody:
 *       $ref: '#/components/requestBodies/AuthCredentials'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/LoggedUser'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 */
router.post(
  '/login',
  [validator('auth.login'), passport.authenticate('local'), abilityProvider],
  getLoggedUser,
);

/**
 * @openapi
 * /auth/logout:
 *   post:
 *     summary: Logout user from API.
 *     description: Current login not required.
 *     tags: [Auth]
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/LogoutSuccess'
 */
router.post('/logout', logout);

/**
 * @openapi
 * /auth/user:
 *   get:
 *     summary: Fetch logged user properties and authorization rules
 *     tags: [Auth]
 *     security:
 *       - tmAuth: []
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/LoggedUser'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 */
router.get('/user', [isAuthenticated, abilityProvider], getLoggedUser);

export default router;
