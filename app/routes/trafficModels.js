import express from 'express';
import {
  getTrafficModels,
  getTrafficModel,
  refreshModel,
} from '../controllers/trafficModels.js';

import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';
import abilityProvider from '../middleware/abilityProvider.js';
import trafficModelFetch from '../middleware/trafficModelFetch.js';

const router = express.Router();

/**
 * @openapi
 * /models:
 *   get:
 *     summary: Fetch all available traffic models
 *     tags: [TrafficModel]
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/TrafficModels'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get('/', [], getTrafficModels);

/**
 * @openapi
 * /models/{modelType}:
 *   get:
 *     summary: Fetch traffic model by model type (expect one traffic model per model type)
 *     tags: [TrafficModel]
 *     parameters:
 *       - $ref: '#/components/parameters/ModelType'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/TrafficModel'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:modelType',
  [validator('trafficModel.getOne'), trafficModelFetch],
  getTrafficModel,
);

const refreshModelMiddleware = [
  isAuthenticated,
  validator('trafficModel.refresh'),
  abilityProvider,
  trafficModelFetch,
];

/**
 * @openapi
 * /models:
 *   post:
 *     summary: Refresh traffic model
 *     description: Request traffic model refresh operation. First baseModel is recalculated, then all scenarios are recalculated.
 *     tags: [TrafficModel]
 *     security:
 *       - tmAuth: []
 *     requestBody:
 *       $ref: '#/components/requestBodies/ModelType'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post('/refresh', refreshModelMiddleware, refreshModel);

export default router;
