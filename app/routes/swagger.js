import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'TraMod app',
      description:
        'Overview of TraMod API endpoints. For tryout of protected routes use login route with valid credentials first.',
      version: '0.0.1',
    },
  },
  apis: ['./app/routes/*.js', './app/routes/docs/*.yaml'], // files containing annotations as above
};
const spec = swaggerJsdoc(options);

export default function initSwagger(app) {
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(spec));
}
