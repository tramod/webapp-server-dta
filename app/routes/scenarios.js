import express from 'express';
import {
  createScenario,
  getScenarios,
  getScenarioById,
  deleteScenario,
  updateScenario,
  copyScenario,
  getScenarioNodeTransit,
  getExtraFeatureModifications,
  updateScenarioPublication,
} from '../controllers/scenarios.js';
import isAuthenticated from '../middleware/authenticator.js';
import validator from '../middleware/validator.js';
import abilityProvider from '../middleware/abilityProvider.js';
import resourceFetch from '../middleware/resourceFetch.js';
import trafficModelFetch from '../middleware/trafficModelFetch.js';
import editModeParser from '../middleware/editModeParser.js';
import isAuthorized from '../middleware/authorizator.js';

const router = express.Router();

const middlewareChainSimple = (validatorMiddleware) => [
  isAuthenticated,
  ...(validatorMiddleware || []),
  abilityProvider,
];

const middlewareChain = ({
  validatorChain,
  resourceFetchOptions,
  skipEditMode = false,
  skipAuth = false,
  authOptions,
} = {}) => [
  ...(skipAuth ? [] : [isAuthenticated]),
  ...(validatorChain || []),
  abilityProvider,
  ...(skipEditMode ? [] : [editModeParser]),
  resourceFetch(resourceFetchOptions),
  trafficModelFetch,
  ...(authOptions ? [isAuthorized(authOptions)] : []),
];

/**
 * @openapi
 * /scenarios:
 *   get:
 *     summary: Get all scenarios (without events) available to user.
 *     description: Returns scenarios user has access to (all access levels).
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Scenarios'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/',
  [abilityProvider, validator('scenarios.getAll'), trafficModelFetch],
  getScenarios,
);

/**
 * @openapi
 * /scenarios:
 *   post:
 *     summary: Create scenario
 *     description: Child events may be included. User info automatically taken from session.
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     requestBody:
 *       $ref: '#/components/requestBodies/Scenario'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/ScenarioCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 */
router.post(
  '/',
  middlewareChainSimple(validator('scenarios.create')),
  createScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}:
 *   get:
 *     summary: Get scenario (by scenarioId)
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/EditMode'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/Scenario'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId',
  middlewareChain({
    validatorChain: validator('scenarios.get'),
    skipAuth: true,
    resourceFetchOptions: { allCalculations: true },
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get scenario',
    },
  }),
  getScenarioById,
);

/**
 * @openapi
 * /scenarios/{scenarioId}:
 *   delete:
 *     summary: Delete scenario (by scenarioId)
 *     description: >
 *       Deletes also all events without another scenario. Editor level access required.
 *       Requires no scenario edit session to be active (if app editMode is enabled)
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ActionSuccess'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.delete(
  '/:scenarioId',
  middlewareChain({
    validatorChain: validator('scenarios.delete'),
    skipEditMode: true,
    authOptions: {
      abilityKey: 'delete',
      errorMsg: 'delete scenario',
    },
  }),
  deleteScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}:
 *   post:
 *     summary: Update scenario (by scenarioId)
 *     description: >
 *       Replace scenario data with data send in request.
 *       Requires editor access.
 *       Requires user to have active edit session (if app editMode is enabled)
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     requestBody:
 *       $ref: '#/components/requestBodies/Scenario'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ScenarioUpdated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.post(
  '/:scenarioId',
  middlewareChain({
    validatorChain: validator('scenarios.update'),
    resourceFetchOptions: { allCalculations: true },
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'update scenario',
    },
  }),
  updateScenario,
);

router.patch(
  '/:scenarioId',
  middlewareChain({
    validatorChain: validator('scenarios.update'),
    resourceFetchOptions: { allCalculations: true },
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'update scenario',
    },
  }),
  updateScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/public:
 *   patch:
 *     summary: Update scenario (by scenarioId) publication props directly
 *     description: >
 *       Updates isPublic and isInPublicList parameters send in request.
 *       Requires editor access.
 *       Does NOT requires user to have active edit session (if app editMode is enabled)
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     requestBody:
 *       $ref: '#/components/requestBodies/ScenarioPublication'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ScenarioUpdated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.patch(
  '/:scenarioId/public',
  middlewareChain({
    validatorChain: validator('scenarios.updatePublication'),
    skipEditMode: true,
    authOptions: {
      abilityKey: 'update',
      errorMsg: 'update scenario',
    },
  }),
  updateScenarioPublication,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/copy:
 *   post:
 *     summary: Copy scenario (by scenarioId)
 *     description: Copy scenario by the resource id
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '201':
 *         $ref: '#/components/responses/ScenarioCreated'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 */
router.post(
  '/:scenarioId/copy',
  [
    ...middlewareChainSimple(),
    resourceFetch({ allCalculations: true, trafficModel: true }),
  ],
  copyScenario,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/node-transit/{nodeId}:
 *   get:
 *     summary: Get node transit of selected node in scenario
 *     description: >
 *       Returns all node transit rules for selected node which are applied
 *       Rules may come from base model or from scenario events
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *       - $ref: '#/components/parameters/NodeId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ScenarioNodeTransit'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '403':
 *         $ref: '#/components/responses/NotAuthorized'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/node-transit/:nodeId',
  middlewareChain({
    resourceFetchOptions: { allEvents: true, trafficModel: true },
    validatorChain: validator('scenarios.nodeTransit'),
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'access scenario node transit',
    },
  }),
  getScenarioNodeTransit,
);

/**
 * @openapi
 * /scenarios/{scenarioId}/extra-feature-modifications:
 *   get:
 *     summary: Get extra feature modifications for this scenario
 *     tags: [Scenario]
 *     security:
 *       - tmAuth: []
 *     parameters:
 *       - $ref: '#/components/parameters/ScenarioId'
 *     responses:
 *       '200':
 *         $ref: '#/components/responses/ExtraFeatureModifications'
 *       '400':
 *         $ref: '#/components/responses/ParametersMissing'
 *       '401':
 *         $ref: '#/components/responses/NotAuthenticated'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 */
router.get(
  '/:scenarioId/extra-feature-modifications',
  middlewareChain({
    validatorChain: validator('scenarios.extraFeatureModifications'),
    skipAuth: true,
    authOptions: {
      abilityKey: 'get',
      errorMsg: 'get scenario',
    },
  }),
  getExtraFeatureModifications,
);

export default router;
