import useSwagger from './swagger.js';
import authRouter from './auth.js';
import scenariosRouter from './scenarios.js';
import eventsRouter from './events.js';
import modificationsRouter from './modifications.js';
import editSessionsRouter from './editSessions.js';
import scenarioAccessesRouter from './scenarioAccesses.js';
import usersRouter from './users.js';
import trafficModelsRouter from './trafficModels.js';
import TmError from '../utils/error.js';
import ErrorHandler from '../utils/errorHandler.js';

export default function createRoutes(app) {
  // Add swagger doc routes
  useSwagger(app);
  // API route handlers - manual entries
  app.use('/auth', authRouter);
  app.use('/scenarios', scenariosRouter);
  app.use('/scenarios', eventsRouter);
  app.use('/scenarios', modificationsRouter);
  app.use('/scenarios', editSessionsRouter);
  app.use('/scenarios', scenarioAccessesRouter);
  app.use('/users', usersRouter);
  app.use('/models', trafficModelsRouter);

  // Catch all routes, in case no match
  app.all('*', (req) => {
    throw new TmError(`Can't find ${req.originalUrl} on this server!`, {
      statusCode: 404,
      isOperational: true,
      customCode: 'NOT_FOUND',
    });
  });

  // Error handler routes, has to have all params to be registered as error handler
  // eslint-disable-next-line no-unused-vars
  app.use((err, req, res, next) => {
    res.status(ErrorHandler.getStatus(err));
    res.json({
      ...(err.code && { code: err.code }),
      error: err.message,
      ...(err.data && { data: err.data }),
    });
    ErrorHandler.handleError(err);
  });
}
