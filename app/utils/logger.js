import winston from 'winston';
import 'winston-daily-rotate-file';
import * as rTracer from 'cls-rtracer';
import { Loggly } from 'winston-loggly-bulk';

// Get envs directly from process, to decouple from config
// make sure dotenv is loaded first if you want to read from .env
const {
  TM_LOG_LEVEL,
  TM_LOG_DISABLE,
  NODE_ENV,
  LOGGLY_SUBDOMAIN,
  LOGGLY_TOKEN,
  LOGGLY_TAG,
} = process.env;

// WINSTON (MAIN) LOGGER
const { createLogger, format, transports } = winston;

const consoleTransport = new transports.Console({
  level: TM_LOG_LEVEL || (NODE_ENV === 'production' ? 'http' : 'debug'),
  format: format.combine(format.colorize(), format.simple()),
});

const fileTransport = new transports.DailyRotateFile({
  format: format.combine(format.timestamp(), format.json()),
  level: NODE_ENV === 'production' ? 'verbose' : 'debug',
  filename: 'app-log-%DATE%.log',
  dirname: './logs',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: NODE_ENV === 'production' ? 60 : 1,
});

const logTransports =
  NODE_ENV !== 'test' ? [consoleTransport, fileTransport] : [consoleTransport];

const winstonLogger = createLogger({
  silent: Boolean(TM_LOG_DISABLE) ?? false,
  transports: logTransports,
  defaultMeta: {
    get requestId() {
      return rTracer.id();
    },
  },
});

if (LOGGLY_TOKEN && LOGGLY_SUBDOMAIN)
  winstonLogger.add(
    new Loggly({
      level: 'http',
      token: LOGGLY_TOKEN,
      subdomain: LOGGLY_SUBDOMAIN,
      tags: [LOGGLY_TAG ?? 'tm-dev'],
      json: true,
    }),
  );

winstonLogger.pass = {
  write(message) {
    winstonLogger.http(message.substring(0, message.lastIndexOf('\n')));
  },
};

const logOperationalMethod = NODE_ENV === 'production' ? 'verbose' : 'warn';
winstonLogger.logOperational = (message) =>
  winstonLogger[logOperationalMethod](message);

export default winstonLogger;
