import logger from './logger.js';

// Operational error should be expected and only log info for debugging in production
const operationalLogLevel =
  process.env.NODE_ENV === 'production' ? 'verbose' : 'warn';

class ErrorHandler {
  static handleError(error) {
    if (!error.logged) ErrorHandler.logError(error);
    // TODO potential other non-logger services
    if (!ErrorHandler.isTrustedError(error) && process.env.NODE_ENV !== 'test')
      process.exit(1);
  }

  static logError(error) {
    const logLevel =
      error.level || error.isOperational ? operationalLogLevel : 'error';
    logger[logLevel](error.message, this.getLogErrorData(error));
  }

  static getLogErrorData(error) {
    const toLog = { ...error };
    delete toLog.statusCode;
    delete toLog.logged;
    delete toLog.isOperational;
    return toLog;
  }

  static isTrustedError(error) {
    return error.isOperational;
  }

  static getStatus(error) {
    return error.statusCode || 500;
  }
}

export default ErrorHandler;
