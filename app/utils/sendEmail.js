import nodemailer from 'nodemailer';
import logger from './logger.js';

export default async ({ email, subject, text, html } = {}) => {
  let sendingError = null;

  try {
    const transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      },
    });

    await transporter.sendMail({
      from: `${process.env.EMAIL_SENDER_NAME} <${process.env.EMAIL_USER}>`,
      to: email,
      subject,
      text: text || html.replace(/<\/?[^>]+(>|$)/g, ''), // plain text in case HTML is not supported by the email client
      html,
    });

    logger.info('Email sent sucessfully');
  } catch (error) {
    sendingError = error;
    logger.info('Email not sent');
  }

  return sendingError;
};
