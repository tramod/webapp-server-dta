/* eslint-disable max-classes-per-file */
// Expanded base Error class based on some best practices
class TmError extends Error {
  constructor(
    message,
    {
      statusCode = 500,
      isOperational = false,
      logged = false,
      customCode,
      level,
      data = {},
    } = {},
    ...params
  ) {
    super(message);

    if (!isOperational) Error.captureStackTrace(this, TmError);

    this.statusCode = statusCode;
    this.isOperational = isOperational;
    this.logged = logged;
    this.code = customCode || this.code;
    this.level = level;
    this.data = data;
    this.params = params;
  }
}

class TmErrorGeneral extends TmError {
  constructor(message, errParams = {}, ...other) {
    super(
      message,
      {
        ...errParams,
        statusCode: errParams.statusCode ?? 400,
        isOperational: true,
      },
      ...other,
    );
  }
}

class TmError3rdParty extends TmError {
  constructor(message, ...params) {
    super(
      message,
      {
        statusCode: 503,
        isOperational: true,
        customCode: '3RD_PARTY_ERROR',
      },
      ...params,
    );
  }
}

class TmInputValidationError extends TmError {
  constructor(message, ...params) {
    super(
      message,
      {
        statusCode: 400,
        isOperational: true,
        customCode: 'INPUT_INVALID',
      },
      ...params,
    );
  }
}

class TmNotFoundError extends TmError {
  constructor(message, ...params) {
    super(
      message,
      {
        statusCode: 404,
        isOperational: true,
        customCode: 'NOT_FOUND',
      },
      ...params,
    );
  }
}

class TmNotAuthorized extends TmError {
  constructor(message = 'UnAuthorized to complete action', ...params) {
    super(
      message,
      {
        statusCode: 403,
        isOperational: true,
        customCode: 'NOT_AUTHORIZED',
        level: 'warn',
      },
      ...params,
    );
  }
}

export default TmError;
export {
  TmInputValidationError,
  TmNotFoundError,
  TmNotAuthorized,
  TmErrorGeneral,
  TmError3rdParty,
};
