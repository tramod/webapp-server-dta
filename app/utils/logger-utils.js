// eslint-disable-next-line import/prefer-default-export
export function getLogParams(req, extraData) {
  return {
    params: req.params,
    reqUser: req.user?.id,
    ...extraData,
  };
}

export class ActivityLog {
  constructor({ logger, messages, scope } = {}) {
    this.logger = logger;
    this.messages = messages;
    this.scope = scope ? `${scope}_` : '';
  }

  logReq(req, type, data) {
    this.logger.verbose({
      type: 'ACTIVITY',
      code: `${this.scope}${type}`,
      message: this.messages[type],
      data,
      ...getLogParams(req),
    });
  }

  log(type, data) {
    this.logger.verbose({
      type: 'ACTIVITY',
      code: `${this.scope}${type}`,
      message: this.messages[type],
      data,
    });
  }
}
